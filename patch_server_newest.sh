#! /bin/bash

#git pull

docker-compose -f docker-compose.yml -f docker-compose.prod.yml build

cd web && npm run build-prod && cd ..

docker stop web-service

docker rm web-service

docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

docker exec web-service python tools/update_alembic.py

docker restart web-service
