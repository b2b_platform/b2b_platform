# PATCH
from tornado.platform.asyncio import AsyncIOMainLoop
import asyncio
import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
AsyncIOMainLoop().install()

import logging, signal, time
from tornado import ioloop, httpserver, web
import tornado.gen

import settings

class Application(web.Application):
    
    def __init__(self, config):
        super(Application, self).__init__([], **config)

    def add_service(self, service):
        if hasattr(self, service.name):
            raise RuntimeError('application already set up {} service'.format(service.name))
        setattr(self, service.name, service)

    def remove_service(self, name):
        delattr(self, name)

def register_shutdown_handler(http_server):
    shutdown_handler = lambda sig, frame:  shutdown(http_server)
    signal.signal(signal.SIGINT, shutdown_handler)
    signal.signal(signal.SIGTERM, shutdown_handler)

def shutdown(http_server):
    ioloop_instance = ioloop.IOLoop.instance()
    logging.info('Stopping server gracefully.')

    http_server.stop()
    
    def finalize():
        ioloop_instance.stop()
        logging.info('Server stopped.')
        
    ioloop_instance.add_timeout(time.time() + 0.3, finalize)

def setup_services(app):
    logging.debug('SETUP SERVICES')
    pass

def add_handlers(app):
    logging.debug('SETUP APP HANDLERS')
    handlers = [
    ]
    app.add_handlers(r'.*', handlers)    

def start(config=None, host='0.0.0.0', port=9001):
    config = config or settings.load()
    logging.debug('SETUP APPLICATION')
    app = Application(config)

    setup_services(app)
    add_handlers(app)

    http_server = httpserver.HTTPServer(app)
    http_server.listen(port, host)
    register_shutdown_handler(http_server)


    asyncio.get_event_loop().run_forever()