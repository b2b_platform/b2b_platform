import os
import logging

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, configure_mappers
from sqlalchemy_searchable import make_searchable
from sqlalchemy_searchable import search as sql_search

import database.services

class Database(object):

    def __init__(self, uri, metadata=None, engine_options=None, session_options=None, scopefunc=None, services=None):        

        engine_options = engine_options or {}
        session_options = session_options or {}

        self.uri = uri

        self._engine = create_engine(self.uri, **engine_options)
        self._session = scoped_session(sessionmaker(bind=self.engine, **session_options), scopefunc=scopefunc)
                
        self.Model = declarative_base(metadata=metadata)  
        self.metadata.bind = self.engine

        configure_mappers()        

        # setup services
        self._services = None
        self._register_services(services=services)        

        self.sql_search = sql_search

        self.app = None

    def init_app(self, app):
        self.app = app
        app.db = self

    def remove_session(self):
        if self.session:
            self.session.remove()

    def set_threadlocal(self, v):
        self.engine.pool._use_threadlocal = v

    def soft_reset(self):
        pass
        #self.create_all()

    def hard_reset(self):
        self.drop_all()
        self.drop_all_tables_and_sequences(force=True)

    def drop_all(self):
        self.metadata.drop_all()

    def create_all(self):
        self.metadata.create_all()

    @property
    def metadata(self):
        return self.Model.metadata

    @property
    def engine(self):
        return self._engine

    @property
    def session(self):
        return self._session

    @property
    def query(self):
        return self.session.query

    def commit(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
            raise

    def _register_services(self, services=None):
        if self._services is None:
            self._services = services or database.services.init(self)
        return self._services

    @property
    def services(self):
        return self._services

    def search(self, query, *args, **kwargs):
        limit = kwargs.pop('limit', None)
        return sql_search(query, *args, **kwargs).limit(limit).all()

    def drop_all_tables_and_sequences(self, force=False):
        if not force:
            raise RuntimeError('set force=True to really do this')

        ''' 
        Drops all tables and sequences (but not VIEWS) from a postgres database
        '''
     
        sequence_sql='''SELECT sequence_name FROM information_schema.sequences
                         WHERE sequence_schema='public'
                      '''
         
        table_sql='''SELECT table_name FROM information_schema.tables
                     WHERE table_schema='public' AND table_type != 'VIEW' AND table_name NOT LIKE 'pg_ts_%%'
                  '''

        type_sql='''
        select t.typname as enum_name
        from pg_type t 
        join pg_enum e on t.oid = e.enumtypid  
        join pg_catalog.pg_namespace n ON n.oid = t.typnamespace group by n.nspname,t.typname;
        '''

        for table in [name for (name, ) in self.engine.execute(table_sql)]:
            try:
                self.engine.execute('DROP TABLE "%s" CASCADE' % table)
            except Exception as e:
                print(e)
         
        for seq in [name for (name, ) in self.engine.execute(sequence_sql)]:
            try:
                self.engine.execute('DROP SEQUENCE "%s" CASCADE' % seq)
            except Exception as e:
                print(e)

        
        for ptype in [name for (name, ) in self.engine.execute(type_sql)]:
            try:
                self.engine.execute('DROP TYPE "%s"' % ptype)
            except Exception as e:
                print(e)