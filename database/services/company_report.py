from .base import DatabaseService
import database.models as models

class CompanyReportService(DatabaseService):

    name = 'company_report'
    Model = models.CompanyReport
    
    # Create - Fill in company report by ID
    def report_file(self, company_id, report_type, content):
        pass
        
    def update_report(self):
        # Update attributes in Company table
        pass
        
    def delete_report(self):
        # Update attributes in Company table
        return None
