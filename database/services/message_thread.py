from sqlalchemy.orm import with_polymorphic, aliased
from sqlalchemy import func, or_, and_, cast, case, not_
from sqlalchemy.dialects.postgresql import JSONB
from .base import DatabaseService
import database.models as models
import math
from datetime import datetime

class MessageThreadService(DatabaseService):

    name = 'thread'
    ComboModel = with_polymorphic(models.MessageThread, [
        models.PrivateThread,
        models.ProductInquiryThread,
        models.RFQThread,
        models.SupportThread,
        models.TradeLeadThread
    ])
    Model = models.MessageThread
    UserToMessageThread = models.UserToMessageThread

    def get_by_id(self, thread_id):
        return self.db.query(self.ComboModel).filter(self.Model.id == thread_id).first()

    def is_product_inquiry(self):
        return isinstance(self.Model, models.ProductInquiryThread)       

    def get_users_by_id(self, thread_id):
        thread = self.db.query(self.Model).filter(self.Model.id == thread_id).first()
        if thread:
            return thread.users_by_id

        return []
    
    def add_user(self, thread, user):
        thread._users.append(user)
        self.db.commit()
        return thread

    def search(self, user, thread_type='message_thread', search_term='thread', 
        search_opt='thread', limit=20, current_page=1,
        #sorted_by='message_thread.create_time desc', is_archived=False):
        sorted_by=models.MessageThread.create_time.desc(), is_archived=False):

        query = self.db.query(self.ComboModel)

        if is_archived == True:
            query = query.filter(self.Model.archived_by.contains(user))
        elif is_archived == False:
            query = query.filter(~self.Model.archived_by.contains(user))

        creator_alias = aliased(models.User, name='creator')
        query = (query
            .join(creator_alias, creator_alias.id==self.ComboModel.creator_id)
            .filter(
                or_(
                    self.Model._users.any(models.User.id == user.id),
                    and_(user.company_id != None, models.ProductInquiryThread.company_id == user.company_id),
                    and_(user.company_id != None, creator_alias.company_id == user.company_id)
                )
            )
        )
        
        #query = query.join(self.Model._users)

        conditions = []

        if search_term and (search_opt == 'thread' or search_opt == 'all'):
            conditions += [
                self.Model.name.ilike(f'%{search_term}%'),
                self.Model.description.ilike(f'%{search_term}%'),
                models.User.company_name.ilike(f'%{search_term}%')
            ]
        
        if search_term and (search_opt == 'user' or search_opt == 'all'):
            conditions += [
                models.User.email.ilike(f'%{search_term}%'),
                models.User.first_name.ilike(f'%{search_term}%'),
                models.User.last_name.ilike(f'%{search_term}%'),
                models.User.company_name.ilike(f'%{search_term}%')
            ]
            
        if thread_type not in ['message_thread', None]:
            conditions += [self.Model.thread_type == thread_type]
        
        query = query.filter(or_(*conditions))

        query = query.group_by(self.ComboModel).order_by(sorted_by)                       
        num_pages = math.ceil(query.count() / limit)
        threads = query.limit(limit).offset((current_page-1) * limit).all()
        return num_pages, threads
    
    def get_rfq_thread(self, seller_id, rfq_id):
        query = (self.db.query(self.ComboModel)
            .filter(self.Model._users.any(models.User.id==seller_id))
            .filter(models.RFQThread.rfq_id==rfq_id))
                
        return query.first()
    
    def add_rfq_thread(self, seller_id, rfq_id, **kwargs):
        rfq = self.db.rfq.get_by_id(rfq_id)

        thread = models.RFQThread()        
        self.db.session.add(thread)        
        thread.update(
            name = f'RFQ: {rfq.product}',
            description = f'Message thread for RFQ {rfq.product}',
            creator_id = seller_id,
            rfq_id = rfq_id,
            users = [seller_id, rfq.buyer_id],
            **kwargs
        )        
        self.db.commit()
        
        # ADD RFQ message first
        rfq_msg = models.Message()
        rfq_msg.update(
            user_id = rfq.buyer_id,
            thread_id = thread.id,
            subject = f'RFQ: {rfq.product}',
            content = self.db.message.rfq_msg_tpl.render(rfq=rfq),
            is_pinned = True,
            pinned_by = seller_id
        )
        self.db.session.add(rfq_msg)

        # ADD QUOTES
        quotes = self.db.quote.filter_by(seller_id=seller_id, rfq_id=rfq_id)
        for quote in quotes:
            quote_msg = models.Message()
            quote_msg.update(
                user_id = seller_id,
                thread_id = thread.id,
                subject = f'Quote: {quote.title}',
                content = self.db.message.quote_msg_tpl.render(quote=quote),
                is_pinned = True,
                pinned_by = seller_id
            )
            self.db.session.add(quote_msg)
                
        self.db.commit()
        return thread


    def get_trade_lead_thread(self, trade_lead_id, user_id=None):
        return self.db.query(models.TradeLeadThread).filter_by(trade_lead_id=trade_lead_id, creator_id=user_id).first()

    def add_trade_lead_thread(self, trade_lead_id, user_id=None, **kwargs):
        thread = models.TradeLeadThread()
        self.db.session.add(thread)

        user = self.db.user.get_by_id(user_id)
        trade_lead = self.db.trade_lead.get_by_id(trade_lead_id)

        lead_name = kwargs.pop('lead_name', '')
        #thread_users = [user_id] + [ u.id for u in user.company.users if user.company_id ]
        thread_users = [user_id, trade_lead.user_id]

        # TBI:
        ## Add other users to existing company

        thread.update(
            name = f'Trade lead inquiry: {trade_lead.title}',
            description = f'Message thread for questions regarding trade lead',
            creator_id = user_id,
            users = thread_users,
            trade_lead_id=trade_lead.id,
            **kwargs
        )        
        self.db.commit()

        pi_msg = models.Message()
        text = kwargs.pop('text', None)
        pi_msg.update(
            user_id = user_id,
            thread_id = thread.id,
            subject = f'We are interested in the following trade lead:',
            content = self.db.message.trade_lead_inquiry_msg_tpl.render(lead=trade_lead, text=text),
            is_pinned = True,
            pinned_by = user_id
        )
        self.db.session.add(pi_msg)
        self.db.commit()

        if len(thread.users) == 1:
            # Add auto reply to trade lead not belonging to a company
            pass
           # buyer = self.db.user.get_by_id(user_)
           # autorep = self.create_product_inquiry_autorep(thread, buyer, product.company)

        # NOTIFY:
        # Notify trade60 trade agent
        # Notify the posted agent
        self.db.notification.new_trade_lead_inquiry(trade_lead.user_id, thread.id, trade_lead_id, user_id)

        return thread


    def get_product_inquiry_threads_by_company(self, company_id):
        query = self.db.query(self.ComboModel).filter(models.ProductInquiryThread.company_id == company_id)
        return query.all()

    def create_product_inquiry_autorep(self, thread, buyer, seller_company):
        t60_user = self.db.user.get_by_email('msg_support@trade60.com')
       
        if not t60_user:
            return None

        msg = models.Message()
        msg.update(
            user_id = t60_user.id,
            thread_id = thread.id,            
            content = self.db.message.product_inquiry_autorep_tpl.render(
                buyer=buyer,
                seller_company=seller_company
            )
        )
        self.db.session.add(msg)
        self.db.commit()
        return msg

    def get_product_inquiry_thread(self, buyer_id, product_id=None, **kwargs):
        product = self.db.product.get_by_id(product_id)        
        thread = (self.db.query(self.ComboModel)
            .filter(self.Model._users.any(models.User.id == buyer_id))
            .filter(models.ProductInquiryThread.company_id == product.company_id)).first()
     
        if thread:
            # create new message
            pi_msg = models.Message()
            text = kwargs.pop('text', None)
            pi_msg.update(
                user_id = buyer_id,
                thread_id = thread.id,
                subject = f'Product inquiry: {product.name}',
                content = self.db.message.product_inquiry_msg_tpl.render(products=[product], text=text),
                is_pinned = True,
                pinned_by = buyer_id
            )
            thread.products.append(product)
            self.db.session.add(pi_msg)
            self.db.commit()            

            if len(product.company.users) == 0:
                buyer = self.db.user.get_by_id(buyer_id)
                autorep = self.create_product_inquiry_autorep(thread, buyer, product.company)

            # NOTIFY:
            self.db.notification.new_product_inquiry(
                product.company_id, thread.id, buyer_id, [product.id]
            )                

        return thread    

    def add_product_inquiry_thread(self, buyer_id, product_id, **kwargs):        
        product = self.db.product.get_by_id(product_id)        
        thread = models.ProductInquiryThread()
        self.db.session.add(thread)
        thread_users = [buyer_id] + [ u.id for u in product.company.users]
        thread.update(
            name = f'Product inquiry: {product.company.name}',
            description = f'Message thread for questions regarding products of {product.company.name}',
            creator_id = buyer_id,            
            company_id = product.company_id,
            users = thread_users,
            **kwargs
        )        
        self.db.commit()

        pi_msg = models.Message()
        text = kwargs.pop('text', None)
        pi_msg.update(
            user_id = buyer_id,
            thread_id = thread.id,
            subject = f'Product inquiry: {product.name}',
            content = self.db.message.product_inquiry_msg_tpl.render(products=[product], text=text),
            is_pinned = True,
            pinned_by = buyer_id
        )
        self.db.session.add(pi_msg)
        thread.products.append(product) 
        self.db.commit()

        if len(product.company.users) == 0:
            buyer = self.db.user.get_by_id(buyer_id)
            autorep = self.create_product_inquiry_autorep(thread, buyer, product.company)

        # NOTIFY:
        self.db.notification.new_product_inquiry(
            product.company_id, thread.id, buyer_id, [product.id]
        )

        return thread

    def get_or_create_product_inquiry_thread_by_company(self, buyer_id, company, **kwargs):
        thread = (self.db.query(self.ComboModel)
            .filter(self.Model._users.any(models.User.id == buyer_id))
            .filter(models.ProductInquiryThread.company_id == company.id)).first()
        
        if thread:
            return thread

        thread = models.ProductInquiryThread()
        self.db.session.add(thread)

        # Notify all users might not be necessary
        thread_users = [buyer_id] + [ u.id for u in company.users]

        thread.update(
            name = f'Product inquiry: {company.name}',
            description = f'Message thread for questions regarding products of {company.name}',
            creator_id = buyer_id,
            company_id = company.id,
            users = thread_users,
            **kwargs
        )
        self.db.commit()

        if len(company.users) == 0:
            buyer = self.db.user.get_by_id(buyer_id)
            autorep = self.create_product_inquiry_autorep(thread, buyer, company)

        return thread

    def get_or_create_bulk_product_inquiry_threads(self, buyer_id, products, **kwargs):
        text = kwargs.get('text', None)                

        products_by_company = {}
        for product in products:            
            try:
                products_by_company[product.company].append(product)
            except KeyError:
                products_by_company[product.company] = [product]

        threads = []

        for company, products in products_by_company.items():

            thread = (self.db.query(self.ComboModel)
                .filter(self.Model._users.any(models.User.id == buyer_id))
                .filter(models.ProductInquiryThread.company_id == company.id)).first()

            if not thread:
                thread = models.ProductInquiryThread()
                self.db.session.add(thread)
                thread_users = [buyer_id] + [ u.id for u in company.users]
                thread.update(
                    name = f'Product inquiry: {company.name}',
                    description = f'Message thread for questions regarding products of {company.name}',
                    creator_id = buyer_id,
                    company_id = company.id,
                    users = thread_users,
                    **kwargs
                )
                self.db.commit()

            # ADD PRODUCT INQUIRY MESSAGE TO THREAD

            pi_msg = models.Message()            
            pi_msg.update(
                user_id = buyer_id,
                thread_id = thread.id,
                subject = 'Product inquiry',
                content = self.db.message.product_inquiry_msg_tpl.render(products=products, text=text),
                is_pinned = True,
                pinned_by = buyer_id
            )
            self.db.session.add(pi_msg)
            for product in products:
                thread.products.append(product)
            self.db.commit()

            if len(company.users) == 0:
                buyer = self.db.user.get_by_id(buyer_id)
                autorep = self.create_product_inquiry_autorep(thread, buyer, company)

            # NOTIFY:
            self.db.notification.new_product_inquiry(
                company.id, thread.id, buyer_id, [product.id for product in products]
            )
            
            threads.append(thread)

        return threads

    def get_user_contacts(self, user):
        query = (self.db.query(models.User)
            .join(models.UserToMessageThread.__table__)
            .filter(self.Model.users.any(models.User.id == user.id))
            .filter(models.User.id != user.id)
            .distinct())
        
        return query.all()
    
    def get_or_create_private_thread(self, user1, user2):

        query = (
            self.db.query(self.ComboModel)
            .filter(self.Model.thread_type == 'private_thread')
            .filter(
                or_(
                    and_(self.Model.creator_id == user1.id, models.PrivateThread.target_user_id == user2.id),
                    and_(self.Model.creator_id == user2.id, models.PrivateThread.target_user_id == user1.id)
                )
            )
            .distinct()
        )
    
        if query.count():
            return query.first()        

        thread = models.PrivateThread()
        self.db.session.add(thread)

        thread.update(
            creator_id = user1.id,
            target_user_id = user2.id,
            name = f'Private message thread',
            description = f'Private message thread between {user1.name} and {user2.name}',
            _users = [user1, user2]
        )

        self.db.commit()

        return thread

    def get_or_create_support_thread(self, user_id, ticket):
        thread = (self.db.query(self.ComboModel)
            .filter(models.SupportThread.ticket_id==ticket.id)).first()

        if thread:
            return thread

        thread = models.SupportThread()
        self.db.session.add(thread)
        thread.update(
            name=f'Support ticket {ticket.id}: {ticket.name}',
            description=f'Message thread for support ticket {ticket.id}',
            ticket_id=ticket.id,
            creator_id=user_id,
            users=[user_id] 
        )

        st_msg = models.Message()
        st_msg.update(
            user_id=user_id,
            thread_id=thread.id,
            subject=f'Support ticket {ticket.id}',
            content=self.db.message.support_ticket_msg_tpl.render(ticket=ticket),
            is_pinned=True,
            pinned_by=user_id
        )
        
        self.db.session.add(st_msg)
        self.db.commit()

        return thread

    def set_user_last_active(self, user_id, thread_id, dt=None):
        dt = dt or datetime.utcnow()       
        user_thread = (
            self.db.query(models.UserToMessageThread)
            .filter(
                (models.UserToMessageThread.user_id==user_id) &
                (models.UserToMessageThread.message_thread_id==thread_id)
            )
        ).first()

        if user_thread:
            user_thread.last_active_time = dt
            self.db.commit()

        return dt

    def read_thread(self, user_id, thread_id, dt=None):
        dt = self.set_user_last_active(user_id, thread_id)
        return dt

    def read_all_threads(self, user_id):
        dt = dt or datetime.utcnow()       
        user_threads = (
            self.db.query(models.UserToMessageThread)
            .filter((models.UserToMessageThread.user_id==user_id))
        ).all()

        for user_thread in user_threads:
            user_thread.last_active_time = dt

        self.db.commit()

        return dt
    
    def get_total_unread_messages_count(self, user_id):
        thread_msgs_count = self.get_unread_messages_count(user_id)        
        return sum(c for _,c in thread_msgs_count)

    def get_unread_messages_count(self, user_id, limit=None):        
        query = (
            self.db.query(self.Model.id, func.count(models.Message.id)
            .filter(
                (models.UserToMessageThread.last_active_time < models.Message.create_time) &
                (models.Message.user_id != user_id)
            ).label('unread_msgs_count'))
            .join(models.user_to_archived_thread_table, full=True)
            .filter(not_(self.Model.archived_by.any(models.user_to_archived_thread_table.c.user_id == user_id)))
            .join(models.UserToMessageThread)
            .join(models.Message, full=True)
            .filter(models.UserToMessageThread.user_id == user_id)            
            .group_by(self.Model.id)
            .order_by('unread_msgs_count desc')
            .order_by(models.MessageThread.create_time.desc())
            #.order_by('message_thread.create_time desc')
            .limit(limit)
        )        

        return query.all()

    def get_unread_messages_count_full_thread(self, user_id, limit=None):        
        query = (
            self.db.query(self.Model, func.count(models.Message.id)
            .filter(
                (models.UserToMessageThread.last_active_time < models.Message.create_time) &
                (models.Message.user_id != user_id)
            ).label('unread_msgs_count'))
            .join(models.user_to_archived_thread_table, full=True)
            .filter(~(self.Model.archived_by.any(models.user_to_archived_thread_table.c.user_id == user_id)))
            .join(models.UserToMessageThread)
            .join(models.Message, full=True)
            .filter(models.UserToMessageThread.user_id == user_id)            
            .group_by(self.Model)
            .order_by('unread_msgs_count desc')
            .order_by('message_thread.create_time desc')
            .limit(limit)
        )

        return query.all()
    
    def get_unread_count_all_users(self):
        query = (
            self.db.query(models.UserToMessageThread.user_id, func.count(models.Message.id)
            .filter(
                (models.UserToMessageThread.last_active_time < models.Message.create_time) &
                (models.Message.user_id != models.UserToMessageThread.user_id)
            ).label('unread_msgs_count'))
            .join(self.Model)
            .join(models.user_to_archived_thread_table, full=True)
            .filter(not_(self.Model.archived_by.any(models.user_to_archived_thread_table.c.user_id == models.UserToMessageThread.user_id)))
            .join(models.Message, full=True)
            .group_by(models.UserToMessageThread.user_id)
        )

        return query.all()
    
    def archive_thread(self, user, thread):
        self.set_user_last_active(user.id, thread.id, dt=datetime.max)
        thread.archived_by.append(user)
        self.db.commit()
        return thread
    
    def unarchive_thread(self, user, thread):
        self.set_user_last_active(user.id, thread.id, dt=datetime.utcnow())
        thread.archived_by.remove(user)
        self.db.commit()
        return thread

