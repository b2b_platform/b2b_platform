from .base import DatabaseService
import database.models as models
import math
from sqlalchemy_searchable import parse_search_query
from sqlalchemy.orm import aliased, make_transient
from sqlalchemy import text, cast, type_coerce, Integer, String, and_, or_, func
from sqlalchemy.dialects.postgresql import JSONB, JSON

class CompanyService(DatabaseService):

    name = 'company'
    Model = models.Company
    
    def search(self, keyword='', limit=20, current_page=1, 
            sorted_by='company.name', filter_list=None, search_hidden=False, **kwargs):


        query = self.query()
        if not search_hidden:
            query = (query.filter_by(is_public=True, **kwargs)                    
                    .join(self.Model._keywords, isouter=True)
                    .group_by(self.Model))
        else:
            query = (query.filter_by(**kwargs)                    
                    .join(self.Model._keywords, isouter=True)
                    .group_by(self.Model))
        if keyword:
            combined_search_vector = self.Model.search_vector | func.coalesce(models.Keyword.search_vector, '')
            query = query.filter(combined_search_vector.match(parse_search_query(keyword)))
  
        # And conditions
        conditions = []
        #print(filter_list)

        if filter_list:
            for attr in filter_list['numeric']:
               
                #query = query.filter(cast(self.Model.min_order_quantity, Integer) < 4000)
                if 'min' in attr.keys() and attr['min']:
                    if attr['separate']:
                        conditions.append(getattr(self.Model, attr['name'] + '_min') >= attr['min'])
                    else:
                        conditions.append(getattr(self.Model, attr['name']) >= attr['min'])
                    #query = query.filter(cast( getattr(self.Model, attr['name']), Integer) >= attr['min'])
                if 'max' in attr.keys() and attr['max']:
                    if attr['separate']:
                        conditions.append(getattr(self.Model, attr['name'] + '_max') <= attr['max'])
                    else:
                        conditions.append(getattr(self.Model, attr['name']) <= attr['max'])    
                    #query = query.filter(cast( getattr(self.Model, attr['name']), Integer) <= attr['max'])
            for attr in filter_list['checkbox']:
                if attr['name'] == 'membership' and attr['chosen']:
                    query = query.join(models.Membership, models.Company.current_membership_id == models.Membership.id).group_by(models.Company)
                    conditions.append(getattr(models.Membership, 'tier').in_(attr['chosen']))  
                elif attr['chosen']:
                    attr['chosen'] = list(filter(None, attr['chosen']))
                    conditions.append(getattr(self.Model, attr['name']).in_(attr['chosen']))
                    #query = query.filter(getattr(self.Model, attr['name']).in_(attr['chosen']))
                    #query = query.filter(cast(getattr(self.Model, attr['name']), String).in_(attr['chosen']))
            
            for attr in filter_list['checkboxjson']:
                if attr['chosen']:
                    attr['chosen'] = list(filter(None, attr['chosen']))
                    conditions.append(or_(
                        getattr(self.Model, attr['name']).contains(cast(c, JSONB)) for c in attr['chosen']
                    ))
                    #query = query.filter(getattr(self.Model, attr['name']).contains(attr['chosen']))
                    #or_()

        query = query.filter(and_(*conditions))
        query = query.order_by(sorted_by)

        num_results = query.count()
        num_pages = math.ceil(num_results / limit)
        #query = query.order_by(sorted_by)
        companies = query.limit(limit).offset((current_page-1) * limit).all()
  
        return num_pages, companies, num_results


    def get_matching_requests(self, product_list, **kwargs):
        # RFQs retrieval
        return None
