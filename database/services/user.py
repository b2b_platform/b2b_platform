from sqlalchemy.orm import aliased, make_transient
from datetime import datetime
from .base import DatabaseService
import database.models as models
import math
from sqlalchemy.orm.attributes import flag_modified

class UserService(DatabaseService):

    name = 'user'
    Model = models.User

    def get_by_email(self, email):
        return self.filter_one(email=email)

    def update_by_email(self, email, **kwargs):
        user = self.get_by_email(email)
        if not user:
            raise RuntimeError('User {} not found'.format(email))

        user.update(**kwargs)
        self.db.commit()
        return user

    def delete_by_email(self, email):
        user = self.get_by_email(email)
        if not user:
            raise RuntimeError('User {} not found'.format(email))

        self.db.session.delete(user)
        self.db.commit()

    def get_colleagues(self, user):

        if not user.company_id:
            return []

        return [u for u in user.company.users if u.id != user.id]

    def confirm(self, user):
        if not user.is_confirmed:
            self.update_one(user, 
                is_account_confirmed=True,
                account_confirmed_time=datetime.utcnow()
            )
        
        return user

    def search(self, search_term='', id_term=None, limit=20, current_page=1, search_hidden=False, sorted_by='"user".id', **kwargs):
        query = self.query()

        if not search_hidden: # Search hidden
            query = query.filter_by(is_hidden=False)

        if search_term:
            qs = f'''lower(first_name) like lower('%{search_term}%') or 
            lower(last_name) like lower('%{search_term}%') or 
            lower(email) = lower('{search_term}')
            '''
            if id_term and id_term.isdigit():
                # Search based on id
                try:
                    qs_add = f' or id = {int(search_term)}'
                    qs += qs_add
                except ValueError:
                    pass

            #users = self.query().filter(qs).distinct().all()
            query = query.filter(qs)


        query = query.order_by(sorted_by)

        num_results = query.count()
        num_pages = math.ceil(num_results / limit)
        users = (query.limit(limit).offset((current_page-1)*limit).all())

        return num_pages, users, num_results

    def search_list_id(self, search_list, limit=20, current_page=1):
        query = self.db.query(self.Model).filter(getattr(models.User, 'id').in_(search_list))
        
        num_results = query.count()
        num_pages = math.ceil(num_results / limit)
        users = (query.limit(limit).offset((current_page-1)*limit).all())
        return num_pages, users, num_results

    def log(self, user_id, activity_type, text, object_id=None, details=None):
        user = self.get_by_id(user_id)
        if not user:
            return None
        ua = models.UserActivity(
            user_id=user.id,
            activity_type=activity_type,
            text=text,
            object_id=object_id,
            details=details
        )
        self.db.session.add(ua)
        self.db.commit()
        return ua

    def clone_user(self, user):
        email_old = user.email
        user.update(is_hidden=True,is_active=False,is_banned=True,email=f'{email_old}disabled')
        self.db.commit()

        self.db.session.expunge(user)
        
        make_transient(user)

        user.id = None
        user.is_hidden = True
        user.company_id = None
        user.email = f'{email_old}'
        self.db.session.add(user)
        self.db.commit()

        return user    

    def update_one(self, user, privacy_args=None, site_args=None, **kwargs):
        user.update(**kwargs)
        if privacy_args:
            if user.privacy_settings:
                user.privacy_settings.update(**privacy_args)
            else:
                user.privacy_settings = privacy_args
            flag_modified(user, 'privacy_settings')
        if site_args:
            if user.site_settings:
                user.site_settings.update(**site_args)
            else:
                user.site_settings = site_args
            flag_modified(user, 'site_settings')
        self.db.commit()

    def add_blocked_user(self, user, user_list):
        if not user.privacy_args:
            user.privacy_settings = {}
        if user.privacy_args['privacy_blocked_users']:
            user.privacy_settings['privacy_blocked_users'].append(user_list)
        else:
            user.privacy_settings['privacy_blocked_users'] = user_list
        flag_modified(user, 'privacy_settings')
        self.db.commit()
