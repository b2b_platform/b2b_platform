from .base import DatabaseService
import database.models as models

class KeywordService(DatabaseService):

    name = 'keyword'
    Model = models.Keyword
    
    # Retrieve related keywords - #TBI
    def get_related_keywords(self, name):
        # String search for related keywords
        # TBI
        
        # Search nearby "sibling" product category with same name
        return self.filter(name=name)
