from .user import UserService
from .company import CompanyService
from .product_category import ProductCategoryService
from .product import ProductService
from .keyword import KeywordService
from .rfq import RFQService
from .quote import QuoteService
from .notification import NotificationService
from .company_rating import CompanyRatingService
from .company_report import CompanyReportService
from .message import MessageService
from .message_thread import MessageThreadService
from .media import MediaService
from .support_ticket import SupportTicketService
from .role import UserCompanyRoleService
from .company_verify import CompanyVerifyService
from .membership import MembershipService
from .trade_lead import TradeLeadService

def init(db):
    services = {service.name for service in [
            UserService(db),
            CompanyService(db),
            ProductService(db),
            ProductCategoryService(db),
            QuoteService(db),
            CompanyReportService(db),
            CompanyRatingService(db),
            RFQService(db),
            KeywordService(db),
            MessageService(db),
            MessageThreadService(db),
            MediaService(db),
            SupportTicketService(db),
            UserCompanyRoleService(db),
            CompanyVerifyService(db),
            NotificationService(db),
            MembershipService(db),
            TradeLeadService(db)
        ]}

    return services
