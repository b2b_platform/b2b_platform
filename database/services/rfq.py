from .base import DatabaseService
import database.models as models
from sqlalchemy import func
from sqlalchemy_searchable import parse_search_query
from sqlalchemy.orm import aliased
import math
from ..constants import RFQ_DURATION_DAYS
from datetime import timedelta, datetime

class RFQService(DatabaseService):

    name = 'rfq'
    Model = models.RFQ
    
    def add(self, **kwargs):
        # ALWAYS add to session first
        rfq = self.Model() 
        self.db.session.add(rfq)
        rfq.update(**kwargs)
        now = datetime.utcnow()
        rfq.update(
            end_time=now + timedelta(days=RFQ_DURATION_DAYS)
        )
        self.db.commit()
        return rfq

    def get_by_buyer_id(self, user_id):
        rfqs = self.filter_by(buyer_id=user_id)
        return rfqs

    def search(self, keyword='', category=None, current_page=1, 
        limit=20, sorted_by='rfq.create_time desc', **kwargs):

        now = datetime.utcnow()

        combined_search_vector = self.Model.search_vector | func.coalesce(models.Keyword.search_vector, '')

        # FILTER ??? WHY IT WORKS?
        included = (self.db.query(models.ProductCategory.id)
                    .filter(models.ProductCategory.parent_category_id == category)
                    .cte(name='included', recursive=True))
        included_alias = aliased(included, name='parent')
        cat_alias = aliased(models.ProductCategory, name='child')
        included = included.union_all(
            self.db.query(cat_alias.id).filter(
                cat_alias.parent_category_id == included_alias.c.id
            )
        )

        cat_ids = map(lambda _t: _t[0], [(category,)] + self.db.query(included.c.id).distinct().all())

        query = (self.query()
                    .filter(self.Model.product_category_id.in_(cat_ids))
                    .filter((self.Model.end_time == None) | (self.Model.end_time > now))
                    .filter_by(**kwargs)
                    .join(self.Model._keywords, isouter=True)
                    .group_by(self.Model))
        
        if keyword:
            query = query.filter(combined_search_vector.match(parse_search_query(keyword)))
        
        query = query.order_by(sorted_by)
                    
        num_pages = math.ceil(query.count() / limit)        
        rfqs = query.limit(limit).offset((current_page-1) * limit).all()

        return num_pages, rfqs