from sqlalchemy.orm import with_polymorphic, aliased
from sqlalchemy import func, or_, and_, cast, case, select
from .base import DatabaseService
import database.models as models
import math
from datetime import datetime

class NotificationService(DatabaseService):

    name = 'notification'
    Model = models.Notification

    NotificationUserReadTime = models.NotificationUserReadTime
    NotificationCompany = models.NotificationCompany
    NotificationUser = models.NotificationUser

    ComboModel = with_polymorphic(models.Notification, [
        models.NotificationCompany,
        models.NotificationUser,
    ])

    def add_to_company(self, **kwargs):
        item = models.NotificationCompany()
        self.db.session.add(item)
        item.update(**kwargs)
        self.db.commit()
        return item

    def add_to_user(self, **kwargs):
        item = models.NotificationUser()
        self.db.session.add(item)
        item.update(**kwargs)
        self.db.commit()
        return item
    
    def new_product_inquiry(self, company_id, thread_id, buyer_id, product_ids, **kwargs):
        notif = self.add_to_company(
            notification_type='product_inquiry',
            company_id=company_id,
            details=dict(            
                thread_id=thread_id,
                buyer_id=buyer_id,
                products=product_ids,
                **kwargs                
            )
        )
        return notif

    def new_trade_lead_inquiry(self, user_id, thread_id, trade_lead_id, inquirer_id, **kwargs):
        notif = self.add_to_user(
            notification_type='trade_lead_inquiry',
            user_id=user_id,
            details=dict(            
                thread_id=thread_id,
                user_id=inquirer_id,
                trade_lead_id=trade_lead_id,
                **kwargs                
            )
        )
        return notif
    
    def new_account_activity(self, user_id, activity, **kwargs):
        noti = self.add_to_user(
            notification_type='account_activity',
            user_id=user_id,
            details=dict(
                activity=activity,
                **kwargs
            )
        )
        return noti
    
    def set_user_last_read(self, user_id, dt=None):
        dt = dt or datetime.utcnow()
        user_record = self.db.query(models.NotificationUserReadTime).get(user_id)
        if not user_record:
            user_record = models.NotificationUserReadTime(user_id=user_id, last_read_time=dt)
            self.db.session.add(user_record)
        else:
            user_record.last_read_time = dt

        self.db.commit()
        return dt

    def read_all(self, user_id, dt=None):
        dt = dt or datetime.utcnow()
        return self.set_user_last_read(user_id, dt)
        
    def search(self, user_id, company_id, limit=30, current_page=1, search_term='', notification_type='all'): 
        last_time_obj = self.db.query(models.NotificationUserReadTime).filter(
        models.NotificationUserReadTime.user_id == user_id).first()        
        last_read_time = last_time_obj.last_read_time if last_time_obj else datetime.min

        query = self.db.query(self.ComboModel, case(
            [
                (last_read_time < self.ComboModel.create_time, 0)
            ], else_=1))
        
        if company_id:
            query = query.filter(
                or_(
                    models.NotificationUser.user_id == user_id,
                    models.NotificationCompany.company_id == company_id
                )
            )
        else:
            query = query.filter(models.NotificationUser.user_id == user_id)
        
        if notification_type and notification_type != 'all':
            query = query.filter(models.Notification.notification_type == notification_type)

        query = query.order_by('notification.create_time desc')
        num_pages = math.ceil(query.count() / limit)
        notifications = query.limit(limit).offset((current_page-1) * limit).all()

        return num_pages, notifications
    
    def get_unread_count(self, user_id, company_id):
        last_time_obj = self.db.query(models.NotificationUserReadTime).filter(
        models.NotificationUserReadTime.user_id == user_id).first()        
        last_read_time = last_time_obj.last_read_time if last_time_obj else datetime.min

        if company_id:
            query = (self.db.query(self.ComboModel).filter(
                or_(
                    models.NotificationUser.user_id == user_id,
                    models.NotificationCompany.company_id == company_id
                )
            ))            
        else:
            query = (self.db.query(self.ComboModel)
                    .filter(models.NotificationUser.user_id == user_id)
                    .order_by('notification.create_time desc'))
        
        query = query.filter(last_read_time < models.Notification.create_time)
        
        return query.count()
        

    def get_unread_notifications(self, user_id, limit=None):        
        query1 = self.get_unread_company_notifications(user_id)
        query2 = self.get_unread_personal_notifications(user_id)

        return query1.limit(limit).all() if query1 else {}, query2.limit(limit).all() if query2 else {}

    def get_unread_personal_notifications(self, user_id, limit=None):
        query = (
            self.db.query(self.ComboModel.notification_type.label('ntype'), func.count(self.ComboModel.notification_type).label('ncount'))
            .filter(models.NotificationUser.user_id == user_id)
            .join(models.NotificationUserReadTime,
                    models.NotificationUserReadTime.user_id == models.NotificationUser.user_id,
                    full=True)
            .filter(
                or_(models.NotificationUserReadTime.last_read_time == None,
                models.NotificationUserReadTime.last_read_time < models.NotificationUser.create_time)
                )
            .group_by(self.ComboModel.notification_type)
        )
        return query

    def get_unread_company_notifications(self, user_id, limit=None):
        last_time_obj = self.db.query(models.NotificationUserReadTime).filter(
            models.NotificationUserReadTime.user_id == user_id).first()
        
        last_time = last_time_obj.last_read_time if last_time_obj else None
        company_id = self.db.user.get_by_id(user_id).company_id

        if not company_id:
            return None

        query = ( self.db.query(self.ComboModel.notification_type.label('ntype'), func.count(self.ComboModel.notification_type).label('ncount'))
                .filter(models.NotificationCompany.company_id == company_id)
                .group_by(self.ComboModel.notification_type)
                )

        if last_time_obj:
            query = query.filter(last_time < models.NotificationCompany.create_time)
                
        return query