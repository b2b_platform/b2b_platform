from .base import DatabaseService
import database.models as models

class CompanyRatingService(DatabaseService):

    name = 'company_rating'
    Model = models.CompanyRating
