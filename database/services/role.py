from .base import DatabaseService
import database.models as models
from sqlalchemy import func
from sqlalchemy_searchable import parse_search_query
from sqlalchemy.orm import aliased
import math
from datetime import timedelta, datetime
from role import role_constants

class UserCompanyRoleService(DatabaseService):

    name = 'role'
    Model = models.UserCompanyRole
    
    def add(self, **kwargs):
        role = self.Model()
        self.db.session.add(role)
        role.update(**kwargs)
        self.db.commit()
        return role

    def get_roles_by_user_id(self, user_id):
        roles = self.filter_by(user_id=user_id)
        return roles

    def get_position_by_userid(self, user_id):
        return self.filter_one(user_id=user_id, operation=role_constants.OPERATION_GENERAL)

    def get_all_positions_by_userid(self, user_id):
        return self.filter_by(user_id=user_id, operation=role_constants.OPERATION_GENERAL)

    def get_position_by_ids(self, user_id, company_id):
        return self.filter_one(user_id=user_id, company_id=company_id, operation=role_constants.OPERATION_GENERAL)
    
    def get_roles_by_ids(self, user_id, company_id):
        return self.filter_by(user_id=user_id, company_id=company_id)

    def update_position_by_userid(self, user_id, **kwargs):
        role = self.filter_one(user_id=user_id, operation=role_constants.OPERATION_GENERAL)
        role.update(**kwargs)
        self.db.commit()
        return role

    def search(self, *args, **kwargs):
        return None