from .base import DatabaseService
import database.models as models
import math
from sqlalchemy_searchable import parse_search_query
from sqlalchemy.orm import aliased, make_transient
from sqlalchemy import text, cast, type_coerce, Integer, String, and_, or_, func
from sqlalchemy.dialects.postgresql import JSONB, JSON

from collections import OrderedDict
from datetime import date
import pandas
from io import BytesIO


# Format product attributes in order to export to Excel file.
def export_format(value=None, is_extended=False):
    if is_extended:
        return value.name

    if type(value) == date:
        # YYYY-MM-DD
        return value.isoformat()

    return str(value)

class ProductService(DatabaseService):

    name = 'product'
    Model = models.Product

    # (attribute_name, display_name, whether_attribute_is_a_result_of_a_relationship)
    CSV_EXPORT_COLUMNS = [
        ('id', 'ID'),
        ('name', 'Name'),
        ('description', 'Description'),
        ('create_time', 'Create Time'),
        ('product_category', 'Product Category'),
        ('capacity', 'Capacity'),
        ('min_order_quantity', 'Min Order Quantity'),
        ('unit_type', 'Unit Type'),
        ('payment_options', 'Payment Options'),
        ('price_range_min', 'Price Range Min'),
        ('price_range_max', 'Price Range Max'),
        ('packaging', 'Packaging'),
        ('processing_time', 'Processing Time'),
        ('other_details', 'Other'),
    ]

    def get_by_ids(self, pids):
        query = self.query().filter(self.Model.id.in_(pids)).distinct()
        return query.all()

    # Get the parent category text
    def get_category_from_product(self, name):
        product_tmp = self.filter_one(name=name)
        parent_cat = models.ProductCategoryService.filter_one(id=tmp.product_category)
        return parent_cat.name


    def get_product_by_name(self, name):
        product_tmp = self.filter_one(name=name)
        if not product_tmp:
            # Return error
            raise RuntimeError('Product {} not found'.format(name))
        return product_tmp


    def find_matching_product_by_keywords(self, *kwards):
        products = self.filter(*kwards)

        # TBI Add manual filters for specific fields

        return products

    def add_combined(self, **kwargs):
        #add keywords
        pass

    def get_by_company_id(self, company_id, paginate=False, limit=10, current_page=1):
        if not paginate:
            return self.filter_by(company_id=company_id)

        query = self.filter_q(self.Model.company_id == company_id)

        num_pages = math.ceil(query.count() / limit)
        products = query.order_by('product.create_time desc').limit(limit).offset((current_page-1)*limit).all()

        return num_pages, products

    def search2(self, keyword='', limit=20, current_page=1, sorted_by='product.name', **kwargs):

        combined_search_vector = self.Model.search_vector | func.coalesce(models.Keyword.search_vector, '')

        query = (self.query()
                    .filter_by(**kwargs)
                    .join(self.Model._keywords, isouter=True)
                    .group_by(self.Model))

        if keyword:
            query = query.filter(combined_search_vector.match(parse_search_query(keyword)))

        num_pages = math.ceil(query.count() / limit)

        products = (query.order_by(sorted_by)
                        .limit(limit).offset((current_page-1)*limit).all())

        return num_pages, products

    def search(self, keyword='', category=None, current_page=1, limit=20, 
            sorted_by='product.create_time', viewtab='tabproducts', search_hidden=False, filter_list=None, **kwargs):

        joined_with_company = False
        
        if viewtab == 'tabcompanies':
            #query_comp = (query.join(models.Company, isouter=True).group_by(models.Company)).all()
            #query = self.db.query(models.Company).join(self.Model,)
            #query = self.db.query(models.Company)#.join(models.Product)
            if not search_hidden:
                query = self.db.query(models.Company).filter(models.Company.is_public==True).join(models.Product).group_by(models.Company)        
            else:
                query = self.db.query(models.Company).join(models.Product).group_by(models.Company)
            joined_with_company = True
        else:
            if not search_hidden:
                query = self.query().join(models.Company).filter(models.Company.is_public==True)
                joined_with_company = True
            else:
                query = self.query()
        
        if category:
            included = (self.db.query(models.ProductCategory.id)
                        .filter(models.ProductCategory.parent_category_id == category)
                        .cte(name='included', recursive=True))
            included_alias = aliased(included, name='parent')
            cat_alias = aliased(models.ProductCategory, name='child')
            included = included.union_all(
                self.db.query(cat_alias.id).filter(
                    cat_alias.parent_category_id == included_alias.c.id
                )
            )
            cat_ids = map(lambda _t: _t[0], [(category,)] + self.db.query(included.c.id).distinct().all())

            query = query.filter(self.Model.product_category_id.in_(cat_ids))

        #query = (self.query()
        #            .filter(self.Model.product_category_id.in_(cat_ids))
        #            #.filter(text("cast(product.min_order_quantity,Integer)>50"))
        #            .filter_by(**kwargs)
        #            .join(self.Model._keywords, isouter=True)
        #            .group_by(self.Model))

        company_id = kwargs.pop('company_id', None)
        if company_id:
            query = query.filter(models.Product.company_id == company_id)
            

        if keyword:
            query = query.filter_by(**kwargs).join(self.Model._keywords, isouter=True).group_by(self.Model)
            combined_search_vector = self.Model.search_vector | models.Company.search_vector | func.coalesce(models.Keyword.search_vector, '')
            #combined_search_vector = self.Model.search_vector | func.coalesce(models.Keyword.search_vector, '')
            query = query.filter(combined_search_vector.match(parse_search_query(keyword)))

        # And conditions
        conditions = []

        if filter_list:
            for attr in filter_list['numeric']:
                #query = query.filter(cast(self.Model.min_order_quantity, Integer) < 4000)
                if 'min' in attr.keys() and attr['min']:
                    if attr['separate']:
                        conditions.append(getattr(self.Model, attr['name'] + '_min') >= attr['min'])
                    else:
                        conditions.append(getattr(self.Model, attr['name']) >= attr['min'])
                    #query = query.filter(cast( getattr(self.Model, attr['name']), Integer) >= attr['min'])
                if 'max' in attr.keys() and attr['max']:
                    if attr['separate']:
                        conditions.append(getattr(self.Model, attr['name'] + '_max') <= attr['max'])
                    else:
                        conditions.append(getattr(self.Model, attr['name']) <= attr['max'])
                    #query = query.filter(cast( getattr(self.Model, attr['name']), Integer) <= attr['max'])
            for attr in filter_list['checkbox']:
                if attr['name'] == 'country' and attr['chosen']:
                    attr['chosen'] = list(filter(None, attr['chosen']))
                    #conditions.append(models.Company.country.in_(attr['chosen']))
                    if viewtab != 'tabcompanies' and not joined_with_company:
                        # Patch join with company table
                        query = query.group_by(models.Product)
                    conditions.append(getattr(models.Company, 'country').in_(attr['chosen']))
                    #continue # Skip this filter, will be processed later    
                elif attr['chosen']:
                    attr['chosen'] = list(filter(None, attr['chosen']))
                    conditions.append(getattr(self.Model, attr['name']).in_(attr['chosen']))
                    #query = query.filter(getattr(self.Model, attr['name']).in_(attr['chosen']))
                    #query = query.filter(cast(getattr(self.Model, attr['name']), String).in_(attr['chosen']))
                
            for attr in filter_list['checkboxjson']:
                if attr['chosen']:
                    attr['chosen'] = list(filter(None, attr['chosen']))
                    conditions.append(or_(
                        getattr(self.Model, attr['name']).contains(cast(c, JSONB)) for c in attr['chosen']
                    ))
                    #query = query.filter(getattr(self.Model, attr['name']).contains(attr['chosen']))
                    #or_()

        query = (query.filter(and_(*conditions)))
        #if viewtab == 'tabcompanies':
        #    #query_comp = (query.join(models.Company, isouter=True).group_by(models.Company)).all()
        #    query_comp = self.db.query(models.Company).all()
        #    print ('companies', query_comp)

        if viewtab == 'tabcompanies':
            #query = query.join(models.Company).group_by(models.Company.id).distinct()
            #query = query.group_by(self.Model.company_id)
            query = query.distinct()
        else:
            query = query.order_by(sorted_by)

        num_results = query.count()

        num_pages = math.ceil(num_results / limit)
        products = query.limit(limit).offset((current_page-1) * limit).all()

        return num_pages, products, num_results


    def duplicate(self, product):
        self.db.session.expunge(product)
        make_transient(product)
        product.id = None
        name = product.name.replace('(Copy)', '')
        product.name = f'{name} (Copy)'
        #product.display_pic_id = None
        self.db.session.add(product)
        self.db.commit()

        return product

    def generate_export_file_for_company(self, company):
        products = self.get_by_company_id(company.id)

        # Collect data
        data = OrderedDict()

        for key, column in self.CSV_EXPORT_COLUMNS:
            is_extended = (key == 'product_category')
            row = [ export_format(getattr(product, key), is_extended) for product in products ]
            data.update({ column: row })

        data_frame = pandas.DataFrame(data)

        # Write file
        output_stream = BytesIO()
        writer = pandas.ExcelWriter(output_stream, engine='xlsxwriter')

        data_frame.to_excel(writer, startrow = 0, merge_cells = False, sheet_name = "Products")
        writer.close()
        output_stream.seek(0)

        filename = 'Products_' + company.name + '.xlsx'

        return filename, output_stream
