from jinja2 import Template
from flask import url_for
from sqlalchemy.orm import with_polymorphic
from sqlalchemy import func, cast, case
from datetime import datetime
from sqlalchemy.dialects.postgresql import JSONB

from .base import DatabaseService
import database.models as models

class MessageService(DatabaseService):

    name = 'message'
    Model = models.Message
    ComboModel = with_polymorphic(models.Message, [models.MediaMessage])
    MediaMessage = models.MediaMessage

    quote_msg_tpl = Template('''
    <div class="quote-msg" data-quote_id='{{ quote.id }}'>
        <p><b>Price: </b>{{ quote.price }}</p>
        <p><b>Quantity: </b>{{ quote.quantity }}</p>
        <label><b>Products</b></label>
        <div class='products-list'>
        {% for product in quote.products %}
            <div class='card product-item'>
                <img class='card-img-top'
                    src="{{ product.display_pic or '/static/img/product_default.png' }}" />
                <div class='card-body'>
                    <div class='card-text'>
                        <a href='/product/{{ product.id }}'>{{ product.name }}</a>
                    </div>
                </div>
            </div>
        {% endfor %}
        </div>
        <p>{{ quote.content }}</p>
        <a href='/quote/{{ quote.id }}'>View quote</a>
    </div>
    ''')

    rfq_msg_tpl = Template('''
    <div class="rfq-msg">
        <p><span class='product-cat'>{{ rfq.product_category.path_as_str }}</span><p>
        <p><b>Quantity: </b>{{ rfq.quantity }}</p>
        <p>Content</p>
        <a href='/rfq/{{ rfq.id }}'>View RFQ</a>
    </div>
    ''')

    support_ticket_msg_tpl = Template('''
    <div class="support-ticket-msg">        
        <p><b>Summary: </b>{{ ticket.name }}</p>
        <p><b>Create time: </b>{{ ticket.create_time }}</p>
        <p><b>Status: </b>{{ ticket.status }}</p>
        <p>{{ ticket.description }}</p>        
        <a href='/support/t/{{ ticket.id }}>View ticket</a>
    </div>
    ''')

    product_inquiry_msg_tpl = Template('''
    <div class="product-inquiry-msg">        
        <div class='products-list'>
            {% for product in products %}
            <div class='card product-item'>
                <img class='card-img-top' 
                    src="{{ product.display_pic or '/static/img/product_default.png' }}" />
                <div class='card-body'>
                    <div class='card-text'>
                        <a href='/product/{{ product.id }}'>{{ product.name }}</a>
                    </div>
                </div>
            </div>
            {% endfor %}
        </div>
        {% if text %}
        <p>{{ text|safe }}</p>
        {% else %}
        <p>Please provide more information regarding <b>{{ product.name }}</b></p>
        {% endif %}
    </div>
    ''')

    trade_lead_inquiry_msg_tpl = Template('''
    <div class="lead-msg">
        <p><span class='product-cat'>{{ lead.product_category.path_as_str }}</span><p>
        <p><span class='product-cat'>{{ lead.product if lead.product }} {{ lead.company if lead.company }}</span><p>
        <p>Content</p>
        <a href='/trade-leads/{{ lead.id }}'>View Trade Lead</a>
    </div>
    ''')

    product_inquiry_autorep_tpl = Template('''
    <div class='auto-reply-msg' data-reply_type='product_inquiry'>
        <p>Hey, {{ buyer.name }}!</p>
        <p>
            Thank you for your interest in {{ seller_company.name }}. Currently, all representatives from {{ seller_company.name }} are not available. We will contact them on your behalf and update you ASAP. You can manage notification settings <a href="/my-account/settings">here</a>.
        </p>
        <p>Have a great day :D!</p>
        <p><b>Trade60 Support Team</b></p>
    </div>
    ''')

    def get_by_thread_id(self, thread_id, keyword='', limit=40, last_index=None):

        qs = f'message.thread_id = {thread_id}'

        if last_index:
            qs += f' AND message.id < {last_index}'

        query = (self.db.sql_search(self.db.query(self.ComboModel).filter(qs), keyword)
            .order_by('message.create_time desc')
            .limit(limit))

        return query.all()[::-1]

    def add_media_message(self, **kwargs):
        message = models.MediaMessage()        
        self.db.session.add(message)
        message.update(**kwargs)        
        self.db.commit()        
        return message

    def get_thread_attachment(self, thread_id, limit=40):
        query = (
            self.db.query(models.Media)
            .join(models.media_to_message_table)
            .join(self.ComboModel)
            .filter(models.Message.thread_id == thread_id)
            .distinct()
            .limit(limit)
        )

        return query.all()

    def get_number_media(self, thread_id, user_id):
        query = (
            self.db.query(models.Media)
            .join(models.media_to_message_table)
            .join(self.ComboModel)
            .filter(models.Message.thread_id == thread_id)
        )
        return query.count() 
    
    def mark_read(self, user_id, message_id):

        table = self.Model.__table__
        dt = datetime.utcnow().timestamp()
        rbk = str(user_id)

        query = table.update()\
            .where(table.c.id == message_id)\
            .values(read_by=case(
                [(table.c.read_by == None, cast({rbk: dt}, JSONB))],
                else_=cast(cast(table.c.read_by, JSONB).concat({rbk: dt}), JSONB)
            )
        )

        self.db.engine.execute(query)
        return dt