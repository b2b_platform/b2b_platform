from .base import DatabaseService
import database.models as models

class QuoteService(DatabaseService):

    name = 'quote'
    Model = models.Quote

    def get_by_seller_id(self, seller_id):
        quotes = self.filter_by(seller_id=seller_id)
        return quotes

    def get_by_rfq_id(self, rfq_id):
        quotes = self.filter_by(rfq_id=rfq_id)
        return quotes

    def add(self, **kwargs):
        # add quote first
        quote = self.Model()
        self.db.session.add(quote)
        quote.update(**kwargs)
        self.db.commit()
        
        seller_id = kwargs.get('seller_id')
        rfq_id = kwargs.get('rfq_id')
        thread = self.db.thread.get_rfq_thread(seller_id, rfq_id)

        if thread:
            # add new message if rfq thread is found
            quote_msg = models.Message()            
            quote_msg.update(
                user_id = seller_id,
                thread_id = thread.id,
                subject = f'Quote: {quote.title}',
                content = self.db.message.quote_msg_tpl.render(quote=quote),
                is_pinned = True,
                pinned_by = seller_id
            )            
            self.db.session.add(quote_msg) 
            self.db.commit()

        return quote