from sqlalchemy.orm import with_polymorphic
from sqlalchemy import func, or_, and_, cast, case
from sqlalchemy.dialects.postgresql import JSONB
from .base import DatabaseService
import database.models as models
import math
from datetime import datetime

class SupportTicketService(DatabaseService):

    name = 'support_ticket'
    Model = models.SupportTicket

    def get_by_user_id(self, user_id):
        tickets = self.query().filter(self.Model.user_id==user_id).all()
        return tickets        

    def get_open_tickets(self, user_id):        
        tickets = self.query().filter(self.Model.user_id==user_id).filter(self.Model.status!='closed').all()        
        return tickets

    def get_closed_tickets(self, user_id):
        tickets = self.query().filter(self.Model.user_id==user_id).filter(self.Model.status=='closed').all()
        return tickets

    def get_all_with_order(self):
        tickets = self.query().order_by(
            case([(self.Model.status=='open',1), (self.Model.status=='resolving', 2)], else_=3),
            self.Model.update_time.desc()
        ).all()

        return tickets

    def update_and_create_message(self, item, user_id, **kwargs):
        item.update(**kwargs)
        # GET THREAD HERE
        try:
            thread = self.db.thread.get_or_create_support_thread(item.user_id, item)
            st_msg = models.Message()
            st_msg.update(
                user_id=user_id,
                thread_id=thread.id,
                subject=f'Ticket updated',
                content=self.db.message.support_ticket_msg_tpl.render(ticket=item),
                is_pinned=True,
                pinned_by=user_id
            )
            self.db.session.add(st_msg)
        except:
            pass

        self.db.commit()
        return item