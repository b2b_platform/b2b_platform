import math

from .base import DatabaseService
import database.models as models

class MediaService(DatabaseService):

    name = 'media'
    Model = models.Media    

    def get_by_company_id(self, company_id, limit=25, current_page=1):
        company = self.db.company.get_by_id(company_id)
        query = company._media
        num_pages = math.ceil(query.count() / limit)
        media = query.limit(limit).offset((current_page-1)*limit).all()
        
        return num_pages, media

    def get_by_product_id(self, product_id):
        product = self.db.product.get_by_id(product_id)
        return product._media.all()

    def get_by_rfq_id(self, rfq_id):
        rfq = self.db.rfq.get_by_id(rfq_id)
        return rfq._media.all()

    def get_by_quote_id(self, quote_id):
        quote = self.db.quote.get_by_id(quote_id)
        return quote._media.all()

    def get_by_company_products(self, company_id, limit=20, current_page=1):
        products = self.db.product.get_by_company_id(company_id)
        product_ids = [p.id for p in products]

        query = (self.query()
            .join(models.Product._media)
            .filter(models.Product.id.in_(product_ids)).distinct())

        num_pages = math.ceil(query.count() / limit)
        media = query.limit(limit).offset((current_page-1)*limit).all()

        return num_pages, media