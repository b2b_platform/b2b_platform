from .base import DatabaseService
import database.models as models

class ProductCategoryService(DatabaseService):

    name = 'product_category'
    Model = models.ProductCategory
    
    def get_first_level_categories(self):
        return self.filter(parent_category_id=None)

    def get_all_immediate_subcategories(self, name):
        product_cat = self.filter_one(name=name)
        if not product_cat:
            # Not limit to 100?
            return self.filter(parent_category_id=product_cat.id)
        else:
            return None
            
    def get_intermediate_parents(self, name):
        # returning a list of nearest parents first, and top of the tree last
        product = self.filter_one(name=name)
        parlist = []
        
        # product.parent_category_id
        
        return parlist 
    
    # List of parents with the first being the direct immediate parent    
    def get_parent_list(self, parent_id):
        result = []
        
        # TBU: Clean up for a more elegant code

        cur_cat = self.get_by_id(parent_id)

        while cur_cat:
            result.append(cur_cat.to_dict())
            if cur_cat.parent_category_id:
                cur_cat = self.get_by_id(cur_cat.parent_category_id)
            else:
                break

        return result
    
    def get_parents(self, cat_id):
        beginning_setter = (self.query().filter(self.Model.id == cat_id)
            .cte(name='parent_for', recursive=True))
        with_recursive = beginning_setter.union_all(
            self.query().filter(self.Model.id == beginning_setter.c.parent_category_id)
        )
        return self.db.query(with_recursive).all()

    def get_children(self, cat_id):
        beginning_setter = self.query().filter(self.Model.id == cat_id).cte(name='children_for', recursive=True)
        with_recursive = beginning_setter.union_all(
            self.query().filter(self.Model.parent_category_id == beginning_setter.c.id)            
        )
        return self.db.query(with_recursive).all()

    def get_first_levels(self, nameonly=False):
        if nameonly:
            query = self.db.query(self.Model.name)
        else:
            query = self.db.query(self.Model.id, self.Model.name)

        return query.filter(self.Model.parent_category_id == None).all()        