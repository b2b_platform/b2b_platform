from .base import DatabaseService
import database.models as models
from sqlalchemy import func
from sqlalchemy_searchable import parse_search_query
from sqlalchemy.orm import aliased
import math
from datetime import timedelta, datetime
from membership import membership_constants as mc

class MembershipService(DatabaseService):
    name = 'membership'
    Model = models.Membership

    MembershipPayment = models.MembershipPayment

    def add_basic_membership(self, company_id):
        item = self.Model(company_id=company_id,tier=mc.TIER_BASIC,
            membership_start=datetime.utcnow())
        self.db.session.add(item)
        self.db.commit()
        self.db.company.update_by_id(company_id, current_membership_id=item.id)
        return item

    def get_membership_by_company(self, company_id):
        #res = db.company.get_by_id(company_id)._current_membership
        #if not res:
        #    return self.add_basic_membership(company_id)
        #else:
        #    return res
        return self.db.company.get_by_id(company_id)._current_membership

    def add_new_membership_by_company(self, company_id, **kwargs):
        item = self.Model(company_id=company_id, **kwargs)
        self.db.session.add(item)
        self.db.commit()
        self.db.company.update_by_id(company_id, current_membership_id=item.id)
        return item

    def update_membership_by_company(self, company_id, **kwargs):
        item = db.company.get_by_id(company_id)._current_membership
        item.update(**kwargs)
        self.db.commit()
        return item

    def add_payment(self, payer_id, membership_id=None, payment_time=datetime.utcnow(), amount=0):
        item = self.MembershipPayment(user_id=payer_id,
            membership_id=membership_id,
            payment_time=payment_time,
            amount=amount)
        self.db.session.add(item)
        self.db.commit()
        return item

    def search(self, *args, **kwargs):
        return None