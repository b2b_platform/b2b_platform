from sqlalchemy.dialects.postgresql import ENUM

company_sizes = ('1-10', '10-100', '100-500', '500-1000', '1000-5000', '5000+')
company_sizes_enum = ENUM(*company_sizes, name='company_sizes')

total_revenue = ('Below US$ 1M', 'US$ 1-2.5M', 'US$ 2.5-5M', 'US$ 5-10M', 'US$ 10-50M', 'US$ 50-100M', 'Above $US 100M')
total_revenue_enum = ENUM(*total_revenue, name='total_revenue')

mgnt_certifications = ('ISO9001', 'ISO14001', 'OHSAS18001', 'SA8000', 'FSC', 'ISO13485', 'BSCI', 'GMP', 'ISO/TS16949', 'BRC', 'Other')
mgnt_certifications_enum = ENUM(*mgnt_certifications, name='mgnt_certifications')

company_report_types = ('Scam',
        'Outdated Information',
        'Closed','Invalid Information',
        'Offensive Information','Illegal Business',
        'Other')
company_report_types_enum = ENUM(*company_report_types, name='company_report_types')

user_activity_types = (
    'VIEW_COMPANY',    
    'UPDATE_COMPANY',
    'CREATE_COMPANY',
    'LINK_COMPANY',
    'LEAVE_COMPANY',
    'VIEW_PRODUCT',
    'UPDATE_PRODUCT',
    'CREATE_PRODUCT',
    'VIEW_RFQ',
    'CREATE_RFQ',
    'DELETE_RFQ',
    'VIEW_QUOTE',
    'CREATE_QUOTE',
    'DELETE_QUOTE',
    'INQUIRE_PRODUCT',
    'CREATE_PRIVATE_THREAD',
    'CREATE_GENERIC_THREAD',
    'CREATE_SUPPORT_TICKET',
    'VIEW_SUPPORT_TICKET',
    'UPDATE_SUPPORT_TICKET',
    'UPDATE_ACCOUNT',
    'VIEW_ACCOUNT',
    'SEARCH_PRODUCTS'
)
user_activity_types_enum = ENUM(*user_activity_types, name='user_activity_types')

tradelead_types = ('buy', 'sell')
tradelead_types_enum = ENUM(*tradelead_types, name='tradelead_types')