import bcrypt
import uuid
from datetime import datetime, timedelta
from sqlalchemy import Table, Column, String, ForeignKey, DateTime, Integer, Float, UnicodeText, Boolean, MetaData, UniqueConstraint, inspect
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils.types import TSVectorType
from sqlalchemy_searchable import make_searchable
from sqlalchemy.orm.session import Session
import shortuuid
from slugify import slugify

from database.types import GUID, CastingArray
from database.enums import (company_sizes_enum, total_revenue_enum, 
    company_report_types_enum, user_activity_types_enum, tradelead_types_enum)
from role import role_constants

from .constants import MAX_NUM_QUOTES

make_searchable()
Model = declarative_base()
metadata = Model.metadata

def _unique(session, cls, hashfunc, queryfunc, constructor, arg, kw):
    cache = getattr(session, '_unique_cache', None)
    if cache is None:
        session._unique_cache = cache = {}

    key = (cls, hashfunc(*arg, **kw))
    if key in cache:
        return cache[key]
    else:
        with session.no_autoflush:
            q = session.query(cls)
            obj = queryfunc(q, *arg, **kw).first()            
            if not obj:
                obj = constructor(*arg, **kw)
                session.add(obj)
        cache[key] = obj
        return obj

class ModelMixin(object):

    id = Column(Integer, primary_key=True)

    def update(self, **kwargs):
        for k, v in kwargs.items():            
            if hasattr(self, k) and (v != getattr(self, k)):
                setattr(self, k, v)

    def to_dict(self):
        raise NotImplementedError
    
class TimeStampMixin(object):

    create_time = Column(DateTime, default=datetime.utcnow)
    update_time = Column(DateTime, onupdate=datetime.utcnow, default=datetime.utcnow)

class UserMixin(object):

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return self.id
        except AttributeError:
            raise NotImplementedError('No id attribute - override get_id')

    def __eq__(self, other):
        if isinstance(other, UserMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal

class UniqueMixin(object):

    @classmethod
    def unique_hash(cls, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, session, *arg, **kw):
        return _unique(
                    session,
                    cls,
                    cls.unique_hash,
                    cls.unique_filter,
                    cls,
                    arg, kw
               )    

keyword_to_company_table = Table('keyword_to_company', metadata,
        Column('keyword_id', Integer, ForeignKey('keyword.id'), primary_key=True),
        Column('company_id', Integer, ForeignKey('company.id'), primary_key=True)
        )

keyword_to_product_table = Table('keyword_to_product', metadata,
        Column('keyword_id', Integer, ForeignKey('keyword.id'), primary_key=True),
        Column('product_id', Integer, ForeignKey('product.id'), primary_key=True)
        )

keyword_to_rfq_table = Table('keyword_to_rfq', metadata,
        Column('keyword_id', Integer, ForeignKey('keyword.id'), primary_key=True),
        Column('rfq_id', Integer, ForeignKey('rfq.id'), primary_key=True)
        )

product_to_quote_table = Table('product_to_quote', metadata,
        Column('product_id', Integer, ForeignKey('product.id'), primary_key=True),
        Column('quote_id', Integer, ForeignKey('quote.id'), primary_key=True)
        )

keyword_to_trade_event_table = Table('keyword_to_trade_event', metadata,
        Column('keyword_id', Integer, ForeignKey('keyword.id'), primary_key=True),
        Column('trade_event_id', Integer, ForeignKey('trade_event.id'), primary_key=True)
        )

keyword_to_trade_lead_table = Table('keyword_to_trade_lead', metadata,
        Column('keyword_id', Integer, ForeignKey('keyword.id'), primary_key=True),
        Column('trade_lead_id', Integer, ForeignKey('trade_lead.id'), primary_key=True)
        )

media_to_company_table = Table('media_to_company', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('company_id', Integer, ForeignKey('company.id'), primary_key=True),    
)

media_to_company_verify_table = Table('media_to_company_verify', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('company_verify_id', Integer, ForeignKey('company_verify.id'), primary_key=True),    
)

media_to_product_table = Table('media_to_product', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('product_id', Integer, ForeignKey('product.id'), primary_key=True)
)

media_to_rfq_table = Table('media_to_rfq', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('rfq_id', Integer, ForeignKey('rfq.id'), primary_key=True)
)

media_to_quote_table = Table('media_to_quote', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('quote_id', Integer, ForeignKey('quote.id'), primary_key=True)
)

media_to_trade_event_table = Table('media_to_trade_event', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('trade_event_id', Integer, ForeignKey('trade_event.id'), primary_key=True)    
)

media_to_trade_lead_table = Table('media_to_trade_lead', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('trade_lead_id', Integer, ForeignKey('trade_lead.id'), primary_key=True)
)

class Media(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'media'

    name = Column(String(500))
    file_type = Column(String(50))
    url = Column(String(2048))
    folder = Column(String(2000))
    other_details = Column(JSONB)
    thumbnails = Column(JSONB)

    def __repr__(self):
        return self.url

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'file_type': self.file_type,
            'folder': self.folder,
            'create_time': self.create_time,            
            'url': self.url,
            'thumbnails': self.thumbnails or [],
            'thumbnail': self.thumbnail(),
            'other_details': self.other_details
        }

    def thumbnail(self, w="360"):
        if not self.thumbnails:
            return self.url

        tn = self.thumbnails.get(w, self.url)
        return tn

    def to_short_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'url': self.url,
            'thumbnail': self.thumbnail()            
        }

class User(Model, ModelMixin, TimeStampMixin, UserMixin):

    __tablename__ = 'user'

    email = Column(String(255), unique=True, nullable=False)
    _password = Column(String(255))

    first_name = Column(String(255), default='')
    last_name = Column(String(255), default='')
    account_type = Column(String(255), default='Basic')
    description = Column(String(2000))
    profile_pic_id = Column(Integer, ForeignKey('media.id'))
    _profile_pic = relationship('Media', foreign_keys=[profile_pic_id], cascade='all, delete, delete-orphan', single_parent=True)
    banner_id = Column(Integer, ForeignKey('media.id'))
    _banner = relationship('Media', foreign_keys=[banner_id], cascade='all, delete, delete-orphan', single_parent=True)
    is_active = Column(Boolean, default=True)
    is_banned = Column(Boolean, default=False)
    is_company_verified = Column(Boolean, default=False)
    is_account_confirmed = Column(Boolean, default=False)
    account_confirmed_time = Column(DateTime)
    social_accounts = Column(JSONB)
    location = Column(String(500))
    zipcode = Column(String(50))
    phone = Column(String(50))
    country = Column(String(50))
    position_title = Column(String(50))
    is_hidden = Column(Boolean, default=False)
    pending_company_id = Column(Integer, ForeignKey('company.id'))

    thread_settings = Column(JSONB) # Thread
    site_settings = Column(JSONB) # Turn on off notification
    privacy_settings = Column(JSONB) # Set email to be visible
    
    # Foreign keys

    # not used directly
    company_name = Column(String(255)) # filled by special function - not directly edible
    company_id = Column(Integer, ForeignKey('company.id'))  # Allowing nullable - allowing to be empty aft first
    company = relationship('Company', backref='users', foreign_keys=[company_id])
    
    website = Column(String(2048))
    interest = Column(String(50)) #buy, sell, trade
    other_details = Column(JSONB)

    def __hash__(self):
        return hash(self.email)

    @hybrid_property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @hybrid_property
    def profile_pic(self):        
        if self._profile_pic:
            return self._profile_pic.thumbnail()
        return None

    @hybrid_property
    def banner(self):
        if self._banner:
            return self._banner.url
        return None

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, password):
        self._password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(12)).decode('utf8')

    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf8'), self._password.encode('utf8'))

    @hybrid_property
    def is_confirmed(self):
        return self.is_account_confirmed
    
    def is_visible(self):
        if self.privacy_settings:
            return self.privacy_settings.get('is_visible', True)
        return True

    def is_system_hidden(self):
        if self.privacy_settings:
            return self.privacy_settings.get('is_system_hidden', False)

    def to_dict(self):
        return {
                'id': self.id,                
                'name': self.name,
                'email': self.email,
                'profile_pic': self.profile_pic,
                'description': self.description,
                'interest': self.interest,
                'location': self.location,
                'company_name': self.company_name,
                'company': None if not self.company else self.company.to_dict(),
                'company_id': self.company_id,
                'website': self.website
                }

    def to_dict_role(self):
        return {
                'id': self.id,                
                'name': self.name,
                'email': self.email,
                'profile_pic': self.profile_pic,
                'description': self.description,
                'company_name': self.company_name,
                'company': None if not self.company else self.company.to_dict(),
                # Join with company?
                'roles' : [] if not self.roles else { role.operation : role.to_dict() for role in self.roles },
                'company_id': self.company_id,
                'website': self.website
                }

    def to_public_dict(self):
        return {
                'id': self.id,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'profile_pic': self.profile_pic,
                'name': self.name,
                'email': self.email,
                'location': self.location,
                # TBI: check is_company_verified flag
                'company_name': self.company_name,
                'company': None if not self.company else self.company.to_dict(),
                'website': self.website
                }
    
    def to_msg_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'name': self.name,
            'email': self.email,
            'company_name': self.company.name if self.company_id else self.company_name,
            'company_id': self.company_id,
            'profile_pic': self.profile_pic
        }
    
    def to_blocked_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'name': self.name,
            'company_name': self.company.name if self.company_id else self.company_name,
            'company_id': self.company_id,
        }

    def to_role_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'name': self.name,
            'email': self.email,
            'role': self.roles[0].create_time if self.roles else None,
            'company_id': self.company_id,
            'profile_pic': self.profile_pic
        }

class UserSession(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'user_session'


    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    last_active_time = Column(DateTime)

    @hybrid_property
    def is_online(self):
        now = datetime.utcnow()
        online_cutoff = now - timedelta.seconds(60*15)
        return self.last_active_time >= online_cutoff

class Keyword(Model, ModelMixin, TimeStampMixin, UniqueMixin):

    __tablename__ = 'keyword'

    name = Column(String(500))
    search_vector = Column(TSVectorType('name'))

    def __init__(self, name=None):        
        self.name = name

    @classmethod
    def unique_hash(cls, name):
        return name

    @classmethod
    def unique_filter(cls, query, name):
        return query.filter(Keyword.name == name)

class KeywordMixin(object):

    _keywords = None
    
    @hybrid_property
    def keywords(self):
        return [ kw.name for kw in self._keywords ]

    @hybrid_property
    def keywords_as_str(self):
        return ', '.join([ kw.name for kw in self._keywords ])

    @keywords.setter
    def _set_keywords(self, kws):
        #session = inspect(self).session
        session = Session.object_session(self)
        if not session:
            raise RuntimeError('No session found')
            
        self._keywords = list(set(Keyword.as_unique(session, kw.lower().strip())  for kw in kws))

class Company(Model, ModelMixin, TimeStampMixin, KeywordMixin):

    __tablename__ = 'company'

    name = Column(String(2000))
    company_types = Column(String(500))  # type - selected from list of enums/values or can be arbitarily    

    main_category_id = Column(Integer, ForeignKey('product_category.id'))
    _main_category = relationship('ProductCategory', foreign_keys=[main_category_id])
    secondary_category_id = Column(Integer, ForeignKey('product_category.id'))
    _secondary_category = relationship('ProductCategory', foreign_keys=[secondary_category_id])

    # Has to be managed separately
    is_verified = Column(Boolean, default=False)
    is_public = Column(Boolean, default=False)
    is_claimed = Column(Boolean, default=False)
    
    # Edible by users
    website = Column(String(2048))
    membership_type = Column(String(50))
    
    current_membership_id = Column(Integer, ForeignKey('membership.id'))
    _current_membership = relationship('Membership', foreign_keys=[current_membership_id])

    description = Column(String(2000))
    company_info = Column(UnicodeText)

    # Media images
    logo_id = Column(Integer, ForeignKey('media.id'))
    _logo = relationship('Media', foreign_keys=[logo_id], 
        cascade='all, delete, delete-orphan', single_parent=True)
    banner_id = Column(Integer, ForeignKey('media.id'))
    _banner = relationship('Media', foreign_keys=[banner_id], 
        cascade='all, delete, delete-orphan', single_parent=True)
    background_id = Column(Integer, ForeignKey('media.id'))
    _background = relationship('Media', foreign_keys=[background_id], 
        cascade='all, delete, delete-orphan', single_parent=True)

    location = Column(String(500))
    country = Column(String(50))
    phone = Column(String(50))
    fax = Column(String(50))
    email = Column(String(255))
    year_founded = Column(Integer)
    company_size = Column(company_sizes_enum)
    capacity = Column(String(500))
    total_revenue = Column(total_revenue_enum)
    total_annual_sales = Column(String(500))
    total_purchase_volume = Column(String(500))
    main_markets = Column(String(1000))
    shipping_handling = Column(String(1000))
    port = Column(String(1000))
    payment = Column(String(1000))
    _media = relationship('Media', 
        secondary=media_to_company_table, lazy='dynamic', 
        backref=backref('company'), cascade='all,delete')
    
    mgnt_certifications = Column(JSONB)
    online_profiles = Column(JSONB) # links to alibaba, tradekey etc
    social_accounts = Column(JSONB)
    other_details = Column(JSONB)
    _keywords = relationship('Keyword', secondary=keyword_to_company_table)    

    search_vector = Column(TSVectorType('name', 'description', 'company_info', 'location', 
                                        'main_markets', 'country'))

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()
    
    @hybrid_property
    def media_list(self):
        return self._media.all()

    @hybrid_property
    def membership_tier(self):
        if self.current_membership_id:
            return self._current_membership.tier
        else:
            return None

    @hybrid_property
    def membership(self):
        return self._current_membership

    # Selective 
    def to_public_selective(self, **kwargs):
        # Should sanitize check for keywords
        
        # Check if keywords are matching to attribute names
        return {}
    
    @hybrid_property
    def logo(self):
        if self._logo:
            return self._logo.thumbnail()
        return None

    @hybrid_property
    def banner(self):
        if self._banner:
            return self._banner.url
        return None

    @hybrid_property
    def background(self):
        if self._background:
            return self._background.url
        return None

    @hybrid_property
    def main_category(self):
        if self._main_category:
            return self._main_category.name
        return None

    @hybrid_property
    def secondary_category(self):
        if self._secondary_category:
            return self._secondary_category.name
        return None

    @hybrid_property
    def featured_products(self):
        products = self.products.filter_by(is_featured=True).all()
        if not products:
            products = self.products.limit(4).all()
        return products

    def to_dict(self):
        return {
                'id': self.id,
                'name' : self.name,                
                'company_types' : self.company_types,
                'is_public' : self.is_public,
                'is_claimed': self.is_claimed,
                'website' : self.website,
                'membership_type' : self.membership_type,                 
                'description'  : self.description,
                'company_info' : self.company_info,
                'location'  : self.location,
                'country' : self.country,
                'phone'   : self.phone,
                'fax' : self.fax,
                'email' : self.email,
                'year_founded' : self.year_founded,
                'company_size' : self.company_size,
                'capacity' : self.capacity,
                'total_revenue' : self.total_revenue,
                'total_annual_sales': self.total_annual_sales,
                'total_purchase_volume': self.total_purchase_volume,
                'main_markets' : self.main_markets,
                'shipping_handling' : self.shipping_handling,
                'port' : self.port,
                'payment' : self.payment,
                'keywords': self.keywords,
                'other_details': self.other_details,
                'membership_tier' : self._current_membership.tier if self.current_membership_id else 0
                }

    def to_public_dict(self):
        return {
                'id': self.id,
                'name' : self.name,                
                'company_types' : self.company_types,
                'is_public' : self.is_public,
                'is_claimed': self.is_claimed,
                'website' : self.website,
                'logo': self.logo,
                'membership_type' : self.membership_type,                 
                'description'  : self.description,
                'company_info' : self.company_info,
                'location'  : self.location,
                'country' : self.country,
                'year_founded' : self.year_founded,
                'company_size' : self.company_size,
                'capacity' : self.capacity,
                'total_revenue' : self.total_revenue,
                'main_markets' : self.main_markets,
                'shipping_handling' : self.shipping_handling,
                'payment' : self.payment,
                'keywords': self.keywords,
                'other_details': self.other_details,
                'membership_tier': self.membership_tier
                }

def product_cat_path(cat, out):
    out.insert(0, {'name': cat.name, 'id': cat.id})
    if cat.parent_category_id:
        product_cat_path(cat.parent, out)
    return out

class ProductCategory(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'product_category'

    name = Column(String(1000))
    description = Column(String(2000))
    note = Column(UnicodeText)
    parent_category_id = Column(Integer, ForeignKey('product_category.id')) # Parent        
    children = relationship('ProductCategory', 
                            cascade='all, delete-orphan', lazy='joined',
                            backref=backref('parent', remote_side='product_category.c.id'))

    search_vector = Column(TSVectorType('name', 'description', 'note'))
    
    @hybrid_property
    def path(self):
        return product_cat_path(self, [])

    @hybrid_property
    def path_as_str(self):
        return ' >> '.join([cat['name'] for cat in self.path ])

    @hybrid_property
    def first_level_parent(self):
        session = Session.object_session(self)
        if not session:
            raise RuntimeError('No session found')
        beginning_setter = (session.query(ProductCategory).filter(self.Model.id == self.id)
            .cte(name='parent_for', recursive=True))
        with_recursive = beginning_setter.union_all(
            session.query(ProductCategory).filter(self.id == beginning_setter.c.parent_category_id)
        )
        return session.query(with_recursive).first()

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'path': self.path,
            'path_as_str': self.path_as_str,
            'description': self.note,
            'parent_category_id': None if not self.parent_category_id else self.parent_category_id,
            'children': {} if not self.children else { child.name: child.to_dict() for child in self.children }
        }

    def to_short_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            #'children': [] if not self.children else [child.to_short_dict() for child in self.children],
            'path_as_str': self.path_as_str,
            'path': self.path,
            'description': self.description,
            'parent_category_id': self.parent_category_id
        }


class Product(Model, ModelMixin, TimeStampMixin, KeywordMixin):

    __tablename__ = 'product'

    sku_id = Column(String(100))
    name = Column(String(2500))
    description = Column(UnicodeText)
    company_id = Column(Integer, ForeignKey('company.id'))
    product_category_id = Column(Integer, ForeignKey('product_category.id')) # lowest product category
    capacity = Column(String(500))
    min_order_quantity = Column(Float)
    min_order_quantity_text = Column(String(500))
    price_min_order = Column(String(500))
    unit_type = Column(String(50))
    payment_options = Column(JSONB)
    
    price_range_min = Column(Integer)
    price_range_max = Column(Integer)

    packaging = Column(String(2000))    
    port = Column(String(500))
    processing_time = Column(String(500))

    is_public = Column(Boolean, default=True)
    is_featured = Column(Boolean, default=False)

    _media = relationship('Media', secondary=media_to_product_table, lazy='dynamic', 
        backref=backref('product'), cascade='all,delete')
    date_added = Column(DateTime, default=datetime.utcnow)
    other_details = Column(JSONB)
    _keywords = relationship('Keyword', secondary=keyword_to_product_table)
    product_category = relationship('ProductCategory')
    company = relationship('Company', backref=backref('products', lazy='dynamic'))

    search_vector = Column(TSVectorType('name', 'description', 'capacity'))

    display_pic_id = Column(Integer, ForeignKey('media.id', ondelete='CASCADE'))
    _display_pic = relationship('Media', foreign_keys=[display_pic_id], 
        cascade='all, delete')

    @hybrid_property
    def display_pic(self):
        if self._display_pic:
            return self._display_pic.thumbnail()
        if self.media:
            return self.media[0].thumbnail()
        return None

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()

    @hybrid_property
    def media_list(self):
        return self._media.all()

    def to_dict(self):
        
        return {
                'id' : self.id,
                'name' : self.name,
                'company_id': self.company_id,
                'company_name': self.company.name,
                'description' : self.description,
                'product_category' : None if not self.product_category else  self.product_category.to_dict(),
                'capacity' : self.capacity,
                'min_order_quantity' : self.min_order_quantity,
                'unit_type' : self.unit_type,
                'packaging' : self.packaging,
                'keywords': self.keywords,
                'date_added': self.date_added,
                'port': self.port,
                'processing_time': self.processing_time,
                'payment_options': self.payment_options,
                'other_details': self.other_details,
                'price_range_min': self.price_range_min,
                'price_range_max': self.price_range_max
                }

    def to_short_dict(self):
        
        return {
            'id' : self.id,
            'name' : self.name,
            'company_id': self.company_id,
            'company_name': self.company.name,
            'description' : self.description,                
            'capacity' : self.capacity,
            'min_order_quantity' : self.min_order_quantity,
            'unit_type' : self.unit_type,
            'packaging' : self.packaging,                                                
            'processing_time': self.processing_time,
            'payment_options': self.payment_options,                
            'price_range_min': self.price_range_min,
            'price_range_max': self.price_range_max
        }

class RFQ(Model, ModelMixin, TimeStampMixin, KeywordMixin):

    __tablename__ = 'rfq'

    buyer_id = Column(Integer, ForeignKey('user.id'))    
    product = Column(String(200))
    product_category_id = Column(Integer,  ForeignKey('product_category.id')) # lowest product category
    quantity = Column(String(100))
    content = Column(UnicodeText)
    _media = relationship('Media', secondary=media_to_rfq_table, lazy='dynamic', 
        backref=backref('rfq'), cascade='all,delete')
    other_details = Column(JSONB)

    _keywords = relationship('Keyword', secondary=keyword_to_rfq_table)
    is_public = Column(Boolean, default=True)
    product_category = relationship('ProductCategory')
    buyer = relationship('User')
    end_time = Column(DateTime)

    search_vector = Column(TSVectorType('product', 'content'))

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()    

    @hybrid_property
    def media_list(self):
        return self._media.all()

    @hybrid_property
    def is_expired(self):
        if not self.end_time:
            return False

        now = datetime.utcnow()
        return now > self.end_time
    
    @hybrid_property
    def time_left_secs(self):
        if self.end_time:
            now = datetime.now()
            delta = self.end_time - now
            delta_secs = delta.total_seconds()
            if delta_secs < 0:
                return 0
            return delta_secs

        return None

    @hybrid_property
    def time_left_str(self):
        tl = self.time_left_secs
        if not tl:
            return 'N/A'

        tls = str(timedelta(seconds=tl))
        tls = 'H'.join(tls.split(':')[:2]).replace(' days, ', 'D ')
        return tls

    @hybrid_property
    def keywords_str(self):
        return ', '.join(self.keywords)  

    @hybrid_property
    def quotes_left(self):
        return MAX_NUM_QUOTES - len(self.quotes)

    def to_dict(self):
        return {
            'id': self.id,
            'buyer': self.buyer.to_dict(),
            'product': self.product,
            'product_category': None if not self.product_category else self.product_category.to_dict(),
            'quantity': self.quantity,
            'content': self.content,            
            'other_details': self.other_details,
            'keywords': self.keywords,
            'is_public': self.is_public,
            'create_time': self.create_time,
            'end_time': self.end_time,
            'time_left_secs': self.time_left_secs,
            'is_expired': self.is_expired     
        }        

class Quote(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'quote'

    seller_id = Column(Integer, ForeignKey('user.id'))    
    rfq_id = Column(Integer, ForeignKey('rfq.id'))
    title = Column(String(250))
    price = Column(String(200))
    quantity = Column(String(200))
    content = Column(UnicodeText)
    _media = relationship('Media', secondary=media_to_quote_table, lazy='dynamic', 
        backref=backref('quote'), cascade='all,delete')
    other_details = Column(JSONB)
    _products = relationship('Product', secondary=product_to_quote_table)

    rfq = relationship('RFQ', backref='quotes')
    seller = relationship('User')

    search_vector = Column(TSVectorType('title', 'content'))

    @hybrid_property
    def products(self):
        return self._products

    @hybrid_property
    def products_ids(self):
        return [ product.id for product in self.products ]

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()

    @hybrid_property
    def media_list(self):
        return self._media.all()

    @products.setter
    def _set_products(self, product_ids):
        session = Session.object_session(self)
        if not session:
            raise RuntimeError('No session found')

        if type(product_ids) != list:
            product_ids = [product_ids]

        product_ids = [] if not product_ids else product_ids

        self._products = [ product 
            for product in session.query(Product).filter(Product.id.in_(product_ids)) ]

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,            
            'seller': self.seller.to_dict(),
            'rfq': self.rfq.to_dict(),
            'price': self.price,
            'quantity': self.quantity,
            'content': self.content,
            'other_details': self.other_details,
            'products': [ product.to_dict() for product in self.products ]
        }

class Notification(Model, ModelMixin, TimeStampMixin):
    __tablename__ = 'notification'
    
    notification_cls = Column(String(50))
    notification_type = Column(String(255)) # Type of notification: product, rfq, quote and etc
    details = Column(JSONB)

    __mapper_args__ = {
        'polymorphic_identity': 'notification',
        'polymorphic_on': notification_cls
    }

    def to_dict(self):
        return {
            'id': self.id,
            'notification_type': self.notification_type,
            'notification_cls' : self.notification_cls            
        }

class NotificationCompany(Notification):
    __tablename__ = 'notification_company'

    id = Column(Integer, ForeignKey('notification.id'), primary_key=True)
    company_id = Column(Integer, ForeignKey('company.id'))
    company = relationship('Company')

    __mapper_args__ = {
        'polymorphic_identity': 'notification_company'
    }

    def to_dict(self):
        return {
            'id': self.id,
            'company_id' : self.company_id,
            'details': self.details,
            'notification_type' : self.notification_type,
            'notification_cls' : self.notification_cls
        }

class NotificationUser(Notification):
    __tablename__ = 'notification_user'

    id = Column(Integer, ForeignKey('notification.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship('User')
    
    __mapper_args__ = {
        'polymorphic_identity': 'notification_user'
    }

    def to_dict(self):
        return {
            'id': self.id,
            'user_id' : self.user_id,
            'details': self.details,
            'notification_type' : self.notification_type,
            'notification_cls' : self.notification_cls
        }

class NotificationUserReadTime(Model):

    __tablename__ = 'notification_user_readtime'

    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    last_read_time = Column(DateTime)

class TradeEvent(Model, ModelMixin, TimeStampMixin, KeywordMixin):

    __tablename__ = 'trade_event'

    name = Column(String(255))
    description = Column(UnicodeText)
    creator_id = Column(Integer, ForeignKey('user.id'))
    is_verified = Column(Boolean, default=False)
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    location = Column(String(500))
    industry = Column(String(500))
    organizer = Column(String(255))
    contact = Column(UnicodeText)  
    featured_pic_id = Column(Integer, ForeignKey('media.id'))
    _featured_pic = relationship('Media', foreign_keys=[featured_pic_id])
    _media = relationship('Media', secondary=media_to_trade_event_table, lazy='dynamic', 
        backref=backref('trade_event'), cascade='all,delete')
    other_details = Column(JSONB)
    _keywords = relationship('Keyword', secondary=keyword_to_trade_event_table)

    search_vector = Column(TSVectorType('name', 'description', 'location', 'industry', 'organizer'))

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()

    @hybrid_property
    def media_list(self):
        return self._media.all()

    @hybrid_property
    def featured_pic(self):
        if self._featured_pic:
            return self._featured_pic.url
        return None

    def to_dict(self):
        return {}

class CompanyReport(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'company_report'

    report_type = Column(company_report_types_enum)
    reason = Column(String(2000))
    user_id = Column(Integer, ForeignKey('user.id')) 
    company_id = Column(Integer, ForeignKey('company.id'))
    is_resolved = Column(Boolean, default=False)
    company = relationship('Company', backref='reports')

    def to_dict(self):
        return {
            'id' : self.id,
            'reason' : self.reason,
            'company_name' : self.company.name,
            'company_id' : self.company_id,
            'is_resolved' : self.is_resolved,
            'create_time' : self.create_time
        }

# Company rating
class CompanyRating(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'company_rating'

    rating = Column(Integer)
    text = Column(String(5000))
    user_id = Column(Integer, ForeignKey('user.id')) # might not be necessary
    company_id = Column(Integer, ForeignKey('company.id'))

    __table_args__ = (
        UniqueConstraint("user_id", "company_id"),
    )

class SupportTicket(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'support_ticket'

    user_id = Column(Integer, ForeignKey('user.id'))
    name = Column(String(250))
    description = Column(UnicodeText)
    ticket_type = Column(String(50))
    status = Column(String(20), default='open')
    user = relationship('User')

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'name': self.name,
            'description': self.description,
            'ticket_type': self.ticket_type,
            'status': self.status            
        }

class UserToMessageThread(Model):

    __tablename__ = 'user_to_message_thread'

    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    message_thread_id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    last_active_time = Column(DateTime)

user_to_archived_thread_table = Table('user_to_archived_thread', metadata,
    Column('user_id', Integer, ForeignKey('user.id'), primary_key=True),
    Column('message_thread_id', Integer, ForeignKey('message_thread.id'), primary_key=True)    
)

class MessageThread(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'message_thread'

    name = Column(String(250))    
    description = Column(String(2000))
    thread_type = Column(String(50))
    is_active = Column(Boolean, default=True)
    other_details = Column(JSONB)
    creator_id = Column(Integer, ForeignKey('user.id'))
    creator = relationship('User', foreign_keys=[creator_id])

    _users = relationship('User', secondary=UserToMessageThread.__table__)

    archived_by = relationship('User', secondary=user_to_archived_thread_table, 
        backref=backref('archived_threads'), cascade='all,delete')

    search_vector = Column(TSVectorType('name', 'description'))

    __mapper_args__ = {
        'polymorphic_identity': 'message_thread',
        'polymorphic_on': thread_type
    }

    @hybrid_property
    def users(self):
        return self._users

    @hybrid_property
    def users_by_id(self):
        return [ u.id for u in self._users ]

    @users.setter
    def _set_users(self, user_ids):
        session = Session.object_session(self)
        if not session:
            raise RuntimeError('No session found')
        
        self._users = [ u for u in session.query(User).filter(User.id.in_(user_ids)) ]

    @hybrid_property    
    def user_companies(self):
        return list(set([ u.company_id for u in self.users ]))
    
    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'description': self.description,
            'users': [ u.to_msg_dict() for u in self.users ],
            'other_details': self.other_details,
            'creator_id': self.creator_id,
            'creator': self.creator.to_msg_dict(),     
            'is_active': self.is_active
        }

    def to_short_dict(self):
        return {
            'id': self.id,
            'name': self.name, 
            'create_time': self.create_time,
            'update_time': self.update_time,
            'thread_type': self.thread_type,
            'description': self.description,
            'is_active': self.is_active
        }        

class PrivateThread(MessageThread):

    __tablename__ = 'private_thread'

    id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    target_user_id = Column(Integer, ForeignKey('user.id'))
    target_user = relationship('User', foreign_keys=[target_user_id])

    __mapper_args__ = {
        'polymorphic_identity': 'private_thread'
    }

    def name_for_user(self, user_id):
        other_user = [u for u in self.users if u.id != user_id]
        if other_user:
            return f'{self.name} with {other_user[0].name}'

        return self.name

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'description': self.description,            
            'other_details': self.other_details,
            'creator_id': self.creator_id,
            'creator': self.creator.to_msg_dict(),     
            'target_user_id': self.target_user_id,
            'target_user': self.target_user.to_msg_dict(),
            'is_active': self.is_active,            
        }           


product_to_inquiry_thread_table = Table('product_to_inquiry_thread', metadata,
    Column('product_id', Integer, ForeignKey('product.id'), primary_key=True),
    Column('message_thread_id', Integer, ForeignKey('product_inquiry_thread.id'), primary_key=True)
)

class ProductInquiryThread(MessageThread):

    __tablename__ = 'product_inquiry_thread'

    id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    products = relationship('Product', secondary=product_to_inquiry_thread_table)
    company_id = Column(Integer, ForeignKey('company.id'))

    __mapper_args__ = {
        'polymorphic_identity': 'product_inquiry'
    }

    @hybrid_property    
    def user_companies(self):
        uc = set([ u.company_id for u in self.users ])
        uc.add(self.company_id)
        return list(uc)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'description': self.description,            
            'other_details': self.other_details,
            'creator_id': self.creator_id,
            'creator': self.creator.to_msg_dict(),     
            'is_active': self.is_active,
            'company_id': self.company_id
        }        

class RFQThread(MessageThread):

    __tablename__ = 'rfq_thread'

    id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    rfq_id = Column(Integer, ForeignKey('rfq.id'))
    rfq = relationship('RFQ')
    
    __mapper_args__ = {
        'polymorphic_identity': 'rfq'
    }

    @hybrid_property    
    def user_companies(self):
        uc = set([ u.company_id for u in self.users ])
        if self.rfq.buyer.company_id:
            uc.add(self.rfq.buyer.company_id)
        return list(uc)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'description': self.description,
            'users': [ u.to_msg_dict() for u in self.users ],
            'other_details': self.other_details,
            'creator_id': self.creator_id,
            'creator': self.creator.to_msg_dict(),     
            'is_active': self.is_active,
            'rfq': self.rfq.to_dict()
        }    

class TradeLeadThread(MessageThread):

    __tablename__ = 'trade_lead_thread'

    id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    trade_lead_id = Column(Integer, ForeignKey('trade_lead.id'))
    trade_lead = relationship('TradeLead')
    
    __mapper_args__ = {
        'polymorphic_identity': 'trade_lead'
    }


    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'description': self.description,
            'users': [ u.to_msg_dict() for u in self.users ],
            'other_details': self.other_details,
            'creator_id': self.creator_id,
            'creator': self.creator.to_msg_dict(),     
            'is_active': self.is_active,
            'trade_lead': self.trade_lead.to_dict() if self.trade_lead else None
        }    


class SupportThread(MessageThread):

    __tablename__ = 'support_thread'

    id = Column(Integer, ForeignKey('message_thread.id'), primary_key=True)
    ticket_id = Column(Integer, ForeignKey('support_ticket.id'))
    ticket = relationship('SupportTicket', backref='support_thread', uselist=False)

    __mapper_args__ = {
        'polymorphic_identity': 'support'
    }

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'thread_type': self.thread_type,
            'users': [ u.to_msg_dict() for u in self.users ],            
            'ticket': self.ticket.to_dict(),
            'creator_id': self.creator.id,
            'creator': self.creator.to_msg_dict()
        }


class MessageTemplate(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'message_template'

    user_id = Column(Integer, ForeignKey('user.id'))
    name = Column(String(250))
    template = Column(UnicodeText)
    is_public = Column(Boolean, default=False)
    is_available_internally = Column(Boolean, default=True)

    search_vector = Column(TSVectorType('name'))

user_to_saved_message_table = Table('user_to_saved_message', metadata, 
    Column('user_id', Integer, ForeignKey('user.id'), primary_key=True),
    Column('message_id', Integer, ForeignKey('message.id'), primary_key=True)
)

class Message(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'message'

    thread_id = Column(Integer, ForeignKey('message_thread.id'))
    user_id = Column(Integer, ForeignKey('user.id'))
    subject = Column(String(1000))
    content = Column(UnicodeText)    
    other_details = Column(JSONB)
    is_html = Column(Boolean, default=False)
    
    user = relationship('User', foreign_keys=[user_id]) # creator

    is_pinned = Column(Boolean, default=False)
    pinned_by = Column(Integer, ForeignKey('user.id'))
    _saved_by = relationship('User', secondary=user_to_saved_message_table)    

    message_type = Column(String(50))

    search_vector = Column(TSVectorType('subject', 'content'))    

    __mapper_args__ = {
        'polymorphic_identity': 'message',
        'polymorphic_on': message_type
    }


    @hybrid_property
    def saved_by(self):        
        return self._saved_by

    @saved_by.setter
    def _set_saved_by(self, user_ids):
        session = Session.object_session(self)
        if not session:
            raise RuntimeError('No session found')
        
        self._saved_by = [ u for u in session.query(User).filter(User.id.in_(user_ids)) ]
    
    def to_dict(self):
        return {
            'id': self.id,
            'thread_id': self.thread_id,
            'user': self.user.to_msg_dict(),
            'user_id': self.user_id,
            'subject': self.subject,
            'content': self.content,
            'is_pinned': self.is_pinned,
            'pinned_by': self.pinned_by,
            'other_details': self.other_details,
            'is_html': self.is_html,
            'create_time': str(self.create_time),
            'saved_by': [ u.id for u in self.saved_by ],
            'message_type': self.message_type
        }

media_to_message_table = Table('media_to_message', metadata,
    Column('media_id', Integer, ForeignKey('media.id'), primary_key=True),
    Column('media_message_id', Integer, ForeignKey('media_message.id'), primary_key=True)
)

class MediaMessage(Message):

    __tablename__ = 'media_message'

    id = Column(Integer, ForeignKey('message.id'), primary_key=True)
    _media = relationship('Media', secondary=media_to_message_table, lazy='select', 
        backref=backref('message'), cascade='all,delete')

    __mapper_args__ = {
        'polymorphic_identity': 'media_message'
    }

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()

    @hybrid_property
    def media_list(self):
        return self._media.all()

    def to_dict(self):
        return {
            'id': self.id,
            'thread_id': self.thread_id,
            'user': self.user.to_msg_dict(),
            'user_id': self.user_id,
            'subject': self.subject,
            'content': self.content,
            'is_pinned': self.is_pinned,
            'pinned_by': self.pinned_by,
            'other_details': self.other_details,
            'is_html': self.is_html,
            'create_time': str(self.create_time),
            'saved_by': [ u.id for u in self.saved_by ],
            'media': [ m.to_short_dict() for m in self._media ],
            'message_type': self.message_type
        }

class MessageAttachment(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'message_attachment'

    message_id = Column(Integer, ForeignKey('message.id'))
    attachment_type = Column(String(100))
    url = Column(String(2048))

class PendingRequest(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'pending_users'

    message_id = Column(Integer, ForeignKey('message.id'))
    attachment_type = Column(String(100))
    url = Column(String(2048))


class UserCompanyRole(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'user_company_role'

    user_id = Column(Integer, ForeignKey('user.id'))
    company_id = Column(Integer, ForeignKey('company.id'))
   
    user = relationship('User', backref='roles')
    company = relationship('Company', backref='roles')
    
    operation = Column(Integer)
    permission = Column(Integer)
    description = Column(String(255))
    notes = Column(String(2000))

    def to_dict(self):
        return {
            'id' : self.id,
            'user_id' : self.user_id,
            'company_id' : self.company_id,
            'operation': self.operation,
            'permission': self.permission,
            'description' : self.description,
            'notes' : self.notes
        }

    def to_user_dict(self):
        return {
            'id' : self.id,
            'user_id' : self.user_id,
            'first_name' : self.user.first_name,
            'last_name': self.user.last_name,
            'email' : self.user.email,
            'permission': self.permission,
            'permission_txt' : role_constants.PERMISSION_TEXT_ARR[self.permission] if (self.permission and self.permission > 0) else 'PENDING',
            'description' : self.description,
            'notes' : self.notes,
            'create_time' : self.create_time
        }

    def to_company_dict(self):
        return {
            'id' : self.id,
            'company_id' : self.company.id,
            'name' : self.company.name,
            'description' : self.description,
            'notes' : self.notes,
            'create_time' : self.create_time
        }

    @hybrid_property
    def to_dict_txt(self):
        return {
            'company' : None if not self.company else self.company.name,
            'operation' : role_constants.OPERATION_TEXT_ARR[self.operation],
            'permission': role_constants.PERMISSION_TEXT_ARR[self.permission] if (self.permission and self.permission > 0) else 'PENDING'
        }


class UserActivity(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'user_activity'

    user_id = Column(Integer, ForeignKey('user.id'))
    object_id = Column(Integer)
    activity_type = Column(user_activity_types_enum)
    text = Column(UnicodeText)
    details = Column(JSONB)    

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'text': self.text,
            'details': self.details
        }

class CompanyVerify(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'company_verify'

    company_id = Column(Integer, ForeignKey('company.id'))
    user_id = Column(Integer, ForeignKey('user.id')) # User - the uploader ID
    
    user = relationship('User', backref='verification')
    company = relationship('Company', backref='verification')

    _media = relationship('Media', 
        secondary=media_to_company_verify_table, lazy='dynamic', 
        backref=backref('company_verify'), cascade='all,delete')

    note = Column(UnicodeText)

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()
    
    @hybrid_property
    def media_list(self):
        return self._media.all()

    def to_dict(self):
        return {
            'id': self.id,
            'company_id': self.company_id,
            'user_id': self.user_id,
            'note': self.note
        }


class Membership(Model, ModelMixin):

    __tablename__ = 'membership'

    company_id = Column(Integer, ForeignKey('company.id'))
    company = relationship('Company', backref='memberships', foreign_keys=[company_id])

    membership_start = Column(DateTime)
    membership_end = Column(DateTime)
    tier = Column(Integer)
    note = Column(String(1000))

    def to_dict(self):
        return {
            'id': self.id,
            'company_id': self.company_id,
            'tier': self.tier,
            'membership_start': self.membership_start,
            'membership_end' : self.membership_start,
            'note': self.note
        }

    @hybrid_property
    def duration(self):
        if self.membership_end:
            return self.membership_end - self.membership_start
        else:
            return datetime.utcnow() - self.membership_start

class MembershipPayment(Model, ModelMixin):

    __tablename__ = 'membership_payment'

    membership_id = Column(Integer, ForeignKey('membership.id')) # Reference
    membership = relationship('Membership', backref='membership_payments', foreign_keys=[membership_id])

    amount = Column(Integer) # Whole integer - US dollars * 100
    payment_time = Column(DateTime)
    user_id = Column(Integer, ForeignKey('user.id')) # Paid by user


class Subscriber(Model, ModelMixin, TimeStampMixin):

    __tablename__ = 'subscriber'

    user_id = Column(Integer, ForeignKey('user.id'))
    email = Column(String(255), unique=True, nullable=False)
    name = Column(String(500))

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'email': self.email,
            'name': self.name,
            'create_time': self.create_time
        }

class TradeLead(Model, ModelMixin, TimeStampMixin, KeywordMixin):

    __tablename__ = 'trade_lead'

    # The person posting
    user_id = Column(Integer, ForeignKey('user.id'))    # Can be null
    user = relationship('User')
    lead_type = Column(Integer) # reference in constants



    # Related to company
    company_name = Column(String(500))
    company_id = Column(Integer, ForeignKey('company.id'))
    company = relationship('Company')
    
    # Optional fields 
    # Related to title
    title = Column(String(500))

    country = Column(String(50))

    reference_url = Column(String(2048))
    reference_type = Column(Integer)

    product_category_id = Column(Integer, ForeignKey('product_category.id')) # lowest product category
    product_category = relationship('ProductCategory')

    # Optional
    quantity = Column(String(100))
    content = Column(UnicodeText)
    
    is_public = Column(Boolean, default=True)
    end_time = Column(DateTime)

    # Media allowance
    _media = relationship('Media', secondary=media_to_trade_lead_table, lazy='dynamic', 
        backref=backref('trade_lead'), cascade='all,delete')

    other_details = Column(JSONB)

    # Keywords support for search
    _keywords = relationship('Keyword', secondary=keyword_to_trade_lead_table)
    
    search_vector = Column(TSVectorType('product', 'company', 'content'))

    @hybrid_property
    def media(self):
        return self._media.limit(1).all()    

    @hybrid_property
    def media_list(self):
        return self._media.all()

    @hybrid_property
    def is_expired(self):
        if not self.end_time:
            return False

        now = datetime.utcnow()
        return now > self.end_time

    @hybrid_property
    def keywords_str(self):
        return ', '.join(self.keywords)

    @hybrid_property
    def lead_name(self):
        if self.product and self.company:
            return ' '.join((self.product, self.company))
        if self.product:
            return self.product
        else:
            return self.company

    def to_dict(self):
        return {
            'id': self.id,
            'user': self.user.to_dict(),
            'title': self.title,
            'product_category': None if not self.product_category else self.product_category.to_dict(),
            'quantity': self.quantity,
            'content': self.content, 
            'country': self.country,       
            'other_details': self.other_details,
            'keywords': self.keywords,
            'is_public': self.is_public,
            'create_time': self.create_time,
            'end_time': self.end_time, 
        }        
