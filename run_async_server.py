from async_server_tornado import server
import argparse
import logging
from settings import load

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Run web flask dev server')
    parser.add_argument('-p', '--port', type=int, default=9001,
                        help='port to run web app')
    parser.add_argument('-L', '--log-level', default='DEBUG', help='log level')

    args = parser.parse_args()
    host = '0.0.0.0'
    port = args.port
    
    log_levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,  
        'warning': logging.WARNING,
        'error': logging.ERROR,        
        'critical': logging.CRITICAL        
    }

    logging.basicConfig(level=log_levels[args.log_level.lower()])

    server.start(port=port, host=host)


    