from werkzeug.utils import secure_filename
from time import mktime, gmtime
import os

from .base import Datastore

def now():
    return int(mktime(gmtime()))

class FSDatastore(Datastore):

    def __init__(self, *args, **kwargs):
        self.UPLOAD_FOLDER = kwargs.pop('UPLOAD_FOLDER', None)
        self.URL_PREFIX = kwargs.pop('URL_PREFIX', None)        
        super(FSDatastore, self).__init__(**kwargs)
    
    def location(self, folder, *args):
        folder = folder.strip('/')
        return os.path.abspath(os.path.join(self.UPLOAD_FOLDER, folder, *args))

    def url(self, folder, filename):
        folder = folder.strip('/')
        return os.path.join(self.URL_PREFIX, folder, filename)

    def list_objs(self, folder, fullpath=False):
        objs_dir = self.location(folder)
        if not os.path.exists(objs_dir):
            return []
            
        if fullpath:
            return [os.path.abspath(os.path.join(objs_dir, fn))
                for fn in os.listdir(objs_dir)]
        
        return os.listdir(objs_dir)

    def get_total_size(self, folder):
        objs_dir = self.location(folder)
        
        total = 0
        for item in os.scandir(folder):
            if item.is_file():
                total += item.stat().st_size
            elif item.is_dir():
                total += self.get_total_size(item.path)

        return total

    def save_obj(self, folder, obj):
        obj_dir = self.location(folder)

        if not os.path.exists(obj_dir):
            os.makedirs(obj_dir)

        # Patching for shorterning file nam
        obj_name = f'{secure_filename(obj.filename)}_{now()}'
        obj_path = os.path.join(obj_dir, obj_name)
        obj_url = self.url(folder, obj_name)
        obj.save(obj_path)
        return (obj_name, obj_url)


    def save_obj_shorten(self, folder, obj):
        obj_dir = self.location(folder)

        if not os.path.exists(obj_dir):
            os.makedirs(obj_dir)

        # Patching for shorterning file name
        obj_name = f'{secure_filename(obj.filename)}_{now()}'
        if len(obj_name) > 40:
            obj_name = obj_name[-30:]
        obj_path = os.path.join(obj_dir, obj_name)
        
        obj_url = self.url(folder, obj_name)
        obj.save(obj_path)
        return (obj_name, obj_url)

    def save_objs(self, folder, objs):
        output = [ self.save_obj(folder, obj) for obj in objs ]
        return output

    def delete_obj(self, folder, obj_name):
        obj_dir = self.location(folder)        

        # delete thumbnails etc here 
        # TODO: USE MEDIA DB OBJECT
        try:
            for fn in os.listdir(obj_dir):
                if not fn.startswith(obj_name): continue

                fp = self.location(folder, fn)
                if os.path.exists(fp):
                    os.remove(fp)
        except:
            pass
    
    def read_obj(self, folder, obj_name):
        data = None
        obj_path = self.location(folder, obj_name)
        if os.path.exists(obj_path):
            with open(obj_path, 'rb') as f:
                data = f.read()
        
        return data
