class Datastore(object):

    def __init__(self, *args, **kwargs):
        pass
    
    def create_objs(self, *args, **kwargs):
        raise NotImplementedError

    def create_one_obj(self, *args, **kwargs):
        raise NotImplementedError

    def list_objs(self, *args, **kwargs):
        raise NotImplementedError

    def delete_objs(self, *args, **kwargs):
        raise NotImplementedError

    def delete_one_obj(self, *args, **kwargs):
        raise NotImplementedError        

    def check_obj_exists(self, *args, **kwargs):
        raise NotImplementedError

    def check_folder_exists(self, *args, **kwargs):
        raise NotImplementedError    

    def create_folder(self, *args, **kwargs):
        raise NotImplementedError
    
    def list_folders(self, *args, **kwargs):
        raise NotImplementedError

    def delete_folders(self, *args, **kwargs):
        raise NotImplementedError
    
    def delete_one_folder(self, *args, **kwargs):
        raise NotImplementedError