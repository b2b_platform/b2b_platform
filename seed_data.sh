#! /bin/bash

export PYTHONPATH=.:$PYTHONPATH

for seed_file in tools/reset_db.py tools/seed_categories.py tools/seed_trade60_data.py tkdata/seed_to_db.py tools/patch_membership.py; do
   if [ -f ${seed_file} ] ; then
	echo 'Seeding '${seed_file} && python ${seed_file}
   fi
done;
