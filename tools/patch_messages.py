import database.models
from database import Database
import settings
import database.models as models

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

# patch private message thread
threads = db.thread.filter_by(thread_type='private_thread')

for thread in threads:
    thread.description = f'Private message thread between {thread.creator.name} and {thread.target_user.name}'
    
db.commit()