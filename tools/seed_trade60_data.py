import database.models as models
from database import Database
import settings
import os
import secrets
import string
from role import role_constants
import sys

conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

basedir = os.path.dirname(os.path.realpath(__file__))
users_file = open(os.path.abspath(os.path.join(basedir, '..', 'trade60_users.txt')), 'w')

def gen_pw(plen=15):    	 
    alphabet = string.ascii_letters + string.digits
    pw = ''.join(secrets.choice(alphabet) for i in range(plen))
    return pw

cat_id = db.product_category.filter_by_q(name='Business Services').first().id

base_user_info = {
    'description': 'Trade60 Support Team',
    'account_type': 'Trade60 Support Team',
    'is_company_verified': True,
    'is_account_confirmed': True,
    'social_accounts': {
        'facebook': 'https://www.facebook.com/trade60/',
        'twitter': 'https://twitter.com/trade60com'
    },
    'country': 'United States',
    'location': 'Boston, MA',
    'website': 'https://www.trade60.com'    
}

company_info = {
    'name': 'Trade60',
    'company_types': 'Internet Company',
    'keywords': ['trade60','internet','b2b','ecommerce','global trade'],
    'company_short_hand': 'trade60',
    'location': '100 Morrissey Boulevard, Boston, MA',
    'country': 'United States',
    'main_category_id': cat_id,
    'is_verified': True,
    'is_public': True,
    'is_claimed': True,
    'website': 'https://www.trade60.com',
    'company_info': '''
        <p>
        <B>Trade60</B> is a global B2B platform that facilitates trades across the globe. Buyers can browse through millions of products from our trusted partners worldwide, communicate seamlessly with instant and easy access to all business information, and research as well as conduct business reviews. Sellers get daily alerts of new buying requests and chat with buyers instantly. We believe that communication is the number one key in cross-border ecommerce that decides a successful, trusted and long lasting business relationship.
        </p>
        <br>
        <p>
            <B>Trade60</B> is founded in 2018 by a team of passionate entrepreneurs and engineers who strive to create the best B2B ecosystem on the planet. We strongly believe that our platform will revolutionize the way cross-border businesses are being conducted online.
        </p>        
    ''',
    'description': 'Trade60 is a Global E-Commerce Platform.',
    'email': 'info@trade60.com',
    'year_founded': 2018,
    'company_sizes': '1-10',
    'social_accounts': {
        'facebook': 'https://www.facebook.com/trade60/',
        'twitter': 'https://twitter.com/trade60com',        
        'AngelList': 'https://angel.co/trade60',
        'f6s': 'https://www.f6s.com/trade60',
        'linkedin': 'https://www.linkedin.com/company/27220638/'
    }
}

company = db.company.get_by_id(1)
if not company:
    company = db.company.add(**company_info)

def create_user(email, first_name, last_name, permission, link_company=True):
    user = db.user.get_by_email(email)
    if user:
        # CHANGE PASSWORD
        new_pw = gen_pw()
        db.user.update_one(user, password=new_pw)
        users_file.write(f'{user.email}|{new_pw}\n')
        return user

    data = {
        'email': email,
        'first_name': first_name,
        'last_name': last_name
    }
    data.update(base_user_info)
    data['password'] = gen_pw()
    data['company_name'] = company.name

    user = db.user.add(**data)
    user_role = db.role.add(
        user_id=user.id,
        operation=role_constants.OPERATION_GENERAL,
        permission=permission
    )

    if link_company:
        user.company_id = company.id
        user_role.company_id = company.id
        db.commit()

    users_file.write(f'{data["email"]}|{data["password"]}\n')
    return user


admin = create_user('admin@trade60.com', 'Trade60', 'Admin', permission=role_constants.PERMISSION_ROLE_MASTER)
support = create_user('support@trade60.com', 'Trade60', 'Support', permission=role_constants.PERMISSION_ROLE_SUPPORT)
bot = create_user('msg_support@trade60.com', 'Trade60', 'Support', permission=role_constants.PERMISSION_ROLE_SUPPORT, link_company=False)

cuong = create_user('cuongle@trade60.com', 'Cuong', 'Le', permission=role_constants.PERMISSION_ROLE_MASTER)
hoang = create_user('hoangvo@trade60.com', 'Hoang', 'Vo', permission=role_constants.PERMISSION_ROLE_MASTER)
ha = create_user('kellytran@trade60.com', 'Kelly', 'Tran', permission=role_constants.PERMISSION_ROLE_MASTER)

users_file.close()