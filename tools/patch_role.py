import database.models
from database import Database
import settings
import alembic.config

from datetime import datetime, timedelta

import database.models as models
from web.core import constants
from membership import membership_constants
from role import role_constants

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

for user in db.user.query().all():
	if user.company_id:
		if not db.role.filter_by(user_id=user.id,company_id=user.company_id):
			db.role.add(user_id=user.id,company_id=user.company_id,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_ADMIN)
		else:
			print ('has role', user.first_name, ' ', user.last_name, ' ', user.id)