import database.models

from database import Database
import settings
import os
from role import role_constants
import random

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)


# Requires creation of db first
#db.drop_all()
#db.create_all()


# Insert entries

import database.models as models

import random
import string

def getrandtxt(textlength=15):
    return "".join( [random.choice(string.ascii_letters) for i in range(textlength)] )

def getrandomemail(textlength=10):
    return "".join( (getrandtxt(), '@b2b.com') )

# Insert companies

comp1 = models.Company(name='First Company',location='India')
comp2 = models.Company(name='Second Company',location='USA')
comp3 = models.Company(name='Third Company',location='USA')
comp4 = models.Company(name='Fourth Company',location='USA')

db.session.add(comp1)
db.session.add(comp2)
db.session.add(comp3)
db.session.add(comp4)
db.session.commit()

# Insert fixed User with passwords
user0 = models.User(email='admin@b2b.com',first_name='Admin',last_name='Smart',password='user0pass',location='USA',is_hidden=True,is_account_confirmed=True)
user1 = models.User(email='user1@b2b.com',first_name='John',last_name='Smith',password='user1pass',location='USA',company_id=comp1.id)
user2 = models.User(email='user2@b2b.com',first_name='John',last_name='Doe',password='user2pass',location='Japan',company_id=comp1.id)
user3 = models.User(email='user3@b2b.com',first_name='Anna',last_name='Smith',password='user3pass',location='Canada')
user4 = models.User(email='msg_support@trade60.com',first_name='Support',last_name='Staff',password='user0pass',location='USA',is_hidden=True,is_account_confirmed=True)


# use setter?
db.session.add(user0)
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)
db.session.add(user4)

db.session.commit()

firstnames = ['John', 'Anna', 'Michael', 'Joseph']
lastnames = ['Fisher', 'Doe', 'Smith', 'LaCruz']
# Insert random User with random pwds
for i in range(0, 10):
    useri = models.User(first_name=random.choice(firstnames),last_name=random.choice(lastnames),
            email=getrandomemail(),password='samepass',location=getrandtxt())
    db.session.add(useri)

db.session.commit()
# Insert product categories
# Static product categories 2 layers
# seed_categories.py
exec(open("./tools/seed_categories.py").read())


product1 = models.Product(name='Product 1', description=getrandtxt(40), company_id=comp1.id, product_category_id=2)
product2 = models.Product(name='Product 2', description=getrandtxt(40), company_id=comp1.id, product_category_id=3)
product3 = models.Product(name='Product 3', description=getrandtxt(40), company_id=comp2.id, product_category_id=4)
product4 = models.Product(name='Product 4', description=getrandtxt(40), company_id=comp2.id, product_category_id=5)
db.session.add(product1)
db.session.add(product2)
db.session.add(product3)
db.session.add(product4)
db.commit()

user0 = db.user.filter_one(email='admin@b2b.com')
user1 = db.user.filter_one(email='user1@b2b.com')
user2 = db.user.filter_one(email='user2@b2b.com')
user4 = db.user.filter_one(email='msg_support@trade60.com')

# Insert role
role_master = models.UserCompanyRole(user_id=user0.id,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_MASTER)
role_user1 = models.UserCompanyRole(user_id=user1.id,company_id=1,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_ADMIN)
role_user2 = models.UserCompanyRole(user_id=user2.id,company_id=1,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_USER)
role_support = models.UserCompanyRole(user_id=user4.id,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_SUPPORT)


# Insert company reports
db.session.add(role_master)
db.session.add(role_user1)
db.session.add(role_user2)
db.session.add(role_support)
db.session.commit()
