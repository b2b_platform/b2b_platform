

import database.models

from database import Database
import settings
import os
from role import role_constants
import random

import database.models as models

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

user_set = db.user.query().all()
for user in user_set:
    if user.company_id: # user has company
        print ('fixing user ', user.id, ' with company ', user.company.name)
        if not db.role.get_position_by_userid(user.id):
            new_role = models.UserCompanyRole(user_id=user.id,company_id=user.company_id,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_USER)
            db.session.add(new_role)
            db.session.commit()
        else:
            print ('already has role inserted')
        

#role_master = models.UserCompanyRole(user_id=user0_id,operation=role_constants.OPERATION_GENERAL,permission=role_constants.PERMISSION_ROLE_MASTER)
#db.session.add(role_master)
#db.session.commit()
