

import database.models

from database import Database
import settings
import os
from role import role_constants
import random
from datetime import datetime

import database.models as models
from web.core import constants

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

#role_set = db.role.query()
#for role in role_set:
#	print(role.to_dict())

#user_set = db.user.query()
#for user in user_set:
#	db.user.update_one(user, is_account_confirmed=True)
#	print(user.id,',',user.company_id,',',user.company_name, ',', user.is_account_confirmed)

#thread = db.thread.get_by_id(4)
#print(thread)

noti1 = models.NotificationUser(notification_type=constants.NOTIFICATION_TYPES[0], user_id=2, text='First notification')
noti2= models.NotificationUser(notification_type=constants.NOTIFICATION_TYPES[0], user_id=2, text='Second notification')
noti3 = models.NotificationUser(notification_type=constants.NOTIFICATION_TYPES[1], user_id=2, text='Third notification')

noti4 = models.NotificationCompany(notification_type=constants.NOTIFICATION_TYPES[1], company_id=1, text='wow comp noti')
noti5 = models.NotificationCompany(notification_type=constants.NOTIFICATION_TYPES[2], company_id=1, text='wow comp noti 2')

db.session.add(noti1)
db.session.add(noti2)
db.session.add(noti3)
db.session.add(noti4)
db.session.add(noti5)

db.session.commit()
noti_set = db.notification.query()
for noti in noti_set:
	print ('noti', noti.to_dict())

dt= datetime.utcnow()
user3_ts = models.NotificationUserReadTime(user_id=3,last_active_time=dt)
db.session.add(user3_ts)

db.session.commit()

#activeuser_set = db.query(models.NotificationUserReadTime).all()
#print ('active',activeuser_set)
#for u in activeuser_set:
#	print ('user_id:', u.user_id, ' time:', u.last_active_time)