import database.models
from database import Database
import settings

from datetime import datetime, timedelta

import database.models as models
from web.core import constants
from membership import membership_constants
from role import role_constants

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

companies = db.company.query().all()
for company in companies:
	print ('company ', company.name)
	if not company.is_public:
		company.update(is_public=True)
		db.session.commit()

for user in db.user.query().all():
	if user.company_id:
		if not db.role.filter_by(user_id=user.id,company_id=user.company_id):
			db.role.add(
				user_id=user.id,
				company_id=user.company_id,
				operation=role_constants.OPERATION_GENERAL,
				permission=role_constants.PERMISSION_ROLE_ADMIN)