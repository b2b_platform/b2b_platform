import database.models
from database import Database
import settings
import alembic.config

from datetime import datetime, timedelta

import database.models as models
from web.core import constants
from membership import membership_constants

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

companies = db.company.query().all()
for company in companies:
	print('processing', company.name)
	if not company.current_membership_id:
		mem = db.membership.add_basic_membership(company.id)
		print ('adding membership')
		

dtnow = datetime.utcnow()
dtend = dtnow + timedelta(days=3650)

trade60_id = db.company.filter_one(name='Trade60').id

#print (db.company.get_by_id(1).to_dict())
db.membership.add_new_membership_by_company(trade60_id, tier=membership_constants.TIER_3,
	membership_start=dtnow, membership_end=dtend)