import database.models
from database import Database
import settings
import alembic.config

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

db.hard_reset()
db.commit()

alembic.config.main(argv=[
    '--raiseerr',
    'upgrade', 'head'
])