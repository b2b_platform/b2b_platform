import pickle
import zlib

def load_data(filename):
    with open(filename, 'rb') as f:     
        data = pickle.loads(zlib.decompress(f.read()))        

    return data