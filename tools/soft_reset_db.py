import database.models
from database import Database
import settings

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

db.metadata.create_all()
db.commit()
