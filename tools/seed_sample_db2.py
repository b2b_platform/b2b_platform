
import database.models

from database import Database
import settings

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

# not doing this

# Insert entries

import database.models as models

import random
import string



#temp = {'name': 'Random', 'company_short_hand': 'radfs', 'company_types': 'Emory University', 'location': 'United States', 'country': '', 'website': '', 'description': '', 'company_info': '', 'phone': '', 'fax': '', 'email': '', 'year_founded': '', 'capacity': '', 'shipping_handling': '', 'port': '', 'payment': ''}
#temp =  {'name': 'Electric motor', 'description': 'Energy'}

#prod1 = models.Product(**temp)
#db.session.add(prod1)
parent1 = models.ProductCategory(name='Parent 1')
db.session.add(parent1)

parent2 = models.ProductCategory(name='Parent 2')
db.session.add(parent2)

parent3 = models.ProductCategory(name='Parent 3')
db.session.add(parent3)

parent4 = models.ProductCategory(name='Parent 4')
db.session.add(parent4)



db.session.commit()


child1 = models.ProductCategory(name='Child 1', parent_category_id=parent2.id)
child2 = models.ProductCategory(name='Child 2', parent_category_id=parent2.id)
db.session.add(child1)
db.session.add(child2)


db.session.commit()

grandchild1 = models.ProductCategory(name='Grandchild 1', parent_category_id=child1.id)
grandchild2 = models.ProductCategory(name='Grandchild 2', parent_category_id=child1.id)
grandchild3 = models.ProductCategory(name='Grandchild 3', parent_category_id=child1.id)
db.session.add(grandchild1)
db.session.add(grandchild2)
db.session.add(grandchild3)

db.session.commit()
