import database.models
from database import Database
import settings

conf = settings.load("db")
db = Database(conf["database_uri"], database.models.metadata)

db.drop_all()
db.drop_all_tables_and_sequences(force=True)
db.commit()