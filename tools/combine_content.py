
#! /usr/bin/python
import sys
import math
import random
import re
from web.core import constants

def main():
    if len(sys.argv) < 2:
        print ('error missing args, run: ', sys.argv[0], ' [language]')
        exit()

    content = ''
    start = False
    
    language = sys.argv[1]
    
    translation_dir =  'web/translations/' + language
    
    output_file = translation_dir + '/LC_MESSAGES/messages.po'
    
    input_file = 'web/messages.pot'
    input_file_translated = translation_dir + '/LC_MESSAGES/extracted_msg.txt'
    
    content = ''
    start = False;  
    
    fin = open(input_file, 'r')
    ftrans = open(input_file_translated, 'r')
    
    fout = open(output_file, 'w')

    for line in fin:
        line = line.rstrip('\r\n')
        
        #if len(line) >= 6 and line[0:6] == 'msgstr':
        if line.startswith('msgstr'):
            # Output content
            line2 = ftrans.readline().rstrip('\r\n')#.strip()
            #print ('msgstr "' + line2 + '"')
            fout.write('msgstr "' + line2 + '"\n')
           
           # print ('end of msgstr', total, ' ', content)
            start = False

        else:
            fout.write(line + '\n')

    fin.close()
    ftrans.close()
    
    fout.flush()
    fout.close()


if __name__ == '__main__':
    main()

