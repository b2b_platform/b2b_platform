import io
from PIL import Image
import os
import sys
from slugify import slugify

out_folder = folder = sys.argv[1]
if len(sys.argv) > 2:
    out_folder = sys.argv[2]

files = os.listdir(folder)
w = 986
for fn in files:
    try:
        *name_items, ext = fn.split('.')
        img_name = slugify('.'.join(name_items))

        with open(os.path.join(folder, fn), 'rb') as f:
            img_data = io.BytesIO(f.read())
        print(os.path.join(folder, fn))
        img = Image.open(img_data)
        print('RESIZING', fn, 'SIZES:', img.size[0], 'x', img.size[1])    
        ratio = w/float(img.size[0])
        h = int(float(img.size[1]) * float(ratio))
        print('RESIZED SIZE', w, 'x', h)
        new_img = img.resize((w,h), Image.ANTIALIAS)       
        new_path = os.path.join(out_folder, f'{img_name}.jpg')
        print('SAVING', new_path)
        new_img.save(new_path, 'JPEG', quality=91, optimize=True, progressive=True)
    except:
        print('ERROR. SKIPPING', fn)
