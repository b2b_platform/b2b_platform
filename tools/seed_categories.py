import database.models as models
from database import Database
import settings
from tools.categories import root as croot


conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

# Insert entries

import random
import string

def insert(pid, node):
    cat = None

    if node.name != 'root':
        cat = models.ProductCategory(name=node.name, parent_category_id=pid)
        db.session.add(cat)
        db.session.flush()
   
    pid = None if not cat else cat.id

    for cname, child in node.children.items():
        insert(pid, child)

    db.session.flush()

insert(None, croot)
db.commit()
