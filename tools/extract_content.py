
#! /usr/bin/python

import sys
import math
import random
import re
from web.core import constants

def main():
    if len(sys.argv) < 2:
        print ('error missing args, run: ', sys.argv[0], ' [language] ')
        exit()

    content = ''
    start = False;
    
    language = sys.argv[1]
    
    translation_dir =  'web/translations/' + language
    
    input_file = translation_dir + '/LC_MESSAGES/messages.po'
    
    output_file = translation_dir + '/LC_MESSAGES/extracted_msg.txt'
    if sys.argv == 3:
        output_file = sys.argv[2]

    #if not os.path.exists(output_file):
    #  exit()
    #  os.makedirs(directory)

    print ('opening ', input_file)
    print ('opening ', output_file)
    
    fin = open(input_file, 'r')
    
    fout = open(output_file, 'w')
    for line in fin:
    #for line in sys.stdin:
          line = line.rstrip('\r\n')
          
          #line = line.rstrip('\r\n')
          
          #if len(line) >= 6 and line[0:6] == 'msgstr':
          if line.startswith('msgstr'):
              # Output content
              #print (content)
              fout.write(content + '\n')
             ## print ('end of msgstr', total, ' ', content)
              start = False
  
          #sp = line.strip().split(delimiter)
          #if len(line) >= 5 and line[0:5] == 'msgid':
          elif line.startswith('msgid'):
              # Resetting
              start = True
              #arr = re.split(' ',str)
              arr = line.split('"')
              #print(arr[1].replace('"', ''))
              content = arr[1]
  
              #read further
          
          elif len(line) > 2 and start :# and line.startswith('"') and line.endswith('"'):
              new_content = line.strip('"')
              content = content + '' + new_content
  
          else:
              pass
    fin.close()
    fout.close()

if __name__ == '__main__':
    main()

