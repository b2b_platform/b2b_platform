import database.models as models
from database import Database
import settings

conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

