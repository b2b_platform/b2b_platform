#! /bin/bash

#./setup_settings.sh
source prod.env
gunicorn web.main:app -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker" --worker-connections 1000 -w 2 -n b2b_platform --log-file logs/gunicorn.log -D
