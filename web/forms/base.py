from cerberus import Validator

class BaseValidator(Validator):
    
    def __init__(self, name, *args, **kwargs):
        if not name:
            raise RuntimeError('name for validator is required')

        self.name = name
        super(BaseValidator, self).__init__(*args, allow_unknown=True, **kwargs)
