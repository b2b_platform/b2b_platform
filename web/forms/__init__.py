#from html_sanitizer import Sanitizer, sanitizer
import bleach
from .auth import *
from .rfq import *
from .company import *
from .product import *
from web.core.constants import MAX_UPLOAD_SIZE

cleaner = bleach.Cleaner(
    tags = [
        'a', 'h1', 'h2', 'h3', 'strong', 'em', 'p', 'ul', 'ol',
        'li', 'br', 'sub', 'sup', 'hr', 'img', 'table', 'thead', 'tbody', 'tr', 'th', 'td',
        'div'
    ],
    attributes = {
        'a': ['href', 'name', 'target', 'title', 'id'], 
        'img': ['src'],
        'div': ['style'],
        'table': ['style']
    },
    strip=True,
    strip_comments=True,
    styles=[]
)

class FormChecker(object):

    def __init__(self, app=None):
        self.app = app
        self.forms = {}
        self._register_forms()        

    def init_app(self, app):
        if not self.app:
            self.app = app

    @staticmethod
    def _error_msg(errors):
        return ' '.join(['{}({})'.format(field, ', '.join(msgs)) for field, msgs in errors.items()])

    def get_schema(self, name):
        try:
            return self.forms[name].schema
        except KeyError:
            return None

    def clean_dict(self, data):
        output = {}
        for k,v in data.items():
            try:
                output[k] = cleaner.clean(v)
            except:
                output[k] = v
    
        return output

    def clean_list(self, seq, idfun=None): 

        idfun = idfun if idfun is not None else lambda x: x

        seen = {}
        result = []
        for item in seq:
            tmp = None if item in ('', 'None', 'null', 'none') else item

            try:                
                #tmp = self._sanitizer.sanitize(tmp)
                tmp = cleaner.clean(tmp)                
            except:                
                pass

            marker = idfun(tmp)
            if marker in seen: continue
            seen[marker] = 1
            result.append(tmp)

        return result

    def serialize_request(self, request, empty_string=False):
        data = {}
        try:
            keys = list(request.values.keys())

            for k in keys:
                '''items = list(filter(lambda x: x is not None and (x != '' or empty_string), 
                            request.values.getlist(k)))'''

                # when can this fail????
                items = self.clean_list(request.values.getlist(k))
                data[k] = items
                if items:
                    data[k] = items if len(items) > 1 else items[0]

                if k.startswith('social_accounts'):                    
                    newk = k.replace('social_accounts_', '')
                    try:
                        data['social_accounts'][newk] = data[k]
                    except KeyError:
                        data['social_accounts'] = { newk: data[k] }

                    del data[k]
                        
            if 'keywords' in data:
                data['keywords'] = [] if not data['keywords'] else  [x.strip() for x in data['keywords'].split(',')]                            

            if 'odk' in keys and 'odv' in keys:
                try:                
                    data['other_details'] = dict(zip(filter(None, data['odk']), 
                                                    filter(None, data['odv'])))

                    del data['odk']
                    del data['odv']

                except (KeyError, TypeError):
                    data['other_details'] = {}            

            
        except (AttributeError, TypeError, ValueError):                   
            pass
        #print(data)
        return data
    
    def process(self, request, name=None, **kwargs):
        data = self.serialize_request(request, **kwargs)

        if name and name in self.forms:
            result = self.forms[name].validate(data)
            if not result:
                raise RuntimeError(self._error_msg(self.forms[name].errors))

        return data

    def _register(self, validators):
        for validator in validators:
            self.forms[validator.name] = validator

    def _register_forms(self):
        self._register([
            rfq_get_form, rfq_create_form, rfq_update_form,
            auth_login_form, auth_register_form,
            company_create_form, company_update_form,
            product_create_form, product_update_form
        ])
