from .base import BaseValidator
import re

email_regex = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

class AuthValidator(BaseValidator):
    def _validate_is_email(self, is_email, field, value):
        if is_email and not email_regex.match(value):
            self._error(field, 'Invalid email address')

    def _validate_match_password(self, match_password, field, value):
        if match_password and not (value == self.document['password']):
            self._error(field, 'Password not match')

auth_login_form = AuthValidator('auth_login_form', {
    'email': {'type': 'string', 'required': True, 'is_email': True},
    'password': {'type': 'string', 'required': True}
})

auth_register_form = AuthValidator('auth_register_form', {
    'email': {'type': 'string', 'required': True, 'is_email': True},
    'password': {'type': 'string', 'required': True},
    'password_again': {'type': 'string', 'required': True, 'match_password': True}
})