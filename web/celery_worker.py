from web import db, create_app
from multiprocessing.util import register_after_fork

register_after_fork(db.engine, db.engine.dispose)

app = create_app()

from web import CLR