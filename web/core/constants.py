from flask_babel import lazy_gettext

from membership import membership_constants as mc

# Enforce by views
COMPANY_MEDIA_LIMIT = 20
PRODUCT_MEDIA_LIMIT = 5
RFQ_MEDIA_LIMIT = 5
QUOTE_MEDIA_LIMIT = 5
MAX_UPLOAD_SIZE = 8 * 1024 * 1024


USER_INTERESTS = ( lazy_gettext(u'Buy'), lazy_gettext(u'Sell'), lazy_gettext(u'Dropship'), lazy_gettext(u'Wholesale'),  lazy_gettext(u'Trade'), lazy_gettext(u'All'))


COMPANY_TYPES = ( lazy_gettext(u'Manufacturer'),  lazy_gettext(u'Trading Company'),  lazy_gettext(u'Buying Office'),  lazy_gettext(u'Agent'),  lazy_gettext(u'Distributor/Wholesaler'),
                  lazy_gettext(u'Government ministry/Bureau/Commission'),  lazy_gettext(u'Association'),  lazy_gettext(u'Business Service (Logistics, Ads, Finance, etc)'),
                  lazy_gettext(u'Other'))
                 
COMPANY_SIZES = ('1-10', '10-100', '100-500', '500-1000', '1000-5000', '5000+')


DELIVERY_TERMS = ('FOB', 'CFR', 'CIF', 'EXW', 'FAS', 'CIP', 'FCA', 'CPT', 'DEQ', 'DDP',
                  'DDU', 'Express Delivery', 'DAF', 'DES')

ACCEPTED_CURRENCIES = ('USD', 'EUR', 'JPY', 'CAD', 'AUD', 'HKD', 'GBP', 'CNY', 'CHF', 'VND', 'INR')

PAYMENT_TYPES = ('T/T', 'L/C', 'D/P D/A', 'MoneyGram', 'Credit Card', 'Paypal',' Western Union',
                 'Cash', 'Escrow')

COMPANY_LANGUAGES = (lazy_gettext(u'English'), lazy_gettext(u'Chinese'), lazy_gettext(u'Vietnamese'), lazy_gettext(u'Spanish'), lazy_gettext(u'Japanese'), lazy_gettext(u'Korean'),
                     lazy_gettext(u'Arabic'), lazy_gettext(u'German'), lazy_gettext(u'French'), lazy_gettext(u'Hindi'), lazy_gettext(u'Italian'), lazy_gettext(u'Russian'), lazy_gettext(u'Portuguese'))

MARKETS = ( lazy_gettext(u'North America'), lazy_gettext(u'South America'), lazy_gettext(u'Eastern Europe'), lazy_gettext(u'Southeast Asia'), lazy_gettext(u'Africa'),
           lazy_gettext(u'Middle East'), lazy_gettext(u'Oceania'), lazy_gettext(u'Eastern Asia'), lazy_gettext(u'Western Europe'), lazy_gettext(u'Central America'),
           lazy_gettext(u'Northern Europe'), lazy_gettext(u'Southern Europe'), lazy_gettext(u'South Asia'), lazy_gettext(u'Domestic'))

EXPORT_PCT = ['{}%-{}%'.format(l,l+9) for l in range(1,100,10)]

MEASURE_UNITS = (
    'Acre','Ampere','Bag','Barrel','Blade','Box','Bushel','Carat','Carton','Case','Centimeter','Chain','Combo','Cubic Centimeter','Cubic Foot','Cubic Inch','Cubic Meter','Cubic Yard'
    ,'Degrees Celsius','Degrees Fahrenheit','Dozen','Dram','Fluid Ounce','Foot','Forty-Foot Container ','Furlong','Gallon','Gill','Grain','Gram','Gross','Hectare','Hertz','Inch'
    ,'Kiloampere','Kilogram','Kilohertz','Kilometer','Kiloohm','Kilovolt','Kilowatt','Liter','Long Ton','Megahertz','Meter','Metric Ton','Mile','Milliampere'
    ,'Milligram','Millihertz','Milliliter','Millimeter','Milliohm','Millivolt','Milliwatt','Nautical Mile','Ohm','Ounce','Pack','Pair','Pallet','Parcel','Perch'
    ,'Piece','Pint','Plant','Pole','Pound','Quart','Quarter','Rod','Roll','Set','Sheet','Short Ton','Square Centimeter','Square Foot','Square Inch','Square Meter'
    ,'Square Mile','Square Yard','Stone','Strand','Ton','Tonne','Tray'
    ,'Twenty-Foot Container' ,'Unit' ,'Volt','Watt', 'Wp' ,'Yard')

SORT_ORDERS_PRODUCTS = (lazy_gettext(u'Best Matching'), lazy_gettext(u'Most Recent First'), lazy_gettext(u'Product A to Z'), lazy_gettext(u'Unit Price - Low to High'), lazy_gettext(u'Unit Price - High to Low'))
SORT_ORDERS_COMPANIES = (lazy_gettext(u'Date Added'), lazy_gettext(u'Best Matching'), lazy_gettext(u'Company A to Z'), lazy_gettext(u'Company Z to A'))

EXTERNAL_SITES = ('Alibaba', 'Tradekey', 'Other')


REVENUES_LEVELS = [
    {'text' : lazy_gettext(u'Below US$ 1M'), 'min': 0, 'max': 1000000 },
    {'text' : lazy_gettext(u'US$ 1-2.5M'), 'min': 1000000, 'max': 2500000},
    {'text' : lazy_gettext(u'US$ 2.5-5M'), 'min': 2500000, 'max': 500000},   
    {'text' : lazy_gettext(u'US$ 5-10M'), 'min': 5000000, 'max': 10000000 },
    {'text' : lazy_gettext(u'US$ 10-50M'), 'min': 10000000, 'max': 50000000 },
    {'text' : lazy_gettext(u'US$ 50-100M'), 'min': 50000000, 'max': 100000000 },
    {'text' : lazy_gettext(u'Above $US 100M'), 'min': 100000000, 'max': -1 },
]

PACKAGING_TYPES = (lazy_gettext(u'Primary'), lazy_gettext(u'Secondary'), lazy_gettext(u'Transit'), lazy_gettext(u'Custom'))

MEMBERSHIP_TYPES = (lazy_gettext(u'Basic'), lazy_gettext(u'Bronze'), lazy_gettext(u'Silver'), lazy_gettext(u'Gold'),)
MEMBERSHIP_TYPES_DICT = {lazy_gettext(u'Basic') : mc.TIER_BASIC, lazy_gettext(u'Bronze') : mc.TIER_1, lazy_gettext(u'Silver'): mc.TIER_2, lazy_gettext(u'Gold') : mc.TIER_3}


revenues_list = [rev['text'] for rev in REVENUES_LEVELS]

list_checkboxes_features = [
                            [{'label' : lazy_gettext(u'Packaging type'), 'name': 'packaging', 'options': PACKAGING_TYPES},
                             {'label' : lazy_gettext(u'Unit type'), 'name': 'unit_type', 'options': MEASURE_UNITS}],

                            [{'label' : lazy_gettext(u'Company size') , 'name': 'company_size', 'options': COMPANY_SIZES},
                             {'label' : lazy_gettext(u'Company type'), 'name': 'company_types', 'options': COMPANY_TYPES},
                             {'label' : lazy_gettext(u'Membership'), 'name': 'membership', 'options': MEMBERSHIP_TYPES}
                             ]

    # Add is_verified company check
                            ]

list_checkboxes_jsonb_features = [
                            [{'label' : lazy_gettext(u'Payment option') , 'name': 'payment_options', 'options': PAYMENT_TYPES}],
                            []
                            ]
                                
list_numeric_features = [[{'label': lazy_gettext(u'Capacity'), 'name': 'capacity', 'unit': 'pcs/months', 'separate' : False}, 
                            {'label': lazy_gettext(u'Minimum order quantity'), 'name': 'min_order_quantity', 'unit' : 'pcs', 'separate' : False },
                            {'label': lazy_gettext(u'Price range'), 'name': 'price_range', 'unit' : 'dollars', 'separate' : True }],
                            []
                            ]

list_company_media_attributes = ['banner', 'background', 'logo']


SUPPORT_TICKET_TYPES = [
    lazy_gettext(u'General Use'),
    lazy_gettext(u'Billing & Sales'),
    lazy_gettext(u'Trade Services'),
    lazy_gettext(u'Subscription/Membership'),
    lazy_gettext(u'Technical Issues and Errors'),
    lazy_gettext(u'Other')
]

SUPPORT_TICKET_TYPES_NONLOGIN = [
    lazy_gettext(u'Account Access'),
    lazy_gettext(u'Company Report'),
    lazy_gettext(u'Technical Issues and Errors'),
    lazy_gettext(u'Other'),
]

REPORT_TYPES = (
    lazy_gettext(u'Scam'),
    lazy_gettext(u'Outdated Information'),
    lazy_gettext(u'Closed Business'),
    lazy_gettext(u'Invalid Information'),
    lazy_gettext(u'Offensive Information'),
    lazy_gettext(u'Illegal business'),
    lazy_gettext(u'Other')
)

# Country/flag code : (Country name, Language code)
LANGUAGES = { "gb": (lazy_gettext(u'English'), 'en'), 
            "us": (lazy_gettext(u'English (United States)'), 'en'), 
            "fr" : (lazy_gettext(u'French'), 'fr'), 
            "de" : (lazy_gettext(u'German'), 'de'), 
            "it" : (lazy_gettext(u'Italian'), 'it'), 
            "jp" : (lazy_gettext(u'Japanese'), 'ja'), 
            "vn" : (lazy_gettext(u'Vietnamese'), 'vi'), 
            "kr" : (lazy_gettext(u'Korean'), 'ko'), 
            "th" : (lazy_gettext(u'Thai'), 'th'), 
            "es" : (lazy_gettext(u'Spanish'), 'es'), 
            "id" : (lazy_gettext(u'Indonesian'), 'id'), 
            "la" : (lazy_gettext(u'Laos'), 'lo'), 
            "my" : (lazy_gettext(u'Malay'), 'ms'), 
            "ru" : (lazy_gettext(u'Russian'), 'ru'), 
            "cz" : (lazy_gettext(u'Czech'), 'cs'), 
            "cn" : (lazy_gettext(u'Chinese'), 'zh')} 

SUPPORT_LOCALES = set( val[1] for key, val in LANGUAGES.items() )
SUPPORT_LANGUAGES = set( val[0] for key, val in LANGUAGES.items() )

LANG_CODE_PAIRS = [ val for key, val in LANGUAGES.items() ]

LANG_COUNTRY_DICT =  {val[1]:key for key, val in LANGUAGES.items()}

SUPPORT_TICKET_STATUSES = [
    'open',
    'resolving',
    'closed'
]

COMPANY_SOCIAL_APPS = [
    ('whatsapp', 'Whatsapp'),
    ('viber', 'Viber'), 
    ('wechat', 'Wechat')
]

USER_SOCIAL_APPS = [
    ('whatsapp', 'Whatsapp'),
    ('viber', 'Viber'), 
    ('wechat', 'Wechat'),
    ('facebook', 'Facebook'),
    ('twitter', 'Twitter')
]


user_activity_types = (
    'VIEW_COMPANY',    
    'UPDATE_COMPANY',
    'CREATE_COMPANY',
    'LINK_COMPANY',
    'LEAVE_COMPANY',
    'VIEW_PRODUCT',
    'UPDATE_PRODUCT',
    'CREATE_PRODUCT',
    'VIEW_RFQ',
    'CREATE_RFQ',
    'DELETE_RFQ',
    'VIEW_QUOTE',
    'CREATE_QUOTE',
    'DELETE_QUOTE',
    'INQUIRE_PRODUCT',
    'CREATE_PRIVATE_THREAD',
    'CREATE_GENERIC_THREAD',
    'CREATE_SUPPORT_TICKET',
    'VIEW_SUPPORT_TICKET',
    'UPDATE_SUPPORT_TICKET',
    'UPDATE_ACCOUNT',
    'VIEW_ACCOUNT',
    'SEARCH_PRODUCTS'
) 

NOTIFICATION_TYPES = ['product_inquiry', 
                    'rfq', 
                    'quote', 
                    'system', 
                    'account_activity',
                    'other_company', 
                    'other_personal',
                    'trade_lead_inquiry'
                    ]

MEMBERSHIP_TIERS = [
    mc.TIER_BASIC,
    mc.TIER_1,
    mc.TIER_2,
    mc.TIER_3
]

TIER_NAME_DICT = {
    mc.TIER_BASIC : lazy_gettext(u'Basic'),
    mc.TIER_1 : lazy_gettext(u'Bronze'),
    mc.TIER_2 : lazy_gettext(u'Silver'),
    mc.TIER_3 : lazy_gettext(u'Gold'),
}

TIER_TEXT_DICT = {
    mc.TIER_BASIC : lazy_gettext(u'Basic Member'),
    mc.TIER_1 : lazy_gettext(u'Bronze Member'),
    mc.TIER_2 : lazy_gettext(u'Silver Member'),
    mc.TIER_3 : lazy_gettext(u'Gold Member'),
}

TIER_CSSCLASS_DICT = {
    mc.TIER_BASIC : 'tier-basic',
    mc.TIER_1 : 'tier-1',
    mc.TIER_2 : 'tier-2',
    mc.TIER_3 : 'tier-3',
}

LIMIT_TEXT_DICT = {
    mc.LIMIT_NUMBER_USERS: lazy_gettext(u'Number of users'),
    mc.LIMIT_NUMBER_MEDIA: lazy_gettext(u'Number of media files'),
   # mc.LIMIT_SIZE_FILE: lazy_gettext(u'File size limit'), # Not enforced
    mc.LIMIT_NUMBER_FILES: lazy_gettext(u'Number of files'),
    mc.LIMIT_MEDIA_PER_THREAD: lazy_gettext(u'Number of media files per thread'),
    mc.LIMIT_NUMBER_PRODUCTS: lazy_gettext(u'Number of products'),
    mc.LIMIT_NUMBER_MEDIA_PER_PRODUCT: lazy_gettext(u'Number of photos or media files per product inquiry/quote/RFQ'),
    mc.LIMIT_NUMBER_RFQS: lazy_gettext(u'Number of RFQs'),
    mc.LIMIT_NUMBER_QUOTES: lazy_gettext(u'Number of quotes'),
    mc.LIMIT_NUMBER_TRADE_LEADS: lazy_gettext(u'Number of trade leads'),
    mc.LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS: lazy_gettext(u'Number of photos or media files per trade lead'),
}

TIER_DESCRIPTIONS = {
    mc.TIER_BASIC: {
        'price': '0',
        'price_note': lazy_gettext(u'per company per month'),
        'features': [
            lazy_gettext(u'Restrictive data quota'),
            lazy_gettext(u'Limited customer service'),
            lazy_gettext(u'Restricted to 2 users'),
        ]
    },
    mc.TIER_1: {
        'price': '19',
        'price_note': lazy_gettext(u'per company per month billed annually'),
        'features': [
            lazy_gettext(u'Limited data quota'),
            lazy_gettext(u'24/7 customer service'),
            lazy_gettext(u'Restricted to 2 users (plus $5/mo each additional user)'),
            lazy_gettext(u'Customizable company page'),
        ]
    },
    mc.TIER_2: {
        'price': '99',
        'price_note': lazy_gettext('per company per month billed annually'),        
        'features': [
            lazy_gettext(u'Generous data quota'),
            lazy_gettext(u'24/7 customer service'),
            lazy_gettext(u'Restricted to 10 users (plus $5/mo each additional user)'),
            lazy_gettext(u'Customizable company page'),
        ]
    },
    mc.TIER_3: {
        'price': '199',
        'price_note': lazy_gettext('per company per month billed annually'),
        'offer': lazy_gettext('Verify your company now for first-month FREE'),
        'features': [
            lazy_gettext(u'Virtually no data restriction'),
            lazy_gettext(u'24/7 and prioritized customer services'),
            lazy_gettext(u'Unlimited number of users'),
            lazy_gettext(u'Customizable company page'),
            lazy_gettext(u'Automated buyer matching'),
            lazy_gettext(u'Seller protection'),            
            lazy_gettext(u'Instant access to promotions from our partners'),
        ]
    }
}

SETTINGS_PRIVACY_KEYS = {
    'privacy_hide_account': bool,
    'privacy_email_public': bool,
    'privacy_social_accounts_public': bool,
    'privacy_blocked_users': list
}
SETTINGS_SITE_KEYS = {
    'site_language': str,
    'noti_interval': int,
    'noti_types_accepted': list
}

NOTIFICATION_DURATION_DICT = {
    lazy_gettext(u'12 Hours') : 12,
    lazy_gettext(u'24 Hours') : 24,
    lazy_gettext(u'7 Days') : 168,
    lazy_gettext(u'30 Days') : 720,
    lazy_gettext(u'Never') : -1,
}

TRADE_LEAD_DURATION_DAYS = 30

LEAD_TYPE_BUY = 0
LEAD_TYPE_SELL = 1
LEAD_TYPE_OTHER = -1

LEAD_TYPE_DICT = {
    lazy_gettext(u'Buy lead'): LEAD_TYPE_BUY,
    lazy_gettext(u'Sell lead'): LEAD_TYPE_SELL,
    lazy_gettext(u'Unspecified'): LEAD_TYPE_OTHER
}

REFERENCE_TYPE_COMPANY = 0 
REFERENCE_TYPE_PRODUCT = 1
REFERENCE_TYPE_OTHER = -1

REFERENCE_TYPE_DICT = {
    lazy_gettext(u'Company'): REFERENCE_TYPE_COMPANY,
    lazy_gettext(u'Product'): REFERENCE_TYPE_PRODUCT,
    lazy_gettext(u'Other'): REFERENCE_TYPE_OTHER
}