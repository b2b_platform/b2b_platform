

import fileinput

region_set = {}

for line in fileinput.input():
    data = line.split("\t")
    region = data[1].strip()
    country = data[0].strip()
   
    if region not in region_set:
        #print region
        region_set[region] = []
    region_set[region].append(country)


count = 0

def to_dict(name, children=None):
    global count
    count = count + 1
    if not children: # country
        return  {'name' : name, 'id' : count}
    #print (child for child in children)
    # region/parent
    return {'name' : name, 'id' : count, 'children' : { child : to_dict(child) for child in children } }

result_set = {}
count = 0
for region in region_set:
    count += 1
    result_set[region] = to_dict(region, children=region_set[region])

print(result_set)
#print(idx)
