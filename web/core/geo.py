countries = [
  {
    "code": "AF",
    "name": "Afghanistan"
  },
  {
    "code": "AX",
    "name": "Åland Islands"
  },
  {
    "code": "AL",
    "name": "Albania"
  },
  {
    "code": "DZ",
    "name": "Algeria"
  },
  {
    "code": "AS",
    "name": "American Samoa"
  },
  {
    "code": "AD",
    "name": "Andorra"
  },
  {
    "code": "AO",
    "name": "Angola"
  },
  {
    "code": "AI",
    "name": "Anguilla"
  },
  {
    "code": "AQ",
    "name": "Antarctica"
  },
  {
    "code": "AG",
    "name": "Antigua and Barbuda"
  },
  {
    "code": "AR",
    "name": "Argentina"
  },
  {
    "code": "AM",
    "name": "Armenia"
  },
  {
    "code": "AW",
    "name": "Aruba"
  },
  {
    "code": "AU",
    "name": "Australia"
  },
  {
    "code": "AT",
    "name": "Austria"
  },
  {
    "code": "AZ",
    "name": "Azerbaijan"
  },
  {
    "code": "BS",
    "name": "Bahamas"
  },
  {
    "code": "BH",
    "name": "Bahrain"
  },
  {
    "code": "BD",
    "name": "Bangladesh"
  },
  {
    "code": "BB",
    "name": "Barbados"
  },
  {
    "code": "BY",
    "name": "Belarus"
  },
  {
    "code": "BE",
    "name": "Belgium"
  },
  {
    "code": "BZ",
    "name": "Belize"
  },
  {
    "code": "BJ",
    "name": "Benin"
  },
  {
    "code": "BM",
    "name": "Bermuda"
  },
  {
    "code": "BT",
    "name": "Bhutan"
  },
  {
    "code": "BO",
    "name": "Plurinational State of Bolivia"
  },
  {
    "code": "BO",
    "name": "Bolivia"
  },  
  {
    "code": "BQ",
    "name": "Sint Eustatius and Saba Bonaire"
  },
  {
    "code": "BA",
    "name": "Bosnia and Herzegovina"
  },
  {
    "code": "BW",
    "name": "Botswana"
  },
  {
    "code": "BV",
    "name": "Bouvet Island"
  },
  {
    "code": "BR",
    "name": "Brazil"
  },
  {
    "code": "IO",
    "name": "British Indian Ocean Territory"
  },
  {
    "code": "BN",
    "name": "Brunei Darussalam"
  },
  {
    "code": "BG",
    "name": "Bulgaria"
  },
  {
    "code": "BF",
    "name": "Burkina Faso"
  },
  {
    "code": "BI",
    "name": "Burundi"
  },
  {
    "code": "KH",
    "name": "Cambodia"
  },
  {
    "code": "CM",
    "name": "Cameroon"
  },
  {
    "code": "CA",
    "name": "Canada"
  },
  {
    "code": "CV",
    "name": "Cape Verde"
  },
  {
    "code": "KY",
    "name": "Cayman Islands"
  },
  {
    "code": "CF",
    "name": "Central African Republic"
  },
  {
    "code": "TD",
    "name": "Chad"
  },
  {
    "code": "CL",
    "name": "Chile"
  },
  {
    "code": "CN",
    "name": "China"
  },
  {
    "code": "CX",
    "name": "Christmas Island"
  },
  {
    "code": "CC",
    "name": "Cocos (Keeling) Islands"
  },
  {
    "code": "CO",
    "name": "Colombia"
  },
  {
    "code": "KM",
    "name": "Comoros"
  },
  {
    "code": "CG",
    "name": "Congo"
  },
  {
    "code": "CD",
    "name": "Congo, the Democratic Republic of the"
  },
  {
    "code": "CD",
    "name": "The Democratic Republic of the Congo"
  },  
  {
    "code": "CK",
    "name": "Cook Islands"
  },
  {
    "code": "CR",
    "name": "Costa Rica"
  },
  {
    "code": "CI",
    "name": "Côte d'Ivoire"
  },
  {
    "code": "HR",
    "name": "Croatia"
  },
  {
    "code": "CU",
    "name": "Cuba"
  },
  {
    "code": "CW",
    "name": "Curaçao"
  },
  {
    "code": "CY",
    "name": "Cyprus"
  },
  {
    "code": "CZ",
    "name": "Czech Republic"
  },
  {
    "code": "DK",
    "name": "Denmark"
  },
  {
    "code": "DJ",
    "name": "Djibouti"
  },
  {
    "code": "DM",
    "name": "Dominica"
  },
  {
    "code": "DO",
    "name": "Dominican Republic"
  },
  {
    "code": "EC",
    "name": "Ecuador"
  },
  {
    "code": "EG",
    "name": "Egypt"
  },
  {
    "code": "SV",
    "name": "El Salvador"
  },
  {
    "code": "GQ",
    "name": "Equatorial Guinea"
  },
  {
    "code": "ER",
    "name": "Eritrea"
  },
  {
    "code": "EE",
    "name": "Estonia"
  },
  {
    "code": "ET",
    "name": "Ethiopia"
  },
  {
    "code": "FK",
    "name": "Falkland Islands (Malvinas)"
  },
  {
    "code": "FO",
    "name": "Faroe Islands"
  },
  {
    "code": "FJ",
    "name": "Fiji"
  },
  {
    "code": "FI",
    "name": "Finland"
  },
  {
    "code": "FR",
    "name": "France"
  },
  {
    "code": "GF",
    "name": "French Guiana"
  },
  {
    "code": "PF",
    "name": "French Polynesia"
  },
  {
    "code": "TF",
    "name": "French Southern Territories"
  },
  {
    "code": "GA",
    "name": "Gabon"
  },
  {
    "code": "GM",
    "name": "Gambia"
  },
  {
    "code": "GE",
    "name": "Georgia"
  },
  {
    "code": "DE",
    "name": "Germany"
  },
  {
    "code": "GH",
    "name": "Ghana"
  },
  {
    "code": "GI",
    "name": "Gibraltar"
  },
  {
    "code": "GR",
    "name": "Greece"
  },
  {
    "code": "GL",
    "name": "Greenland"
  },
  {
    "code": "GD",
    "name": "Grenada"
  },
  {
    "code": "GP",
    "name": "Guadeloupe"
  },
  {
    "code": "GU",
    "name": "Guam"
  },
  {
    "code": "GT",
    "name": "Guatemala"
  },
  {
    "code": "GG",
    "name": "Guernsey"
  },
  {
    "code": "GN",
    "name": "Guinea"
  },
  {
    "code": "GW",
    "name": "Guinea-Bissau"
  },
  {
    "code": "GY",
    "name": "Guyana"
  },
  {
    "code": "HT",
    "name": "Haiti"
  },
  {
    "code": "HM",
    "name": "Heard Island and McDonald Islands"
  },
  {
    "code": "VA",
    "name": "Holy See (Vatican City State)"
  },
  {
    "code": "VA",
    "name": "Vatican"
  },  
  {
    "code": "HN",
    "name": "Honduras"
  },
  {
    "code": "HK",
    "name": "Hong Kong"
  },
  {
    "code": "HU",
    "name": "Hungary"
  },
  {
    "code": "IS",
    "name": "Iceland"
  },
  {
    "code": "IN",
    "name": "India"
  },
  {
    "code": "ID",
    "name": "Indonesia"
  },
  {
    "code": "IR",
    "name": "Iran"
  },
  {
    "code": "IQ",
    "name": "Iraq"
  },
  {
    "code": "IE",
    "name": "Ireland"
  },
  {
    "code": "IM",
    "name": "Isle of Man"
  },
  {
    "code": "IL",
    "name": "Israel"
  },
  {
    "code": "IT",
    "name": "Italy"
  },
  {
    "code": "JM",
    "name": "Jamaica"
  },
  {
    "code": "JP",
    "name": "Japan"
  },
  {
    "code": "JE",
    "name": "Jersey"
  },
  {
    "code": "JO",
    "name": "Jordan"
  },
  {
    "code": "KZ",
    "name": "Kazakhstan"
  },
  {
    "code": "KE",
    "name": "Kenya"
  },
  {
    "code": "KI",
    "name": "Kiribati"
  },
  {
    "code": "KP",
    "name": "Democratic People's Republic of Korea"
  },
  {
    "code": "KP",
    "name": "North Korea"
  },  
  {
    "code": "KR",
    "name": "Republic of Korea"
  },
  {
    "code": "KR",
    "name": "South Korea"
  },  
  {
    "code": "KW",
    "name": "Kuwait"
  },
  {
    "code": "KG",
    "name": "Kyrgyzstan"
  },
  {
    "code": "LA",
    "name": "Lao People's Democratic Republic"
  },
  {
    "code": "LA",
    "name": "Lao"
  },  
  {
    "code": "LV",
    "name": "Latvia"
  },
  {
    "code": "LB",
    "name": "Lebanon"
  },
  {
    "code": "LS",
    "name": "Lesotho"
  },
  {
    "code": "LR",
    "name": "Liberia"
  },
  {
    "code": "LY",
    "name": "Libya"
  },
  {
    "code": "LI",
    "name": "Liechtenstein"
  },
  {
    "code": "LT",
    "name": "Lithuania"
  },
  {
    "code": "LU",
    "name": "Luxembourg"
  },
  {
    "code": "MO",
    "name": "Macao"
  },
  {
    "code": "MK",
    "name": "The Former Yugoslav Republic of Macedonia"
  },
  {
    "code": "MK",
    "name": "Macedonia"
  },  
  {
    "code": "MG",
    "name": "Madagascar"
  },
  {
    "code": "MW",
    "name": "Malawi"
  },
  {
    "code": "MY",
    "name": "Malaysia"
  },
  {
    "code": "MV",
    "name": "Maldives"
  },
  {
    "code": "ML",
    "name": "Mali"
  },
  {
    "code": "MT",
    "name": "Malta"
  },
  {
    "code": "MH",
    "name": "Marshall Islands"
  },
  {
    "code": "MQ",
    "name": "Martinique"
  },
  {
    "code": "MR",
    "name": "Mauritania"
  },
  {
    "code": "MU",
    "name": "Mauritius"
  },
  {
    "code": "YT",
    "name": "Mayotte"
  },
  {
    "code": "MX",
    "name": "Mexico"
  },
  {
    "code": "FM",
    "name": "Micronesia, Federated States of"
  },
  {
    "code": "FM",
    "name": "Federated States of Micronesia"
  },  
  {
    "code": "FM",
    "name": "Micronesia"
  },  
  {
    "code": "MD",
    "name": "Moldova, Republic of"
  },
  {
    "code": "MD",
    "name": "Moldova"
  },  
  {
    "code": "MC",
    "name": "Monaco"
  },
  {
    "code": "MN",
    "name": "Mongolia"
  },
  {
    "code": "ME",
    "name": "Montenegro"
  },
  {
    "code": "MS",
    "name": "Montserrat"
  },
  {
    "code": "MA",
    "name": "Morocco"
  },
  {
    "code": "MZ",
    "name": "Mozambique"
  },
  {
    "code": "MM",
    "name": "Myanmar"
  },
  {
    "code": "NA",
    "name": "Namibia"
  },
  {
    "code": "NR",
    "name": "Nauru"
  },
  {
    "code": "NP",
    "name": "Nepal"
  },
  {
    "code": "NL",
    "name": "Netherlands"
  },
  {
    "code": "NC",
    "name": "New Caledonia"
  },
  {
    "code": "NZ",
    "name": "New Zealand"
  },
  {
    "code": "NI",
    "name": "Nicaragua"
  },
  {
    "code": "NE",
    "name": "Niger"
  },
  {
    "code": "NG",
    "name": "Nigeria"
  },
  {
    "code": "NU",
    "name": "Niue"
  },
  {
    "code": "NF",
    "name": "Norfolk Island"
  },
  {
    "code": "MP",
    "name": "Northern Mariana Islands"
  },
  {
    "code": "NO",
    "name": "Norway"
  },
  {
    "code": "OM",
    "name": "Oman"
  },
  {
    "code": "PK",
    "name": "Pakistan"
  },
  {
    "code": "PW",
    "name": "Palau"
  },
  {
    "code": "PS",
    "name": "Palestine, State of"
  },
  {
    "code": "PS",
    "name": "State of Palestine"
  },
  {
    "code": "PS",
    "name": "Palestine"
  },  
  {
    "code": "PA",
    "name": "Panama"
  },
  {
    "code": "PG",
    "name": "Papua New Guinea"
  },
  {
    "code": "PY",
    "name": "Paraguay"
  },
  {
    "code": "PE",
    "name": "Peru"
  },
  {
    "code": "PH",
    "name": "Philippines"
  },
  {
    "code": "PN",
    "name": "Pitcairn"
  },
  {
    "code": "PL",
    "name": "Poland"
  },
  {
    "code": "PT",
    "name": "Portugal"
  },
  {
    "code": "PR",
    "name": "Puerto Rico"
  },
  {
    "code": "QA",
    "name": "Qatar"
  },
  {
    "code": "RE",
    "name": "Réunion"
  },
  {
    "code": "RO",
    "name": "Romania"
  },
  {
    "code": "RU",
    "name": "Russia"
  },
  {
    "code": "RW",
    "name": "Rwanda"
  },
  {
    "code": "BL",
    "name": "Saint Barthélemy"
  },
  {
    "code": "SH",
    "name": "Saint Helena, Ascension and Tristan da Cunha"
  },
  {
    "code": "KN",
    "name": "Saint Kitts and Nevis"
  },
  {
    "code": "LC",
    "name": "Saint Lucia"
  },
  {
    "code": "MF",
    "name": "Saint Martin (French part)"
  },
  {
    "code": "PM",
    "name": "Saint Pierre and Miquelon"
  },
  {
    "code": "VC",
    "name": "Saint Vincent and the Grenadines"
  },
  {
    "code": "WS",
    "name": "Samoa"
  },
  {
    "code": "SM",
    "name": "San Marino"
  },
  {
    "code": "ST",
    "name": "Sao Tome and Principe"
  },
  {
    "code": "SA",
    "name": "Saudi Arabia"
  },
  {
    "code": "SN",
    "name": "Senegal"
  },
  {
    "code": "RS",
    "name": "Serbia"
  },
  {
    "code": "SC",
    "name": "Seychelles"
  },
  {
    "code": "SL",
    "name": "Sierra Leone"
  },
  {
    "code": "SG",
    "name": "Singapore"
  },
  {
    "code": "SX",
    "name": "Sint Maarten (Dutch part)"
  },
  {
    "code": "SK",
    "name": "Slovakia"
  },
  {
    "code": "SI",
    "name": "Slovenia"
  },
  {
    "code": "SB",
    "name": "Solomon Islands"
  },
  {
    "code": "SO",
    "name": "Somalia"
  },
  {
    "code": "ZA",
    "name": "South Africa"
  },
  {
    "code": "GS",
    "name": "South Georgia and the South Sandwich Islands"
  },
  {
    "code": "SS",
    "name": "South Sudan"
  },
  {
    "code": "ES",
    "name": "Spain"
  },
  {
    "code": "LK",
    "name": "Sri Lanka"
  },
  {
    "code": "SD",
    "name": "Sudan"
  },
  {
    "code": "SR",
    "name": "Suriname"
  },
  {
    "code": "SJ",
    "name": "Svalbard and Jan Mayen"
  },
  {
    "code": "SZ",
    "name": "Swaziland"
  },
  {
    "code": "SE",
    "name": "Sweden"
  },
  {
    "code": "CH",
    "name": "Switzerland"
  },
  {
    "code": "SY",
    "name": "Syrian Arab Republic"
  },
  {
    "code": "TW",
    "name": "Taiwan"
  },
  {
    "code": "TJ",
    "name": "Tajikistan"
  },
  {
    "code": "TZ",
    "name": "Tanzania, United Republic of"
  },
  {
    "code": "TZ",
    "name": "Tanzania"
  },
  {
    "code": "TH",
    "name": "Thailand"
  },
  {
    "code": "TL",
    "name": "Timor-Leste"
  },
  {
    "code": "TG",
    "name": "Togo"
  },
  {
    "code": "TK",
    "name": "Tokelau"
  },
  {
    "code": "TO",
    "name": "Tonga"
  },
  {
    "code": "TT",
    "name": "Trinidad and Tobago"
  },
  {
    "code": "TN",
    "name": "Tunisia"
  },
  {
    "code": "TR",
    "name": "Turkey"
  },
  {
    "code": "TM",
    "name": "Turkmenistan"
  },
  {
    "code": "TC",
    "name": "Turks and Caicos Islands"
  },
  {
    "code": "TV",
    "name": "Tuvalu"
  },
  {
    "code": "UG",
    "name": "Uganda"
  },
  {
    "code": "UA",
    "name": "Ukraine"
  },
  {
    "code": "AE",
    "name": "United Arab Emirates"
  },
  {
    "code": "AE",
    "name": "UAE"
  },
  {
    "code": "GB",
    "name": "UK"
  },
  {
    "code": "GB",
    "name": "England"
  },    
{
    "code": "GB",
    "name": "Great Britain"
  },      
  {
    "code": "GB",
    "name": "United Kingdom"
  },  
  {
    "code": "US",
    "name": "USA"
  },    
  {
    "code": "US",
    "name": "United States"
  },
  {
    "code": "UM",
    "name": "United States Minor Outlying Islands"
  },
  {
    "code": "UY",
    "name": "Uruguay"
  },
  {
    "code": "UZ",
    "name": "Uzbekistan"
  },
  {
    "code": "VU",
    "name": "Vanuatu"
  },
  {
    "code": "VE",
    "name": "Venezuela, Bolivarian Republic of"
  },
  {
    "code": "VE",
    "name": "Venezuela"
  },  
  {
    "code": "VN",
    "name": "Vietnam"
  },
  {
    "code": "VN",
    "name": "Viet Nam"
  },
  {
    "code": "VG",
    "name": "Virgin Islands, British"
  },
  {
    "code": "VG",
    "name": "British Virgin Islands"
  },  
  {
    "code": "VI",
    "name": "U.S. Virgin Islands"
  },
  {
    "code": "WF",
    "name": "Wallis and Futuna"
  },
  {
    "code": "EH",
    "name": "Western Sahara"
  },
  {
    "code": "YE",
    "name": "Yemen"
  },
  {
    "code": "ZM",
    "name": "Zambia"
  },
  {
    "code": "ZW",
    "name": "Zimbabwe"
  }
]

code_countries_dict = dict((country['code'], country['name']) for country in countries)
countries_list = sorted(code_countries_dict.values())
countries_as_tuples = [(country, country) for _,country in code_countries_dict.items()]
countries_dict = dict((country['name'].lower(), country['code'].lower()) for country in countries)

region_and_countries_set = {'Europe': {'name': 'Europe', 'id': 2, 'children': {'Andorra': {'name': 'Andorra', 'id': 3}, 'Albania': {'name': 'Albania', 'id': 4}, 'Austria': {'name': 'Austria', 'id': 5}, 'Bosnia and Herzegovina': {'name': 'Bosnia and Herzegovina', 'id': 6}, 'Belgium': {'name': 'Belgium', 'id': 7}, 'Bulgaria': {'name': 'Bulgaria', 'id': 8}, 'Switzerland': {'name': 'Switzerland', 'id': 9}, 'Cyprus': {'name': 'Cyprus', 'id': 10}, 'Czech Republic': {'name': 'Czech Republic', 'id': 11}, 'Germany': {'name': 'Germany', 'id': 12}, 'Denmark': {'name': 'Denmark', 'id': 13}, 'Estonia': {'name': 'Estonia', 'id': 14}, 'Spain': {'name': 'Spain', 'id': 15}, 'Finland': {'name': 'Finland', 'id': 16}, 'Faroe Islands': {'name': 'Faroe Islands', 'id': 17}, 'France': {'name': 'France', 'id': 18}, 'France, Metropolitan': {'name': 'France, Metropolitan', 'id': 19}, 'United Kingdom': {'name': 'United Kingdom', 'id': 20}, 'Gibraltar': {'name': 'Gibraltar', 'id': 21}, 'Greenland': {'name': 'Greenland', 'id': 22}, 'Greece': {'name': 'Greece', 'id': 23}, 'Croatia': {'name': 'Croatia', 'id': 24}, 'Hungary': {'name': 'Hungary', 'id': 25}, 'Ireland': {'name': 'Ireland', 'id': 26}, 'Israel': {'name': 'Israel', 'id': 27}, 'Iceland': {'name': 'Iceland', 'id': 28}, 'Italy': {'name': 'Italy', 'id': 29}, 'Liechtenstein': {'name': 'Liechtenstein', 'id': 30}, 'Lithuania': {'name': 'Lithuania', 'id': 31}, 'Luxembourg': {'name': 'Luxembourg', 'id': 32}, 'Latvia': {'name': 'Latvia', 'id': 33}, 'Monaco': {'name': 'Monaco', 'id': 34}, 'Moldova, Republic of': {'name': 'Moldova, Republic of', 'id': 35}, 'Macedonia': {'name': 'Macedonia', 'id': 36}, 'Malta': {'name': 'Malta', 'id': 37}, 'Netherlands': {'name': 'Netherlands', 'id': 38}, 'Norway': {'name': 'Norway', 'id': 39}, 'Poland': {'name': 'Poland', 'id': 40}, 'Portugal': {'name': 'Portugal', 'id': 41}, 'Romania': {'name': 'Romania', 'id': 42}, 'Russian Federation': {'name': 'Russian Federation', 'id': 43}, 'Sweden': {'name': 'Sweden', 'id': 44}, 'Slovenia': {'name': 'Slovenia', 'id': 45}, 'Svalbard and Jan Mayen': {'name': 'Svalbard and Jan Mayen', 'id': 46}, 'Slovakia': {'name': 'Slovakia', 'id': 47}, 'San Marino': {'name': 'San Marino', 'id': 48}, 'Turkey': {'name': 'Turkey', 'id': 49}, 'Holy See (Vatican City State)': {'name': 'Holy See (Vatican City State)', 'id': 50}, 'Serbia': {'name': 'Serbia', 'id': 51}, 'Montenegro': {'name': 'Montenegro', 'id': 52}, 'Aland Islands': {'name': 'Aland Islands', 'id': 53}, 'Guernsey': {'name': 'Guernsey', 'id': 54}, 'Isle of Man': {'name': 'Isle of Man', 'id': 55}, 'Jersey': {'name': 'Jersey', 'id': 56}}}, 'Arab States': {'name': 'Arab States', 'id': 58, 'children': {'United Arab Emirates': {'name': 'United Arab Emirates', 'id': 59}, 'Bahrain': {'name': 'Bahrain', 'id': 60}, 'Djibouti': {'name': 'Djibouti', 'id': 61}, 'Algeria': {'name': 'Algeria', 'id': 62}, 'Egypt': {'name': 'Egypt', 'id': 63}, 'Iraq': {'name': 'Iraq', 'id': 64}, 'Jordan': {'name': 'Jordan', 'id': 65}, 'Comoros': {'name': 'Comoros', 'id': 66}, 'Kuwait': {'name': 'Kuwait', 'id': 67}, 'Lebanon': {'name': 'Lebanon', 'id': 68}, 'Libya': {'name': 'Libya', 'id': 69}, 'Morocco': {'name': 'Morocco', 'id': 70}, 'Mauritania': {'name': 'Mauritania', 'id': 71}, 'Oman': {'name': 'Oman', 'id': 72}, 'Palestinian Territory': {'name': 'Palestinian Territory', 'id': 73}, 'Qatar': {'name': 'Qatar', 'id': 74}, 'Saudi Arabia': {'name': 'Saudi Arabia', 'id': 75}, 'Sudan': {'name': 'Sudan', 'id': 76}, 'Somalia': {'name': 'Somalia', 'id': 77}, 'Tunisia': {'name': 'Tunisia', 'id': 78}, 'Yemen': {'name': 'Yemen', 'id': 79}}}, 'Asia & Pacific': {'name': 'Asia & Pacific', 'id': 81, 'children': {'Afghanistan': {'name': 'Afghanistan', 'id': 82}, 'Antarctica': {'name': 'Antarctica', 'id': 83}, 'American Samoa': {'name': 'American Samoa', 'id': 84}, 'Australia': {'name': 'Australia', 'id': 85}, 'Bangladesh': {'name': 'Bangladesh', 'id': 86}, 'Brunei Darussalam': {'name': 'Brunei Darussalam', 'id': 87}, 'Bhutan': {'name': 'Bhutan', 'id': 88}, 'Cocos (Keeling) Islands': {'name': 'Cocos (Keeling) Islands', 'id': 89}, 'Cook Islands': {'name': 'Cook Islands', 'id': 90}, 'China': {'name': 'China', 'id': 91}, 'Christmas Island': {'name': 'Christmas Island', 'id': 92}, 'Fiji': {'name': 'Fiji', 'id': 93}, 'Micronesia, Federated States of': {'name': 'Micronesia, Federated States of', 'id': 94}, 'Guam': {'name': 'Guam', 'id': 95}, 'Hong Kong': {'name': 'Hong Kong', 'id': 96}, 'Heard Island and McDonald Islands': {'name': 'Heard Island and McDonald Islands', 'id': 97}, 'Indonesia': {'name': 'Indonesia', 'id': 98}, 'India': {'name': 'India', 'id': 99}, 'British Indian Ocean Territory': {'name': 'British Indian Ocean Territory', 'id': 100}, 'Iran, Islamic Republic of': {'name': 'Iran, Islamic Republic of', 'id': 101}, 'Japan': {'name': 'Japan', 'id': 102}, 'Cambodia': {'name': 'Cambodia', 'id': 103}, 'Kiribati': {'name': 'Kiribati', 'id': 104}, "Korea, Democratic People's Republic of": {'name': "Korea, Democratic People's Republic of", 'id': 105}, 'Korea, Republic of': {'name': 'Korea, Republic of', 'id': 106}, "Lao People's Democratic Republic": {'name': "Lao People's Democratic Republic", 'id': 107}, 'Sri Lanka': {'name': 'Sri Lanka', 'id': 108}, 'Marshall Islands': {'name': 'Marshall Islands', 'id': 109}, 'Myanmar': {'name': 'Myanmar', 'id': 110}, 'Mongolia': {'name': 'Mongolia', 'id': 111}, 'Macau': {'name': 'Macau', 'id': 112}, 'Northern Mariana Islands': {'name': 'Northern Mariana Islands', 'id': 113}, 'Maldives': {'name': 'Maldives', 'id': 114}, 'Malaysia': {'name': 'Malaysia', 'id': 115}, 'New Caledonia': {'name': 'New Caledonia', 'id': 116}, 'Norfolk Island': {'name': 'Norfolk Island', 'id': 117}, 'Nepal': {'name': 'Nepal', 'id': 118}, 'Nauru': {'name': 'Nauru', 'id': 119}, 'Niue': {'name': 'Niue', 'id': 120}, 'New Zealand': {'name': 'New Zealand', 'id': 121}, 'French Polynesia': {'name': 'French Polynesia', 'id': 122}, 'Papua New Guinea': {'name': 'Papua New Guinea', 'id': 123}, 'Philippines': {'name': 'Philippines', 'id': 124}, 'Pakistan': {'name': 'Pakistan', 'id': 125}, 'Pitcairn Islands': {'name': 'Pitcairn Islands', 'id': 126}, 'Palau': {'name': 'Palau', 'id': 127}, 'Reunion': {'name': 'Reunion', 'id': 128}, 'Solomon Islands': {'name': 'Solomon Islands', 'id': 129}, 'Singapore': {'name': 'Singapore', 'id': 130}, 'Syrian Arab Republic': {'name': 'Syrian Arab Republic', 'id': 131}, 'French Southern Territories': {'name': 'French Southern Territories', 'id': 132}, 'Thailand': {'name': 'Thailand', 'id': 133}, 'Tokelau': {'name': 'Tokelau', 'id': 134}, 'Tonga': {'name': 'Tonga', 'id': 135}, 'Timor-Leste': {'name': 'Timor-Leste', 'id': 136}, 'Tuvalu': {'name': 'Tuvalu', 'id': 137}, 'Taiwan': {'name': 'Taiwan', 'id': 138}, 'United States Minor Outlying Islands': {'name': 'United States Minor Outlying Islands', 'id': 139}, 'Vietnam': {'name': 'Vietnam', 'id': 140}, 'Vanuatu': {'name': 'Vanuatu', 'id': 141}, 'Wallis and Futuna': {'name': 'Wallis and Futuna', 'id': 142}, 'Samoa': {'name': 'Samoa', 'id': 143}}}, 'South/Latin America': {'name': 'South/Latin America', 'id': 145, 'children': {'Antigua and Barbuda': {'name': 'Antigua and Barbuda', 'id': 146}, 'Anguilla': {'name': 'Anguilla', 'id': 147}, 'Netherlands Antilles': {'name': 'Netherlands Antilles', 'id': 148}, 'Argentina': {'name': 'Argentina', 'id': 149}, 'Aruba': {'name': 'Aruba', 'id': 150}, 'Barbados': {'name': 'Barbados', 'id': 151}, 'Bolivia': {'name': 'Bolivia', 'id': 152}, 'Brazil': {'name': 'Brazil', 'id': 153}, 'Bahamas': {'name': 'Bahamas', 'id': 154}, 'Bouvet Island': {'name': 'Bouvet Island', 'id': 155}, 'Belize': {'name': 'Belize', 'id': 156}, 'Chile': {'name': 'Chile', 'id': 157}, 'Colombia': {'name': 'Colombia', 'id': 158}, 'Costa Rica': {'name': 'Costa Rica', 'id': 159}, 'Cuba': {'name': 'Cuba', 'id': 160}, 'Dominica': {'name': 'Dominica', 'id': 161}, 'Dominican Republic': {'name': 'Dominican Republic', 'id': 162}, 'Ecuador': {'name': 'Ecuador', 'id': 163}, 'Falkland Islands (Malvinas)': {'name': 'Falkland Islands (Malvinas)', 'id': 164}, 'Grenada': {'name': 'Grenada', 'id': 165}, 'French Guiana': {'name': 'French Guiana', 'id': 166}, 'Guadeloupe': {'name': 'Guadeloupe', 'id': 167}, 'South Georgia and the South Sandwich Islands': {'name': 'South Georgia and the South Sandwich Islands', 'id': 168}, 'Guatemala': {'name': 'Guatemala', 'id': 169}, 'Guyana': {'name': 'Guyana', 'id': 170}, 'Honduras': {'name': 'Honduras', 'id': 171}, 'Haiti': {'name': 'Haiti', 'id': 172}, 'Jamaica': {'name': 'Jamaica', 'id': 173}, 'Saint Kitts and Nevis': {'name': 'Saint Kitts and Nevis', 'id': 174}, 'Cayman Islands': {'name': 'Cayman Islands', 'id': 175}, 'Saint Lucia': {'name': 'Saint Lucia', 'id': 176}, 'Martinique': {'name': 'Martinique', 'id': 177}, 'Montserrat': {'name': 'Montserrat', 'id': 178}, 'Nicaragua': {'name': 'Nicaragua', 'id': 179}, 'Panama': {'name': 'Panama', 'id': 180}, 'Peru': {'name': 'Peru', 'id': 181}, 'Puerto Rico': {'name': 'Puerto Rico', 'id': 182}, 'Paraguay': {'name': 'Paraguay', 'id': 183}, 'Suriname': {'name': 'Suriname', 'id': 184}, 'El Salvador': {'name': 'El Salvador', 'id': 185}, 'Turks and Caicos Islands': {'name': 'Turks and Caicos Islands', 'id': 186}, 'Trinidad and Tobago': {'name': 'Trinidad and Tobago', 'id': 187}, 'Uruguay': {'name': 'Uruguay', 'id': 188}, 'Saint Vincent and the Grenadines': {'name': 'Saint Vincent and the Grenadines', 'id': 189}, 'Venezuela': {'name': 'Venezuela', 'id': 190}, 'Virgin Islands, British': {'name': 'Virgin Islands, British', 'id': 191}, 'Virgin Islands, U.S.': {'name': 'Virgin Islands, U.S.', 'id': 192}, 'Saint Barthelemy': {'name': 'Saint Barthelemy', 'id': 193}, 'Saint Martin': {'name': 'Saint Martin', 'id': 194}}}, 'CIS': {'name': 'CIS', 'id': 196, 'children': {'Armenia': {'name': 'Armenia', 'id': 197}, 'Azerbaijan': {'name': 'Azerbaijan', 'id': 198}, 'Belarus': {'name': 'Belarus', 'id': 199}, 'Georgia': {'name': 'Georgia', 'id': 200}, 'Kyrgyzstan': {'name': 'Kyrgyzstan', 'id': 201}, 'Kazakhstan': {'name': 'Kazakhstan', 'id': 202}, 'Tajikistan': {'name': 'Tajikistan', 'id': 203}, 'Turkmenistan': {'name': 'Turkmenistan', 'id': 204}, 'Ukraine': {'name': 'Ukraine', 'id': 205}, 'Uzbekistan': {'name': 'Uzbekistan', 'id': 206}}}, 'Africa': {'name': 'Africa', 'id': 208, 'children': {'Angola': {'name': 'Angola', 'id': 209}, 'Burkina Faso': {'name': 'Burkina Faso', 'id': 210}, 'Burundi': {'name': 'Burundi', 'id': 211}, 'Benin': {'name': 'Benin', 'id': 212}, 'Botswana': {'name': 'Botswana', 'id': 213}, 'Congo, The Democratic Republic of the': {'name': 'Congo, The Democratic Republic of the', 'id': 214}, 'Central African Republic': {'name': 'Central African Republic', 'id': 215}, 'Congo': {'name': 'Congo', 'id': 216}, "Côte D'Ivoire": {'name': "Côte D'Ivoire", 'id': 217}, 'Cameroon': {'name': 'Cameroon', 'id': 218}, 'Cape Verde': {'name': 'Cape Verde', 'id': 219}, 'Western Sahara': {'name': 'Western Sahara', 'id': 220}, 'Eritrea': {'name': 'Eritrea', 'id': 221}, 'Ethiopia': {'name': 'Ethiopia', 'id': 222}, 'Gabon': {'name': 'Gabon', 'id': 223}, 'Ghana': {'name': 'Ghana', 'id': 224}, 'Gambia': {'name': 'Gambia', 'id': 225}, 'Guinea': {'name': 'Guinea', 'id': 226}, 'Equatorial Guinea': {'name': 'Equatorial Guinea', 'id': 227}, 'Guinea-Bissau': {'name': 'Guinea-Bissau', 'id': 228}, 'Kenya': {'name': 'Kenya', 'id': 229}, 'Liberia': {'name': 'Liberia', 'id': 230}, 'Lesotho': {'name': 'Lesotho', 'id': 231}, 'Madagascar': {'name': 'Madagascar', 'id': 232}, 'Mali': {'name': 'Mali', 'id': 233}, 'Mauritius': {'name': 'Mauritius', 'id': 234}, 'Malawi': {'name': 'Malawi', 'id': 235}, 'Mozambique': {'name': 'Mozambique', 'id': 236}, 'Namibia': {'name': 'Namibia', 'id': 237}, 'Niger': {'name': 'Niger', 'id': 238}, 'Nigeria': {'name': 'Nigeria', 'id': 239}, 'Rwanda': {'name': 'Rwanda', 'id': 240}, 'Seychelles': {'name': 'Seychelles', 'id': 241}, 'South Sudan': {'name': 'South Sudan', 'id': 242}, 'Saint Helena': {'name': 'Saint Helena', 'id': 243}, 'Sierra Leone': {'name': 'Sierra Leone', 'id': 244}, 'Senegal': {'name': 'Senegal', 'id': 245}, 'Sao Tome and Principe': {'name': 'Sao Tome and Principe', 'id': 246}, 'Swaziland': {'name': 'Swaziland', 'id': 247}, 'Chad': {'name': 'Chad', 'id': 248}, 'Togo': {'name': 'Togo', 'id': 249}, 'Tanzania, United Republic of': {'name': 'Tanzania, United Republic of', 'id': 250}, 'Uganda': {'name': 'Uganda', 'id': 251}, 'Mayotte': {'name': 'Mayotte', 'id': 252}, 'South Africa': {'name': 'South Africa', 'id': 253}, 'Zambia': {'name': 'Zambia', 'id': 254}, 'Zimbabwe': {'name': 'Zimbabwe', 'id': 255}}}, 'North America': {'name': 'North America', 'id': 257, 'children': {'Bermuda': {'name': 'Bermuda', 'id': 258}, 'Canada': {'name': 'Canada', 'id': 259}, 'Mexico': {'name': 'Mexico', 'id': 260}, 'Saint Pierre and Miquelon': {'name': 'Saint Pierre and Miquelon', 'id': 261}, 'United States': {'name': 'United States', 'id': 262}}}}

regions_list = [ val['id'] for _ , val in region_and_countries_set.items() ]

#countries_id_list = { reg : region_and_countries_set[reg] for reg in sorted(region_and_countries_set) }
countries_id_dict = {}
for reg, val in region_and_countries_set.items():
  countries_id_dict[val['id']] = val['name']
  for country, val2 in val['children'].items():
    countries_id_dict[val2['id']] = val2['name']

countries_path_dict = {}
for reg, val in region_and_countries_set.items():
  countries_path_dict[val['id']] = val['name']
  for country, val2 in val['children'].items():
    countries_path_dict[val2['id']] = ' >> '.join((val['name'], val2['name']))

countries_by_region_dict = {}
for reg, val in region_and_countries_set.items():
  countries_by_region_dict[val['id']] = [ country for country in val['children'] ]


countries_id_by_name = {}
for reg, val in region_and_countries_set.items():
    for country, val2 in val['children'].items():
        countries_id_by_name[country] = val2['id']