import ujson
from flask import url_for
from flask import current_app as app
from flask_login import current_user
from markupsafe import Markup
import babel
from flask_babel import lazy_gettext, gettext
from slugify import slugify
from  web.core.geo import countries_dict
import os
from datetime import datetime

@app.template_filter('fmt_datetime')
def fmt_datetime(value, format='medium', fmt='%m-%d-%y %H:%M:%S'):
    if not value:
        return ''
    if format == 'full':
        format="EEEE, d. MMMM y 'at' HH:mm"
    elif format == 'medium':
        format="EE dd.MM.y HH:mm"
    elif format == 'short':
        format="dd.MM.y HH:mm"
    return babel.dates.format_datetime(value, format)
    #return value.strftime(fmt)

@app.template_filter('fmt_datetime_friendly')
def fmt_datetime_friendly(value, default=lazy_gettext(u'just now')):
    now = datetime.utcnow()
    diff = now - value
    #if diff.days > 7:
    #    return fmt_datetime(value, format='medium')

    periods = [
        (diff.days // 365, "year", "years"),
        (diff.days // 30, "month", "months"),
        (diff.days // 7, "week", "weeks"),
        (diff.days, "day", "days"),
        (diff.seconds // 3600, "hour", "hours"),
        (diff.seconds // 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    ]
    for period, singular, plural in periods:   
        if period != 0:
            return "%d %s ago" % (period, singular if period == 1 else plural)
    return default

@app.template_filter('max')
def fmt_max(l):
    return max(l)

@app.template_filter('min')
def fmt_min(l):
    return min(l)

@app.template_filter('fmt_list')
def fmt_list(value):
    if type(value) == list:
        return ', '.join(filter(None,value))
    return value

@app.template_filter('fmt_json_str')
def fmt_json_str(value):
    try:
        return ujson.dumps(value)
    except:
        return value

@app.template_filter('filter_list')
def fmt_filter_list(l, func):
    try:
        return list(filter(func, l))
    except:
        return l

@app.template_filter('fmt_title')
def fmt_title(value, numchars=60):
    try:
        txt = value if type(value) == str else str(value)        
        if len(txt) < numchars:
            return txt
        return f'{txt[:numchars]}...'
    except:
        return value

@app.template_filter('fmt_link')
def fmt_link(value, ltype='url', public=False):
    if not public and not current_user.is_authenticated:
        return Markup(f'<a href="/login">Log in to see</a>')

    if not value or value == '':
        return lazy_gettext(u'Not Available')

    if ltype == 'url':
        url = value if value.lower().startswith('http') else f'http://{value}'
        return Markup(f'<a target="_blank" class="link" href="{url}">{value}</a>')

    if ltype == 'whatsapp':
        return Markup(f'<a target="_blank" class="whatsapp" href="https://api.whatsapp.com/send?phone={value}"></a>')

    if ltype == 'viber':
        return Markup(f'<a target="_blank" class="viber" href="viber://pa/info?uri={value}"></a>')

    if ltype == 'email':
        return Markup(f'<a target="_blank" class="email" href="mailto:{value}">{value}</a>')

    if ltype == 'tel':
        return Markup(f'<a target="_blank" class="tel" href="tel:{value}">{value}</a>')

    if ltype == 'fax':
        return Markup(f'<a target="_blank" class="fax" href="fax:{value}">{value}</a>')

    if ltype == 'wechat':
        return Markup(f'<a target="_blank" class="wechat" href="weixin://dl/chat?{value}"></a>')

    if ltype == 'facebook':
        return Markup(f'<a target="_blank" class="facebook" href="{value}"></a>')

    if ltype == 'twitter':
        return Markup(f'<a target="_blank" class="twitter" href="{value}"></a>')

    if ltype == 'alibaba':        
        return Markup(f'<a target="_blank" class="alibaba" href="{value}">{value}</a>')

@app.template_filter('auth_only')
def auth_only(value):
    if current_user.is_authenticated:
        return value

    return Markup('<a href="/login">Log in to see</a>')

@app.template_filter('fmt_flag')
def fmt_flag(value):
    try:
        country = value.lower()
        code = countries_dict[country]
        return Markup(f'<img class="flag flag-{code}" src="/static/img/blank.gif" alt="{country}">')
    except:
        return value

@app.template_filter('strip_spaces')
def strip_spaces(value):
    try:
        return ' '.join(value.split()).strip()                
    except:
        return value

#TODO: CACHE?
@app.template_filter('company_banner')
def company_banner(category):
    try:
        img_name = f'{slugify(category)}.jpg'
        img_path = os.path.join(app.static_folder, 'img', 'company_banners', img_name)        
        if not os.path.exists(img_path):
            return '/static/img/company_banner_1.jpg'
        img_url = f'/static/img/company_banners/{img_name}'
        return img_url
    except:
        return category

def finalize(value):
    return value

@app.template_filter('fmt_noti_type')
def fmt_noti_type(ntype):
    return ' '.join(ntype.split('_')).upper()

@app.template_filter('fmt_notification')
def fmt_notification(noti):
    try:                
        if noti.notification_type == 'product_inquiry':            
            buyer_id = noti.details['buyer_id']            
            buyer = app.db.user.get_by_id(buyer_id)
            thread_id = noti.details['thread_id']
            thread_url = url_for('messages.thread_page', pr_id=thread_id)
            content = Markup(f"{buyer.name} sent a new product inquiry. <a href='{thread_url}'>View thread</a")
            return content
        elif noti.notification_type == 'account_activity':
            activity = noti.details['activity']
            content = f'{activity}'
            if noti.details.get('ip', None):
                ip = noti.details['ip']
                content += f' from IP Address {ip}'
            return content

        elif noti.notification_type == 'trade_lead_inquiry':
            user_id = noti.details['user_id']
            user = app.db.user.get_by_id(user_id)
            thread_id = noti.details['thread_id']
            trade_lead_id = noti.details['trade_lead_id']
            thread_url = url_for('messages.trade_lead_thread_page', trade_lead_id=trade_lead_id)
            content = Markup(f"{user.name} sent a new trade lead inquiry. <a href='{thread_url}'>View thread</a")
            return content
        else:
            return noti
    except:
        raise
        return noti

@app.template_filter('fmt_media_file')
def fmt_media_file(filename):
    try:
        arr = filename.rsplit('_', 1)
        return arr[0]
    except:
        return filename