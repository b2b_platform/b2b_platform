import os
import glob
import flask
from flask import Flask, _app_ctx_stack, g, render_template, abort, request, url_for, redirect
from flask_login import LoginManager
from flask_webpack import Webpack
import flask_login
import flask_jwt_extended
from flask_assets import Environment, Bundle
from functools import partial
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import event
from redis import StrictRedis
from flask_babel import Babel
from flask_mail import Mail
from flask_humanize import Humanize
from celery import Celery
from flask_session import Session
import sys
import ujson

from datastores import fs
from database import Database
from database.models import metadata
from role import RoleManager
from membership import MembershipManager
from .forms import FormChecker
from .core import constants, decorators, geo, utils, converters
import settings

import locale
import re


basedir = os.path.dirname(os.path.realpath(__file__))
location = lambda x: os.path.join(basedir, x)
parentdir = os.path.abspath(location('..'))

def load_config():
    mode_affix = 'dev'
    current_mode = os.getenv('SETTINGS_MODE', 'DEV')
    if current_mode == 'PROD':
        mode_affix = 'prod'
    

    conf = settings.load( 'web.base', 
            'web', 
            'mail', 
            'recaptcha', 
            'language')

    conf.update(DATABASE=settings.load('db'))
    conf.update(DATASTORES=settings.load('datastores'))
    conf.update(CELERY=settings.load('celery'))

    return conf


app_config = load_config()
sess = Session()


flask_jwt_extended.jwt_required = decorators.new_jwt_required
flask_jwt_extended.jwt_optional = decorators.new_jwt_optional

old_login_required = flask_login.login_required
old_fresh_login_required = flask_login.fresh_login_required

if not app_config['DEBUG']:
    flask_jwt_extended.jwt_required = decorators.new_jwt_required_with_confirm
    flask_jwt_extended.jwt_optional = decorators.new_jwt_optional_with_confirm
    flask_login.login_required = decorators.new_login_required
    flask_login.fresh_login_required = decorators.new_fresh_login_required

    flask.render_template = decorators.uglify(flask.render_template)

db = Database(app_config['DATABASE']['database_uri'], metadata,
              engine_options=app_config['DATABASE']['engine_options'],
              session_options=app_config['DATABASE']['session_options'],
              scopefunc=_app_ctx_stack.__ident_func__)


ds = fs.FSDatastore(**app_config['DATASTORES']['FS'])

r = StrictRedis(
    host=app_config['REDIS_HOST'],
    port=app_config['REDIS_PORT'],
    db=app_config['REDIS_DB']
)

mail = Mail()
role_manager = RoleManager(db)
form_checker = FormChecker()
login_manager = LoginManager()
membership_manager = MembershipManager(db, ds)
jwt = flask_jwt_extended.JWTManager()
webpack = Webpack()
babel = Babel()
humanize = Humanize()
CLR = Celery(**app_config['CELERY'])

def create_app(conf=app_config, setup=True, **kwargs):
    app = Flask(__name__,
                static_url_path='/static',
                static_folder=location('static'),
                template_folder=location('templates'))

    app.config.update(conf)
    app.config.update(kwargs)
    app.url_map.strict_slashes = False
    app.basedir = basedir
    app.parentdir = parentdir

    if setup:
        with app.app_context():
            setup_session(app)
            setup_jinja(app)
            setup_babel(app)
            setup_extensions(app)
            setup_api(app)
            setup_url_converters(app)
            setup_views(app)
            setup_error_pages(app)
            setup_celery(app)
            setup_humanize(app)
            setup_cli(app)

    return app

def setup_url_converters(app):
    app.url_map.converters['id_slug'] = converters.IDSlugConverter

def setup_session(app):
    # USE REDIS
    app.config.update({
        'SESSION_TYPE': 'redis',
        'SESSION_REDIS': r
    })

    sess.init_app(app)


def setup_jinja(app):

    #################
    # JINJA GLOBALS #
    #################

    app.jinja_env.globals['constants'] = constants
    app.jinja_env.globals['geo'] = geo

    app.jinja_env.policies['json.dumps_function'] = ujson.dumps

    ######################
    # TEMPLATE EXTENSION #
    ######################
    app.jinja_env.add_extension('jinja2.ext.do')
    #app.jinja_env.add_extension('jinja2.ext.i18n')

    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    ######################
    # TEMPLATE FILTERS   #
    ######################    
    from .core import template_filters    

def setup_extensions(app):

    @app.teardown_appcontext
    def shutdown_session(response_or_exc=None):
        if db.session:        
            db.session.remove()
        return response_or_exc    

    form_checker.init_app(app)

    jwt.init_app(app)    
    jwt._set_error_handler_callbacks(app)
    
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    @login_manager.user_loader
    def load_user(id):
        return db.user.get_by_id(id)    

    webpack.init_app(app)

    mail.init_app(app)

    db.init_app(app)

def setup_error_pages(app):    

    if not app.config['DEBUG']:
        @app.errorhandler(SQLAlchemyError)
        def db_error(e):
            return render_template('errors/500.html'), 500
    

    @app.errorhandler(404)
    def page_not_found(e):        
        return render_template('errors/404.html'), 404

    @app.errorhandler(500)
    def server_error(e):
        return render_template('errors/500.html'), 500

    @app.errorhandler(403)
    def page_forbidden(e):
        return render_template('errors/403.html'), 403

    @app.errorhandler(401)
    def page_unauthorized(e):
        return render_template('errors/401.html'), 401

    @app.errorhandler(405)
    def method_not_allowed(e):
        return render_template('errors/405.html'), 405


def setup_babel(app):
    babel.init_app(app)

    # This remove lange_code from URL and add to values
    @app.url_value_preprocessor
    def pull_lang_code(endpoint, values):
        return
        #if values is not None:
        #    #print('values', values)
        #    g.lang_code = values.pop('lang_code', None)
        #    #print('g', g, ' and lang_code', g.lang_code)
        #    #g.lang_code = request.cookies['lang_code']

    @app.url_defaults
    def set_language_code(endpoint, values):
        return
        #if request.view_args and 'lang_code' in request.view_args:
        #print ('request:', request, ' g ', g)
        #if 'lang_code' in values or not g or not g.lang_code:
        #    #values.pop('lang_code')
        #    return
        ##else:
        #if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):  
        #    values['lang_code'] = g.lang_code
            
    # Last step before request reaching view
    @app.before_request
    def ensure_lang_support():
        pass
        # do nothing?

        #lang_code = g.get('lang_code', 'en')
        #if lang_code and lang_code not in constants.SUPPORT_LOCALES:
        #    # Switch to english
        #    g.lang_code = 'en'
            #return abort(404)

    def to_locale(language, to_lower=False):
        """
        Turns a language name (en-us) into a locale name (en_US). If 'to_lower' is
        True, the last component is lower-cased (en_us).
        """
        p = language.find('-')
        if p >= 0:
            if to_lower:
                return language[:p].lower()+'_'+language[p+1:].lower()
            else:
                # Get correct locale for sr-latn
                if len(language[p+1:]) > 2:
                    return language[:p].lower()+'_'+language[p+1].upper()+language[p+2:].lower()
                return language[:p].lower()+'_'+language[p+1:].upper()
        else:
            return language.lower()

    accept_language_re = re.compile(r'''
        ([A-Za-z]{1,8}(?:-[A-Za-z]{1,8})*|\*)         # "en", "en-au", "x-y-z", "*"
        (?:\s*;\s*q=(0(?:\.\d{,3})?|1(?:.0{,3})?))?   # Optional "q=1.00", "q=0.8"
        (?:\s*,\s*|$)                                 # Multiple accepts per header.
        ''', re.VERBOSE)


    def parse_http_accept_language(accept):
        for accept_lang, unused in parse_accept_lang_header(accept):
            if accept_lang == '*':
                break

            # We have a very restricted form for our language files (no encoding
            # specifier, since they all must be UTF-8 and only one possible
            # language each time. So we avoid the overhead of gettext.find() and
            # work out the MO file manually.

            # 'normalized' is the root name of the locale in POSIX format (which is
            # the format used for the directories holding the MO files).
            normalized = locale.locale_alias.get(to_locale(accept_lang, True))
            if not normalized:
                continue
            # Remove the default encoding from locale_alias.
            normalized = normalized.split('.')[0]

            for lang_code in (accept_lang, accept_lang.split('-')[0]):
                lang_code = lang_code.lower()                
                if lang_code in constants.SUPPORT_LOCALES:
                    return lang_code
        return None


    def parse_accept_lang_header(lang_string):
        """
        Parses the lang_string, which is the body of an HTTP Accept-Language
        header, and returns a list of (lang, q-value), ordered by 'q' values.
        Any format errors in lang_string results in an empty list being returned.
        """
        result = []
        pieces = accept_language_re.split(lang_string)
        if pieces[-1]:
            return []
        for i in range(0, len(pieces) - 1, 3):
            first, lang, priority = pieces[i : i + 3]
            if first:
                return []
            priority = priority and float(priority) or 1.0
            result.append((lang, priority))
        result.sort(key=lambda k: k[1], reverse=True)
        return result

    @babel.localeselector
    def get_locale():        
        if 'lang_code' in request.cookies:
            lang = request.cookies['lang_code']
            if lang in constants.SUPPORT_LOCALES:
                return lang
        return 'en'

    @babel.timezoneselector
    def get_timezone():
        user = g.get('user', None)
        if user is not None:
            return user.timezone


def setup_humanize(app):
    humanize.init_app(app)    
    
    @humanize.localeselector
    def get_locale():        
        if 'lang_code' in request.cookies:
            lang = request.cookies['lang_code']
            if lang in constants.SUPPORT_LOCALES:
                return lang
        return 'en'


def setup_api(app):

    from .api.v1 import bp as api_v1

    @api_v1.errorhandler(SQLAlchemyError)
    def api_db_error(e):
        return utils.api_output(error=str(e), code=500)

    app.register_blueprint(api_v1)



def setup_views(app):

    from .views import (base, hello, static_pages, auth, home, account, company, company_report,
                        product, rfq, quote, search, messages, media, support_ticket, 
                        membership, notification, admin, trade_lead)
    
    app.register_blueprint(base.bp)
    app.register_blueprint(hello.bp)
    app.register_blueprint(static_pages.bp)
    app.register_blueprint(auth.bp)
    app.register_blueprint(account.bp)
    app.register_blueprint(home.bp)
    app.register_blueprint(search.bp)
    app.register_blueprint(company.bp)
    app.register_blueprint(product.bp)
    app.register_blueprint(rfq.bp)
    app.register_blueprint(quote.bp)
    app.register_blueprint(messages.bp)
    app.register_blueprint(media.bp)
    app.register_blueprint(support_ticket.bp)
    app.register_blueprint(admin.bp)
    app.register_blueprint(company_report.bp)
    app.register_blueprint(notification.bp)
    app.register_blueprint(membership.bp)
    app.register_blueprint(trade_lead.bp)


def setup_sockets(app):

    from flask_sockets import Sockets

    sockets = Sockets(app)

    from .ws import bp as ws_bp
    
    sockets.register_blueprint(ws_bp)

def setup_celery(app):

    from .jobs import emails, test_job, img_processing

    TaskBase = CLR.Task

    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    CLR.Task = ContextTask

def setup_js_variables(app):
    pass

def setup_cli(app):
    from web.commands import emails

    app.cli.add_command(emails.cli)