import time
from flask import current_app as app
from web import db, CLR

@CLR.task
def add(a,b):    
    return a + b

@CLR.task
def db_get_user(user_id):
    user = app.db.user.get_by_id(user_id)
    return user.to_dict()