import time
from PIL import Image
import io
import os
from contextlib import contextmanager
from flask import current_app as app
from sqlalchemy import event

from web import db, ds, CLR

@contextmanager
def session_scope():

    session = db.session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()    

@CLR.task
def create_thumbnail(media_id):
    with session_scope() as session:
        query = session.query(db.media.Model).filter_by(id=media_id)
        trials = 10
        while not query.count() and trials > 0:
            trials -= 1
            time.sleep(0.1)
            query = session.query(db.media.Model).filter_by(id=media_id)
        
        try:
            media = query.one()
            img_data = io.BytesIO(ds.read_obj(media.folder, media.name))    
            img = Image.open(img_data)    
            ratio = lambda basew: basew/float(img.size[0])
            sizes = [180, 360, 540]

            thumbnails = {}

            for w in sizes:
                h = int(float(img.size[1]) * float(ratio(w)))
                new_img = img.resize((w, h), Image.ANTIALIAS)
                
                thumbnail_name = f'{media.name}_thumbnailw{w}.{img.format}'
                thumbnail_path = os.path.join(ds.UPLOAD_FOLDER, media.folder, thumbnail_name)

                new_img.save(thumbnail_path)
                thumbnails[w] = ds.url(media.folder, thumbnail_name)
                media.update(thumbnails=thumbnails)
        except:
            pass

    return f'Done creating thumbnail MEDIA {media_id}'


@event.listens_for(db.media.Model, 'after_insert')
def db_on_new_media_create_thumbnail(cls, conn, target):
    create_thumbnail.delay(target.id)