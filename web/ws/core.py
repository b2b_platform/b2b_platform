import gevent
import ujson
from contextlib import contextmanager
from web import r, db

class MSG:

    __slots__ = ['msg_type', 'data']

    def __init__(self, msg_type, data):
        self.msg_type = msg_type
        self.data = data

@contextmanager
def session_scope():
    session = db.session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

class RedisClient(object):

    __slots__ = ['channels', 'socket', 'user_id', 'ps', '_gls']

    def __init__(self, socket, user_id, channels=None, gls=None, funcs=None):
        self.channels = channels or []
        self.socket = socket
        self.user_id = user_id

        self.ps = r.pubsub()
        self.ps.subscribe(*self.channels)

        self._gls = gls or []
        funcs = funcs or []

        self._gls += [gevent.spawn(func) for func in funcs]

    def add_func(self, func, *args, **kwargs):
        self._gls.append(gevent.spawn(func, *args, **kwargs))

    def add_gl(self, gl):
        self._gls.append(gl)

    def send(self, msg):
        self.socket.send(ujson.dumps(msg))
    
    def send_data(self, msg_type, data):
        msg = MSG(msg_type, data)
        self.socket.send(ujson.dumps(msg))
    
    def start(self):
        for gl in self._gls:
            gl.start()

    def stop(self):
        gevent.killall(self._gls)
        self.ps.unsubscribe()   
        