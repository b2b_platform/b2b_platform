from flask import Blueprint
bp = Blueprint('ws', __name__, url_prefix='/ws')

from .messages import *