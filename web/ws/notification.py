import pickle
from datetime import datetime
from flask_login import login_required, current_user
from geventwebsocket.exceptions import WebSocketError
import ujson
import gevent
from sqlalchemy import event

from .core import MSG, RedisClient
from . import bp
from web import r, db, form_checker
from .constants import *

CHANNEL_NAME = 'USER_NOTIFICATIONS_{user_id}'

def notify(user_id, ntype, ndata):
    cname = CHANNEL_NAME.format(user_id=user_id)
    msg = pickle.dumps(MSG(ntype, ndata))
    #print('PUBLISH REDIS', ntype, cname)
    r.publish(cname, msg)

def notify_new_msg(user_id, thread_id):
    data = {'thread_id': thread_id}
    notify(user_id, NOTIFY_NEW_MSG, data)

def notify_new_notification(user_id, notification_id, notification_type):
    data = {'notification_id': notification_id}
    notify(user_id, NEW_NOTIFICATION, data)

def notify_read_thread(user_id, thread_id, dt):
    data = {'thread_id': thread_id, 'dt': dt}    
    notify(user_id, NOTIFY_READ_THREAD, data)

def notify_read_notification(user_id, dt):
    data = {'dt': dt}
    notify(user_id, READ_NOTIFICATION, data)

def listen_notifications(client):
    try:
        while True:
            for m in client.ps.listen():
                if m['type'] == 'message':                    
                    msg = pickle.loads(m['data'])
                    #print('GOT REDIS MESSAGE', msg.msg_type, msg.data)
                    client.send(msg)
    except:
        pass

@bp.route('/notification')
@login_required
def ws_notify(socket):
    channel = CHANNEL_NAME.format(user_id=current_user.id)
    client = RedisClient(socket, current_user.id, channels=[channel])
    client.add_func(listen_notifications, client)
    handle(client)

def handle(client):
    on_open(client)

    try:
        while True:
            message = client.socket.receive()
            on_message(client, message)
        client.socket.close()
    except WebSocketError:
        pass
    finally:
        on_close(client)

def on_open(client):
    db.remove_session()
    client.start()

def on_close(client):
    client.stop()    

    if not client.socket.closed:
        client.socket.close()

    db.remove_session()

    del client

def on_message(client, message):
    try:
        msg = ujson.loads(message)
        msg_type = msg.get('msg_type', None)
        data = form_checker.clean_dict(msg.get('data', None))        

        # ALWAYS HANDLE MESSAGE IN GREEN THREADS !
        gevent.spawn(WEBSOCKET_MESSAGE_HANDLERS[msg_type], client, data)

    except (KeyError, ValueError,TypeError):
        pass

WEBSOCKET_MESSAGE_HANDLERS = {
}

##########################
## CONNECT TO DB SIGNALS #
##########################

@event.listens_for(db.notification.NotificationUserReadTime.last_read_time, 'set')
def db_on_set_notification_last_read(target, value, old_value, initiator):
    user_id = target.user_id
    dt = value
    gevent.spawn(notify_read_notification, user_id, dt)

# Notifications
@event.listens_for(db.notification.NotificationCompany, 'after_insert')
def db_on_set_notification_company(cls, conn, target):
    notification_id = target.id
    notification_type = target.notification_type
    users = []

    if target.company is None:
        users = db.company.get_by_id(target.company_id).users
    # Delay against spam?
    for u in users:
        gevent.spawn(notify_new_notification, u.id, notification_id, notification_type)

@event.listens_for(db.notification.NotificationUser, 'after_insert')
def db_on_set_notification_user(cls, conn, target):
    notification_id = target.id
    notification_type = target.notification_type
    user_id = target.user_id
    
    gevent.spawn(notify_new_notification, user_id, notification_id, notification_type)