from flask import Blueprint, request
from flask_login import login_required, current_user
from geventwebsocket.exceptions import WebSocketError
import ujson
import gevent
import logging
import requests
import pickle
from sqlalchemy import event
from datetime import datetime

from . import bp
from .core import MSG, RedisClient, session_scope
from web import db, form_checker, r
from .notification import notify_new_msg, notify_read_thread, notify_new_notification, notify_read_notification
from .constants import *

REDIS_THREAD_META_SECS = 60*2 #2 MIN
REDIS_THREAD_META_KEY = 'THREAD_META_{thread_id}'

def r_broadcast(thread_id, data):
    msg = pickle.dumps(data)
    r.publish(thread_id, msg)

def r_recv(client):
    try:
        while True:
            for m in client.ps.listen():
                if m['type'] == 'message':                    
                    msg = pickle.loads(m['data'])
                    client.send(msg)
    except:
        pass
        
@bp.route('/messages')
@login_required
def ws_messages(socket):

    args = form_checker.process(request)
    thread_id = args.pop('thread_id', None)

    if not thread_id:
        socket.close()
        return

    thread = db.thread.get_by_id(thread_id)

    if current_user.id not in thread.users_by_id:
        socket.close()
        return
        
    client = RedisClient(socket, current_user.id, channels=[thread_id])
    client.add_func(r_recv, client)
    handle(client)

# TODO: DO THIS ONCE FOR ALL CLIENTS
def heartbeat(user_id, thread_id):
    while True:        
        with session_scope() as session:
            db.thread.set_user_last_active(user_id, thread_id)
        gevent.sleep(1)

def handle(client):    
    on_open(client)

    try:
        while True:
            message = client.socket.receive()
            on_message(client, message)
        client.socket.close()
    except WebSocketError:
        pass
    finally:    
        on_close(client)
        

def on_open(client):
    # close db connection - websocket no need long handing db connection?    
    db.remove_session()
    client.start()    
                
def on_close(client): 
    client.stop()    

    if not client.socket.closed:
        client.socket.close()

    db.remove_session()

    del client

def on_message(client, message):

    try:
        msg = ujson.loads(message)
        msg_type = msg.get('msg_type', None)
        data = form_checker.clean_dict(msg.get('data', None))        

        # ALWAYS HANDLE MESSAGE IN GREEN THREADS !
        gevent.spawn(WEBSOCKET_MESSAGE_HANDLERS[msg_type], client, data)

    except (KeyError, ValueError,TypeError):
        pass

def new_msg_handler(client, data):
    with session_scope() as session:
        item = db.message.add(**data)                

def pin_msg_handler(client, data):
    with session_scope() as session:
        message_id = data.get('message_id', None)
        pinned_by = data.get('pinned_by', None)
        is_pinned = data.get('is_pinned', False)        
        item = db.message.update_by_id(message_id, is_pinned=is_pinned, pinned_by=pinned_by)        

def read_notification_handler(client, data):
    user_id = data.get('user_id', None)
    with session_scope() as session:
        item = db.notification.set_user_last_active(user_id)

def read_msg_handler(client, data):
    user_id = data.get('user_id', None)
    thread_id = data.get('thread_id', None)
    dt = datetime.utcnow()
    with session_scope() as session:
        item = db.thread.set_user_last_active(user_id, thread_id)

def pingpong_handler(client, data):
    msg = MSG(PONG, None)
    client.send(msg)

WEBSOCKET_MESSAGE_HANDLERS = {
    NEW_MSG: new_msg_handler,
    PIN_MSG: pin_msg_handler,    
    READ_MSG: read_msg_handler,
    PING: pingpong_handler
}

##########################
## CONNECT TO DB SIGNALS #
##########################

@event.listens_for(db.message.Model, 'after_insert')
@event.listens_for(db.message.MediaMessage, 'after_insert')
def db_on_new_message(cls, conn, target):    
    if target.user is None:
        # load user object
        target.user = db.user.get_by_id(target.user_id)
    
    item = target.to_dict()
    thread_id = target.thread_id
    msg = MSG(NEW_MSG, item)    
    gevent.spawn(r_broadcast, thread_id, msg)

    ##### NOTIFY USERS IN THREADS
    rkey = REDIS_THREAD_META_KEY.format(thread_id=thread_id)
    thread_meta = r.get(rkey)

    if thread_meta:
        thread_meta = ujson.loads(thread_meta)
    else:
        thread = db.thread.get_by_id(thread_id)

        thread_meta = {
            'users': thread.users_by_id,
            'archived_list': [u.id for u in thread.archived_by]
        }
        r.set(rkey, ujson.dumps(thread_meta), ex=REDIS_THREAD_META_SECS)  
    
    for uid in thread_meta['users']:
        if uid not in thread_meta['archived_list']:
            gevent.spawn(notify_new_msg, uid, thread_id)

@event.listens_for(db.message.Model, 'after_update')
@event.listens_for(db.message.MediaMessage, 'after_update')
def db_on_update_message(cls, conn, target):
    if target.user is None:
        # load user object
        target.user = db.user.get_by_id(target.user_id)
    
    item = target.to_dict()
    thread_id = target.thread_id
    msg = MSG(UPDATE_MSG, item)
    
    gevent.spawn(r_broadcast, thread_id, msg)

@event.listens_for(db.thread.UserToMessageThread.last_active_time, 'set')
def db_on_set_thread_last_active(target, value, old_value, initiator):
    user_id = target.user_id
    thread_id = target.message_thread_id
    dt = value
    gevent.spawn(notify_read_thread, user_id, thread_id, dt)