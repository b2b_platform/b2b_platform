from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort, flash, send_file
from flask_login import login_required, current_user, login_user
from werkzeug import secure_filename
from io import BytesIO
import xlsxwriter
from flask import current_app as app
from flask_babel import lazy_gettext

import database.models as models
from web import db, form_checker, role_manager, membership_manager, ds
from web.core import constants, geo
import database.enums as db_enums
from role import role_constants
from membership import membership_constants

bp = Blueprint('membership', __name__)

# Disabled - not allowing creating new company at the moment
# HShould be andled instead by views in register

@bp.route('/membership-info')
def membership_info():
    return render_template('membership/membership_info.html', 
        membership_tiers=membership_constants.TIER_LIMITS)


# Company associated to the current user
@bp.route('/my-company-membership')
@login_required
def my_company_membership():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    
    if not user.company_id: 
        # No company associated with current user
        return redirect(url_for('company.link_my_company'))
    
    # Obtaining company information
    company = user.company
    previous_memberships = membership_manager.get_all_memberships(user.company_id)
    return render_template('membership/membership_my_company.html', 
        company=company,
        previous_memberships=previous_memberships)

