from flask import render_template, Blueprint

bp = Blueprint('static_pages', __name__)

@bp.route('/privacy')
def privacy_page():
    return render_template('static_pages/privacy.html')

@bp.route('/terms-of-service')
def tos_page():
    return render_template('static_pages/tos.html')

@bp.route('/about')
def about_page():
    return render_template('static_pages/about.html')

@bp.route('/team')
def team_page():
    return render_template('static_pages/team.html')

@bp.route('/careers')
def careers():
    return render_template('static_pages/careers.html')

@bp.route('/contact')
def contact():
    return render_template('static_pages/contact.html')

@bp.route('/help')
def help():
    return render_template('static_pages/help.html')

@bp.route('/learning-center')
def learning_center():
    return render_template('static_pages/learning_center.html')

@bp.route('/sitemap')
def sitemap():
    return render_template('static_pages/sitemap.html')    

@bp.route('/company-verification')
def company_verification():
    return render_template('static_pages/company_verification.html')

@bp.route('/inspection')
def inspection_services():
    return render_template('static_pages/inspection_services.html')

@bp.route('/policy-and-rules')
def policy_and_rules():
    return render_template('static_pages/policy_and_rules.html')

@bp.route('/report-abuse')
def report_abuse():
    return render_template('static_pages/report_abuse.html')