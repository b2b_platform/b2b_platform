from flask import Blueprint, render_template, abort, request
from flask_login import current_user, login_required

from web import db, form_checker, role_manager
from web.core import constants

bp = Blueprint('trade_lead', __name__)

@bp.route('/create-trade-lead', methods=['GET'])
@login_required
def create_trade_lead():
    args = form_checker.process(request)
    product = args.get('product', '')    
    quantity = ''
    if args.get('quantity', None):
        quantity += f'{args.get("quantity")} '
    
    if args.get('unit', None):
        quantity += args.get('unit')
    
    return render_template('trade_lead/create_trade_lead.html', product=product, quantity=quantity)

@bp.route('/manage-trade-leads')
@login_required
def manage_trade_lead():
    trade_leads = db.trade_lead.get_by_user_id(current_user.id)    
    return render_template('/trade_lead/manage_trade_leads.html', trade_leads=trade_leads)

@bp.route('/trade-leads/<pr_id>')
@login_required
def trade_lead_page(pr_id):
    trade_lead = db.trade_lead.get_by_id(pr_id)

    if not trade_lead:
        return abort(404)
    
    is_owner = False
    if current_user.is_authenticated and trade_lead.user_id == current_user.id:
        is_owner = True

    return render_template('/trade_lead/trade_lead.html', 
                            trade_lead=trade_lead, 
                            is_owner=is_owner)

@bp.route('/update-trade-lead/<pr_id>')
@login_required
def update_trade_lead(pr_id):
    trade_lead = db.trade_lead.get_by_id(pr_id)

    if not trade_lead:
        return abort(404)

    if current_user.id != trade_lead.user_id:
        return abort(403)
    
    return render_template('/trade_lead/update_trade_lead.html', trade_lead=trade_lead)

@bp.route('/trade-leads')
def listing():

    args = form_checker.process(request, 'search_trade_lead')
    keyword = args.pop('keyword', '')
    category = args.pop('category', None)
    viewtab = args.pop('viewtab', 'all')
    lead_type = None
    if viewtab == 'buy':
        lead_type = constants.LEAD_TYPE_BUY
    elif viewtab == 'sell':
        lead_type = constants.LEAD_TYPE_SELL

    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1)) 
    

    if lead_type:
        num_pages, trade_leads = db.trade_lead.search(keyword=keyword, category=category, 
                                    limit=limit, current_page=current_page, lead_type=lead_type)
    else:
        num_pages, trade_leads = db.trade_lead.search(keyword=keyword, category=category, 
                                    limit=limit, current_page=current_page)

    categories = [('', 'All')] + (db.query(db.product_category.Model.id, db.product_category.Model.name)
                    .filter(db.product_category.Model.parent_category_id == None).all())    

    #trade_leads = [ lead.to_dict() for lead in trade_leads ]

    return render_template('/trade_lead/listing.html',
                            keyword=keyword,
                            category=category,
                            categories=categories,
                            current_page=current_page,
                            num_pages=num_pages,
                            trade_leads=trade_leads,
                            viewtab=viewtab)