from flask import Blueprint, render_template, redirect, url_for, request, abort, flash, send_file
from flask_login import login_required, current_user
from flask import current_app as app
from flask_babel import lazy_gettext
import ujson
from werkzeug import secure_filename
from io import BytesIO
import xlsxwriter
import string

from web import db, form_checker, role_manager, ds
from web.core import constants, geo
from role import role_constants

bp = Blueprint('product', __name__)

# Create a new product
@bp.route('/create-product')
@login_required
def create_product():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)

    if not user.company_id:
        return redirect(url_for('company.link_my_company'))

    # TBI: needs to check for Company or Organization or not enforce restriction
    if not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_CREATE):
        return abort(401)

    return render_template('product/create_product.html')

@bp.route('/company-products/<id_slug(model="company"):pr_id>')
def company_products(pr_id=None):

    company = db.company.get_by_id(pr_id)

    if not company:
        return abort(404)
    
    args = form_checker.process(request, 'product_list_form')        

    limit = 20

    keyword = args.pop('keyword', '')
    category = args.pop('category', '')    
    current_page = int(args.pop('current_page', 1))
    sorted_by = 'product.' + args.pop('sorted_by', 'name')    
    view = args.pop('view', 'grid')

    if category:
        num_pages, products, num_results = db.product.search(keyword=keyword, current_page=current_page, sorted_by=sorted_by,
            company_id=company.id, product_category_id=category)
    else:
        num_pages, products, num_results = db.product.search(keyword=keyword, current_page=current_page, sorted_by=sorted_by,
            company_id=company.id)

    categories = set((db.query(db.product_category.Model.id, db.product_category.Model.name)
                    .join(db.product.Model)
                    .filter(db.product.Model.company_id == company.id)).all())
    #categories = (db.query(db.product_category.Model.id, db.product_category.Model.name)
    #                .filter(db.product_category.Model.parent_category_id == None).all())    
    #categories = (db.query(db.product_category.Model.id, db.product_category.Model.name)
    #                .join(db.product.Model)).all()
                  #  .filter(db.product.Model.company_id == company.id)).all()
    

    return render_template('product/company_products.html', 
                            keyword=keyword,
                            category=category,
                            categories=categories,
                            company=company,
                            products=products,
                            current_page=current_page,
                            num_pages=num_pages,
                            view=view)

# All products belonging to current company associated with users
@bp.route('/manage-products')
@login_required
def manage_products():
    user = db.user.get_by_id(current_user.id)
    if not user.company_id:
        return redirect(url_for('company.link_my_company'))
        
    args = form_checker.process(request, 'product_list_form')
    
    if not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_VIEW):
        return abort(403)

    limit = 20
    keyword = args.pop('keyword', '')    
    current_page = int(args.pop('current_page', 1))
    sorted_by = 'product.' + args.pop('sorted_by', 'name')
    view = args.pop('view', 'grid')
    

    num_pages, products, num_results = db.product.search(keyword=keyword, current_page=current_page,
        sorted_by=sorted_by, company_id=user.company_id, search_hidden=True)

    return render_template('product/manage_products.html',
                            products=products,
                            current_page=current_page,
                            num_pages=num_pages,
                            keyword=keyword,
                            view=view)

# Specific product ID - using ajax retrieval
@bp.route('/product/<id_slug(model="product"):pr_id>')
def product_page(pr_id=None):
    product = db.product.get_by_id(pr_id)

    if not product:
        return abort(404)    

    is_updatable = False
    if current_user.is_authenticated:
        is_updatable = current_user.company_id == product.company.id

        user = db.user.get_by_id(current_user.id)
        if product.company_id != user.company_id and (not product.company.is_public):
            return abort(404)

    else:
        if not product.company.is_public:
            return abort(404)

    return render_template('product/product.html', product=product, is_updatable=is_updatable)
    
@bp.route('/update-product/<id_slug(model="product"):pr_id>')
@login_required
def update_product(pr_id):
    user = db.user.get_by_id(current_user.id)
    
     # Obtain product info
    product = db.product.get_by_id(pr_id)

    if not product:
        return abort(404)

    if (product.company_id != user.company_id) and (not product.company.is_public):
        return abort(404)

    
    # Only loads the page if the product is editable by current user
    if (product.company_id != user.company_id or 
            not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_UPDATE) ):
        # TBI: Add product_id to check permission parameter - separate permission for users
        return abort(403)
    
    view = request.args.get('view', None)

    return render_template('product/update_product.html', view=view, product=product)

@bp.route('/import-products', methods=['GET', 'POST'])
@login_required
def import_products():
    if not current_user.company_id:
        return abort(500)

    company = db.company.get_by_id(current_user.company_id)
    if not company:
        return abort(404)

    if request.method == 'POST':

        # Have permission to access page
        if not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_COMPANY, 
                role_constants.ACTION_UPDATE):
            flash(lazy_gettext('Sorry, you do not have permission to update company. Please contact your manager or us for more details'), 'danger')
            return redirect(url_for("product.import_products"))

        args = form_checker.process(request)

        if not request.files.get('import_doc', None):
            flash(lazy_gettext('Please upload a spreadsheet using our template'))
            return redirect(url_for("product.import_products"))

        folder = f'company/{company.id}/import_products_docs'
        import_doc = request.files['import_doc']
        if import_doc.filename.split('.')[-1] not in ['xlsx', 'xls']:
            flash(lazy_gettext('Invalid file type'), 'danger')
            return redirect(url_for("product.import_products"))

        obj_name, obj_url = ds.save_obj(folder, import_doc)

        flash(lazy_gettext('You have successfuly uploaded the spreadsheet! We are processing your company products and will keep you in the loop.'), 'success')
        return redirect(url_for("product.import_products"))

    folder = f'company/{company.id}/import_products_docs'
    import_docs = ds.list_objs(folder)

    return render_template('product/import_products.html', import_docs=import_docs)

spreadsheet_columns = [    
    ('name', 'Name', 25),
    ('description', 'Description', 50),
    ('product_category', 'Product Category', 15),
    ('capacity', 'Capacity', 15),
    ('min_order_quantity', 'Min Order Quantity', 15),
    ('unit_type', 'Unit Type', 15),
    ('payment_options', 'Payment Options', 15),
    ('price_range_min', 'Minimum Price', 15),
    ('price_range_max', 'Maximum Price', 15),
    ('packaging', 'Packaging', 15),
    ('port', 'Port', 15),
    ('payment_options', 'Payment Options', 15),
    ('processing_time', 'Processing Time', 15)
]

@bp.route('/import-products/download-template')
@login_required
def download_import_template():
    if not current_user.company_id:
        return abort(500)

    company = db.company.get_by_id(current_user.company_id)
    if not company:
        return abort(404)

    first_level_categories = db.product_category.get_first_levels(nameonly=True)
    file_name = f'{app.config["APP_NAME"]}_import_products_template.xlsx'

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    header_format = workbook.add_format({
        'border': 1,
        'bg_color': '#C6EFCE',
        'bold': True,
        'text_wrap': True,
        'valign': 'vcenter',
        'indent': 1,
    })

    worksheet.set_column('U:U', 15)    
    worksheet.write('U1', 'Product Categories', header_format)
    for idx, (cat,) in enumerate(first_level_categories):
        row_idx = idx + 2
        worksheet.write(f'U{row_idx}', cat)

    worksheet.set_column('V:V', 15) 
    worksheet.write('V1', 'Measure Units', header_format)
    for idx, mu in enumerate(constants.MEASURE_UNITS):
        row_idx = idx + 2
        worksheet.write(f'V{row_idx}', mu)

    alphabets = list(string.ascii_uppercase)

    for idx, items in enumerate(spreadsheet_columns):
        field,label,width = items
        col = alphabets[idx]
        worksheet.set_column(f'{col}:{col}', width)
        worksheet.write(f'{col}1', label, header_format)

        if field == 'product_category':
            worksheet.data_validation(f'{col}2:{col}1048576', 
                {'validate': 'list', 'source': f'=$U$2:$U${len(first_level_categories)}'})

        elif field == 'unit_type':
            worksheet.data_validation(f'{col}2:{col}1048576', 
                {'validate': 'list', 'source': f'=$V$2:$V${len(constants.MEASURE_UNITS)}'})
        
        elif field in ['min_order_quantity', 'price_range_min', 'price_range_max']:
            worksheet.data_validation(f'{col}2:{col}1048576', 
                {'validate': 'float'})

        elif field == 'payment_options':
            worksheet.data_validation(f'{col}2:{col}1048576', 
                {'validate': 'list', 'source': list(constants.PAYMENT_TYPES)})

    workbook.close()
    output.seek(0)

    return send_file(
        output,
        attachment_filename=file_name,
        as_attachment=True
    )

@bp.route('/export-products')
@login_required
def export_products():
    user = db.user.get_by_id(current_user.id)
    if not user.company_id:
        return redirect(url_for('company.my_company'))

    return render_template('product/export_products.html')