from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort, flash, send_file
from flask_login import login_required, current_user, login_user
from werkzeug import secure_filename
from io import BytesIO
import xlsxwriter
from flask import current_app as app
from flask_babel import lazy_gettext

import database.models as models
from web import db, form_checker, role_manager, ds
from web.core import constants, geo
import database.enums as db_enums
from role import role_constants

bp = Blueprint('company', __name__)

# Disabled - not allowing creating new company at the moment
# HShould be andled instead by views in register

@bp.route('/create-company')
@login_required
def create_company():
    # Check if user already has a linked company
    #if current_user.is_authenticated:  # checked by decorator
    user = db.user.get_by_id(current_user.get_id())
    if user.company_id: # Company already associated with user
        # user.company_name is used for verification purpose
        return redirect(url_for('company.my_company'))
    
    return render_template('company/create_company.html')

# updating current company associated with user
@bp.route('/my-company/update')
@login_required
def update_company_rv():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    if not user.company_id: 
        # No company associated with current user
        return redirect(url_for('company.link_my_company'))

    # Have permission to access page
    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_COMPANY, 
                role_constants.ACTION_UPDATE):
        return redirect(url_for('company.my_company'))
    
    company_info = user.company.to_dict()

    return render_template('company/update_company_rv.html', my_company=company_info)

# Company associated to the current user
@bp.route('/my-company')
@login_required
def my_company():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    
    if not user.company_id: 
        # No company associated with current user
        return redirect(url_for('company.link_my_company'))
        
    # Obtaining company information
    company = user.company
    #company_info = company.to_dict()
    
    # Checking for editing permission
    company_is_editable = False

    if role_manager.check_permission(user_id, 
                role_constants.OPERATION_COMPANY, 
                role_constants.ACTION_UPDATE):
        company_is_editable = True
   
    # Obtaining employee lists
    #employees = company.users  
    #employee_list = [ employee.to_public_dict() for employee in employees ]    

    first_level_categories = db.product_category.get_first_levels()
  
    return render_template('company/my_company_rv.html', 
        company=company, 
        company_is_editable=company_is_editable,
        datatype="company",
        first_level_categories=first_level_categories)
        #, employee_list=employee_list)


# Specific company by ID with full rendering in template - grouped for public and internal views
@bp.route('/company/<id_slug(model="company"):pr_id>')
def company_page(pr_id):
    is_company_emp = False
    company = db.company.get_by_id(pr_id)
    if not company:
        return abort(404)

    if current_user.is_authenticated:
        user_id = current_user.get_id()
        user = db.user.get_by_id(user_id)
        if user.company_id == company.id:
            is_company_emp = True
        
    if (not company.is_public) and (not is_company_emp):
        return abort(404)

    args = form_checker.process(request)
    current_tab = args.get('tab', 'home')
    
    return render_template('company/company_rv.html', 
        company=company, current_tab=current_tab)


@bp.route('/change-company-role', methods=['GET'])
@login_required
def change_company_role():
    return render_template('company/change_company_role.html')


@bp.route('/link-my-company')
@login_required
def link_my_company():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)

    if role_manager.is_master(user_id):
        return redirect(url_for('admin.user_master'))

    if user.company_id:
        return redirect(url_for('company.my_company'))

    args = form_checker.process(request, 'company_search_link_form')

    ref_company_id = args.pop('cid', None)
    ref_user_id = args.pop('uid', None)
    # TBI do something with ref_user_id? Notify when user joined
  
    if ref_company_id:
        # There are referrals
        if user.company_id: # User already in a company            
            if int(ref_company_id) == user.company_id:
                # Redirect to its own company
                return redirect(url_for('company.my_company'))
            else:
                # Leave old company and go to a new one
                company_old = db.company.get_by_id(user.company_id).to_public_dict()

                list_companies = [ (db.company.get_by_id(ref_company_id).to_public_dict()) ]
                
                return render_template('company/link_my_company.html', list_companies=list_companies,
                    current_page=1,
                    num_results=1,
                    num_pages=1, 
                    company_old=company_old)

        else:
            list_companies = [ (db.company.get_by_id(ref_company_id).to_public_dict()) ]
            return render_template('company/link_my_company.html', list_companies=list_companies,
                current_page=1,
                num_results=1,
                num_pages=1)
    # No referral or existing company case

    # Add form checker
    # Default type
    limit = int(args.pop('limit', 10))
    current_page = int(args.pop('current_page', 1))
    search_term = args.pop('search_term', '')

    list_companies = []
    if search_term.isdigit():
        # Search based on id
        try:
            list_companies = [ db.company.get_by_id(int(search_term)).to_public_dict() ]
        except ValueError:
            pass

    num_pages, companies, num_results = db.company.search(keyword=search_term, limit=limit, 
            current_page=current_page)
    
    list_companies = list_companies + [ comp.to_public_dict() for comp in companies ]
    pending_request = None

    # Obtain user's pending request
    if user.company_name:
        existing = db.role.filter_by(user_id=user_id, 
            operation=role_constants.OPERATION_GENERAL,
            permission=role_constants.REQUEST_PENDING)
            # Currently overwriting request
        pending_request = existing if existing else None


    return render_template('company/link_my_company.html', list_companies=list_companies,
        current_page=current_page,
        num_results=num_results,
        num_pages=num_pages,
        pending_request=pending_request)
    
    
@bp.route('/my-company-employees')
@login_required
def get_employee_list():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    
    if not user.company_id: 
        # No company associated with current user
        return redirect(url_for('company.link_my_company'))
        
    # Checking for editing permission
    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_USER_MANAGEMENT, 
                role_constants.ACTION_VIEW):
        return abort(403) # Access denied
    
    # Obtaining employee lists
    company = user.company
    #employees = company.users 

    pending_requests = []
    
    employees_list = db.role.filter_by_q(company_id=user.company_id,
                    operation=role_constants.OPERATION_GENERAL).filter(
                    models.UserCompanyRole.permission >= role_constants.PERMISSION_ROLE_USER)
    employees = [ emp.to_user_dict() for emp in employees_list]
    
    # Limit permission levels
    current_user_permission = role_manager.get_permission_level(user_id)
    if current_user_permission >= role_constants.PERMISSION_ROLE_MANAGER:
        current_user_permission == role_manager.check_permission(user_id, 
                role_constants.OPERATION_USER_MANAGEMENT,
                role_constants.ACTION_UPDATE)
        # Obtain only verified email
        #pending_requests = (db.user.query().filter_by(company_name=str(company.id),company_id=None,is_account_confirmed=True)
        #)
        #pending_requests = (db.user.query().filter_by(company_name=str(company.id),company_id=None,is_account_confirmed=True)
        #        .join(models.UserCompanyRole).group_by(models.User))#.order_by(models.UserCompanyRole.create_time))
        pending = db.role.filter_by_q(company_id=user.company_id,operation=role_constants.OPERATION_GENERAL,
                permission=role_constants.REQUEST_PENDING).order_by('user_company_role.create_time desc')
                #.join(models.User).group_by(models.User)
        pending_requests = [ (req.user, req.create_time) for req in pending ]

    #employee_list = [ employee.to_public_dict() for employee in employees ]    
    return render_template('company/my_company_employees_rv.html', 
            company=company, 
            employees=employees, 
            permission_levels=role_constants.PERMISSION_TEXT_ARR,
            current_user_permission=current_user_permission,
            pending_requests=pending_requests)
    
    
@bp.route('/verify-company')
@login_required
def verify_company():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    company = user.company
    company_id = None
    verified = False

    if company:
        company_id = company.id
    else:
        company_id = user.company_name # user that have submitted link request
    
    if not company_id:
        return redirect(url_for('company.link_my_company'))

    company_verify = db.company_verify.filter_by(company_id=company_id)

    submitted_by_user = False # Check if current user has ever submitted a verify request
    # There could be other requests by other users in the same company

    for ver in company_verify:
        if ver.user_id == user_id:
            submitted_by_user = True

    if company and company.is_verified:
        verified = True

    return render_template('company/verify_company.html', 
                company=company,
                company_verify=company_verify,
                verified=verified,
                submitted_by_user=submitted_by_user,
                datatype='company')


@bp.route('/change_membership')
@login_required
def change_membership():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)

    company_is_editable = False

    # Check for privilege - only admin and manager can update
    if role_manager.check_permission(user_id, 'company','update_account'):
        company_is_editable = True

    return render_template('company/change_membership.html', 
                company=user.company, 
                company_is_editable=True)


@bp.route('/company-import-export')
def import_export():
    user_id = current_user.get_id()
    user = db.user.get_by_id(user_id)
    
    if not user.company_id: 
        # No company associated with current user
        return redirect(url_for('company.link_my_company'))
        
    # Obtaining company information
    company = user.company

    folder = f'company/{company.id}/import_company_docs'
    import_docs = ds.list_objs(folder)
    
    return render_template('company/my_company_import_export.html', company=company, import_docs=import_docs)
        #, employee_list=employee_list)


### TBI: lazy_gettext() for all field descriptions
spreadsheet_columns = [
    ('name', 'Name', 'Your company name'),
    ('keywords_as_str', 'Keywords', 'Business keywords that best describe your company. Separated by commas'),
    ('company_types', 'Business Type', 'Your company business type, e.g: manufacturer, wholesaler etc'),    
    ('main_category', 'Main Category', "Please select your company's main category"),
    ('secondary_category', 'Secondary Category', "Please select your company's secondary category"),
    ('website', 'Website', None),
    ('description', 'Short Description', 'A short (less than 1000 characters description'),
    ('company_info', 'Company Information', 'Your company information in details'),
    ('location', 'Location', 'Your company main address'),
    ('country', 'Country', None),
    ('phone', 'Phone', 'Your company phone, including country and area code'),
    ('fax', 'Fax', None),
    ('email', 'Email', 'Your company email'),
    ('year_founded', 'Year Founded', None),
    ('company_size', 'Company Size', 'Number of employees'),
    ('capacity', 'Capacity', 'Your company annual capacity'),
    ('total_revenue', 'Total Annual Revenue', None),
    ('total_purchase_volume', 'Total Purchase Volume', None),
    ('main_markets', 'Main Markets', None),
    ('shipping_handling', 'Shipping & Handling', None),
    ('port', 'Shipping Port', None),
    ('payment', 'Payment Options', None),
    ('mgnt_certifications', 'Management Certification', None),
    ('wechat', 'Wechat', None),
    ('whatsapp', 'Whatsapp', None),
    ('skype', 'Skype', None),
    ('viber', 'Viber', None)
]

@bp.route('/company-import-export/download-template')
@login_required
def download_import_template():
    if not current_user.company_id:
        return abort(500)

    company = db.company.get_by_id(current_user.company_id)
    if not company:
        return abort(404)

    first_level_categories = db.product_category.get_first_levels(nameonly=True)

    file_name = f'{app.config["APP_NAME"]}_import_company_template.xlsx'

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    # Add a format for the header cells.
    header_format = workbook.add_format({
        'border': 1,
        'bg_color': '#C6EFCE',
        'bold': True,
        'text_wrap': True,
        'valign': 'vcenter',
        'indent': 1,
    })

    # Set up layout of the worksheet.
    worksheet.set_column('A:A', 22)
    worksheet.set_column('B:B', 70)
    worksheet.set_column('C:C', 30)
    worksheet.set_column('K:K', 15)
    worksheet.set_column('L:L', 15)    

    heading1 = 'Field name'
    heading2 = 'Enter values this column'
    heading3 = 'Help'

    heading4 = 'Countries'
    heading5 = 'Categories'

    worksheet.write('A1', heading1, header_format)
    worksheet.write('B1', heading2, header_format)
    worksheet.write('C1', heading3, header_format)

    worksheet.write('K1', heading4, header_format)
    worksheet.write('L1', heading5, header_format)

    for idx, country in enumerate(geo.countries_list):
        row_idx = idx + 2
        worksheet.write(f'K{row_idx}', country)

    for idx, (cat,) in enumerate(first_level_categories):
        row_idx = idx + 2
        worksheet.write(f'L{row_idx}', cat)    

    for idx,items in enumerate(spreadsheet_columns):
        field,label,helptxt = items
        row_idx = idx + 2
        worksheet.write(f'A{row_idx}', label)
        worksheet.write(f'C{row_idx}', helptxt)

        if field == 'main_category' or field == 'secondary_category':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': f'=$L$2:$L${len(first_level_categories)}'})
        
        elif field == 'country':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': f'=$K$2:$K${len(geo.countries_list)+1}'})
        
        elif field == 'total_revenue':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': list(db_enums.total_revenue)})

        elif field == 'company_size':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': list(db_enums.company_sizes)})

    workbook.close()
    output.seek(0)

    return send_file(
        output,
        attachment_filename=file_name,
        as_attachment=True
    )


@bp.route('/company-import-export/import-profile', methods=['POST'])
@login_required
def import_profile():
    if not current_user.company_id:
        return abort(500)

    company = db.company.get_by_id(current_user.company_id)
    if not company:
        return abort(404)

    # Have permission to access page
    if not role_manager.check_permission(current_user.id, 'company', 'update'):
        flash(lazy_gettext('Sorry, you do not have permission to update company profile. Please contact your manager or us for more details'), 'danger')
        return redirect(url_for("company.import_export"))

    args = form_checker.process(request)

    if not request.files.get('import_doc', None):
        flash(lazy_gettext('Please upload a spreadsheet using our template'))
        return redirect(url_for("company.import_export"))

    folder = f'company/{company.id}/import_company_docs'
    import_doc = request.files['import_doc']
    if import_doc.filename.split('.')[-1] not in ['xlsx', 'xls']:
        flash(lazy_gettext('Invalid file type'), 'danger')
        return redirect(url_for("company.import_export"))

    obj_name, obj_url = ds.save_obj(folder, import_doc)

    flash(lazy_gettext('You have successfuly uploaded the spreadsheet! We are processing your company and will keep you in the loop.'), 'success')
    return redirect(url_for("company.import_export"))

@bp.route('/company-import-export/export-profile', methods=['POST'])
@login_required
def export_profile():
    if not current_user.company_id:
        return abort(500)

    company = db.company.get_by_id(current_user.company_id)
    if not company:
        return abort(404)

    first_level_categories = db.product_category.get_first_levels(nameonly=True)

    file_name = f'{company.name}_profile.xlsx'

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    # Add a format for the header cells.
    header_format = workbook.add_format({
        'border': 1,
        'bg_color': '#C6EFCE',
        'bold': True,
        'text_wrap': True,
        'valign': 'vcenter',
        'indent': 1,
    })

    # Set up layout of the worksheet.
    worksheet.set_column('A:A', 22)
    worksheet.set_column('B:B', 70)
    heading1 = 'Field name'
    heading2 = 'Field value'
    heading4 = 'Countries'
    heading5 = 'Categories'
    
    worksheet.write('A1', heading1, header_format)
    worksheet.write('B1', heading2, header_format)
    worksheet.write('K1', heading4, header_format)
    worksheet.write('L1', heading5, header_format)

    for idx, country in enumerate(geo.countries_list):
        row_idx = idx + 2
        worksheet.write(f'K{row_idx}', country)

    for idx, (cat,) in enumerate(first_level_categories):
        row_idx = idx + 2
        worksheet.write(f'L{row_idx}', cat) 

    for idx, items in enumerate(spreadsheet_columns):
        row_idx = idx+2
        field,label,_ = items
        try:
            val = getattr(company, field) or '-'
        except AttributeError:
            continue

        worksheet.write(f'A{row_idx}', label)
        worksheet.write(f'B{row_idx}', val)

        if field == 'main_category' or field == 'secondary_category':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': f'=$L$2:$L${len(first_level_categories)}'})
        
        elif field == 'country':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': f'=$K$2:$K${len(geo.countries_list)+1}'})
        
        elif field == 'total_revenue':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': list(db_enums.total_revenue)})

        elif field == 'company_size':
            worksheet.data_validation(f'B{row_idx}', {'validate': 'list', 'source': list(db_enums.company_sizes)})
        
    
    workbook.close()
    output.seek(0)

    return send_file(
        output,
        attachment_filename=file_name,
        as_attachment=True
    )

    