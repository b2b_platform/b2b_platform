from flask import render_template, request, Blueprint, redirect, url_for, make_response, g
from web import CLR
import time
from web.core.utils import api_output

bp = Blueprint('hello', __name__)

@bp.route('/create-request')
def dummy_create_request():
    return render_template('hello/create_request.html')

@bp.route('/demo-rfq/1')
def demo_rfq():
    rfq = {
        'product': 'Need to buy electric device',
        'buyer': 'James Smith',
        'create_time': '25/3/2018 6:37am UTC',
        'remaining_time': '28 days',
        'quotes_left': '25/30',
        'specs': {
            'Unit Price': 'please quote',
            'Quantity': '10 pieces',
            'Speed': '1200 mhr',
            'Automation': 'automatic'
        }
    }

    quotes = [
        ['Shanghai Machinery Company', 'cn', '25/3/2018', '$1200-1250',2, 1, 1],
        ['Milner Automatic Corp', 'de', '27/3/2018', '$1025-1400',2, 1, 0],
        ['Vinamach JSC', 'vn', '27/3/2018', '$900',1, 1, 1],
        ['BBV AcroLabs', 'be' ,'5/4/2018', '$2300-2350',1, 0, 1],
        ['Arjay Engineering Ltd.', 'ca', '7/4/2018', '$1000', 0, 1, 2]
    ]

    return render_template('hello/rfq_item.html', rfq=rfq, quotes=quotes)

@bp.route('/new-sourcing')
def early_access():
    return render_template('hello/early_access.html')