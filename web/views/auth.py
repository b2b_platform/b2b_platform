from flask import render_template, Blueprint, redirect, url_for, make_response, abort, request, flash, session
from flask import current_app as app
from flask_login import login_required, current_user, logout_user
from flask_jwt_extended import unset_jwt_cookies
from flask_mail import Message
from flask_babel import lazy_gettext
from datetime import datetime, timedelta
from web import old_login_required, mail, db, form_checker
from web.core import utils

bp = Blueprint('auth', __name__)

@bp.route('/login')
def login():    
    if current_user.is_authenticated:
        return redirect(url_for('home.default'))

    return render_template('auth/login.html')

@bp.route('/register')
def register():    
    if current_user.is_authenticated:
        return redirect(url_for('home.default'))
    
    # Might need to filter/sanitize via form checker
    ref_user_id = request.args.get('uid', None)
    ref_company_id = request.args.get('cid', None)
    if ref_user_id:
        try:
            ref_user = db.user.get_by_id(ref_user_id)
            if ref_user.company_id != int(ref_company_id):
                # Mismatch/not sufficient privilege
                print ('mismatch', ref_user.company_id, ' and ', ref_company_id)
                return abort(404)
            ref_company_name = db.company.get_by_id(ref_company_id).name
        
            return render_template('auth/register.html', 
                ref_company_id=ref_company_id, 
                ref_company_name=ref_company_name,
                ref_user_id=ref_user_id,
                referral=True)
        except:
            return abort(404)
    return render_template('auth/register.html')

@bp.route('/logout')
@old_login_required
def logout():
    db.notification.new_account_activity(current_user.id, 'Logged Out', 
        ip=request.environ.get('HTTP_X_REAL_IP', request.remote_addr))

    logout_user()
    resp = make_response(redirect(url_for('home.default')))
    unset_jwt_cookies(resp)
    return resp

@bp.route('/confirm/<token>', methods=['GET'])
def confirm(token): 
    if current_user.is_authenticated and current_user.is_confirmed:
        return redirect(url_for('account.my_account'))

    try:
        email = utils.check_confirmation_token(token)
    except:
        return abort(401)

    if not email:
        return abort(401)
    
    user = db.user.filter_one(email=email)

    if not user:
        return abort(404)

    db.user.confirm(user)

    return redirect(url_for("home.default"))

@bp.route('/unconfirmed')
@old_login_required
def unconfirmed():
    if current_user.is_confirmed:
        return redirect(url_for('account.my_account'))
    return render_template('auth/unconfirmed.html')

@bp.route('/resend-confirmation')
@old_login_required
def resend_confirmation():
    if current_user.is_confirmed:
        return redirect(url_for('account.my_account'))

    last_resent = session.get('EMAIL_CONFIRMATION_RESENT', datetime.min)
    now = datetime.utcnow()

    if now - last_resent < timedelta(seconds=5*60):
        flash(lazy_gettext(u'You cannot resend confirmation within 5-minute intervals. Please check your email (and spam box)'), 'danger')
        return redirect(url_for('auth.unconfirmed'))    

    confirm_token = utils.generate_confirmation_token(current_user.email)
    confirm_url = url_for('auth.confirm', token=confirm_token, _external=True)
    html = render_template('emails/account_confirmation.html',
        confirm_url=confirm_url,
        user=current_user
    )
    subject = 'Email confirmation'

    resp = utils.send_mailgun(
        current_user.email,
        subject,
        html=html
    )

    session['EMAIL_CONFIRMATION_RESENT'] = datetime.utcnow()

    return redirect(url_for('auth.unconfirmed'))

@bp.route('/forgot-password')
def forgot_password_page():
    if current_user.is_authenticated:
        return redirect(url_for('account.my_account'))
    
    return render_template('auth/forgot_password_page.html')

@bp.route('/send-reset-password-link', methods=['POST'])
def send_reset_password_link():
    if current_user.is_authenticated:
        return redirect(url_for('account.my_account'))
    
    args = form_checker.process(request)
    email = args.get('email', None)

    if not email:
        return abort(500)

    user = db.user.get_by_email(email)
    
    if user:
        last_pw_reset = session.get('EMAIL_PASSWORD_RESET', datetime.min)
        now = datetime.utcnow()

        if now - last_pw_reset < timedelta(seconds=5*60):
            flash(lazy_gettext(u'You cannot reset your password within 5-minute intervals. Please check your email (and spam box).'), 'danger')
            return redirect(url_for('auth.forgot_password_page'))


        token = utils.generate_confirmation_token(user.email)
        reset_password_url = url_for('auth.reset_password', 
            token=token, _external=True)
        html = render_template('emails/reset_password.html',
            reset_password_url=reset_password_url,
            user=user
        )
        subject = 'Reset password'

        resp = utils.send_mailgun(
            user.email,
            subject,
            html=html
        )

        session['EMAIL_PASSWORD_RESET'] = datetime.utcnow()

    return render_template('auth/reset_password_link_sent.html')

@bp.route('/reset-password/<token>', methods=['GET', 'POST'])
def reset_password(token):

    if current_user.is_authenticated:
        logout_user()
        resp = make_response(redirect(url_for('auth.reset_password', token=token)))
        unset_jwt_cookies(resp)
        return resp    

    try:
        email = utils.check_confirmation_token(token)
    except:
        return abort(401)
    
    if not email:
        return abort(401)
    
    user = db.user.get_by_email(email)
    if not user:
        return abort(401)

    if request.method == 'POST':
        args = form_checker.process(request)
        new_password = args.get('new_password', None)
        new_password_again = args.get('new_password_again', None)

        if new_password is None or new_password != new_password_again:
            return abort(500)

        user = db.user.update_one(user, password=new_password)

        db.notification.new_account_activity(user.id, 'Changed password', 
            ip=request.environ.get('HTTP_X_REAL_IP', request.remote_addr))

        return redirect(url_for('auth.login'))    

    return render_template('auth/reset_password.html', token=token, user=user)


@bp.route('/disabled')
@old_login_required
def banned():
    if not current_user.is_banned:
        return redirect(url_for('account.my_account'))
    return render_template('auth/banned.html')
