
from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort
from flask_login import login_required, current_user, login_user
from werkzeug import secure_filename

import database.models as models

from web import db, form_checker, role_manager
from role import role_constants

bp = Blueprint('admin', __name__)

@bp.route('/admin/resolve-tickets', methods=['GET'])
@login_required
def resolve_tickets():
    if not role_manager.is_master(current_user.id):
        return abort(401)
    tickets = db.support_ticket.get_all_with_order()
    return render_template('admin/resolve_tickets.html', tickets=tickets)

@bp.route('/admin', methods=['GET'])
@login_required
def home():
    if not role_manager.is_master(current_user.id):
        return abort(401)    
    return redirect(url_for('admin.user_master'))

    # if not role_manager.is_master(current_user.id):
    #     return abort(401)
        
    # return render_template('admin/home.html')

@bp.route('/admin/manage-users', methods=['GET'])
@login_required
def user_master():
    user_id = current_user.get_id()
    if not role_manager.is_master(user_id):  
        return abort(401)

    current_user_permission = role_manager.get_permission_level(current_user.id)
    args = form_checker.process(request, 'master_list_user') 
    # Filter by criteria
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))
    search_term = args.pop('search_term', None)

    num_pages, users, num_results = db.user.search(search_term=search_term, 
            limit=limit, current_page=current_page, search_hidden=True) 

    return render_template('admin/user_master.html', users=users, 
            search_term = search_term,
            current_page=current_page,
            num_results=num_results,
            num_pages=num_pages,
            permission_levels=role_constants.PERMISSION_TEXT_ARR,
            current_user_permission=current_user_permission)

@bp.route('/admin/manage-companies', methods=['GET'])
@login_required
def manage_companies():
    if not role_manager.is_master(current_user.id):
        return abort(401)

    args = form_checker.process(request)
    current_page = int(args.pop('current_page', 1))
    search_term = args.pop('search_term', None)
    num_pages, companies, num_results = db.company.search(keyword=search_term, search_hidden=True,
            current_page=current_page)
    return render_template('admin/manage_companies.html', 
        search_term=search_term,
        companies=companies, num_pages=num_pages,
        num_results=num_results, current_page=current_page)