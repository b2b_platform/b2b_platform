from flask import Blueprint, render_template, redirect, url_for, request, abort
from flask_login import login_required, current_user
import ujson
from functools import partial

from web import db, form_checker, role_manager

bp = Blueprint('media', __name__)

@bp.route('/manage-company-media')
@login_required
def manage_company_media():
    if not current_user.company_id:
        return redirect(url_for('company.link_my_company'))

    company = current_user.company
    current_page = int(request.args.get('current_page', 1))

    num_pages, media = db.media.get_by_company_id(current_user.company_id, current_page=current_page)

    return render_template('media/manage_company_media.html', 
        company=company,
        media=media,
        current_page=current_page,
        num_pages=num_pages
    )

@bp.route('/manage-product-media')
@login_required
def manage_product_media():
    if not current_user.company_id:
        return redirect(url_for('company.link_my_company'))

    company = current_user.company
    keyword = request.args.get('keyword', '')
    current_page = int(request.args.get('current_page', 1))
    num_pages, products, _ = db.product.search(
        current_page=current_page,
        keyword=keyword,
        company_id=company.id
    )
    
    return render_template('media/manage_product_media.html',
        keyword=keyword,
        current_page=current_page,
        num_pages=num_pages,
        products=products,
        company=company
    )

@bp.route('/manage-product-media/all')
@login_required
def manage_product_media_all():
    if not current_user.company_id:
        return redirect(url_for('company.link_my_company'))

    company = current_user.company
    current_page = int(request.args.get('current_page', 1))

    num_pages, media = db.media.get_by_company_products(company.id, current_page=current_page)
    
    return render_template('media/manage_product_media_all.html', 
        company=company,
        media=media,
        current_page=current_page,
        num_pages=num_pages
    )