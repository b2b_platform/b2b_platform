import os
from flask import render_template, Blueprint, redirect, url_for, flash, request
from flask import current_app as app
from flask_login import current_user, login_required
from flask_babel import lazy_gettext
from web import db, form_checker

from sqlalchemy.sql.expression import func

bp = Blueprint('home', __name__)

@bp.route('/')
def default():
    if current_user.is_authenticated:
        if current_user.company_id and current_user.company.products.count() > 0:
            default_view = 'sell'
        else:
            default_view = 'buy'
        view = request.args.get('view', default_view).lower()
        return authenticated_homepage(view=view)

    return generic_homepage()

@bp.route('/index')
def generic_homepage():
    featured_products = db.product.query().order_by(func.random()).limit(3).all()
    newest_products = (db.product.query()
        .order_by(func.random())
        .order_by('product.create_time desc').limit(10).all()
    )
    top_companies = db.company.query().order_by(func.random()).limit(10).all()

    _, newest_rfqs = db.rfq.search(limit=10)

    cats = ['Agriculture', 'Apparel', 'Chemicals', 'Consumer Electronics','Food & Beverage',
        'Furniture', 'Machinery']
    
    cats_with_id = (
        db.query(db.product_category.Model.id, db.product_category.Model.name)
        .filter(db.product_category.Model.name.in_(cats))
        .all()
    )
    cats_dict = {cat:id for id,cat in cats_with_id}

    top_countries = [
        ('us', 'United States'),
        ('cn', 'China'),
        ('vn', 'Vietnam'),
        ('in', 'India'),
        ('hk', 'Hong Kong'),
        ('th', 'Thailand'),
        ('kr', 'Korea'),
        ('jp', 'Japan'),
        ('id', 'Indonesia'),
        ('bd', 'Bangladesh'),
        ('tw', 'Taiwan'),
        ('sg', 'Singapore')
    ]
    
    return render_template('home/index.html',
        featured_products=featured_products,        
        top_companies=top_companies,
        newest_products=newest_products,
        newest_rfqs=newest_rfqs,
        cats_with_id=cats_with_id,
        cats_dict=cats_dict,
        top_countries=top_countries
    )


@bp.route('/my-homepage')
@login_required
def authenticated_homepage(view='buy'):
    if current_user.company_id and current_user.company.products.count() > 0:
        default_view = 'sell'
    else:
        default_view = 'buy'

    view = view or request.args.get('view', default_view).lower()
    contacts = db.thread.get_user_contacts(current_user)
    company = current_user.company if current_user.company_id else None

    if current_user.company_id:
        most_viewed_products = db.product.filter_by(company_id=current_user.company_id, limit=10)
    else:
        most_viewed_products = []
    
    rfqs = db.rfq.filter_by(buyer_id=current_user.id, limit=5)
    quotes = db.quote.filter_by(seller_id=current_user.id, limit=5)

    search_history = db.product.query().order_by(func.random()).limit(10).all()
    recommended_products = db.product.query().order_by(func.random()).limit(10).all()
    
    unread_msgs_count = db.thread.get_total_unread_messages_count(current_user.id)
    threads_with_count = db.thread.get_unread_messages_count_full_thread(current_user.id, limit=5)
    
    return render_template('home/authenticated.html',
        company=company,
        rfqs=rfqs,
        quotes=quotes,
        unread_msgs_count=unread_msgs_count,
        threads_with_count=threads_with_count,
        most_viewed_products=most_viewed_products,
        recommended_products=recommended_products,
        search_history=search_history,
        contacts=contacts,
        view=view
    )



@bp.route('/subscribe', methods=['POST'])
def subscribe():
    args = form_checker.process(request)
    name = args.get('name', None)
    email = args.get('email', None)
    if not name or not email:
        flash(lazy_gettext('Please provide both name and email in the subscriber form'), 'danger')
        return redirect(url_for('home.generic_homepage'))
        
    #TODO SAVE INTO DB INSTEAD OF FILE
    fp = os.path.join(app.parentdir, 'subscribers.txt')
    with open(fp, 'a') as f:
        f.write(f'{name}|{email}\n')

    flash(lazy_gettext('Thank you for subscribing!'), 'success')

    return redirect(url_for('home.generic_homepage'))

@bp.route('/welcome')
def welcome():
    return render_template('home/welcome.html')

#@bp.route('/features')
def features():
    return render_template('home/features.html')