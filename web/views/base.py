

from flask import Blueprint, redirect, url_for, g, request, make_response
from web.core import constants, utils
import babel

bp = Blueprint('base', __name__)

# By default use English
@bp.route('/change_lang')
def change_lang():
    # Check session or cookie before resetting to page?
    redirect_url = redirect(utils.get_redirect_target())
    response = make_response(redirect_url)

    newlang = request.args.get('lang_code', 'en')
    if newlang in constants.SUPPORT_LOCALES:
        response.set_cookie('lang_code',value=newlang)
    return response

