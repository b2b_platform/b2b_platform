from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort
from flask_login import login_required, current_user, login_user
from werkzeug import secure_filename
import itertools
from flask_babel import lazy_gettext
from web import db, form_checker, role_manager
from role import role_constants

bp = Blueprint('messages', __name__)

@bp.route('/messages')
@login_required
def home():
    args = form_checker.process(request)    
    thread_types = [
        ('message_thread', lazy_gettext('All')), 
        ('product_inquiry', lazy_gettext('Product Inquiry')),
        ('private_thread', lazy_gettext('Private Messages')),
        ('rfq', lazy_gettext('Request for Quotation')),
        ('support', lazy_gettext('Support Ticket'))
    ]
    thread_type = args.pop('thread_type', 'message_thread')

    search_options = [
        ('thread', lazy_gettext('Thread')), 
        ('user', lazy_gettext('User')),
        ('all', lazy_gettext('All'))
    ]
    search_opt = args.pop('search_opt', 'thread')
    search_term = args.pop('search_term', '')
    current_page = int(args.pop('current_page', 1)) 
    sorted_by = 'message_thread.' + args.pop('sorted_by', 'create_time desc')    
    
    num_pages, threads = db.thread.search(current_user, 
        thread_type=thread_type,
        search_term=search_term, search_opt=search_opt, 
        current_page=current_page, sorted_by=sorted_by,
        is_archived=False)

    return render_template('messages/home.html',
                            thread_type = thread_type,
                            thread_types = thread_types,
                            search_options = search_options,
                            search_opt = search_opt,
                            search_term = search_term,
                            threads = threads,
                            current_page = current_page,
                            num_pages = num_pages)

@bp.route('/messages/archived')
@login_required
def archived():
    args = form_checker.process(request)    

    search_options = [
        ('thread', lazy_gettext('Thread')), 
        ('user', lazy_gettext('User')),
        ('all', lazy_gettext('All'))
    ]
    search_opt = args.pop('search_opt', 'thread')
    search_term = args.pop('search_term', '')
    current_page = int(args.pop('current_page', 1)) 
    sorted_by = 'message_thread.' + args.pop('sorted_by', 'create_time desc')    
    
    num_pages, threads = db.thread.search(current_user,         
        search_term=search_term, search_opt=search_opt, 
        current_page=current_page, sorted_by=sorted_by,
        is_archived=True)

    return render_template('messages/archived.html',                            
                            search_options = search_options,
                            search_opt = search_opt,
                            search_term = search_term,
                            threads = threads,
                            current_page = current_page,
                            num_pages = num_pages)

@bp.route('/messages/new-thread')
@login_required
def new_thread():
    view = request.args.get('view', None)
    return render_template('messages/new_thread.html', view=view)

@bp.route('/messages/contacts')
@login_required
def contacts():
    user = db.user.get_by_id(current_user.id)
    colleagues = []
    biz_contacts = []
    if user.company_id:
        colleagues = [u for u in user.company.users if u.id != user.id]        
    
    biz_contacts = db.thread.get_user_contacts(user)
    biz_contacts = [u for u in biz_contacts if u not in colleagues]    
  
    return render_template('messages/contacts.html', 
        colleagues=colleagues, biz_contacts=biz_contacts)

@bp.route('/messages/settings')
@login_required
def settings():
    return render_template('messages/settings.html')

@bp.route('/messages/t/<pr_id>')
@login_required
def thread_page(pr_id):
    view = request.args.get('view', None)

    thread = db.thread.get_by_id(pr_id)
    
    if not thread:
        return abort(404)

    if current_user.id not in thread.users_by_id:
        return redirect(url_for('messages.join_thread', pr_id=thread.id))

    db.thread.read_thread(current_user.id, thread.id)

    companies = (db.query(db.company.Model)
        .filter(db.company.Model.id.in_(thread.user_companies))
        .filter(db.company.Model.id != current_user.company_id)
        .distinct().all())

    related_products = []

    if thread.thread_type == 'product_inquiry':
        related_products = thread.products
    elif thread.thread_type == 'rfq':
        related_products = list(set(itertools.chain(p for q in thread.rfq.quotes 
            if q.seller_id == current_user.id for p in q.products) ))

    return render_template('messages/thread.html',
        view=view,
        related_products=related_products,
        thread=thread, 
        companies=companies)

@bp.route('/messages/join/<pr_id>')
@login_required
def join_thread(pr_id):    
    thread = db.thread.get_by_id(pr_id)
    if not thread:
        return abort(404)

    if current_user.id in thread.users_by_id:
        return redirect(url_for('messages.thread_page', pr_id=thread.id))

    if (role_manager.is_master(current_user.id) or
        (current_user.company_id in thread.user_companies and
        role_manager.check_permission(current_user.id, role_constants.OPERATION_MESSAGE, role_constants.ACTION_OTHER_PRIV)) or
        (thread.thread_type == 'private_thread' and current_user.id in [thread.creator_id, thread.target_user_id])):
        # join user into thread:
        db.thread.add_user(thread, current_user)
        
        return redirect(url_for('messages.thread_page', pr_id=thread.id))

    return abort(401)

@bp.route('/messages/rfq/contact-buyer')
@login_required
def rfq_thread_page():
    
    args = form_checker.process(request)
    rfq_id = args.pop('rfq_id', None)

    rfq = db.rfq.get_by_id(rfq_id)
    if not rfq:
        return abort(404)

    # IF CREATOR OF RFQ => BACK TO /MESSAGES
    if rfq.buyer_id == current_user.id:
        return redirect(url_for('messages.home'))
        
    thread = db.thread.get_rfq_thread(current_user.id, rfq_id)

    if thread:
        return redirect(url_for('messages.thread_page', pr_id=thread.id))

    thread = db.thread.add_rfq_thread(current_user.id, rfq_id)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/rfq/contact-seller')
@login_required
def rfq_thread_contact_seller():

    args = form_checker.process(request)    
    quote_id = args.pop('quote_id', None)

    if not quote_id:
        return abort(404)

    quote = db.quote.get_by_id(quote_id)

    if not quote:
        return abort(404)
    
    if quote.rfq.buyer_id != current_user.id:
        return redirect(url_for('messages.home'))

    thread = db.thread.get_rfq_thread(quote.seller_id, quote.rfq.id)

    if thread:
        return redirect(url_for('messages.thread_page', pr_id=thread.id))

    thread = db.thread.add_rfq_thread(quote.seller_id, quote.rfq.id)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/trade-lead-inquiry', methods=['GET', 'POST'])
@login_required
def trade_lead_thread_page():
    args = form_checker.process(request)

    trade_lead_id = args.pop('trade_lead_id', None)
    if not trade_lead_id:
        return abort(404)

    trade_lead = db.trade_lead.get_by_id(trade_lead_id)

    if not trade_lead:
        return abort(404)
    
    if trade_lead.user_id == current_user.id:
        return redirect(url_for('messages.home'))

    thread = db.thread.get_trade_lead_thread(trade_lead.id, user_id=current_user.id)

    if not thread:
        thread = db.thread.add_trade_lead_thread(trade_lead.id, user_id=current_user.id)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/product-inquiry', methods=['GET', 'POST'])
@login_required
def product_inquiry_thread_page():    
    args = form_checker.process(request)
    product_id = args.pop('product_id', None)

    if not product_id:
        return abort(500)

    product = db.product.get_by_id(product_id)

    if not product:
        return abort(404)

    # ALREADY SELLER
    if product.company_id == current_user.company_id:
        return redirect(url_for('messages.home'))

    if request.method == 'GET':
        return render_template('messages/new_product_inquiry_thread.html', product=product)
    
    # POST FORM
    text = args.get('text', None)
    if not text:
        return abort(500)

    thread = db.thread.get_product_inquiry_thread(current_user.id, product_id, text=text)

    if not thread:
        thread = db.thread.add_product_inquiry_thread(current_user.id, product_id, text=text)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/private')
@login_required
def private_thread():
    args = form_checker.process(request)
    user_id = args.pop('user_id', None)
    user = db.user.get_by_id(user_id)

    if not user:
        return abort(404)

    if user.id == current_user.id:
        return redirect(url_for("messages.home"))

    thread = db.thread.get_or_create_private_thread(user, current_user)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/company')
@login_required
def company_thread_page():
    args = form_checker.process(request)
    company_id = args.pop('company_id', None)

    company = db.company.get_by_id(company_id)

    if not company:
        return abort(404)

    # ALREADY SELLER
    if company.id == current_user.company_id:
        return redirect(url_for('messages.home'))

    thread = db.thread.get_or_create_product_inquiry_thread_by_company(current_user.id, company)

    return redirect(url_for('messages.thread_page', pr_id=thread.id))

@bp.route('/messages/support-ticket')
@login_required
def support_thread():
    args = form_checker.process(request)
    ticket_id = args.pop('ticket_id', None)
    ticket = db.support_ticket.get_by_id(ticket_id)
    if not ticket:
        return abort(404)

    if current_user.id != ticket.user_id and not role_manager.is_master(current_user.id):
        return abort(401)

    thread = db.thread.get_or_create_support_thread(current_user.id, ticket)

    if current_user.id not in thread.users_by_id and role_manager.is_master(current_user.id):
        thread._users.append(current_user)
        thread.update(status='resolving')
        db.commit()

    return redirect(url_for('messages.thread_page', pr_id=thread.id))