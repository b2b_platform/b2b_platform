from flask import Blueprint, render_template, abort, request
from flask_login import current_user, login_required

from web import db, form_checker, role_manager

bp = Blueprint('rfq', __name__)

@bp.route('/create-rfq', methods=['GET'])
@login_required
def create_rfq():
    args = form_checker.process(request)
    product = args.get('product', '')    
    quantity = ''
    if args.get('quantity', None):
        quantity += f'{args.get("quantity")} '
    
    if args.get('unit', None):
        quantity += args.get('unit')
    
    return render_template('rfq/create_rfq.html', product=product, quantity=quantity)

@bp.route('/manage-rfqs')
@login_required
def manage_rfqs():
    rfqs = db.rfq.get_by_buyer_id(current_user.id)    
    return render_template('/rfq/manage_rfqs.html', rfqs=rfqs)

@bp.route('/rfq/<id_slug(model="rfq",attr="product"):pr_id>')
@login_required
def rfq_page(pr_id):
    rfq = db.rfq.get_by_id(pr_id)

    if not rfq:
        return abort(404)
    
    is_owner = False
    quotes = db.quote.get_by_rfq_id(rfq.id)

    if current_user.is_authenticated and rfq.buyer_id == current_user.id:
        is_owner = True
    else:
        quotes = [ x for x in quotes if x.seller_id == current_user.id ]
    
    return render_template('/rfq/rfq.html', 
                            rfq=rfq, 
                            quotes=quotes,
                            is_owner=is_owner)

@bp.route('/update-rfq/<id_slug(model="rfq",attr="product"):pr_id>')
@login_required
def update_rfq(pr_id):
    rfq = db.rfq.get_by_id(pr_id)

    if not rfq:
        return abort(404)

    if current_user.id != rfq.buyer_id:
        return abort(403)
    
    return render_template('/rfq/update_rfq.html', rfq=rfq)

@bp.route('/sourcing')
def sourcing():

    args = form_checker.process(request, 'search_rfq')
    keyword = args.pop('keyword', '')
    category = args.pop('category', None)
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))    

    num_pages, rfqs = db.rfq.search(keyword=keyword, category=category, 
                                    limit=limit, current_page=current_page)

    categories = [('', 'All')] + (db.query(db.product_category.Model.id, db.product_category.Model.name)
                    .filter(db.product_category.Model.parent_category_id == None).all())    

    return render_template('/rfq/sourcing.html',
                            keyword=keyword,
                            category=category,
                            categories=categories,
                            current_page=current_page,
                            num_pages=num_pages,
                            rfqs=rfqs)