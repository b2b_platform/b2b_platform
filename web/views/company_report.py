from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort
from flask_login import login_required, current_user, login_user
from werkzeug import secure_filename

import database.models as models

from web import db, form_checker, role_manager
from web.core import constants
from role import role_constants

bp = Blueprint('company_report', __name__)

  
@bp.route('/report-company/<pr_id>')
@login_required
def report_company(pr_id=None):
    user_id = current_user.get_id()
    report_type_list = constants.REPORT_TYPES
    company = db.company.get_by_id(pr_id)

    if not company:
        return abort(404)
    
    if current_user.company_id == company.id:
        return abort(403)

    args = form_checker.process(request, 'company_report_form')
    report_type = args.pop('report_type', None)
    reported = False
    
    if report_type:
        reported = True
        reason = args.pop('reason')
        # TBI update Report db
        db.company_report.add(report_type=report_type, reason=reason, user_id=user_id, company_id=pr_id)

    return render_template('company_report/report.html', company_name=company.name, 
            #report_type_list=report_type_list,
            company=company,
            reported=reported)


@bp.route('/my-reports')
@login_required
def list_reports():
    user_id = current_user.get_id()
    
    reports = db.company_report.filter_by_q(user_id=user_id).order_by('company_report.create_time desc').all()

    return render_template('company_report/list_reports.html', reports=reports)

@bp.route('/user-company-report/<report_id>')
@login_required
def update_report(report_id):
    user_id = current_user.get_id()
    
    view = request.args.get('view', None)
    
    report = db.company_report.get_by_id(report_id)

    if view == 'clean':
        return render_template('company_report/update_report_clean.html', report=report)
    else:
        return render_template('company_report/update_report.html', report=report)