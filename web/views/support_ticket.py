from flask import Blueprint, render_template, redirect, url_for, request, abort
from flask_login import login_required, current_user
import ujson

from web import db, form_checker, role_manager
from role import role_constants

bp = Blueprint('support_ticket', __name__)

@bp.route('/support')
@login_required
def main():
    if role_manager.is_master(current_user.id):
        return redirect(url_for('admin.resolve_tickets'))

    open_tickets = db.support_ticket.get_open_tickets(current_user.id)
    closed_tickets = db.support_ticket.get_closed_tickets(current_user.id)

    return render_template('support_ticket/main.html',
        open_tickets=open_tickets,
        closed_tickets=closed_tickets
    )

@bp.route('/support/t/<pr_id>')
@login_required
def ticket(pr_id):    
    ticket = db.support_ticket.get_by_id(pr_id)
    if not ticket:
        return abort(404)
    
    #user_role = db.role.get_position_by_userid(current_user.id)

    is_master = role_manager.is_master(current_user.id)
    if current_user.id != ticket.user_id and not is_master:
        return abort(401)
        
    return render_template('support_ticket/ticket.html', ticket=ticket, is_master=is_master)

@bp.route('/support-general')
def support_nologin():
    return render_template('support_ticket/support_nologin.html')