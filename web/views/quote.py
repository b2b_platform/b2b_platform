from flask import Blueprint, render_template, abort, redirect, url_for, request
from flask_login import current_user, login_required

from web import db, form_checker, role_manager

bp = Blueprint('quote', __name__)

@bp.route('/create-quote')
@login_required
def create_quote():
    rfq_id = request.args.get('rfq_id', None)
    if not rfq_id:
        return render_template('/quote/create_quote_no_rfq.html')

    rfq = db.rfq.get_by_id(rfq_id)
    if not rfq:
        return abort(404)

    if rfq.buyer_id == current_user.id:
        return abort(401)
    
    user = db.user.get_by_id(current_user.id)
    if not user.company_id:
        return redirect(url_for('company.link_my_company'))
    
    products = db.product.get_by_company_id(user.company_id)
    
    view = request.args.get('view', None)
    
    return render_template('/quote/create_quote.html', view=view, rfq=rfq, products=products)

@bp.route('/quote/<id_slug(model="quote",attr="title"):pr_id>')
@login_required
def quote_page(pr_id):
    quote = db.quote.get_by_id(pr_id)
    if not quote:
        return abort(404)

    is_owner = current_user.id == quote.seller_id

    return render_template('quote/quote.html', quote=quote, is_owner=is_owner)

@bp.route('/update-quote/<id_slug(model="quote",attr="title"):pr_id>')
@login_required
def update_quote(pr_id):
    quote = db.quote.get_by_id(pr_id)
    if not quote:
        return abort(404)

    if current_user.id != quote.seller_id:
        return abort(401)

    user = db.user.get_by_id(current_user.id)
    if not user.company_id:
        return redirect(url_for('company.link_my_company'))
    
    products = db.product.get_by_company_id(user.company_id)

    return render_template('/quote/update_quote.html', quote=quote, products=products)

@bp.route('/manage-quotes')
@login_required
def manage_quotes():
    quotes = db.quote.get_by_seller_id(current_user.id)
    return render_template('/quote/manage_quotes.html', quotes=quotes)