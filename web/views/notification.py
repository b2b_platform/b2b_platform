from flask import Blueprint, render_template, request, abort, views, redirect, url_for, abort
from flask_login import login_required, current_user, login_user
from flask_babel import lazy_gettext
from werkzeug import secure_filename
import itertools

from web import db, form_checker, role_manager
from role import role_constants

bp = Blueprint('notification', __name__)

@bp.route('/my-notifications')
@login_required
def home():
    user = db.user.get_by_id(current_user.id)

    notification_types = [
        ('all', lazy_gettext('All')),
        ('product_inquiry', lazy_gettext('Product Inquiry')),
        ('account_activity', lazy_gettext('Account Activity'))
    ]

    args = form_checker.process(request)

    current_page = int(args.pop('current_page', 1))
    search_term = args.pop('search_term', '')
    notification_type = args.pop('notification_type', 'all')

    num_pages, notifications = db.notification.search(
        user.id, user.company_id,
        notification_type = notification_type,
        search_term = search_term,
        current_page = current_page
    )
    
    # Mark as read or not?
    db.notification.read_all(user.id)

    return render_template('notification/home.html',
        notification_types=notification_types,
        notifications=notifications,
        num_pages=num_pages, current_page=current_page, 
        search_term=search_term,
        notification_type=notification_type)