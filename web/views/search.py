
from flask import Blueprint, render_template, redirect, url_for, request, abort
from flask_login import login_required, current_user
import ujson
from sqlalchemy.sql.expression import func

from web import db, form_checker, role_manager
from web import constants, geo
from flask_babel import gettext, ngettext
from membership import membership_constants

bp = Blueprint('search', __name__)

def get_sorted_by_field_product(sort_field):
    return {
        gettext('Most Recent First'): 'product.create_time desc',
        gettext('Best Matching'): None,
        gettext('Product A to Z') : 'product.name',
        gettext('Unit Price - Low to High') : 'product.price_range_min',
        gettext('Unit Price - High to Low') : 'product.price_range_max desc'
    }.get(sort_field, 'product.name')

def get_sorted_by_field_company(sort_field):
    return {
        gettext('Date Added'): 'company.create_time',
        gettext('Best Matching'): None,
        gettext('Company A to Z') : 'company.name',
        gettext('Company Z to A') : 'company.name desc',
    }.get(sort_field, 'company.name')


def get_list(args, feat):
    res = args.get(feat['name'], None)
    if res:
        if not isinstance(res, (list, tuple)):
            res = [res]
    return res

# Retrieve features
def get_features(args, pos):
    features_list = []
    chosen_selectboxes = []

    for feat in constants.list_checkboxes_features[pos]:
        chosen = get_list(args, feat)
        if chosen:
            for item in chosen:
                chosen_selectboxes.append((item,feat['name']))
        features_list.append({'label': feat['label'], 'name': feat['name'], 'type': 'checkbox', 
                        'options': feat['options'], 'chosen': chosen})
   
    for feat in constants.list_checkboxes_jsonb_features[pos]:
        chosen = get_list(args, feat)
        if chosen:
            for item in chosen:
                chosen_selectboxes.append((item,feat['name']))
        features_list.append({'label': feat['label'], 'name': feat['name'], 'type': 'checkboxjson', 
                        'options': feat['options'], 'chosen': chosen})
        #                   'chosen': args.get(feat['name'], None)})      
    for feat in constants.list_numeric_features[pos]:
        #print (feat['name'], "min: ",  args.get(feat['name'] + '_min', None))
        features_list.append({'label': feat['label'], 'name': feat['name'], 'type': 'numeric',
                            'val_min' : args.get(feat['name'] + '_min', None), 
                            'val_max' : args.get(feat['name'] + '_max', None),
                            'separate' : feat['separate']})
    
    return features_list, chosen_selectboxes

# idx == 0 for products
# idx == 1 for companies
def get_filter_list(args, idx):
    filter_list = {}
    
    checkbox_filter_list = []
    for feat in constants.list_checkboxes_features[idx]:
        if feat['name'] in args.keys():
            chosen = get_list(args, feat)
            
            if feat['name'] == 'membership':
                # Patching membership to numeric values
                chosen = [ constants.MEMBERSHIP_TYPES_DICT[item] for item in chosen ]
            print ('chosen elem', chosen)
            checkbox_filter_list.append({'name': feat['name'], 'chosen': chosen })

   
    # Process country/region
    # Works for both products and companies tab
    search_region_id = args.get('search_region_id', None)
    country = args.get('country', None)

    # Should check for exclusiveness
    if search_region_id:
        reg_id = int(args['search_region_id'])
        chosen_countries = None
            
        if reg_id in geo.regions_list:
            chosen_countries = geo.countries_by_region_dict[reg_id] 
        else:
            chosen_countries = geo.countries_id_dict[reg_id]
            
        if not isinstance(chosen_countries, (list, tuple)):
            chosen_countries = [chosen_countries]    
        checkbox_filter_list.append({'name': 'country', 'chosen': chosen_countries })
    if country:
        feat = {'label' : gettext('Country'), 'name': 'country'}
        chosen_countries = get_list(args,  feat)
        checkbox_filter_list.append({'name': 'country', 'chosen': chosen_countries })

    filter_list['checkbox'] = checkbox_filter_list

    checkbox_json_filter_list = []
    for feat in constants.list_checkboxes_jsonb_features[idx]:
        if feat['name'] in args.keys():
            checkbox_json_filter_list.append({'name': feat['name'], 'chosen': get_list(args, feat) })
    filter_list['checkboxjson'] = checkbox_json_filter_list
    
    numeric_filter_list = []
    for feat in constants.list_numeric_features[idx]:
        if (feat['name'] + '_min') in args.keys():
            numeric_filter_list.append({'name' : feat['name'], 
                    'min' : args[feat['name'] + '_min'], 'separate' : feat['separate'] })
        if (feat['name'] + '_max') in args.keys():
            numeric_filter_list.append({'name' : feat['name'], 
                    'max' : args[feat['name'] + '_max'], 'separate' : feat['separate'] })
    filter_list['numeric'] = numeric_filter_list

    return filter_list#, selected_list

@bp.route('/search')
def search(stype=None):
    args = form_checker.process(request, 'search_all')

    args.pop('', '') # remove undefined

    keyword = args.pop('keyword', '')
    category = args.pop('category', None)

    # Default type
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))
    #sorted_by = 'product.' + args.pop('sorted_by', 'name')    
    view = args.pop('view', 'grid')
    stype = args.pop('stype', 'products')

    features = []
    chosen_selectboxes = []
    viewtab = ''
    sorted_by_text = args.pop('sorted_by', None)

    num_pages = 1
    num_results = 0

    selected_region_id = args.get('search_region_id', None)
    selected_region_path = geo.countries_path_dict[int(selected_region_id)] if selected_region_id else None
    
    # Unique flow logic - TBI
    if stype == 'products':
        sorted_by = get_sorted_by_field_product(sorted_by_text)
    elif stype == 'companies':
        sorted_by = get_sorted_by_field_company(sorted_by_text)
        
    else:
        # TBI
        sorted_by = 'product.' + args.pop('sorted_by', 'name') 

    companies = []
    products = []
    categories = []

    if stype == 'products':
        # Obtain attribute for processing
        viewtab = args.pop('viewtab', 'tabproducts')
        filter_list = get_filter_list(args, 0)
        
        num_pages, products, num_results = db.product.search(keyword=keyword, 
                limit=limit, current_page=current_page, sorted_by=sorted_by,
                category=category,filter_list=filter_list, viewtab=viewtab)
        
        if viewtab == 'tabcompanies':
            companies = products
            products = []

        # Retrieve subcategories
        categories = [('', 'All')] + (db.query(db.product_category.Model.id, db.product_category.Model.name)
                    .filter(db.product_category.Model.parent_category_id == category).all()) 

        features, chosen_selectboxes = get_features(args, 0)

    if stype == 'companies':
        filter_list = get_filter_list(args, 1)
        num_pages, companies, num_results = db.company.search(keyword=keyword, 
                limit=limit, current_page=current_page, sorted_by=sorted_by,
                filter_list=filter_list)
        categories = [('', 'All')] + (db.query(db.product_category.Model.id, db.product_category.Model.name)
                    .filter(db.product_category.Model.parent_category_id == None).all()) 

        # Perhaps needs a separate instance of num_page for dual view
        features, chosen_selectboxes = get_features(args, 1)

    # Obtain category breadcrumb text
    cat = None
    if category:
        cat = db.product_category.get_by_id(category).path

    return render_template('search/default.html',
                            keyword=keyword,
                            category=cat,
                            categories=categories,
                            current_page=current_page,
                            num_results=num_results,
                            num_pages=num_pages,
                            products=products,
                            companies=companies,
                            view=view,
                            stype=stype,
                            sorted_by_text=sorted_by_text,
                            selected_region_id=selected_region_id,
                            selected_region_path=selected_region_path,
                            chosen_selectboxes=chosen_selectboxes,
                            viewtab=viewtab,
                            features=features)


def get_most_recent_products(limit=20):
    products = []
    return products

def get_most_recent_rfqs(limit=20):
    rfqs = []
    return rfqs