from flask import render_template, Blueprint, redirect, url_for, abort, request, session, flash
from flask import current_app as app
from flask_login import login_required, current_user
from flask_mail import Message
from flask_babel import lazy_gettext
from web import db, form_checker
from web.core import utils

bp = Blueprint('account', __name__)

@bp.route('/my-account')
@login_required
def my_account():
    return render_template('account/my_account.html')

@bp.route('/change-password')
@login_required
def change_password():
    return render_template('account/change_password.html')

@bp.route('/my-account/settings')
@login_required
def settings():
    blocked_users = None
    num_pages = 0
    #limit = 20
    num_results = 0

    if current_user.privacy_settings:
        blocked_ids = current_user.privacy_settings['privacy_blocked_users']
        num_pages, users, num_results = db.user.search_list_id(blocked_ids)
        blocked_users = [ u.to_blocked_dict() for u in users if u.is_visible() ]

    return render_template('account/settings.html', 
        num_pages=num_pages, 
        num_results=num_results, 
        blocked_users=blocked_users)

@bp.route('/my-contacts')
@login_required
def my_contacts():
    user = db.user.get_by_id(current_user.id)
    colleagues = []
    biz_contacts = []
    if user.company_id:
        colleagues = [u for u in user.company.users if u.id != user.id]
    
    biz_contacts = db.thread.get_user_contacts(user)
    biz_contacts = [u for u in biz_contacts if u not in colleagues]

    return render_template('account/my_contacts.html',
        colleagues=colleagues, biz_contacts=biz_contacts
    )

@bp.route('/account/<pr_id>')
def account_page(pr_id):
    user = db.user.get_by_id(pr_id)

    if not user:
        return abort(404)

    return render_template('account/account.html', user=user)

@bp.route('/invitation')
def invite():
    ref_user_id = request.args.get('uid', None)
    ref_company_id = request.args.get('cid', None)

    if current_user.is_authenticated:
        user = db.user.get_by_id(current_user.get_id())

        return redirect(url_for("company.link_my_company", uid=ref_user_id, cid=ref_company_id))
    else:
        return redirect(url_for("auth.register", uid=ref_user_id, cid=ref_company_id))

@bp.route('/my-basket', methods=['GET', 'POST'])
@login_required
def my_basket():
    pids = session.get('BASKET_PRODUCTS', None)
    products = []
    
    if pids:
        products = db.product.get_by_ids(list(pids))

    if request.method == 'GET':
        return render_template('account/my_basket.html', products=products)


    if request.method == 'POST':
        # POST FORM
        args = form_checker.process(request)
        text = args.get('text', None)

        if not text:
            flash(lazy_gettext('You have to fill in the message form.'), 'danger')
            return render_template('account/my_basket.html', products=products)

        num_products = len(products)

        if not num_products:
            flash(lazy_gettext('Your basket is empty'))
            return render_template('account/my_basket.html', products=products)

        threads = db.thread.get_or_create_bulk_product_inquiry_threads(current_user.id, products, text=text)
        
        products = session['BASKET_PRODUCTS'] = set()
        
        #flash(lazy_gettext(f'Successfully sent {num_products} product inquiries to sellers.'), 'success')
        flash(lazy_gettext(u'Successfully sent {} product inquiries to sellers.').format(num_products), 'success')

        return render_template('account/my_basket.html', products=list(products))
