import { query } from './common_utils';
import { API } from './api';

export const handle_create_quote_form = function() {

    var $form = $('#create-quote-form');
    var $products_select = $('#select-products-input');
    var $select_products_items = $('.select-products-item');

    $select_products_items.on('click', function() {
        var $this = $(this);
        var product_id = this.getAttribute('data-product_id');
        var $opt = $products_select.find('option[value="' + product_id + '"]');
        $this.toggleClass('selected');  
        $opt.attr('selected', !$opt.is(':selected'));        
    });
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                
                if (window.parent.submit_callback === undefined) {
                    location.href = '/quote/' + d.result.id;
                } else {
                    window.parent.submit_callback();
                }                
                
                return false;
            };
            var data = new FormData($form[0]);
            query(API.QUOTE_CREATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            title: {
                required: true
            },
            price: {
                required: true
            },
            quantity: {
                required: true
            },
            content: {
                required: true
            }
        }
    });

}

export const handle_update_quote_form = function() {
    
    var $form = $('#update-quote-form');

    var $products_select = $('#select-products-input');
    var $select_products_items = $('.select-products-item');

    $select_products_items.on('click', function() {
        var $this = $(this);
        var product_id = this.getAttribute('data-product_id');
        var $opt = $products_select.find('option[value="' + product_id + '"]');
        $this.toggleClass('selected');  
        $opt.attr('selected', !$opt.is(':selected'));        
    });
    

    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                location.href = '/quote/' + d.result.id;
                return false;
            };
            var data = new FormData($form[0]);                
            query(API.QUOTE_UPDATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            title: {
                required: true
            },
            price: {
                required: true
            },
            quantity: {
                required: true
            },
            content: {
                required: true
            }
        }
    });
    
}