import { query } from 'js/common_utils';
import { API } from 'js/api';
import { user_item_tpl } from './tpls';

export const start_page = function() {
    var $search_input = $('#search-users-input');
    var $search_output = $('#search-users-output');

    $search_input.on('keyup', function(e) {
        e.preventDefault();

        if (e.keyCode === 13) {            
            $search_output.html('');

            if ($search_input.val() === '') {
                return false;
            }
            
            var cb = function(d, err) {
                if (err != null) return false;

                var items = d.result.map(function(user, idx) {
                    return user_item_tpl.render({user: user, show_message_btn: true});
                });
                
                $search_output.html(items.join(''));

            }
            query(API.ACCOUNT_SEARCH, 'GET', {search_term: $search_input.val()}, cb);

            return false;
        }
    });
}