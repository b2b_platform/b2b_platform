import { query } from 'js/common_utils';
import { API } from 'js/api';
import { thread_view_iframe_tpl, thread_iframe_tpl } from './tpls';
import { get, isEmpty, forEach, map } from 'lodash';
import React, { Component } from 'react';
import ReactDom from 'react-dom';

const ThreadItem = ({ thread, handleClick, is_active }) => (
    <div
        className={'thread-item media mb-2 ' + (is_active ? 'active' : '')}
        onClick={(e) => {e.stopPropagation(); handleClick(thread.id);}}
    >
        <div className='media-body'>
            <div className='thread-item-title'>
                { thread.unread_msgs.count > 0 ?
                <span className='unread-msg-count-label'>{ thread.unread_msgs.count }</span> : '' }
                <b>{ thread.name }</b>
            </div>
            <p>{ thread.description }</p>
        </div>
    </div>
);

class ThreadsSelectorContainer extends Component {
    state = {
        threads: [],
        search_term: '',
        active_thread_id: null
    };

    set_threads = (threads = []) => {
        threads = threads.sort(function(t1,t2) {
            //if (t1.id === active_thread_id) return -1;
            //else if (t2.id === active_thread_id) return 1;

            if (t1.unread_msgs.ts > t2.unread_msgs.ts) return -1;
            else if (t1.unread_msgs.ts < t2.unread_msgs.ts) return 1;
            //else if (t1.unread_msgs.count > t2.unread_msgs.count) return -1;
            //else if (t1.unread_msgs.count < t2.unread_msgs.count) return 1;
            return 0;
        });
        this.setState({ threads });
    }

    set_active_thread = (active_thread_id=null) => {
        this.setState({ active_thread_id });
    }

    set_search_term = (search_term='') => {
        this.setState({ search_term });
    }

    render() {
        const { qm } = this.props;
        const { threads, active_thread_id, search_term } = this.state;     

        const thread_list = map(
            threads,
            (thread, idx) => {
                if (thread.name.toLowerCase().indexOf(search_term) === -1 &&
                    thread.description.toLowerCase().indexOf(search_term) === -1)
                    return;

                var is_active = thread.id === active_thread_id;
                return (<ThreadItem
                    handleClick={qm.set_active_thread}
                    is_active={is_active}
                    thread={thread}
                    key={idx}
                />);
            },
        );

        const no_thread_message = isEmpty(threads) && <p>No thread available</p>;

        return (
            <div className='threads-list'>
                {no_thread_message}
                {thread_list}
            </div>
        );
    }
}

function QuickMessages() {
    var qm = this;
    var $global_wrapper = $('.global-wrapper');
    var $qm = $('.quick-messages-wrapper');
    var $qm_btn = $qm.find('.quick-messages-btn');
    var $qm_content_wrapper = $qm.find('.quick-messages-content-wrapper');
    var $qm_x_btn = $qm.find('.x-btn');
    var $qm_threads_selector = $qm.find('#threads-selector');
    var $qm_show_threads_selector = $qm.find('#show-threads-selector');
    var $qm_thread_view_content = $qm.find('#thread-view-content');
    var $qm_searchbox = $qm.find('#threads-searchbox');

    var $qm_new_thread_btn = $qm.find('#new-thread-btn');

    var notiman = window.notiman;

    var threads_selector_root = document.getElementById('react-root__threads-selector');
    var threads_selector = null;

    var active_thread_id = null;
    var threads = [];

    var search_term = '';

    $qm_btn.on('click', function() {
        $global_wrapper.hide();
        $qm_content_wrapper.addClass('active');
        $qm_threads_selector.removeClass('expanded');
        get_threads();
    });

    $qm_x_btn.on('click', function() {
        $global_wrapper.show();
        $qm_content_wrapper.removeClass('active');
        $qm_thread_view_content.html(null);
    });

    $qm_show_threads_selector.on('click', function() {
        $qm_threads_selector.toggleClass('expanded');
    });

    $qm_threads_selector.on('click', function(e) {
        if (e.target != this) return;

        $qm_threads_selector.removeClass('expanded');
    });

    $qm_new_thread_btn.on('click', function(e) {
        $qm_threads_selector.removeClass('expanded');
        active_thread_id = null;
        $qm_thread_view_content.html(thread_iframe_tpl.render({url: '/messages/new-thread?view=clean'}));
    });

    $qm_searchbox.find('input').on('input', function(e) {        
        threads_selector.set_search_term(this.value);
    });

    function set_active_thread(thread_id) {
        $qm_threads_selector.removeClass('expanded');

        if (active_thread_id === thread_id) return false;

        active_thread_id = thread_id;

        threads_selector.set_active_thread(active_thread_id);

        if (active_thread_id) {
            $qm_thread_view_content.html(thread_view_iframe_tpl.render({thread_id: active_thread_id}));
        } else {
            $qm_thread_view_content.html('<p>You do not have any active message thread.</p>');
        }
    }

    function get_threads() {
        var cb = function(d, e) {
            if (e != null) return false;

            threads = get(d, 'result', []);
            threads_selector.set_threads(threads);
            
            if (!active_thread_id) set_active_thread(get(d, 'result.0.id'));
            
            if (active_thread_id) {
                $qm_thread_view_content.html(thread_view_iframe_tpl.render({thread_id: active_thread_id}));
            } else {
                $qm_thread_view_content.html('<p class="mt-3">You do not have any active message thread.</p>');
            }  

            forEach(threads, ({ id }) =>
                notiman.unread_msgs_notihan().add_per_thread_cb(id, on_update_unread_per_thread_cb)
            );            
        }

        query(API.MESSAGES_LIST_THREADS, 'GET', null, cb);
    }

    function on_update_unread_per_thread_cb(thread_id, unread_per_thread) {
        for (var i=0; i<threads.length; i++) {
            if (threads[i].id != thread_id) continue;
            threads[i].unread_msgs = unread_per_thread;
            threads_selector.set_threads(threads);
        }
    }

    function init() {
        threads_selector = ReactDom.render(<ThreadsSelectorContainer qm={qm}/>, threads_selector_root);
    }

    $.extend(this, {
        init: init,
        set_active_thread: set_active_thread
    });
}

export { QuickMessages };
