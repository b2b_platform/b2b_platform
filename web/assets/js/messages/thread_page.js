import React from 'react';
import ReactDom from 'react-dom';
import _ from 'lodash';
import WatchJS from 'melanke-watchjs';
import 'trumbowyg/dist/trumbowyg.min.js';

import { query, htmlSanitizer, to_emoji, build_file_preview } from 'js/common_utils';
import { API } from 'js/api';
import { user_item_tpl, added_user_tpl } from './tpls';

import 'emojione-picker/css/picker.css';
import 'emojione/assets/css/emojione.css';
import EmojiPicker from 'emojione-picker';

function DataManager({page, protocol, hostname, port, current_thread, current_user}) {
    var page = page;
    var protocol = protocol || (location.protocol === 'https:' ? 'wss://' : 'ws://');
    var hostname = hostname || location.hostname;
    var port = port || location.port;
    var current_user = current_user || {};
    var current_thread = current_thread || {};

    var url = protocol + hostname + ':' + port + '/ws/messages?thread_id=' + current_thread.id;

    var ws = null;

    var last_index = null;
    var start_of_thread = false;

    var messages = [];
    var watch_callbacks = [];

    start_ws();

    function start_ws() {
        ws = new WebSocket(url) || null;
        ws.onopen = on_open;
        ws.onerror = on_error;
        ws.onmessage = on_message;
        ws.onclose = on_close;
    }

    function on_close(e) {                
        setTimeout(start_ws, 3000);
    }

    function on_open(e) {}

    function on_error(e) {}

    function on_message(e) {
        var r = JSON.parse(e.data);
        switch (r.msg_type) {
            case 'NEW_MSG':
                on_new_message(r.data);                
                break;
            case 'PIN_MSG':
                on_update_message(r.data);
                break;
            case 'UPDATE_MSG':
                on_update_message(r.data);
                break;
            default:
                break;
        }
    }

    function send(msg_type, data) {        
        var message = {
            msg_type: msg_type,
            data: data
        }
        ws.send(JSON.stringify(message));
    }

    function new_message(data) {
        send('NEW_MSG', data);
    }

    function pin_message(data) {
        send('PIN_MSG', data);
    }

    function read_message() {
        send('READ_MSG', {user_id: current_user.id, thread_id: current_thread.id});
    }

    function get_message(data) {
        var cb = function(d, e) {
            if (e != null) return false;
            on_update_message(d.result);
        }
        
        query(API.MESSAGES_GET_MESSAGE, 'GET', data, cb);
    }

    function on_new_message(msg) {        
        read_message();
        messages.push(msg);        
    }
    
    function on_update_message(msg) {
        var idx = null;
        for (var i = 0; i < messages.length; i++) {
            if (messages[i].id === msg.id) {
                idx = i;
                break;
            }
        }
        messages.splice(idx, 1, msg);
    }

    function get_history() {
        var cb = function(d, e) {
            if (e != null) return false;
            
            if (d.result.length) {
                messages.unshift(...d.result);
                last_index = messages[0].id;
            } else {
                start_of_thread = true;
            }
            
        }

        var data = {
            thread_id: current_thread.id,
            last_index: last_index
        }

        if (!start_of_thread) {
            query(API.MESSAGES_GET_THREAD_MESSAGES, 'GET', data, cb);
        }        
    }

    function search_messages(keyword, cb) {
        var data = {
            thread_id: current_thread.id,
            limit: 100,
            keyword: keyword
        }
        query(API.MESSAGES_GET_THREAD_MESSAGES, 'GET', data, cb);
    }

    function add_watch_callback(cb) {
        watch_callbacks.push(cb);
    }

    WatchJS.watch(messages, function() {
        for (var i=0; i < watch_callbacks.length; i++) {
            watch_callbacks[i](messages);
        }
    });        

    $.extend(this, {
        new_message: new_message,
        pin_message: pin_message,
        get_history: get_history,
        get_message: get_message,
        add_watch_callback: add_watch_callback,
        search_messages: search_messages
    })

}

class MessageContent extends React.Component {

    render() {
        const message = this.props.message;        

        if (message.message_type === 'media_message') {
            return (
            <div>
                <div className='media-grid-hz'>
                    { message.media.map(function(item, idx) {
                        return (
                        <div className='media-item-hz' id={item.id + '-media-item'} key={idx}>
                            <img src={ item.thumbnail } alt={ item.name } />
                        </div>);
                    })}
                </div>
                <div className="msg-text" dangerouslySetInnerHTML={{ __html: message.content }}></div>
            </div>
            );
        }

        return (
            <div className="msg-text" dangerouslySetInnerHTML={{ __html: message.content }}></div>
        );
    }
}

class Message extends React.Component {

    render() {
        const message = this.props.message;
        const page = this.props.page;
        const cls = [
            (this.props.is_me ? 'me' : ''),
            (message.is_pinned ? 'pinned' : '')
        ].join(' ');

        const profile_pic = message.user.profile_pic ? 
            message.user.profile_pic : '/static/img/user_default.png';

        const pin_icon = message.is_pinned ? <i className='fa fa-thumb-tack' /> :
                        <i className='fa fa-thumb-tack fa-rotate-90' />;
        
        return (
        <div className={'msg ' + cls + ' media'}>
            <img className='msg-profile-pic' src={ profile_pic } />
            <div className={'msg-content media-body ' + message.message_type}>
                { message.subject ? <h5 dangerouslySetInnerHTML={{ __html: message.subject}}></h5> : '' }
                <MessageContent message={message} />
                <span className='pin-msg' onClick={() => { page.pin_message(message.id, !message.is_pinned); }}>
                    { pin_icon }
                </span>
            </div>
            <div className='msg-tooltip'>
                <span className='msg-senttime'>{ message.create_time }</span>
            </div>       
        </div>
        )
    }
}

class MessagesContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: []            
        }

        this.was_at_bottom = false;
        this.wrapper = document.getElementById('thread-messages');
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.set_messages = this.set_messages.bind(this);
        this.onScrollTop = this.onScrollTop.bind(this);
    }

    set_messages(msgs) {        
        this.setState(prevState => ({
            messages: msgs
        }));
    }

    isScrolledIntoView(el) {
        var rect = el.getBoundingClientRect();
        var elemTop = rect.top;
        var elemBottom = rect.bottom;
            
        //var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        var isVisible = elementTop < window.innerHeight && elementBottom >= 0;
        return isVisible;
    }

    scrollToBottom() {
        if (this.was_at_bottom) {
            this.wrapper.scrollTop = this.wrapper.scrollHeight;
        }
    }

    onScrollTop() {
        if (this.wrapper.scrollTop == 0) {
            this.props.page.dm().get_history();
        }        
    }

    componentDidMount() {
        this.wrapper.addEventListener('scroll', this.onScrollTop);
        this.props.page.dm().add_watch_callback(this.set_messages); 
        this.scrollToBottom();
    }
    
    componentWillUpdate() {        
        this.was_at_bottom = this.wrapper.scrollTop === (this.wrapper.scrollHeight - this.wrapper.offsetHeight);
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    componentWillUnmount() {
        this.wrapper.removeEventListener('scroll', this.onScrollTop);
    }    

    render() {
        const messages = this.state.messages;
        const props = this.props;        
        const current_user = props.page.current_user();
        const current_thread = props.page.current_thread();        

        return (        
        <div className='messages-list'>
            {messages.map(function(msg, idx) {                
                return <Message {...props} is_me={msg.user_id === current_user.id} 
                                message={msg} key={idx} />
            })}
        </div>                    
        );
    }
}

class SearchMessage extends React.Component {

    render() {
        const message = this.props.message;
        const page = this.props.page;
        const cls = this.props.is_me ? 'me' : '';        

        const profile_pic = message.user.profile_pic ? 
            message.user.profile_pic : '/static/img/user_default.png';        

        return (
        <div className={'msg ' + cls + ' media'}>
            <img className='msg-profile-pic' src={ profile_pic } />
            <div className='msg-content media-body'>
                { message.subject ? <h5>{message.subject}</h5> : '' }
                <MessageContent message={message} />              
            </div>
        </div>
        )
    }
}

class SearchMessagesContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: []            
        }
        
        this.set_messages = this.set_messages.bind(this);
        this.handleSearchEnter = this.handleSearchEnter.bind(this);
    }

    set_messages(msgs) {
        this.setState(prevState => ({
            messages: msgs
        }));
    }

    clear_messages() {
        this.setState(prevState => ({
            messages: []
        }));
    }

    handleSearchEnter(e) {
        var that = this;
        if (e.keyCode === 13) {
            e.preventDefault();
            var keyword = e.target.value;

            if (!keyword.length) {
                that.clear_messages();
                return false;
            }

            var cb = function(d, err) {
                if (err != null) return false;
                that.set_messages(d.result);
            }
            that.props.page.dm().search_messages(keyword, cb);
            return false;
        }
    }

    render() {
        const messages = this.state.messages;
        const props = this.props;        
        const current_user = props.page.current_user();
        const current_thread = props.page.current_thread();        

        return (        
        <div>
            <div className='search-messages-box mb-3'>
                <div className="input-group with-shadow">
                    <input type="text" className="form-control" name='search-input' 
                        aria-label="" placeholder='Search messages' onKeyDown={this.handleSearchEnter}/>
                    <span className="input-group-addon">
                        <i className='fa fa-search'></i>
                    </span>
                </div>
            </div>
                
            <div className='messages-list'>
                { messages.length ? 
                    messages.map(function(msg, idx) {
                        return <SearchMessage {...props} is_me={msg.user_id === current_user.id} 
                                    message={msg} key={idx} />
                    }) : 
                    <p> No messages found or no search terms.</p>
                }
            </div>
        </div>
        );
    }
}

class PinnedMessage extends React.Component {
    render() {
        const page = this.props.page;
        const message = this.props.message;
        const profile_pic = message.user.profile_pic ? 
            message.user.profile_pic : '/static/img/user_default.png';

        return (
        <div className='pinned-msg media'>
            <img className='msg-profile-pic' src={ profile_pic } />
            <div className='msg-content media-body'>
                { message.subject ? <h5>{message.subject}</h5> : '' }
                <MessageContent message={message} />
                <span className='pin-msg' onClick={() => { page.pin_message(message.id, !message.is_pinned); }}>
                    <i className='fa fa-thumb-tack' /> 
                </span>
            </div>
        </div>
        );
    }
}

class PinnedMessagesContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            search_term: ''
        }

        this.set_messages = this.set_messages.bind(this);
        this.handleChange = this.handleChange.bind(this);
        
    }

    set_messages(msgs) {
        const pinned_msgs = _.reverse(_.filter(msgs, function(m) { return m.is_pinned; }));

        this.setState(prevState => ({
            messages: pinned_msgs
        }));
    }
    
    componentDidMount() {        
        this.props.page.dm().add_watch_callback(this.set_messages);
    }

    handleChange(e) {
        this.setState({
            search_term: e.target.value
        });
    }

    render() {        
        const props = this.props;
        const that = this;        
        const messages = _.filter(this.state.messages, function(m) {
            return (m.content && m.content.indexOf(that.state.search_term) > -1) || 
                (m.subject && m.subject.indexOf(that.state.search_term) > -1) || 
                (that.state.search_term === '');
        });

        return (
        <div>
            <div className='pinned-messages-search mb-4'>
                <div className='input-group with-shadow'>
                    <input type='text' className='form-control' name='pinned-messages-search-input' 
                        placeholder='Search pinned messages'
                        id='pinned-messages-search-input' onChange={this.handleChange} />
                    <span className='input-group-addon'>
                        <i className='fa fa-search'/>
                    </span>
                </div>                
            </div>
            <div className='pinned-messages-list'>
                {messages.map(function(msg, idx) {
                    return <PinnedMessage message={msg} key={idx} {...props} />
                })}
            </div>
        </div>
        );
    }
}

class AttachmentItem extends React.Component {

    render() {
        var item = this.props.item;

        return (
        <div className='media-item' id={ item.id + '-media-item' }>
            <a href={ item.url }>
                <img src={ item.thumbnail } alt={ item.name } />
            </a>
        </div>
        );
    }

}

class AttachmentsContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            items: []
        }

        this.set_messages = this.set_messages.bind(this);
    }

    set_messages(msgs) {
        var items = [];

        for (var i=0; i < msgs.length; i++) {
            if (msgs[i].message_type === "media_message") {
                for (var j=0; j < msgs[i].media.length; j++) {
                    items.push(msgs[i].media[j]);
                }
            }
        }

        this.setState(prevState => ({
            items: _.reverse(items)
        }));
    }

    componentDidMount() {
        this.$el = $(this.el);        
        this.props.page.dm().add_watch_callback(this.set_messages);
    }

    componentDidUpdate() {    
        this.$el.magnificPopup('destroy');
        this.$el.magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true,            
                navigateByImgClick: true
            }
        });
    }

    componentWillUnmount() {
        this.$el.magnificPopup('destroy');
    }

    render() {
        var items = this.state.items;
        var attachments = items.length === 0 ? <div className='col-12'><p><i>No attachments.</i></p></div> :
            items.map(function(item, idx) {
                return <AttachmentItem item={item} key={idx}/>
            });
        
        return (
        <div ref={el => this.el = el} className='media-grid' id='attachments-media-grid'>
            { attachments }
        </div>
        );
    }

}

function Editor(page) {
    var editor = this;
    var page = page;
    
    var $editor = $('#messages-editor-basic');
    var $input = $editor.find('textarea');
    var $emoji_btn = $('#messages-emoji-btn');
    var $send_btn = $('#messages-send-btn');

    var $add_media_form = $('#add-media-form');
    var $add_media_input = $('#messages-media-attachment-input');
    var $add_media_preview = $('#messages-media-attachment-preview');
    var $add_media_wrapper = $('#messages-media-attachment-wrapper');
    var $add_media_x_btn = $add_media_wrapper.find('.x-btn');

    var $emojipicker_wrapper = $('#emojipicker-wrapper');
    var emojipicker_root = document.getElementById('react-root__emojipicker');


    var emojipicker_box = ReactDom.render(<EmojiPicker onChange={function(data) {
        var cur_value = $input.val();        
        var emo = to_emoji(data.unicode);
        $input.val(cur_value + emo + '\u00A0');

    }}/>, emojipicker_root);
    
    $emoji_btn.on('click', function() {
        $emojipicker_wrapper.show();
    });

    $(document).on('mouseup', function(e) {
        if (!$emojipicker_wrapper.is(e.target) &&
            $emojipicker_wrapper.has(e.target).length === 0) 
            $emojipicker_wrapper.hide();
    });

    function handle_on_click_send() {
        $send_btn.on('click', function() {
            send();
        });
    }

    function handle_on_input_enter() {
        
        //if (window.mobilecheck()) { return false; }

        $input.on('keypress', function(e) {

            if (e.keyCode === 13 && e.shiftKey) {
                e.preventDefault();            
                var val = this.value;

                if (typeof this.selectionStart === "number" && typeof this.selectionEnd === "number") {
                    var start = this.selectionStart;
                    this.value = val.slice(0, start) + "\n" + val.slice(this.selectionEnd);
                    this.selectionStart = this.selectionEnd = start + 1;
                } else if (window.document.selection && window.document.selection.createRange) {                    
                    var range = window.document.selection.createRange();
                    range.text = "\r\n";
                    range.collapse(false);
                    range.select();
                }
                return false;            
            }
            if (e.keyCode === 13 && !e.shiftKey) {
                e.preventDefault();
                send();
                return false;
            }            

        })
    }

    function send() {
        $add_media_wrapper.hide();
        var content = htmlSanitizer($input.val());
        var files = $add_media_input[0].files;

        if (!(content || files.length)) return false;

        if (!files.length) {
            page.new_message(null, content);
        } else {            
            var cb = function(d, e) {
                return false;
            }

            // SEND NEW MEDIA MESSAGE HERE
            $add_media_form.find('input[name="content"]').val(content);            
            var data = new FormData($add_media_form[0]);

            query(API.MESSAGES_CREATE_MEDIA_MESSAGE, 'POST', data, cb);

            $add_media_input[0].value = '';
        }
        
        $input.val('');
    }
    
    $add_media_input.on('change', function() { 
        var files = this.files;
        $add_media_preview.html('');

        if (!files.length) {
            $add_media_wrapper.hide();
            return false;
        }
        
        var __show = false;

        for (var i=0; i < files.length; i++) {
            var f = files[i];
            var reader = new FileReader();
            reader.onload = (function (fp) {
                return function (e) {
                    if (!__show) $add_media_wrapper.show();
                    else __show = true;

                    $add_media_preview.append(build_file_preview(fp.type, fp.name, e.target.result));
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    $add_media_x_btn.on('click', function() {
        $add_media_wrapper.hide();
        $add_media_input[0].value = '';
    });

    function init() {
        handle_on_input_enter();
        handle_on_click_send();

        // setTimeout(function() {
        //     $input.focus();
        // }, 0);
    }    

    init();

    $.extend(this, {        
    });
}

function AdvancedEditor(page) {
    var page = page;
    var $advanced_toggle = $('#messages-advanced-btn');
    var $editor = $('#messages-editor-advanced');
    var $subject_input = $('#messages-editor-advanced-subject-input');
    var $input = $('#messages-editor-advanced-input');
    var $x_btn = $('#messages-editor-advanced-x');
    var $send_btn = $('#messages-editor-advanced-send');
    var $close_btn = $('#messages-editor-advanced-close');

    var $editor_views = $('.messages-editor-advanced-view');
    var $template_select = $('#messages-editor-advanced-template-select');

    function setup() {
        $input.trumbowyg({
            removeformatPasted: true,
            autogrow: false
        });

        $advanced_toggle.on('click', function() {
            $editor.toggleClass('active');            
        });

        $x_btn.on('click', function() {
            $editor.removeClass('active');
        });

        $close_btn.on('click', function() {
            $editor.removeClass('active');
        });

        $send_btn.on('click', function() {
            send();
        });

        $template_select.on('change', function(e) {
            var selected = $template_select.find('option:selected');
            var $target_view = $(selected.attr('data-target_view'));

            $editor_views.addClass('hide');
            $target_view.removeClass('hide');

            var view_type = $target_view.attr('data-view_type');
            if (view_type === 'iframe') {
                var $iframe = $target_view.find('iframe');
                $iframe.attr('src', $iframe.attr('data-href'));
            }
        });

        window.submit_callback = function() {            
            $editor.removeClass('active');
        }
            
    }

    function send() {
        var subject = htmlSanitizer($subject_input.val());
        var content = htmlSanitizer($input.trumbowyg('html'));
        if (!content) return false;
        page.new_message(subject, content);
        $input.trumbowyg('empty');
        $editor.removeClass('active');
    }

    function init() {
        setup();
    }

    init();

    $.extend(this, {
    })
}

function UsersTab(page) {
    var page = page;
    var $form = $('#add-users-form');
    var $add_btn = $('#add-users-btn');
    var $remove_btns = $('.remove-users-btn');
    var $leave_btn = $('#leave-thread-btn');

    var $search_input = $('#search-users-input');
    var $search_output = $('#search-users-output');
    var $added_users = $('#search-users-added');
    var added_users = [];

    $search_input.on('keyup', function(e) {
        e.preventDefault();

        if (e.keyCode === 13) {            
            $search_output.html('');

            if ($search_input.val() === '') {
                return false;
            }
            
            var cb = function(d, err) {
                if (err != null) return false;

                var items = d.result.map(function(user, idx) {
                    return user_item_tpl.render({user: user});
                });
                
                $search_output.html(items.join(''));

                handle_select_users();
            }
            
            query(API.ACCOUNT_SEARCH, 'GET', {search_term: $search_input.val()}, cb);

            return false;
        }
    });

    function handle_select_users() {
        $('.search-users-item').off('click')
        .on('click', function() {            
            var user_id = this.getAttribute('data-user_id');
            var user_name = this.getAttribute('data-user_name');

            if (added_users.indexOf(user_id) > -1) return false;        

            var el = added_user_tpl.render({
                user: {id: user_id, name: user_name}
            });

            var $el = $(el);

            added_users.push(user_id);
            $added_users.append($el);

            $el.find('.x-btn').on('click', function() {
                added_users.pop(user_id);
                $el.remove();
            });
        });
    }

    $add_btn.on('click', function() {
        
        var cb = function(d, err) {
            if (err != null) return false;
            location.reload();
        };

        query(API.MESSAGES_ADD_USERS_TO_THREAD, 'POST', $form.serialize(), cb);
    });    

    $remove_btns.on('click', function() {
        if (!confirm('Are you sure to remove this user from the thread?')) return false;

        var user_id = this.getAttribute('data-user_id');

        var cb = function(d, err) {
            if (err != null) {
                alert(err.responseText);
                return false;
            }            
            return location.reload();
        }

        query(API.MESSAGES_REMOVE_USERS_FROM_THREAD, 'POST', {
            thread_id: page.current_thread().id,
            users: [user_id]
        }, cb);
    });

    $leave_btn.on('click', function() {
        if (!confirm('Are you sure to leave this thread?')) return false;
        
        var user_id = this.getAttribute('data-user_id');

        var cb = function(d, err) {
            if (err != null) {
                alert(err.responseText);
                return false;
            }            
            parent.location.href = '/messages';
        }

        query(API.MESSAGES_REMOVE_USERS_FROM_THREAD, 'POST', {
            thread_id: page.current_thread().id,
            users: [user_id]
        }, cb);
    });

    $.extend(this, {
    });

}

function MessageThreadPage() {
    var page = this;

    var $thread_initial_data = $('#thread-initial-data');    

    var current_thread = JSON.parse($thread_initial_data.attr('data-current_thread'));
    var current_user = JSON.parse($thread_initial_data.attr('data-current_user'));

    var $tabs = $('.tab');
    var $views = $('.tab-view');
        
    var dm = new DataManager({page, current_thread, current_user});

    var editor = new Editor(page);
    var adv_editor = new AdvancedEditor(page);
    var users_tab = new UsersTab(page);

    var messages_box_root = document.getElementById('react-root__messages_box');
    var messages_box = null;

    var pinned_messages_box_root = document.getElementById('react-root__pinned_messages_box');
    var pinned_messages_box = null;

    var search_messages_box_root = document.getElementById('react-root__search_messages_output_box');
    var search_messages_box = null;
    
    var attachments_box_root = document.getElementById('react-root__attachments_box');
    var attachments_box = null;
    
    function get_dm() {
        return dm;
    }
    
    function get_current_thread() {
        return current_thread;
    }

    function get_current_user() {
        return current_user;
    }

    function get_messages_box() {
        return messages_box;
    }

    function get_pinned_messages_box() {
        return pinned_messages_box;
    }

    function handle_tabs() {
        // first time set active tab
        var hash = window.location.hash || '#messages';
        switch_tab(hash);

        $tabs.on('click', function(e) {
            e.preventDefault();        
            var id = this.getAttribute('data-view');
            window.location.hash = id;
            return false;
        });
        
        $(window).on('hashchange', function() {
            var hash = window.location.hash || '#messages';
            switch_tab(hash);
        });
    }

    function switch_tab(id) {
        $views.removeClass('active');
        $tabs.removeClass('active');
        
        $(id).addClass('active');
        $('.tab[data-view="' + id + '"]').addClass('active');        
    }

    function new_message(subject, content) {        

        var data = {
            user_id: current_user.id,            
            thread_id: current_thread.id,
            content: content,
            subject: subject,
        }
        
        dm.new_message(data);
    }    

    function pin_message(message_id, is_pinned) {
        var data = {
            message_id: message_id,
            is_pinned: is_pinned,
            pinned_by: is_pinned ? current_user.id : null
        }
        dm.pin_message(data);
    }

    function save_message(message_id, is_saved) {

    }

    function get_message(message_id) {
        var data = {
            message_id: message_id,
            thread_id: current_thread.id
        }
        dm.get_message(data);
    }

    function init() {
        handle_tabs();
        // SET UP MESSAGES BOX HERE

        messages_box = ReactDom.render(<MessagesContainer page={page} />, messages_box_root);        
        pinned_messages_box = ReactDom.render(<PinnedMessagesContainer page={page} />, pinned_messages_box_root);
        search_messages_box = ReactDom.render(<SearchMessagesContainer page={page} />, search_messages_box_root);
        attachments_box = ReactDom.render(<AttachmentsContainer page={page} />, attachments_box_root);
        dm.get_history();
        
    }

    $.extend(this, {
        init: init,
        dm: get_dm,
        current_user: get_current_user,
        current_thread: get_current_thread,
        messages_box: get_messages_box,
        pinned_messages_box: get_pinned_messages_box,
        attachments_box: attachments_box,
        new_message: new_message,
        pin_message: pin_message,
        get_message: get_message
    });
}

export const start = function() {
    var page = new MessageThreadPage();
    page.init();
}


export {
    DataManager,
    Message,
    MessageContent,
    MessagesContainer,
    AttachmentItem,
    AttachmentsContainer
}