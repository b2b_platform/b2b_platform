import { query, get_params, generate_qs } from 'js//common_utils';
import { API } from 'js/api';

export const start_page = function() {
    var $search_btn = $('#threads-search-btn');
    var $search_input = $('#threads-search-input');
    var $search_opts_select = $('#search-opts-select');
    var $pages = $('.page-item');
    var $thread_type_select = $('select[name="thread_type"]');
    var $archive_btns = $('.thread-archive-btn');

    var params = get_params();

    function on_update_keyword() {
        params.search_opt = $search_opts_select.val();
        params.search_term = $search_input.val();
        params.current_page = 1; // reset to page 1        
        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $thread_type_select.on('change', function() {
        params.thread_type = this.value;
        params.current_page = 1;
        location.href = location.pathname + '?' + generate_qs(params);
    });
 
    
    $archive_btns.on('click', function() {
        var thread_id = this.getAttribute('data-thread_id');
        var user_id = this.getAttribute('data-user_id');
        var is_archived = this.getAttribute('data-is_archived') != 'false';
        
        var cb = function(d,e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }

            location.reload();
            return false;
        }

        var data = {thread_id: thread_id, user_id: user_id};        
        
        if (is_archived) {
            query(API.MESSAGES_UNARCHIVE_THREAD, 'POST', data, cb);
        } else {
            query(API.MESSAGES_ARCHIVE_THREAD, 'POST', data, cb);
        }
    });
}