import { query } from 'js/common_utils';
import { API } from 'js/api';
import { user_item_tpl, added_user_tpl } from './tpls';

export const handle_new_thread_form = function(usecapt) {    
    var $form = $('#new-thread-form');
    var $search_input = $('#search-users-input');
    var $search_output = $('#search-users-output');
    var $added_users = $('#search-users-added');
    var added_users = [];


    $search_input.on('keyup', function(e) {
        e.preventDefault();

        if (e.keyCode === 13) {            
            $search_output.html('');

            if ($search_input.val() === '') {
                return false;
            }
            
            var cb = function(d, err) {
                if (err != null) return false;

                var items = d.result.map(function(user, idx) {
                    return user_item_tpl.render({user: user});
                });
                
                $search_output.html(items.join(''));

                handle_select_users();
            }
            
            query(API.ACCOUNT_SEARCH, 'GET', {search_term: $search_input.val()}, cb);

            return false;
        }
    });

    function handle_select_users() {
        $('.search-users-item').off('click')
        .on('click', function() {            
            var user_id = this.getAttribute('data-user_id');
            var user_name = this.getAttribute('data-user_name');

            if (added_users.indexOf(user_id) > -1) return false;        

            var el = added_user_tpl.render({
                user: {id: user_id, name: user_name}
            });

            var $el = $(el);

            added_users.push(user_id);
            $added_users.append($el);

            $el.find('.x-btn').on('click', function() {
                added_users.pop(user_id);
                $el.remove();
            });
        });
    }
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                var thread = d.result;
                var thread_url = '/messages/t/' + thread.id;
                parent.location.href = thread_url;            
                return false;
            };        

            query(API.MESSAGES_CREATE_THREAD, 'POST', $form.serialize(), cb);
            
            return false;            
        },
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            users: {
                required: true
            }
        }
    });

}
