import nunjucks from 'nunjucks';

export const user_item_tpl = nunjucks.compile(`
<div class='search-users-item media' data-user_name='{{ user.name }}' data-user_id='{{ user.id }}'>
    <img height='30px' width='30px' class='rounded-circle' alt='{{ user.email }}'
        src='{% if not user.profile_pic %} /static/img/user_default.png {% else %}{{ user.profile_pic }}{% endif %}'>
    <div class='media-body'>
        <div><b>{{ user.name }}</b></div>
        <div><i>{{ user.company_name }}</i></div>
    </div>
    {% if show_message_btn %}
    <a class='message-user-btn' href='/messages/private?user_id={{ user.id }}'>
        <span class='btn-message btn-sm'>Message</span>
    </a>
    {% endif %}
</div>
`);

export const added_user_tpl = nunjucks.compile(`
<span id='added-user-{{ user.id }}' class="added-user-item">{{ user.name }}
<input type='hidden' name='users' value='{{ user.id }}'>
<span class='fa fa-close x-btn'></span>
</span>
`);

export const added_blocked_user_tpl = nunjucks.compile(`
<span id='added-user-{{ user.id }}' class="added-user-item">{{ user.name }}
<input type='hidden' name='privacy_blocked_users' value='{{ user.id }}'>
<span class='fa fa-close x-btn'></span>
</span>
`);

export const thread_view_iframe_tpl = nunjucks.compile(`
<div class="thread-loading">
    <img height='30px' width='30px' src="/static/img/loading.gif" id="thread-iframe-loading">
</div>
<div id='thread-view-iframe-wrapper'>
<iframe height='100%' frameBorder="0" width='100%' src='/messages/t/{{ thread_id }}?view=clean'
    data-thread_id='{{ thread_id }}' onload='window.hide_thread_iframe_loading("#thread-iframe-loading");'></iframe>
</div>
<script>
    window.hide_thread_iframe_loading = function() {
        document.getElementById('thread-iframe-loading').style.display = 'none';
        document.getElementById('thread-view-iframe-wrapper').style.display = 'block';
        document.getElementById('thread-view-iframe-wrapper').style.height = '100%';
    }
</script>
`);

export const thread_iframe_tpl = nunjucks.compile(`
<div class="thread-loading">
    <img height='30px' width='30px' src="/static/img/loading.gif" id="thread-iframe-loading">
</div>
<div id='thread-view-iframe-wrapper'>
<iframe height='100%' frameBorder="0" width='100%' src='{{ url }}' 
    onload='window.hide_thread_iframe_loading("#thread-iframe-loading");'></iframe>
</div>
<script>
    window.hide_thread_iframe_loading = function() {
        document.getElementById('thread-iframe-loading').style.display = 'none';
        document.getElementById('thread-view-iframe-wrapper').style.display = 'block';
        document.getElementById('thread-view-iframe-wrapper').style.height = '100%';
    }
</script>
`)