import { query, get_params, generate_qs } from './common_utils';
import { API } from './api';

export const start_page = function() {

    var $select = $('#select-notification-type');    

    var $pages = $('.page-item');

    var params = get_params();
    
    $select.on('change', function() { 
        params.notification_type = this.value;
        params.current_page = 1;    
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });


}