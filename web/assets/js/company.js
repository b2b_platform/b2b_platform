import { query, generate_qs, get_params } from './common_utils';
import _ from 'lodash';
import { handle_editable_table } from './forms';
import { API } from './api';

export const handle_create_company_form = function() {
    var company_create_form = $('#company_create_form');

    company_create_form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                // should redirect to company new page - waiting for approval from system admin
                location.href = '/my-company';
                return false;
            };
            query(API.COMPANY_CREATE, 'POST', $(form).serialize(), cb);
            return false;
        },
        rules: {
            location: "required",
            company_short_hand: "required",
            name: "required",
            email: "required"
        }
    });
}


export const my_company_page = function() {
    var cb = function() {
        var $form = $('#company_update_form');
        var data = new FormData($form[0]);
        query(API.COMPANY_UPDATE, 'POST', data, function(d, e) {
            if (e != null) {
                alert(e);
            } else {
                console.log(d);
            }
            location.reload();
            return false;
        });        
    }
    handle_editable_table('#my-company-registration-table', cb);
    handle_editable_table('#my-company-general-table', cb);
    handle_editable_table('#my-company-contact-table', cb);
    handle_editable_table('#my-company-manufacturing-table', cb);
    handle_editable_table('#my-company-other-details-table', cb);    
}

function CompanyPage() {

    // Placeholders for React
    // init params
    var params = {
      /*  keyword: '',
        category: '',
        posted_at: 'last_hour',
        sorted_by: 'earliest',
        limit: 50,
        page: 1,
        num_pages: 1
        */
    };

    var is_init = false;
    _.extend(params, get_params());

    function get_company_info() {
        var cb = function(d, e) {
            if (e != null) return false;            
            update_company_dom(d.result);

            if (!is_init) is_init = true;                
        }
        query(API.COMPANY_GET, 'GET', params, cb);
    }
    
    function get_employee_info() {
        var cb = function(d, e) {
            if (e != null) return false;            
            update_employee_dom(d.result_list);

            if (!is_init) is_init = true;                
        }
        query(API.COMPANY_EMPLOYEE_LIST, 'GET', params, cb);
    }
    
 
    function update_company_dom(data) {
        $.each(data, function( fieldname, value ) {
          if (value != null) {   
            $('#company_info span[name=' + fieldname + ']').html(value);
          } else {
            $('#company_info span[name=' + fieldname + ']').html("N/A");
          }
        });
    }
    
    function update_employee_dom(data) {
        var emp_html = '';
        $.each(data, function (index, emp) {
          emp_html += '<div class="col-sm-6 col-md-4"><a href="' + emp.id + '">' + emp.email +  '</a></div>'
             + '<div class="col-sm-3 col-md-4">' + emp.last_name  + ' </div>'
             + '<div class="col-sm-3 col-md-4">' + emp.first_name  + ' </div>';
        });
        $("#employee_list").html(emp_html);
    }

    function load_carousel() {
        $.ready(function() {
            $('#carousel-company').carousel({
            });
        });
    }

    function init() {
        ('[data-target=#first-tab]').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });
        get_company_info();
        get_employee_info();
      //  $('.nav-tabs a:first').tab('show'); 
    }
        
    $.extend(this, {
        init: init        
    });
    
}

export const start_company_page = function() {
    var page = new CompanyPage(); 
    page.init();
}

    
export const link_company_page = function() {
    var $more_info_btns = $('.more-info-btn');
    var $link_company_btns = $('.link-company-btn');
    var $search_company_btn = $('#search-company-btn');
    var $pages = $('.page-item');

    var params = get_params();

    $link_company_btns.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        
        var fdata = { 'company_id' : this.getAttribute('data-company_id')};           
        query(API.COMPANY_LINK, 'POST', fdata, cb);
           
        return false;
    });

    $search_company_btn.click(function() {
        $("#company_search_form").submit();
    })

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });
}

export const company_employee_page = function() {
   
    var $search_employee_btn = $('#search-emp-btn');
    var $search_user_input = $('#search-user-input');
    var $search_user_btn = $('#search-user-btn');
    //var $search_user_add_btns = $('#  .invite-user-btn');
    var $search_user_add_btns = $('#user_candidate_list td');
    var $roleModal = $('#roleModal');
    var $permission_btns = $('.permission-badge-btn');
    var $modal_save_btn = $('#modal-save-btn');
    var $request_btn_accept = $('.request-btn-accept');
    var $request_btn_reject = $('.request-btn-reject');

    function get_employee_info() {
        var params = {};
        var cb = function(d, e) {
            if (e != null) return false;            
            update_employee_dom(d.result);           
        }
        query(API.COMPANY_EMPLOYEE_LIST, 'GET', params, cb);
    }
    
    function update_employee_dom(data) {
        var emp_html = '';
        $.each(data, function (index, emp) {
          emp_html += '<div class="row">' 
             + '<div class="col-sm-6 col-md-4"><a href="' + emp.id + '">' + emp.email +  '</a></div>'
             + '<div class="col-sm-3 col-md-4">' + emp.last_name  + ' </div>'
             + '<div class="col-sm-3 col-md-4">' + emp.first_name  + ' </div>  </div>';
        });
        $("#employee_list").html(emp_html);
    }
     
    function get_user_info() {
        
        var params = {};
        params.search_term = $search_user_input.val();
        var cb = function(d, e) {
            if (e != null) return false;            
            update_user_dom(d.result);              
        }
        query(API.ACCOUNT_SEARCH, 'GET', params, cb);
    }
    
    function update_user_dom(data) {
        var emp_html = '';
        $.each(data, function (index, emp) {
            emp_html += '<tr><th scope="row">' + emp.id
            + '</th><td>' + emp.last_name
            + '</td><td>' + emp.first_name 
            + '</td><td>' + emp.email
            + '</td><td><button class="btn btn-primary btn-sm invite-user-btn" data-userid="' + emp.id + '">Invite</button>'
            + '</td></tr>';
        });
        $("#user_candidate_list").html(emp_html);
    }
    $roleModal.modal({ show: false})
    
    // $search_user_add_btns.click(function() {
    $('#user_candidate_list').on('click', 'td button', function () {
        var target_user = this.getAttribute('data-userid');
       
        var cb2 = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        var fdata = { 'target_user' : target_user };           
        query(API.ROLE_CREATE, 'POST', fdata, cb2);
       
        return false;
    });  


    $permission_btns.click(function() {
        $("#permission-input").val(this.getAttribute('data-permission'));
        $("#target-user-input").val(this.getAttribute('data-targetuser'));
        $("#company-id-input").val(this.getAttribute('data-company_id'));
        $roleModal.modal('show');
        
    });  

    $.fn.exists = function () {
        return this.length !== 0;
    }

    // Updating permission
    $modal_save_btn.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        
        var params = new Object();
        params.target_user = $("#target-user-input").val();
        params.permission = $("#permission-input").val();
        if ($("#company-id-input").exists()) {
            params.company_id = $("#company-id-input").val();
        }  
        //alert('fdata id' + target_user_id + ' permission: ' +  permission);      
        query(API.ROLE_UPDATE, 'POST', params, cb);
        
        return false;
    });  
    
    // Accept  pending request
    $request_btn_accept.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            location.reload();
            return false;
        };

        var params = new Object();
        params.target_user = this.getAttribute('data-targetuser');
        query(API.ROLE_UPDATE, 'POST',  params, cb);
        return false;
    });  

    // Reject  pending request
    $request_btn_reject.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            location.reload();
            return false;
        };

        var params = new Object();
        params.target_user = this.getAttribute('data-targetuser');
        query(API.ROLE_DELETE, 'POST',  params, cb);
        return false;
    }); 

    $('#search-user-btn').on('click', get_user_info);
}


export const verify_page = function() {
    var company_verify_form = $('#company_verify_form');
    company_verify_form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                // should redirect to company new page - waiting for approval from system admin
                location.reload();
                return false;
            };
            query(API.COMPANY_VERIFY, 'POST', $(form).serialize(), cb);
            return false;
        },
        rules: {
            location: "required",
            company_short_hand: "required",
            name: "required",
            email: "required"
        }
    });
    
}