import { API } from './api';
import { query } from './common_utils';

export const start_page = function() {
    var $form = $('#bulk-product-inquiry-form');    

    $form.validate({
        ignore: [],
        rules: {
            text: "required",
        }
    });
}