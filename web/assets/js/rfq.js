import { query } from './common_utils';
import { API } from './api';
import { select_category_cascade } from './product_category';

export const handle_create_rfq_form = function() {
    select_category_cascade('#create-rfq-select-category');

    var $form = $('#create-rfq-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                if (window.parent.submit_callback === undefined) {
                    location.href = '/manage-rfqs';
                } else {
                    window.parent.submit_callback();
                }
                return false;
            };
            var data = new FormData($form[0]);
            query(API.RFQ_CREATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            product: {
                required: true
            },
            quantity: {
                required: true
            },
            product_category_id: {
                required: true,
            },
            content: {
                required: true
            }
        }
    });

}


export const handle_update_rfq_form = function() {
    select_category_cascade('#update-rfq-select-category');

    var $form = $('#update-rfq-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                location.href = '/rfq/' + d.result.id;
                return false;
            };
            var data = new FormData($form[0]);
            query(API.RFQ_UPDATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            product: {
                required: true
            },
            quantity: {
                required: true
            },
            product_category_id: {
                required: true,
            },
            content: {
                required: true
            }
        }
    });

}