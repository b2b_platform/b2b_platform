const BASE_URL = '/api/v1';

export const API = {

    // HELLO
    HELLO_PROTECTED: BASE_URL + '/hello',
    HELLO_PROTECTED: BASE_URL + '/hello/protected',

    // ACCOUNTS
    ACCOUNT_GET: BASE_URL + '/account.get',
    ACCOUNT_UPDATE: BASE_URL + '/account.update',
    ACCOUNT_CHANGE_PASSWORD: BASE_URL + '/account.change_password',
    ACCOUNT_SEARCH: BASE_URL + '/account.search',
    ACCOUNT_LOG_ACTIVITY: BASE_URL + '/account.log',
    ACCOUNT_ADD_TO_BASKET: BASE_URL + '/account.add_to_basket',
    ACCOUNT_REMOVE_FROM_BASKET: BASE_URL + '/account.remove_from_basket',

    // AUTH
    IS_LOGGED_IN: BASE_URL + '/auth.is_logged_in',
    REGISTER_URL: BASE_URL + '/auth.register',
    LOGIN_URL: BASE_URL + '/auth.login',
    REQUEST_TOKEN: BASE_URL + '/auth.request_token',
    REFRESH_TOKEN: BASE_URL + '/auth.refresh_token',
    REMOVE_TOKEN_COOKIES: BASE_URL + '/auth.remove_token_cookies',

    // COMPANY
    COMPANY_GET: BASE_URL + '/company.get',
    COMPANY_CREATE: BASE_URL + '/company.create',
    COMPANY_UPDATE: BASE_URL + '/company.update',
    COMPANY_LIST: BASE_URL + '/company.list',
    COMPANY_SEARCH: BASE_URL + '/company.search',
    COMPANY_DELETE: BASE_URL + '/company.delete',
    COMPANY_DATA: BASE_URL + '/company/',
    COMPANY_LINK: BASE_URL + '/company.link',
    COMPANY_EMPLOYEE_LIST: BASE_URL + '/company.employee_list',
    COMPANY_LEAVE: BASE_URL + '/company.leave',

    // PRODUCT

    PRODUCT_GET: BASE_URL + '/product.get',    
    PRODUCT_CREATE: BASE_URL + '/product.create',
    PRODUCT_SEARCH: BASE_URL + '/product.search',
    PRODUCT_LIST: BASE_URL + '/product.list',
    PRODUCT_UPDATE: BASE_URL + '/product.update',
    PRODUCT_DELETE: BASE_URL + '/product.delete',
    PRODUCT_DUPLICATE: BASE_URL + '/product.duplicate',
    PRODUCT_EXPORT: BASE_URL + '/product.export',

    PRODUCT_CATEGORY_GET: BASE_URL + '/product_category.get',
    PRODUCT_CATEGORY_CREATE: BASE_URL + '/product_category.create',
    PRODUCT_CATEGORY_LIST: BASE_URL + '/product_category.list',

    // RFQ
    RFQ_GET: BASE_URL + '/rfq.get',
    RFQ_LIST: BASE_URL + '/rfq.list',
    RFQ_SEARCH: BASE_URL + '/rfq.search',
    RFQ_CREATE: BASE_URL + '/rfq.create',
    RFQ_UPDATE: BASE_URL + '/rfq.update',
    RFQ_DELETE: BASE_URL + '/rfq.delete',

    // QUOTE
    QUOTE_GET: BASE_URL + '/quote.get',
    QUOTE_CREATE: BASE_URL + '/quote.create',
    QUOTE_UPDATE: BASE_URL + '/quote.update',
    QUOTE_LIST: BASE_URL + '/quote.list',

    // TRADE_LEAD
    TRADE_LEAD_GET: BASE_URL + '/trade_lead.get',
    TRADE_LEAD_LIST: BASE_URL + '/trade_lead.list',
    TRADE_LEAD_SEARCH: BASE_URL + '/trade_lead.search',
    TRADE_LEAD_CREATE: BASE_URL + '/trade_lead.create',
    TRADE_LEAD_UPDATE: BASE_URL + '/trade_lead.update',
    TRADE_LEAD_DELETE: BASE_URL + '/trade_lead.delete',

    // SEARCH
    SEARCH: BASE_URL + '/search',
    REGION_SELECT_LIST: BASE_URL + '/region.list',

    // MESSAGES
    MESSAGES_CREATE_THREAD: BASE_URL + '/messages.create_thread',
    MESSAGES_DELETE_THREAD: BASE_URL + '/messages.delete_thread',
    MESSAGES_ARCHIVE_THREAD: BASE_URL + '/messages.archive_thread',
    MESSAGES_UNARCHIVE_THREAD: BASE_URL + '/messages.unarchive_thread',
    MESSAGES_GET_THREAD: BASE_URL + '/messages.get_thread',
    MESSAGES_GET_PRODUCT_INQUIRY_THREADS: BASE_URL + '/messages.get_product_inquiry_threads',
    MESSAGES_GET_THREAD_MESSAGES: BASE_URL + '/messages.get_thread_messages',
    MESSAGES_GET_THREAD_ATTACHMENTS: BASE_URL + '/messages.get_thread_attachments',
    MESSAGES_ADD_USERS_TO_THREAD: BASE_URL + '/messages.add_users_to_thread',
    MESSAGES_REMOVE_USERS_FROM_THREAD: BASE_URL + '/messages.remove_users_from_thread',
    MESSAGES_CREATE_MEDIA_MESSAGE: BASE_URL + '/messages.create_media_message',
    MESSAGES_GET_MESSAGE: BASE_URL + '/messages.get_message',
    MESSAGES_GET_UNREAD_MESSAGES_COUNT: BASE_URL + '/messages.get_unread_messages_count',
    MESSAGES_LIST_THREADS: BASE_URL + '/messages.list_threads',

    // NOTIFICATION
    NOTIFICATION_GET: BASE_URL + '/notification.get',
    NOTIFICATION_GET_UNREAD: BASE_URL + '/notification.get_unread',
    NOTIFICATION_GET_UNREAD_COUNT: BASE_URL + '/notification.get_unread_count',
    NOTIFICATION_READ_ALL: BASE_URL + '/notification.read_all',

    // MEDIA
    COMPANY_UPLOAD_MEDIA: BASE_URL + '/company.upload_media',
    COMPANY_DELETE_MEDIA: BASE_URL + '/company.delete_media',
    COMPANY_VERIFY: BASE_URL + '/company.verify',

    PRODUCT_UPLOAD_MEDIA: BASE_URL + '/product.upload_media',
    PRODUCT_DELETE_MEDIA: BASE_URL + '/product.delete_media',

    RFQ_UPLOAD_MEDIA: BASE_URL + '/rfq.upload_media',
    RFQ_DELETE_MEDIA: BASE_URL + '/rfq.delete_media',

    QUOTE_UPLOAD_MEDIA: BASE_URL + '/quote.upload_media',
    QUOTE_DELETE_MEDIA: BASE_URL + '/quote.delete_media',

    TRADE_EVENT_UPLOAD_MEDIA: BASE_URL + '/trade_event.upload_media',
    TRADE_EVENT_DELETE_MEDIA: BASE_URL + '/trade_event.delete_media',

    // ROLES
    ROLE_GET: BASE_URL + '/role.get',
    ROLE_LIST: BASE_URL + '/role.list',
    ROLE_SEARCH: BASE_URL + '/role.search',
    ROLE_CREATE: BASE_URL + '/role.create',
    ROLE_UPDATE: BASE_URL + '/role.update',
    ROLE_DELETE: BASE_URL + '/role.delete',

    // SUPPORT TICKET
    SUPPORT_TICKET_GET: BASE_URL + '/support_ticket.get',
    SUPPORT_TICKET_CREATE: BASE_URL + '/support_ticket.create',
    SUPPORT_TICKET_UPDATE: BASE_URL + '/support_ticket.update',
    SUPPORT_TICKET_DELETE: BASE_URL + '/support_ticket.delete',
    SUPPORT_TICKET_LIST: BASE_URL + '/support_ticket.list',

    // ADMIN
    ADMIN_UPDATE_USER: BASE_URL + '/admin.update_user',
    ADMIN_UPDATE_COMPANY: BASE_URL + '/admin.update_company',
    ADMIN_UPDATE_COMPANY_MEMBERSHIP: BASE_URL + '/admin.update_company_membership'
};
