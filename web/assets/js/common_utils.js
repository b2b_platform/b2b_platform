export const htmlSanitizer = require('sanitize-html');

export const get_param_by_name = function(name, url) {
    if (!url) var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export const get_params = function(str) {
    // Currently slightly buggy
    //if (str == undefined) return {};
    //return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
    return decodeURIComponent(str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];    
}

export const generate_qs = function(obj) {
    var str = [];
    for(var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");    
}

export const get_cookie = function(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
    return null;
}

export const clean_obj = function(obj) {
    var output = {};
    for (var key in obj) {
        if (!obj.hasOwnProperty(key)) continue;

        try {
            output[key] = htmlSanitizer(obj[key]);
        } catch (e) {
            output[key] = obj[key];
        }
    }
    return output;
}

export const query = function(url, type, data, callback, headers, is_json) {
    if (callback === null || callback === undefined) {
        callback = function(d,e) {
            if (e != null) {
                alert(e.responseText);
            } else {
                console.log(d);
            }
            return false;
        }        
    }

    var headers = headers === undefined || headers === null ? {} : headers;

    if (url.startsWith('/api/v1/auth.refresh_token')) {
        headers['X-JWT-CSRF-TOKEN'] = get_cookie('csrf_refresh_token');
    } else {
        headers['X-JWT-CSRF-TOKEN'] = get_cookie('csrf_access_token');
    }

    var data = data || {};
    
    var ajax_params = {
        url: url,
        type: type,
        data: data,
        headers: headers,        
        success: function(d) {
            callback(d, null);
        },
        error: function(e) {
            callback(null, e);
        }
    }


    if (typeof(data) === 'object') {
        try {
            ajax_params.data = $.param(data, true);
        } catch(e) {
            // data is formData
            ajax_params.processData = false;
            ajax_params.contentType = false;
        }
    }
    
    if (is_json === true) {
        ajax_params.contentType = 'application/json';
        ajax_params.dataType = 'json';
        ajax_params.data = JSON.stringify(data);
    }


    $.ajax(ajax_params);
}

export const show_loading = function(div) {
    var img = '<img class="loading-img" src="/static/img/loading.gif" />';
    div.innerHTML = img;
}

export const debuglog = function(debug_obj) {
    var propvalue;
    for (var propname in debug_obj) {
        propvalue = debug_obj[propname];
        console.log(propname, propvalue);
    }
}

export const is_external_url = function(url) {
    var domain = function(url) {
        return url.replace('http://','').replace('https://','').split('/')[0];
    };

    return domain(location.href) !== domain(url);
}

// export const to_emoji = function(code) {
//     var point = '0x' + code;
//     var offset = point - 0x10000,
//         lead = 0xd800 + (offset >> 10),
//         trail = 0xdc00 + (offset & 0x3ff); 
//     return String.fromCharCode(lead) + String.fromCharCode(trail);
// }

// Convert supplementary plane code point to two surrogate 16-bit code units:
function surrogate(cp) {
    return [
        ((cp - 0x010000 & 0xffc00) >> 10) + 0xd800,
        (cp - 0x010000 & 0x3ff) + 0xdc00
    ];
}

export const to_emoji = function(str) {
    return str.replace(/([0-9a-fA-F]+)/g, function(match, hex) {
        return String.fromCharCode.apply(null, surrogate(Number.parseInt(hex, 16)))
    });
}

export const build_file_preview = function (ftype, fname, furl) {
    var elem;

    var class_type = '';
    switch (ftype) {
        case 'image/jpeg':
        case 'image/png':
        case 'image/x-citrix-jpeg':
            elem = document.createElement('img');
            elem.src = furl;
            break;
        case 'text/plain':
        case 'text/csv':
            elem = document.createElement('div');
            class_type = 'fa fa-file-text';
            break;
        case 'application/pdf':
            elem = document.createElement('div');
            class_type = 'fa fa-file-pdf-o';
            break;
        default:
            elem = document.createElement('div')
            class_type = 'fa fa-file-o';
            break;
    }

    if (ftype != 'image/jpeg' && ftype != 'image/png' && ftype != 'image/x-citrix-jpeg' ) {
        elem.style.fontSize = '20px';
        var ch = document.createTextNode(fname);
        elem.appendChild(ch);
    }

    elem.className = 'media-preview ' + class_type;
    return elem;
}