import { handle_editable_table, enable_tooltip } from 'js/forms';


export const handle_rfq_table = function() {
    var cb = function() {
        alert('Your RFQ has been updated and notified to suppliers successfully!');
        location.reload();
        return false;
    }
    
    handle_editable_table('#rfq-details-table', cb);
}
