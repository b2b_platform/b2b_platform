import { query, generate_qs, get_params } from 'js/common_utils';
import { API } from 'js/api';

import { select_category_cascade } from 'js/product_category';

export const start_page = function() {
    select_category_cascade('#create-trade-lead-select-category');

    $('.sectarget').hide();

    $('.suggest-term-inputs').hide();
    $('.search-request-output').hide();

    $('#request-input').on('keypress', function(e) {
        if (e.keyCode === 13) {
            $('.search-request-output').show();
        }
    });

    $('.search-request-output').on('click', function(e) {
        
        $('.search-request-output').hide();
        $('.suggest-term-inputs').show();
    });

    $('.input-terms .keyword').on('click', function() {      
        $(this).hide();  
        var suggestitem = this.getAttribute('data-sectarget');
        $('#' + suggestitem).toggle();
    });
}