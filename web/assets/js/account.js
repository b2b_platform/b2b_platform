import { handle_editable_table, enable_tooltip } from './forms';
import { query, get_param_by_name, is_external_url } from './common_utils';
import { API } from './api';
import { user_item_tpl, added_blocked_user_tpl } from './messages/tpls';

export const handle_my_account_table = function() {
    var cb = function() {
        var $form = $('#my-account-form');
        var data = new FormData($form[0]);
        query(API.ACCOUNT_UPDATE, 'POST', data, function(d, e) {
            if (e != null) {
                alert(e);
            } else {
                console.log(d);
            }
            location.reload();
            return false;
        });        
    }
    
    handle_editable_table('#my-account-table', cb);
}

export const handle_my_account_settings = function() {
    enable_tooltip();

    var $form = $('#my-settings-form');
    
    var $search_input = $('#search-users-input');
    var $search_input_btn = $('#search-users-input-btn');
    var $search_output = $('#search-users-output');
    var $added_users = $('#search-users-added');
    var added_users = [];
  
    $form.validate({
        submitHandler: function(form) {
            var data = new FormData($form[0]);
            query(API.ACCOUNT_UPDATE, 'POST', data, function(d, e) {
                if (e != null) {
                    alert(e);
                } else {
                    console.log(d);
                }
                location.reload();
                return false;
            });  
            return false;
        },
        rules: {
        }
    });

    $search_input_btn.on('click', function(e) {
        e.preventDefault();
         
        $search_output.html('');
        if ($search_input.val() === '') {
            return false;
        }
            
        var cb = function(d, err) {
            if (err != null) return false;
                var items = d.result.map(function(user, idx) {
                    return user_item_tpl.render({user: user, show_message_btn: false});
            });    
            $search_output.html(items.join(''));
        }
        query(API.ACCOUNT_SEARCH, 'GET', {search_term: $search_input.val()}, cb);

        return false;
    });
      
    $('#search-users-output').on('click', '.search-users-item', function() {
        var user_id = this.getAttribute('data-user_id');
        var user_name = this.getAttribute('data-user_name');

        if (added_users.indexOf(user_id) > -1) return false;        

        var el = added_blocked_user_tpl.render({
            user: {id: user_id, name: user_name}
        });

        var $el = $(el);

        added_users.push(user_id);
        $added_users.append($el);

        $el.find('.x-btn').on('click', function() {
            added_users.pop(user_id);
            $el.remove();
        });
    });
    
    $('.current-blocked-users').each(function() {
        var user = JSON.parse($(this).attr('data-blockedusers'));
        var user_id = user.id;
        var el = added_blocked_user_tpl.render({
            user: {id: user.id, name: user.name}
        });

        var $el = $(el);

        added_users.push(user_id);
        $added_users.append($el);

        $el.find('.x-btn').on('click', function() {
            added_users.pop(user_id);
            $el.remove();
        });
    });
    $('#blocked-users-wrapper').html('')
    
}

window.leave_my_company = function() {
    if (confirm("Are you sure to leave this company? You will need to be added again by a company or a system admin. You will need to sign in again after leaving a company.")) {
        query(API.COMPANY_LEAVE, 'POST', '', function(d, e) {
            if (e != null) {
                alert(e);
            } else {
                location.href = '/logout';
            }
            location.reload();
            return false;
        });
    }      
}

export const handle_change_password_form = function() {
    var $form = $('#change-password-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                location.href = '/my-account';
                return false;
            };
            
            query(API.ACCOUNT_CHANGE_PASSWORD, 'POST', $form.serialize(), cb);

            return false;
        },
        rules: {            
            old_password: {
                required: true,
            },
            new_password: {
                required: true,
            },            
            new_password_again: {
                required: true,
                equalTo: 'input[name="new_password"]'
            }            
        }
    });
    
}

