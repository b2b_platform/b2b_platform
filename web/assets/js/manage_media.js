import { query, get_params, generate_qs, show_loading, build_file_preview } from './common_utils';
import { API } from './api';



export const start_company = function () {
    var $add_media_form = $('#add-media-form');
    var $add_media_input = $('#add-media-input');
    var $add_media_btn = $('#add-media-upload-btn');
    var $add_media_preview = $('#add-media-preview');
    var $image_modal = $('#imageModal');
    var $del_media_btns = $('.delete-media-btn');
    var $download_img_btn = $('.btn-img-download');

    var $pages = $('.page-item');
    var params = get_params();

    $pages.on('click', function () {
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $add_media_input.on('change', function () {
        var files = this.files;
        $add_media_preview.html('');

        for (var i = 0; i < files.length; i++) {
            var f = files[i];
            var reader = new FileReader();
            //reader.readAsDataURL(f);            
            //reader.onload = read_cb;

            reader.onload = (function (fp) {
                return function (e) {
                    $add_media_preview.append(build_file_preview(fp.type, fp.name, e.target.result));
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    $add_media_btn.on('click', function () {
        if (!$add_media_input[0].files.length) return false;

        var data = new FormData($add_media_form[0]);
        var cb = function (d, e) {
            $add_media_preview.html('');

            if (e != null) {
                return false;
            }

            location.reload();
        }
        show_loading($add_media_preview[0]);
        query(API.COMPANY_UPLOAD_MEDIA, 'POST', data, cb);
    });

    $del_media_btns.on('click', function () {
        var company_id = this.getAttribute('data-company_id');
        var item_name = this.getAttribute('data-item_name');
        var item_id = this.getAttribute('data-item_id');

        if (!confirm('Are you sure to delete ' + item_name + '?')) return false;

        var cb = function (d, e) {
            if (e != null) return false;
            location.reload();
        }

        query(API.COMPANY_DELETE_MEDIA, 'POST', {
            company_id: company_id,
            item_id: item_id
        }, cb);
    });

    $image_modal.on('show.bs.modal', function (event) {
        var orig = $(event.relatedTarget);
        var modal = $(this);
        var elem = document.createElement('img');
        var imgurl = orig.data('imgurl');
        var imgname = orig.data('imgname');
        elem.src = imgurl;
        elem.className = 'img-fluid';
        modal.find('.modal-title').text(orig.data('imgname'));
        modal.find('.modal-body').html(elem);

        modal.find('.btn-img-download').attr("href", imgurl).attr("download", imgname);

        /* $download_img_btn.on('click ', function(e) {
            e.preventDefault();  //stop the browser from following
            window.location.href = imgurl;
        });*/
      });
}

export const start_product_all = function () {
    var $del_media_btns = $('.delete-media-btn');
    var $pages = $('.page-item');


    var params = get_params();

    $pages.on('click', function () {
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $del_media_btns.on('click', function () {
        var product_id = this.getAttribute('data-product_id');
        var item_name = this.getAttribute('data-item_name');
        var item_id = this.getAttribute('data-item_id');

        if (!confirm('Are you sure to delete ' + item_name + '?')) return false;

        var cb = function (d, e) {
            if (e != null) return false;
            location.reload();
        }

        query(API.PRODUCT_DELETE_MEDIA, 'POST', {
            product_id: product_id,
            item_id: item_id
        }, cb);
    });

}

export const start_product = function () {
    var $add_media_inputs = $('.add-media-input');
    var $add_media_btns = $('.add-media-upload-btn');
    var $del_media_btns = $('.delete-media-btn');
    var $search_btn = $('#search-btn');
    var $search_input = $('#search-input');

    var $pages = $('.page-item');
    var params = get_params();

    $pages.on('click', function () {
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $add_media_inputs.on('change', function () {
        var $this = $(this);
        var files = this.files;
        var add_media_preview = document.getElementById(this.getAttribute('data-preview_id'));

        add_media_preview.innerHtml = '';

        for (var i = 0; i < files.length; i++) {
            var f = files[i];
            var reader = new FileReader();
            //reader.readAsDataURL(f);            
            //reader.onload = read_cb;

            reader.onload = (function (fp) {
                return function (e) {
                    $add_media_preview.append(build_file_preview(fp.type, fp.name, e.target.result));
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    $add_media_btns.on('click', function () {
        var add_media_input = document.getElementById(this.getAttribute('data-input_id'));
        var add_media_form = document.getElementById(this.getAttribute('data-form_id'));
        var add_media_preview = document.getElementById(this.getAttribute('data-preview_id'));

        if (!add_media_input.files.length) return false;

        var data = new FormData(add_media_form);
        var cb = function (d, e) {
            add_media_preview.innerHTML = '';

            if (e != null) {
                return false;
            }

            location.reload();
        }
        show_loading(add_media_preview);
        query(API.PRODUCT_UPLOAD_MEDIA, 'POST', data, cb);
    });

    $del_media_btns.on('click', function () {
        var product_id = this.getAttribute('data-product_id');
        var item_name = this.getAttribute('data-item_name');
        var item_id = this.getAttribute('data-item_id');

        if (!confirm('Are you sure to delete ' + item_name + '?')) return false;

        var cb = function (d, e) {
            if (e != null) return false;
            location.reload();
        }

        query(API.PRODUCT_DELETE_MEDIA, 'POST', {
            product_id: product_id,
            item_id: item_id
        }, cb);
    });

    function on_update_keyword() {
        params.keyword = $search_input.val();
        params.current_page = 1;
        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function (e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

}