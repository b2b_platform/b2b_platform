import { API } from './api';
import { query } from './common_utils';
import WatchJS from 'melanke-watchjs';
import _ from 'lodash';

export const NotificationManager = function({protocol, hostname, port}) {
    var nm = this;
    var protocol = protocol || (location.protocol === 'https:' ? 'wss://' : 'ws://');
    var hostname = hostname || location.hostname;
    var port = port || location.port;    
    var url = protocol + hostname + ':' + port + '/ws/notification';

    var ws = null;
    var unread_msgs_notihan = new UnreadMsgNotiHandler(nm);
    var app_notihan = new AppNotificationHandler(nm);

    start_ws();

    function start_ws() {
        ws = new WebSocket(url) || null;
        ws.onopen = on_open;
        ws.onerror = on_error;
        ws.onmessage = on_message;
        ws.onclose = on_close;
    }

    function on_close(e) {        
        setTimeout(start_ws, 3000);
    }

    function on_open(e) {}
    function on_error(e) {}

    function on_message(e) {
        var r = JSON.parse(e.data);
        switch (r.msg_type) {
            case 'NOTIFY_NEW_MSG':
                unread_msgs_notihan.on_notify_new_msg(r.data);
                break;
            case 'NOTIFY_READ_THREAD':
                unread_msgs_notihan.on_notify_read_thread(r.data);
                break;
            case 'NEW_NOTIFICATION':
                app_notihan.on_new_notification(r.data);
                break;
            case 'READ_NOTIFICATION':
                app_notihan.on_read_notification(r.data);
                break;
            default:
                break;
        }
    }

    function send(msg_type, data) {
        var message = {
            msg_type: msg_type,
            data: data
        }
        ws.send(JSON.stringify(message));
    }

    function init() {
        unread_msgs_notihan.init();
        app_notihan.init();
    }

    function get_unread_msgs_notihan() {
        return unread_msgs_notihan;
    }

    $.extend(this, {
        init: init,
        send: send,
        unread_msgs_notihan: get_unread_msgs_notihan
    });
}

function AppNotificationHandler(nm) {
    var nm = nm;  
    var unread_count = {total: 0};
    var $total_spans = $('.total-unread-notifications-count');

    function on_new_notification(data) {
        unread_count.total += 1;
    }

    function on_read_notification(data) {
        if (unread_count.total <= 0) return false;
        unread_count.total -= 1;
    }

    function reset_unread_count() {
        var cb = function(d, e) {
            if (e != null) {
                unread_count.total = 0;
                return false;
            }            
            unread_count.total = d.result.unread_count;
        }

        query(API.NOTIFICATION_GET_UNREAD_COUNT, 'GET', null, cb);
    }

    WatchJS.watch(unread_count, "total", function() {

        if (unread_count.total === 0) {
            $total_spans.html('');
        } else {
            $total_spans.html('<span class="unread-notification-count-label">' + unread_count.total + '</span>');
        }
    });    

    function init() {
        reset_unread_count();
    }

    $.extend(this, {
        init: init,
        on_new_notification: on_new_notification,
        on_read_notification: on_read_notification
    })
}

function UnreadMsgNotiHandler(nm) {

    var nm = nm;    
    var unread_msgs_count = {
        total: 0,
        threads: {}
    };   

    var per_thread_callbacks = {};

    var $total_spans = $('.total-unread-messages-count');
    var $total_notification_spans = $('.total-unread-notification-count')
    var $per_thread_spans = $('.unread-per-thread');

    //var pop_sound = new Audio('/static/sounds/notification_pop.wav');

    function on_notify_new_msg(data) {
        if (!unread_msgs_count.threads.hasOwnProperty(data.thread_id))
            unread_msgs_count.threads[data.thread_id] = {
                count: 0,
                ts: Date.now()
            };

        unread_msgs_count.threads[data.thread_id].count += 1;
        unread_msgs_count.threads[data.thread_id].ts = Date.now();
        unread_msgs_count.total += 1;

        //pop_sound.play();
    }

    function on_notify_read_thread(data) {
        if (!unread_msgs_count.threads.hasOwnProperty(data.thread_id)) return false;
        unread_msgs_count.total -= unread_msgs_count.threads[data.thread_id].count;
        unread_msgs_count.threads[data.thread_id].count = 0;
    }

    function reset_unread_msgs_count() {
        var cb = function(d,e) {
            if (e != null) return false;

            unread_msgs_count.total = _.sum(_.values(d.result.messages_count));            
            
            for (var tid in d.result.messages_count) {
                if (!d.result.messages_count.hasOwnProperty(tid)) continue;
                unread_msgs_count.threads[tid] = {                    
                    count: d.result.messages_count[tid],
                    ts: Date.now()
                }
            }

        }

        query(API.MESSAGES_GET_UNREAD_MESSAGES_COUNT, 'GET', null, cb);
    }

    function add_per_thread_cb(thread_id, cb) {
        if (!per_thread_callbacks.hasOwnProperty(thread_id)) {
            per_thread_callbacks[thread_id] = [];
        }

        if (per_thread_callbacks[thread_id].indexOf(cb) === -1) {            
            per_thread_callbacks[thread_id].push(cb);
        }
    }

    function remove_per_thread_cb(thread_id, cb) {
        _.remove(per_thread_callbacks[thread_id], function(f) {
            return f === cb;
        });
    }

    WatchJS.watch(unread_msgs_count, "total", function() {
        if (unread_msgs_count.total === 0) {
            $total_spans.html('');
        } else {
            $total_spans.html('<span class="unread-msg-count-label">' + unread_msgs_count.total + '</span>');
        }
    });

    WatchJS.watch(unread_msgs_count, function() {
        for (var tid in unread_msgs_count.threads) {
            if (!unread_msgs_count.threads.hasOwnProperty(tid)) continue;

            var $span = $per_thread_spans.filter('[data-thread_id=' + tid + ']');
            var unread_per_thread = unread_msgs_count.threads[tid];
            
            if (unread_per_thread.count === 0) {
                $span.html('');
            } else {
                $span.html('<span class="unread-msg-count-label">' + unread_per_thread.count + '</span>');
            }

            // RUN CALLBACKS
            if (per_thread_callbacks.hasOwnProperty(tid)) {
                for (var i=0; i < per_thread_callbacks[tid].length; ++i) {
                    per_thread_callbacks[tid][i](tid, unread_per_thread);
                }
            }
        }
    }, 1);

    function init() {
        reset_unread_msgs_count();
    }

    $.extend(this, {
        init: init,
        on_notify_new_msg: on_notify_new_msg,
        on_notify_read_thread: on_notify_read_thread,        
        add_per_thread_cb: add_per_thread_cb,
        remove_per_thread_cb: remove_per_thread_cb
    })
}