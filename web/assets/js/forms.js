export const handle_editable_table = function(table_id, save_cb, cb_args) {
    var $table = $(table_id);
    var $edit_btn = $table.find('.btn-update');
    var $save_btn = $table.find('.btn-submit');
    var $cancel_btn = $table.find('.btn-cancel');

    $edit_btn.on('click', function(e) {
        e.preventDefault();
        $table.removeClass('editing').addClass('editing');        
    });

    $save_btn.on('click', function(e) {        
        e.preventDefault();
        save_cb(cb_args);
    });

    $cancel_btn.on('click', function(e) {
        e.preventDefault();
        $table.removeClass('editing');        
    });

}

export const enable_tooltip = function() {
    $('[data-toggle="tooltip"]').tooltip()
}