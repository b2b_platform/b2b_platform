import jQuery from 'jquery';
import 'jquery-validation';
import { query } from './common_utils';
import { API } from './api';
import { NotificationManager } from './notification';
import { open_quick_messages } from 'js/messages/quick_messages';
import { QuickMessages } from './messages/quick_messages';
import { handle_create_ticket_form } from './support_ticket';
import { log_current_page } from './client_activity';
import { open_product_quickview } from './product';
import { open_trade_lead_quickview } from './trade_lead';

import 'trumbowyg/dist/trumbowyg.min.js';
import 'trumbowyg/dist/ui/trumbowyg.min.css';
$.trumbowyg.svgPath = require('trumbowyg/dist/ui/icons.svg');

jQuery.validator.setDefaults({
    ignore: [],
    errorElement: "div",
    errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        error.addClass( "help-block invalid-feedback" );
        if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.parent( "label" ) );
        } else {
            error.insertAfter( element );
        }
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
    },
});


jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
    return this.each(function() {
      var select = this;
      var $select = $(select);
      var options = [];
      $select.find('option').each(function() {
        options.push({value: $(this).val(), text: $(this).text()});
      });
      $select.data('options', options);
      $(textbox).bind('change keyup', function() {
        var options = $select.empty().scrollTop(0).data('options');
        var search = $.trim($(this).val());
        var regex = new RegExp(search,'gi');
   
        $.each(options, function(i) {
          var option = options[i];
          if(option.text.match(regex) !== null) {
            $select.append(
               $('<option>').text(option.text).val(option.value)
            );
          }
        });
        if (selectSingleMatch === true && 
            $select.children().length === 1) {
            $select.children().get(0).selected = true;
        }
      });
    });
};

window.toggle_menu = function(id, class_) {
    var class_ = class_ || id;
    var x = document.getElementById(id);

    if (x.className === class_) {
        x.className += " responsive";
    } else {
        x.className = class_;
    }
}


window.sm_query = query;

window.clone_new_row = function(container_id) {    
    var container = document.getElementById(container_id);
    var itm = container.firstElementChild;
    var cln = itm.cloneNode(true);
    cln.classList.remove('hide');
    container.appendChild(cln);

    return false;
}

window.delete_current_row = function(btn) {
    var $row = $(btn).closest('.row');
    $row.remove();
}

window.show_advanced_editor = function(id) {
    var $div = $(id);

    $div.trumbowyg({
        btns: [
            ['viewHTML'],
            ['undo', 'redo'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat']
        ],
        autogrow: false
    });
}

window.toggle_checkbox_value = function(inp, target_id) {    
    var target = document.getElementById(target_id);
    if (inp.checked) {
        target.value = 'True';
    } else {
        target.value = 'False';
    }
}

window.submit_action_btn = function(url, confirm_text, redirect_url) {
    var confirm_text = confirm_text || 'Are you sure to do this?';
    if( !confirm(confirm_text) ) { return false; }
    var cb = function(d, e) {
        if (e != null) {
            alert(e.responseText);
            return false;
        }
        location.href = redirect_url;
    }

    query(url, 'POST', null, cb);
}

window.toggle_el = function(id) {
    var el = document.getElementById(id);
    if (el.style.display === 'none') {
        el.style.display = 'block';
    } else {
        el.style.display = 'none';
    }
}

window.mobileAndTabletcheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

window.click_del_media_btn = function(btn, url, data, cb) {
    if (!confirm('Are you sure to delete this media item?')) return false;

    var cb = cb || function(d, e) {
        if (e != null) return false;
        location.reload();
    }
    query(url, 'POST', data, cb);
}


window.load_media_grid_lightbox = function(id) {
    var $grid = $(id);

    $grid.magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,            
            navigateByImgClick: true
        }        
    });
}


window.start_notification_manager = function() {
    window.notiman = new NotificationManager({});
    window.notiman.init();    
}


window.start_quick_messages_service = function() {
    var qm = new QuickMessages();
    qm.init();
}

window.handle_support_bubble = function() {
    handle_create_ticket_form();
}

window.filter_table = function(inp, selector) {
    var search_term = inp.value.toLowerCase();
    var $items = $(selector);
    $items.each(function(idx, item) {
        var tr = item.parentElement;
        tr.className = tr.className.replace('filtered', '');
        if (item.innerHTML.toLowerCase().indexOf(search_term) === -1)
            tr.className += ' filtered';
    });
}

window.enable_anchor_tabs = function(tabs_id) {
    // Javascript to enable link to tab
    var $tabs = $(tabs_id);

    var url = document.location.toString();
    if (url.match('#')) {
        $tabs.find('a[href="#' + url.split('#')[1] + '"]').tab('show');
    } 

    // Change hash for page-reload
    $tabs.find('a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
        window.scrollTo(0,0);
    });
}

window.open_product_quickview = open_product_quickview;

window.log_current_page = log_current_page;

window.open_trade_lead_quickview = open_trade_lead_quickview;

window.add_to_basket = function(pid) {
    if (!confirm('Are you sure to add this product to your basket?')) return false;

    var cb = function(d,e) {
        if (e != null) {            
            if (e.status === 401 || e.status === 403) {
                location.href = '/login';
                return false;
            } else {
                alert(e.responseText);
                return false;
            }
        }
        location.reload();
        return false;
    }

    var data = { product_id: pid };

    query(API.ACCOUNT_ADD_TO_BASKET, 'POST', data, cb);
}

window.remove_from_basket = function(pid) {
    if (!confirm('Are you sure to remove this product from your basket?')) return false;

    var cb = function(d,e) {
        if (e != null) {
            if (e.status === 401 || e.status === 403) {
                location.href = '/login';
                return false;
            } else {
                alert(e.responseText);
                return false;
            }
        }        
        location.reload();
        return false;
    }

    var data = { product_id: pid };

    query(API.ACCOUNT_REMOVE_FROM_BASKET, 'POST', data, cb);
}