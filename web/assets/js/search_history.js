import { store } from './client_storage';

const SEARCH_HISTORY = 'SEARCH_HISTORY';

if (store.get(SEARCH_HISTORY) === undefined) store.set(SEARCH_HISTORY, {products: {}, companies: {}});
