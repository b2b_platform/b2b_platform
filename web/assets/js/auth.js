import { query, get_param_by_name, is_external_url } from './common_utils';
import { API } from './api';

export const handle_login_form = function(usecapt) {
    var login_form = $('#login-form');

    login_form.validate({
        submitHandler: function(form) {            
            if (usecapt) { 
                grecaptcha.execute();                
            } else {                
                submit_login_form(null);                
            }
            return false;
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        }
    });
}

export const handle_register_form = function(usecapt) {
    var register_form = $('#register-form');

    register_form.validate({
        submitHandler: function(form) {       
            if (usecapt) { 
                grecaptcha.execute();
            } else {
                submit_register_form(null);
            }            
            return false;
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },
            password_again: {
                required: true,
                equalTo: 'input[name="password"]'
            },
            first_name: "required",
            last_name: "required",
            location: "required",
            position_title: "required",
            company_name: "required",
            country: "required",
            accept_tos: "required"
        }
    });
    
}

window.submit_login_form = function(token) {
    var login_form = $('#login-form');
    var cb = function(d, e) {
        if (e != null) {
            alert(e.responseText);
            location.reload();
            return false;
        }
        var next_url = get_param_by_name('next');

        if (next_url != null) {
            location.href = encodeURI(next_url);
        } else {
            location.href = '/';
        }

        return false;
    };
    var data = new FormData(login_form[0]);     
    query(API.LOGIN_URL, 'POST', data, cb);
}

window.submit_register_form = function(token) {
    var register_form = $('#register-form');
    var cb = function(d, e) {
        if (e != null) {
            alert(e.responseText);
            location.reload();
            return false;
        }
        location.href = '/';
        return false;
    };
    var data = new FormData(register_form[0]);
    query(API.REGISTER_URL, 'POST', data, cb);
}
