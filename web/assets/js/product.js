import { query, get_params, generate_qs } from './common_utils';
import { API } from './api';
import { select_category_cascade } from './product_category';

export const handle_create_product_form = function() {
    select_category_cascade('#create-product-select-category');

    var $form = $('#create-product-form');

    $form.validate({
        ignore: [],
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }

                location.href = '/manage-products';
                return false;
            };
            var data = new FormData($form[0]);
            query(API.PRODUCT_CREATE, 'POST', data, cb);
            return false;
        },

        rules: {
            name: "required",
            product_category_id: "required"
        }
    });
}

export const handle_update_product_form = function() {
    select_category_cascade('#update-product-select-category');

    var $form = $('#update-product-form');

    $form.validate({
        ignore: [],
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }

                if (window.parent.modal_submit_callback === undefined) {
                    location.href = '/manage-products';
                } else {
                    window.parent.modal_submit_callback();
                }

                return false;
            };
            var data = new FormData($form[0]);
            query(API.PRODUCT_UPDATE, 'POST', data, cb);
            return false;
        },

        rules: {
            name: "required",
            product_category_id: "required"
        }
    });
}

export const open_product_quickview = function(el) {
    var quickview_modal = document.getElementById('product-quickview-modal');
    var $modal = null;
    
    if (quickview_modal === null) {
        quickview_modal = document.createElement('div');
        quickview_modal.id = 'product-quickview-modal';
        quickview_modal.className = 'modal fade';
        quickview_modal.setAttribute('tabindex', '-1');
        quickview_modal.setAttribute('role', 'dialog');
        quickview_modal.setAttribute('arialabelledby', 'quickview-btn');
        quickview_modal.setAttribute('aria-hidden', 'true');        
        quickview_modal.innerHTML = `
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="product-quickview-title"><a href='' class='product-name'></a>                
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id='product-quickview-content'>
                    <p>
                        <i class="fa fa-industry"></i>&nbsp;<b><span class='company-name'></span></b>
                        <a href='' class='company-url'>Go to company page</a>
                    </p>
                    <div style='width: 100%; overflow-x: auto;'>
                        <p class='product-description'><p>
                    </div>
                    <a href='' class='product-url'>View product</a>
                </div>              
            </div>
            <div class="modal-footer">
                <a class='inquire-product-btn'><button class='btn-contact'>Inquire</button></a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        `;
        document.body.appendChild(quickview_modal);
    }

    var $modal = $('#product-quickview-modal');
    var product_inquiry_url = el.getAttribute('data-product_inquiry_thread_url');
    var product_url = el.getAttribute('data-product_url');
    var company_url = el.getAttribute('data-company_url');
    var product = JSON.parse(el.getAttribute('data-product'));

    if ($modal) {
        $modal.find('.inquire-product-btn').attr('href', product_inquiry_url);
        $modal.find('.company-url').attr('href', company_url);
        $modal.find('.company-name').text(product.company_name);
        $modal.find('.product-name').text(product.name).attr('href', product_url);
        $modal.find('.product-description').html(product.description);
        $modal.find('.product-url').attr('href', product_url);
        $modal.modal();
    }
}

export const handle_product_inquiry_form = function() {
    var $form = $('#product-inquiry-form');    

    $form.validate({
        ignore: [],
        rules: {
            text: "required",
        }
    });
}
