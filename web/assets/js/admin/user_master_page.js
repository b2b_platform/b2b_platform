import { query, generate_qs, get_params } from 'js/common_utils';
import { API } from 'js/api';


export const start_page = function() {
   
    var $search_employee_btn = $('#search-emp-btn');
    var $search_user_input = $('#search-user-input');
    var $search_user_btn = $('#search-user-btn');
    //var $search_user_add_btns = $('#  .invite-user-btn');
    var $search_user_add_btns = $('#user_candidate_list td');
    var $roleModal = $('#roleModal');
    var $permission_btns = $('.permission-badge-btn');
    var $modal_save_btn = $('#modal-save-btn');
    var $request_btn_accept = $('.request-btn-accept');
    var $request_btn_reject = $('.request-btn-reject');
    var $acc_confirmation_btns = $('.is-confirmed-account');
    var $pages = $('.page-item');
    var params = get_params();

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    function get_employee_info() {
        var params = {};
        var cb = function(d, e) {
            if (e != null) return false;            
            update_employee_dom(d.result);           
        }
        query(API.COMPANY_EMPLOYEE_LIST, 'GET', params, cb);
    }
    
    function update_employee_dom(data) {
        var emp_html = '';
        $.each(data, function (index, emp) {
          emp_html += '<div class="row">' 
             + '<div class="col-sm-6 col-md-4"><a href="' + emp.id + '">' + emp.email +  '</a></div>'
             + '<div class="col-sm-3 col-md-4">' + emp.last_name  + ' </div>'
             + '<div class="col-sm-3 col-md-4">' + emp.first_name  + ' </div>  </div>';
        });
        $("#employee_list").html(emp_html);
    }
     
    function get_user_info() {
        
        var params = {};
        params.search_term = $search_user_input.val();
        var cb = function(d, e) {
            if (e != null) return false;            
            update_user_dom(d.result);              
        }
        query(API.ACCOUNT_SEARCH, 'GET', params, cb);
    }
    
    function update_user_dom(data) {
        var emp_html = '';
        $.each(data, function (index, emp) {
            emp_html += '<tr><th scope="row">' + emp.id
            + '</th><td>' + emp.last_name
            + '</td><td>' + emp.first_name 
            + '</td><td>' + emp.email
            + '</td><td><button class="btn btn-primary btn-sm invite-user-btn" data-userid="' + emp.id + '">Invite</button>'
            + '</td></tr>';
        });
        $("#user_candidate_list").html(emp_html);
    }
    $roleModal.modal({ show: false})
    
    // $search_user_add_btns.click(function() {
    $('#user_candidate_list').on('click', 'td button', function () {
        var target_user = this.getAttribute('data-userid');
       
        var cb2 = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        var fdata = { 'target_user' : target_user };           
        query(API.ROLE_CREATE, 'POST', fdata, cb2);
       
        return false;
    });  


    $permission_btns.click(function() {
        $("#permission-input").val(this.getAttribute('data-permission'));
        $("#target-user-input").val(this.getAttribute('data-targetuser'));
        $("#company-id-input").val(this.getAttribute('data-company_id'));
        $roleModal.modal('show');
        
    });  

    $.fn.exists = function () {
        return this.length !== 0;
    }

    $acc_confirmation_btns.change(function(e) {
        if (!confirm('Are you sure to make this change?')) {
            this.checked = !this.checked;            
            return false;
        }
        // this will contain a reference to the checkbox
       
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        
        var params = new Object();
        params.target_user = this.getAttribute('data-targetuser');
        params.is_account_confirmed = this.checked;
        //alert('fdata id' + target_user_id + ' permission: ' +  permission);      
        query(API.ADMIN_UPDATE_USER, 'POST', params, cb);
        
        return false;
    });
    
    // Updating permission
    $modal_save_btn.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            // should redirect to company new page - waiting for approval from system admin
            location.reload();
            return false;
        };
        
        var params = new Object();
        params.target_user = $("#target-user-input").val();
        params.permission = $("#permission-input").val();
        if ($("#company-id-input").exists()) {
            params.company_id = $("#company-id-input").val();
        }  
        //alert('fdata id' + target_user_id + ' permission: ' +  permission);      
        query(API.ROLE_UPDATE, 'POST', params, cb);
        
        return false;
    });  
    
    // Accept  pending request
    $request_btn_accept.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            location.reload();
            return false;
        };

        var params = new Object();
        params.target_user = this.getAttribute('data-targetuser');
        query(API.ROLE_UPDATE, 'POST',  params, cb);
        return false;
    });  

    // Reject  pending request
    $request_btn_reject.click(function() {
        var cb = function(d, e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }
            location.reload();
            return false;
        };

        var params = new Object();
        params.target_user = this.getAttribute('data-targetuser');
        query(API.ROLE_DELETE, 'POST',  params, cb);
        return false;
    }); 

    $('#search-user-btn').on('click', get_user_info);
}

