import { query, generate_qs, get_params } from 'js/common_utils';
import { API } from 'js/api';


export const start_page = function() {
    var $search_btn = $('#search-btn');
    var $search_input = $('#search-input');
    var params = get_params();
    var $pages = $('.page-item');

    var $update_membership_btns = $('.update-membership-btn');
    var $update_membership_modal = $('#membership-update-modal');
    var $submit_update_membership_btn = $('#submit-update-membership-btn');    

    var $is_verified_btns = $('input[name="is_company_verified"]');
    var $is_public_btns = $('input[name="is_company_public"]');

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    function on_update_keyword() {
        params.search_term = $search_input.val();        
        params.current_page = 1 //reset current_page

        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $is_verified_btns.on('change', function() {
        if (!confirm('Are you sure to make this change?')) {
            this.checked = !this.checked;
            return false;
        }

        var cb = function(d,e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }

            location.reload();
            return false;
        }
        var data = {company_id: this.getAttribute('data-company_id'), is_verified: this.checked};
        query(API.ADMIN_UPDATE_COMPANY, 'POST', data, cb);
    });

    $is_public_btns.on('change', function() {
        if (!confirm('Are you sure to make this change?')) {
            this.checked = !this.checked;
            return false;
        }

        var cb = function(d,e) {
            if (e != null) {
                alert(e.responseText);
                return false;
            }

            location.reload();
            return false;
        }
        var data = {company_id: this.getAttribute('data-company_id'), is_public: this.checked};
        query(API.ADMIN_COMPANY_UPDATE, 'POST', data, cb);
    }); 

    $update_membership_btns.on('click', function() {
        var current_tier_txt = this.getAttribute('data-current_membership_tier_txt');
        var current_tier = this.getAttribute('data-current_membership_tier');
        var company_id = this.getAttribute('data-company_id');

        $update_membership_modal.find('.current-membership-tier').text(current_tier_txt);
        $update_membership_modal.modal('show');

        $submit_update_membership_btn.off('click').on('click', function() {
            if (!confirm('Are you sure to make this change?')) return false;

            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
    
                location.reload();
                return false;                
            }
            var new_tier = $update_membership_modal.find('select[name="membership_tier"] option:selected').val();
            var duration = $update_membership_modal.find('input[name="membership_duration"]').val();
            var data = {company_id: company_id, membership_tier: new_tier, membership_duration: duration};
            
            query(API.ADMIN_UPDATE_COMPANY_MEMBERSHIP, 'POST', data, cb);
        });
    });
}