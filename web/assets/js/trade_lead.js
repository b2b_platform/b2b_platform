import { query,  get_params, generate_qs } from './common_utils';
import { API } from './api';
import { select_category_cascade } from './product_category';

export const handle_create_trade_lead_form = function() {
    select_category_cascade('#create-trade-lead-select-category');

    var $form = $('#create-trade-lead-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                if (window.parent.submit_callback === undefined) {
                    location.href = d.url;
                } else {
                    window.parent.submit_callback();
                }
                return false;
            };
            var data = new FormData($form[0]);
            query(API.TRADE_LEAD_CREATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            title: {
                required: true
            },
            product_category_id: {
                required: true
            },
            content: {
                required: true
            },
            lead_type: {
                required: true
            }
        }
    });

}


export const handle_update_trade_lead_form = function() {
    select_category_cascade('#update-trade-lead-select-category');

    var $form = $('#update-trade-lead-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                location.href = d.url;
                return false;
            };
            var data = new FormData($form[0]);
            query(API.TRADE_LEAD_UPDATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            title: {
                required: true
            },
            product_category_id: {
                required: true
            },
            content: {
                required: true
            },
            lead_type: {
                required: true
            }
        }
    });

}

export const handle_listing_page = function() {
    var $switch_tab_btns = $('#search-result-viewtab').find('button');
    var $search_btn = $('#search-btn');
    var $search_input = $('#search-input');
    var $categories_btn = $('#categories-btn');
    var $categories_container = $('#categories-container');

    var $pages = $('.page-item');
    var $categories = $('.category-item');

    var params = get_params();

    $categories_btn.on('click', function() {
        $categories_container.toggle();        
    });    

    function on_update_keyword() {
        params.keyword = $search_input.val();        
        params.current_page = 1 //reset current_page

        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $categories.on('click', function() {        
        var category = this.getAttribute('data-category');
        var qs = category === '' ? '' : '?category=' + category;
        location.href = location.pathname + qs;
    });
    
    $switch_tab_btns.on('click', function() {            
        params.viewtab = this.getAttribute('data-viewtab');
        location.href = location.pathname + '?' + generate_qs(params);
    });
}

export const open_trade_lead_quickview = function(el) {
    var quickview_modal = document.getElementById('trade-lead-quickview-modal');
    var $modal = null;
    
    if (quickview_modal === null) {
        quickview_modal = document.createElement('div');
        quickview_modal.id = 'trade-lead-quickview-modal';
        quickview_modal.className = 'modal fade';
        quickview_modal.setAttribute('tabindex', '-1');
        quickview_modal.setAttribute('role', 'dialog');
        quickview_modal.setAttribute('arialabelledby', 'quickview-btn');
        quickview_modal.setAttribute('aria-hidden', 'true');        
        quickview_modal.innerHTML = `
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="trade-lead-quickview-title"><a href='' class='trade-lead-name'></a>                
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id='trade-lead-quickview-content'>
                    <div style='width: 100%; overflow-x: auto;'>
                        <p class='trade-lead-description'><p>
                    </div>
                    <a href='' class='trade-lead-url'>View Trade Lead</a>
                </div>              
            </div>
            <div class="modal-footer">
                <a class='inquire-trade-lead-btn'><button class='btn-contact'>Contact</button></a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        `;
        document.body.appendChild(quickview_modal);
    }

    var $modal = $('#trade-lead-quickview-modal');
    var trade_lead_thread_url = el.getAttribute('data-trade_lead_thread_url');
    var user_url = el.getAttribute('data-user_url');
    var trade_lead_url = el.getAttribute('data-trade_lead_url');
    var trade_lead = JSON.parse(el.getAttribute('data-trade_lead'));

    if ($modal) {
        $modal.find('.inquire-trade-lead-btn').attr('href', trade_lead_thread_url);
        $modal.find('.trade-lead-name').text(trade_lead.title);

        $modal.find('.trade-lead-description').html(trade_lead.content);
        $modal.find('.trade-lead-url').attr('href', trade_lead_url);
        $modal.modal();
    }
}