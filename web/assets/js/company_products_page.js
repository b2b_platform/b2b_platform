import { query, get_params, generate_qs } from './common_utils';
import { API } from './api';

export const start_page = function() {
    var $search_btn = $('#cp-search-btn');
    var $search_input = $('#cp-search-input');
    var $pages = $('.page-item');
    var $switch_view_btns = $('#pm-select-view').find('span');

    var params = get_params();

    function on_update_keyword() {
        params.keyword = $search_input.val();
        params.current_page = 1; // reset to page 1        
        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $switch_view_btns.on('click', function() {            
        params.view = this.getAttribute('data-view');
        location.href = location.pathname + '?' + generate_qs(params);
    });
    
}