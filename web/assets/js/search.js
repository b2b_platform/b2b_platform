import { query, get_params, generate_qs} from './common_utils';
import nunjucks from 'nunjucks';
import { API } from './api';

export const start_page = function() {
    select_region_cascade('#select-region-wrapper');    

    var $search_btn = $('#search-btn');
    var $search_input = $('#search-input-field');
    var $search_type_btn = $('#search-type-btn');
    var $categories_btn = $('#categories-btn');
    var $search_btn_new = $('#search-btn-new');
    var $filter_btn = $('#filter-btn'); 
    var $filter_clear_btn = $('#filter-clear-btn');
    var $filter_form = $('#refine-form');
    var $filter_non_empty = $("#refine-form :input[value!='']");
    var $categories_container = $('#categories-container');
    var $switch_view_btns = $('#pm-select-view').find('span');
    var $switch_tab_btns = $('#search-result-viewtab').find('button');
    var $switch_sort_container = $('#sort_by-select');
    var $region_input = $('#search_region_id');
    var $refine_list_container = $('#refine-list-wrapper a');

    var $pages = $('.page-item');
    var $categories = $('.category-item');

    var params = get_params();
    
    function on_update_keyword() {
        params.keyword = $search_input.val();
        params.current_page = 1;        
        location.href = location.pathname + '?' + generate_qs(params);
    }

    function get_carried_params() {
        var params2 = new Object();
        // params2.current_page = 1; 

        var keyword = $search_input.val();
        if (keyword) {
            params2.keyword = keyword;
        }

        var search_region_id = $region_input.val();
        if (search_region_id) {
            params2.search_region_id =search_region_id;
        }

        if ('stype' in params){
            params2.stype = params.stype;
        }
        if ('category' in params) {
            params2.category = params.category;
        }
        return params2;
    }

    function reload_filter_result() {
        location.href = location.pathname + '?' + [generate_qs(get_carried_params(params)), $filter_form.serialize()].join('&');
    }

    $categories_btn.on('click', function() {
        $categories_container.toggle();        
    });    

    $switch_sort_container.on('change', function() {
        params.sorted_by= $(this).find("option:selected").val();
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $filter_btn.on('click', reload_filter_result);

    $filter_clear_btn.on('click', function() {     
        location.href = location.pathname + '?' + generate_qs(get_carried_params(params));
    });

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', reload_filter_result);

    // Keep only type (products or companies)
    $search_btn_new.on('click', function() {
        var params2 = new Object();
        if ('stype' in params){
            params2.stype = params.stype;
        }
        location.href = location.pathname + '?' + generate_qs(params2);
    });

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $categories.on('click', function() {        
        var category = this.getAttribute('data-category');
        params.category = category;
        // var qs = category === '' ? '' : '?category=' + category;
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $switch_view_btns.on('click', function() {            
        params.view = this.getAttribute('data-view');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $switch_tab_btns.on('click', function() {            
        params.viewtab = this.getAttribute('data-viewtab');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    // Remove feature
    $refine_list_container.each( function( index ) {
        $(this).on("click", function(e){
            e.preventDefault();
            $('input:checkbox[name="' + this.getAttribute('data-featurename') + '"][value="' + $(this).text().trim() + '"]')
            .prop('checked', false);
            reload_filter_result();
        });
    });


}

export const select_region_cascade = function(box_id) {

    var NUM_LEVELS = 2;
    var $box = $(box_id);
    var $show = $box.find('.show-select-category');
    var $content = $box.find('.select-category-content');      
    var $selected_category = $box.find('#selected-region');
    var $input = $box.find('input[name="search_region_id"]');
    
    var $ok = $box.find('#select-region-ok-btn');
    var $cancel = $box.find('#select-region-cancel-btn');

    var categories = null;

    var select_tpl = nunjucks.compile(`
        <select class='form-control' size='10'>
            <option class='option-level{{ lvl }}' value='' disabled selected>Select a region</option>
            {% for name,cat in cats %}
            <option class='option-level{{ lvl }}' data-cat_name='{{ cat.name }}' value='{{ cat.id }}'>{{ cat.name }}</option>
            {% endfor %}
        </select>
    `);

    function render_selects(cats, lvl) {

        for (var i=lvl+1; i < NUM_LEVELS+1; i++) {
            $content.find('#cat-level'+i).html('');
        }

        if (cats != undefined && !Object.keys(cats).length) {
            for (var i=lvl; i < NUM_LEVELS+1; i++) {
                $content.find('#cat-level'+i).html('');
            }
            return false;
        }

        var $level = $content.find('#cat-level' + lvl);

        $level.html(select_tpl.render({
            cats: cats,            
            lvl: lvl
        }));

        var $select = $level.find('select');

        $select.on('change', function(e) {            
            var opt = $select.find('option:selected');
            var name = opt.attr('data-cat_name');
            var cat = cats[name];
            render_selects(cat.children, lvl+1);
        });        
    }

    function populate_box(parent, lvl) {
        if (parent && parent.children && Object.keys(parent.children).length) {
            render_selects(parent.children, lvl);            
            return false;
        }

        var data = !parent ? null : { region_id: parent.id };
        
        
        query(API.REGION_SELECT_LIST, 'GET', data, function(d, e) {
            if (e != null) return false;
            
            if (categories === null) {
                // Loading first time highest level
                categories = d.result;
               
                render_selects(categories, lvl);
            } else {
                // Loading successive level

                parent.children = d.result;
                render_selects(parent.children, lvl);
            }            
        });
    }

    function toggle_content() {
        if ($content.hasClass('hide')) {
            $content.removeClass('hide');
        } else {
            $content.addClass('hide');
        }
    }

    $show.on('click', function(e) {       
        toggle_content();
    });

    $cancel.on('click', function(e) {
        toggle_content();
    });

    $ok.on('click', function(e) {
        var current_cat = [];
        var current_cat_id = null;

        for (var i=1; i < NUM_LEVELS+1; i++) {
            var $select = $('#cat-level' + i + ' select');
            if (!$select.length) break;
            
            var selected = $select.find('option:selected');

            if (selected[0] === undefined) break;

            if (selected.val()) {
                current_cat.push(selected.text());
                current_cat_id = selected.val();
            }
        }
        
        $selected_category.text(current_cat.join(' >> '));
        $input.val(current_cat_id);
        
        $('#search-btn').click(); 
        // toggle_content();
        
    });
    
    populate_box(null, 1);
    
}
