import { query } from './common_utils';
import { API } from './api';

export const handle_create_ticket_form = function() {
    var $form = $('#support-bubble-new-ticket-form');
    
    $form.validate({
        submitHandler: function(form) {
            var cb = function(d, e) {
                if (e != null) {
                    alert(e.responseText);
                    return false;
                }
                if (window.parent.submit_callback === undefined) {
                    location.href = '/support';
                } else {
                    window.parent.submit_callback();
                }
                return false;
            };
            var data = new FormData($form[0]);
            query(API.SUPPORT_TICKET_CREATE, 'POST', data, cb);
            
            return false;
        },
        rules: {
            name: {
                required: true
            },
            ticket_type: {
                required: true,
            },
            description: {
                required: true
            }
        }
    });   
}

export const handle_update_ticket_status = function() {
    var $select = $('select#support-ticket-status-select');

    $select.on('change', function() {
        var new_status = this.value;
        var ticket_id = this.getAttribute('data-ticket_id');

        var cb = function(d, e) {
            if (e != null) return false;
            location.reload();
            return false;
        }

        var data = {
            ticket_id:ticket_id,
            status:new_status
        }

        query(API.SUPPORT_TICKET_UPDATE, 'POST', data, cb);
        return false;
    });
}