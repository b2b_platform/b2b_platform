import { query, get_params, generate_qs } from './common_utils';
import { API } from './api';

export const start_page = function() {

    var $search_btn = $('#search-btn');
    var $search_input = $('#search-input');
    var $categories_btn = $('#categories-btn');
    var $categories_container = $('#categories-container');

    var $pages = $('.page-item');
    var $categories = $('.category-item');

    var params = get_params();

    $categories_btn.on('click', function() {
        $categories_container.toggle();        
    });    

    function on_update_keyword() {
        params.keyword = $search_input.val();        
        params.current_page = 1 //reset current_page

        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $categories.on('click', function() {        
        var category = this.getAttribute('data-category');
        var qs = category === '' ? '' : '?category=' + category;
        location.href = location.pathname + qs;
    });
    
}