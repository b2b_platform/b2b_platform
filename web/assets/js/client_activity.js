import store from './client_storage';
import { API } from './api';
import { query } from './common_utils';

const CLIENT_ACTIVITY = 'CLIENT_ACTIVITY';
const MAX_NUM_ACTIVITY = 25;

function log_current_page(user_id) {    
    var url = location.pathname+location.search;

    var data = {
        user_id: user_id,
        url: url,
        ts: new Date().getTime()
    }

    if (store.get(CLIENT_ACTIVITY) === undefined) store.set(CLIENT_ACTIVITY, []);
    
    if (store.get(CLIENT_ACTIVITY).length >= MAX_NUM_ACTIVITY) {

        // SEND TO SERVER
        var cb = function(d,e) {
            //if (e != null) return alert(e.responseText);
            // RESET
            store.set(CLIENT_ACTIVITY, []);
        }

        query(API.ACCOUNT_LOG_ACTIVITY, 'POST', {
            activities: store.get(CLIENT_ACTIVITY)
        }, cb, null, true);
    }
    store.push(CLIENT_ACTIVITY, data);
}


export {
    log_current_page
}