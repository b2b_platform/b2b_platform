import { query } from './common_utils';
import { API } from './api';
import nunjucks from 'nunjucks';

export const handle_select_category = function() {
    // Event handler for 1st box option - loading subcategories
  
    $("#prcat_1").change(function(){
        var cat_cb_2 = function(d, e) {
            if (e != null) {
                // alert("Error getting 2" + e.responseText); // sometimes occurs
                return false;
            }
            var categories = d.results;
            
            $("#prcat_2 option").remove();
            $.each(categories, function (i, item) {
            $('#prcat_2').append($('<option>', { 
                    value: item.href,
                    text : item.text 
                }));
            });
            
            $('#prcat_2').filterByText($('#prcat_2_filter'));
        };
        var parent_cat_id = $('#prcat_1').find(":selected").val();
        var cat_query_data = { cat_level: 1, parent_cat_id: parent_cat_id };
        query(API.PRODUCT_CATEGORY_GET, 'GET', cat_query_data, cat_cb_2);
       
    });
    
    $("#prcat_2").change(function(){
        var cat_cb_3 = function(d, e) {
            if (e != null) {
                // alert("Error getting 2" + e.responseText); // sometimes occurs
                return false;
            }
            var categories = d.results;
            
            $("#pr_category_3_dl option").remove();
            $.each(categories, function (i, item) {
                $('#pr_category_3_dl').append($('<option>', { 
                    value: item.href,
                    text : item.text 
                }));
            });
            
        };
        var parent_cat_id = $('#prcat_2').find(":selected").val();
        var cat_query_data = { cat_level: 1, parent_cat_id: parent_cat_id };
        query(API.PRODUCT_CATEGORY_GET, 'GET', cat_query_data, cat_cb_3);
       
    });       
   
    var cat_cb_1 = function(d, e) {
        if (e != null) {
            //alert(e.responseText);
            return false;
        }
        var categories = d.results;
        $("#prcat_1 option").remove();
        $.each(categories, function (i, item) {
        $('#prcat_1').append($('<option>', { 
                value: item.href,
                text : item.text 
            }));
        }); 
        
        
        // Trigger first category load
        $("#prcat_1").trigger("change");
        $('#prcat_1').filterByText($('#prcat_1_filter'));
    };
    var cat_query_data = { cat_level: 1}; 
    query(API.PRODUCT_CATEGORY_GET, 'GET', jQuery.param(cat_query_data), cat_cb_1);

}

export const select_category_cascade = function(box_id) {
    // 4 LEVELS ONLY

    var NUM_LEVELS = 4;
    var $box = $(box_id);
    var $show = $box.find('.show-select-category');
    var $content = $box.find('.select-category-content');      
    var $selected_category = $box.find('#selected-category');
    var $input = $box.find('input[name="product_category_id"]');
    
    var $ok = $box.find('#select-category-ok-btn');
    var $cancel = $box.find('#select-category-cancel-btn');

    var categories = null;

    var select_tpl = nunjucks.compile(`
        <select class='form-control' size='10'>
            <option class='option-level{{ lvl }}' value='' disabled selected>Select a category</option>
            {% for name,cat in cats %}
            <option class='option-level{{ lvl }}' data-cat_name='{{ cat.name }}' value='{{ cat.id }}'>{{ cat.name }}</option>
            {% endfor %}
        </select>
    `);

    function render_selects(cats, lvl) {

        for (var i=lvl+1; i < NUM_LEVELS+1; i++) {
            $content.find('#cat-level'+i).html('');
        }

        if (!Object.keys(cats).length) {
            for (var i=lvl; i < NUM_LEVELS+1; i++) {
                $content.find('#cat-level'+i).html('');
            }
            return false;
        }

        var $level = $content.find('#cat-level' + lvl);

        $level.html(select_tpl.render({
            cats: cats,            
            lvl: lvl
        }));

        var $select = $level.find('select');

        $select.on('change', function(e) {            
            var opt = $select.find('option:selected');
            var name = opt.attr('data-cat_name');
            var cat = cats[name];
            populate_box(cat, lvl+1);
            //render_selects(cat.children, cat.id, lvl+1);
        });        
    }

    function populate_box(parent, lvl) {
        if (parent && parent.children && Object.keys(parent.children).length) {
            render_selects(parent.children, lvl);            
            return false;
        }

        var data = !parent ? null : { parent_category_id: parent.id };

        query(API.PRODUCT_CATEGORY_LIST, 'GET', data, function(d, e) {
            if (e != null) return false;

            if (categories === null) {
                categories = d.result;
                render_selects(categories, lvl);
            } else {
                parent.children = d.result;
                render_selects(parent.children, lvl);
            }            
        });
    }

    function toggle_content() {
        if ($content.hasClass('hide')) {
            $content.removeClass('hide');
        } else {
            $content.addClass('hide');
        }
    }

    $show.on('click', function(e) {        
        toggle_content();
    });

    $cancel.on('click', function(e) {
        toggle_content();
    });

    $ok.on('click', function(e) {
        var current_cat = [];
        var current_cat_id = null;

        for (var i=1; i < NUM_LEVELS+1; i++) {
            var $select = $('#cat-level' + i + ' select');
            if (!$select.length) break;
            
            var selected = $select.find('option:selected');

            if (selected[0] === undefined) break;

            if (selected.val()) {
                current_cat.push(selected.text());
                current_cat_id = selected.val();
            }
        }
        
        $selected_category.text(current_cat.join(' >> '));
        $input.val(current_cat_id);

        toggle_content();
    });

    populate_box(null, 1);
}
