import { query, get_params, generate_qs } from './common_utils';
import { API } from './api';
import nunjucks from 'nunjucks';

export const start_page = function() {
    var $search_btn = $('#pm-search-btn');
    var $search_input = $('#pm-search-input');
    var $pages = $('.page-item');

    var $switch_view_btns = $('#pm-select-view').find('span');

    var $update_btns = $('.update-btn');
    var $update_modal = $('#update-product-modal');
    var $update_modal_content = $update_modal.find('.modal-content');

    var $duplicate_btns = $('.duplicate-btn');

    var $inquiry_threads = $('#customer-inquiry-threads');
    var threads_tpl = nunjucks.compile(`
        <div class='threads-list-wrapper'>
            {% if threads %}
            {% for thread in threads %}
            <div class='thread-item media mt-3'>
                <img height='35px' width='35px' class='rounded-circle mr-3'
                    src='{% if not thread.creator.profile_pic %} /static/img/user_default.png {% else %}{{ thread.creator.profile_pic }}{% endif %}'>
                <div class='media-body'>
                    <a href='/messages/t/{{thread.id}}'><b>{{ thread.name }}</b></a>
                    <p>{{ thread.description }}</p>
                </div>
            </div>
            {% endfor %}
            {% else %}
            <p><i>No customer inquiries found.</i></p>
            {% endif %}
        </div>
    `);

    var params = get_params();

    function on_update_keyword() {
        params.keyword = $search_input.val();
        params.current_page = 1;        
        location.href = location.pathname + '?' + generate_qs(params);
    }

    $search_input.on('keypress', function(e) {
        if (e.keyCode === 13) on_update_keyword();
    });

    $search_btn.on('click', on_update_keyword);

    $pages.on('click', function() {        
        params.current_page = this.getAttribute('data-page');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    $update_btns.on('click', function() {
        var url = this.getAttribute('data-update-url');
        var iframe = ['<iframe src="', url,'" width="100%" height="100%" frameBorder="0"></iframe>'].join('');
        $update_modal_content.html(iframe);
        $update_modal.modal('show');
    })

    $switch_view_btns.on('click', function() {            
        params.view = this.getAttribute('data-view');
        location.href = location.pathname + '?' + generate_qs(params);
    });

    window.modal_submit_callback = function() {
        $update_modal.modal('hide');
        location.reload();
    }

    $duplicate_btns.on('click', function() {
        if (!confirm('Are you sure to make a copy of this product?')) return false;

        var cb = function(d,e) {
            if (e != null) return false;
            location.reload();
        }
        var product_id = this.getAttribute('data-product_id');
        var data = {product_id: product_id};
        query(API.PRODUCT_DUPLICATE, 'POST', data, cb);
    });

    query(API.MESSAGES_GET_PRODUCT_INQUIRY_THREADS, 'GET', {}, function(d, e) {
        if (e != null) return false;
        var threads = d.result;
        if (threads.length) {
            var html = threads_tpl.render({threads:threads});
        } else {
            var html = '<p><i>No inquiries found<i></p>';
        }

        $inquiry_threads.html(html);

    });
    
}