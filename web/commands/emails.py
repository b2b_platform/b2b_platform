import click
from flask import current_app as app
from flask import render_template
from flask.cli import AppGroup

from web import db
from web.core import utils

cli = AppGroup('emails')

@cli.command('new_messages_notification')
def new_messages_notification():

    unread_count_all_users = db.thread.get_unread_count_all_users()
    excludes = [
        '@trade60.com'
    ]
    for user_id, unread_count in unread_count_all_users:

        if unread_count <= 0: continue
        
        user = db.user.get_by_id(user_id)
        for item in excludes:
            if user.email.endswith(item): continue

        threads, _ = zip(*db.thread.get_unread_messages_count_full_thread(user.id, limit=5))

        print('SENDING EMAIL NOTIFICATION', user.email)

        html = render_template('emails/new_messages_notification.html',
            user=user,
            threads=threads,
            unread_count=unread_count
        )
        subject = 'New messages alert'

        resp = utils.send_mailgun(
            user.email,
            subject,
            html=html
        )