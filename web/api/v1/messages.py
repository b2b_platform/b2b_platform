from flask import jsonify, request, url_for
from flask_jwt_extended import jwt_required, get_jwt_identity
from datetime import datetime

from web import db, form_checker, role_manager, membership_manager, ds
from . import bp
from web.core.utils import api_output
from role import role_constants
from collections import Counter
from membership import membership_constants as mc

@bp.route('/messages.create_thread', methods=['POST'])
@jwt_required
def thread_create():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    args = form_checker.process(request, 'thread_create_form')

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MESSAGE, 
            role_constants.ACTION_OTHER_NONPRIV):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    # Check blocked users

    users = args.pop('users', [])
    if type(users) != list:
        users = [users]

    if user_id not in users:
        users.append(user_id)

    thread = db.thread.add(users=users, **args)

    return api_output(msg='Thread created', result=thread.to_dict())

@bp.route('/messages.get_product_inquiry_threads')
@jwt_required
def thread_get_product_inquiry_threads():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    user = db.user.get_by_id(user_id)

    if not user.company_id:
        return api_output(error='No company profile found', code=500)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MESSAGE, 
            role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    threads = db.thread.get_product_inquiry_threads_by_company(user.company_id)
    result = [thread.to_dict() for thread in threads]
    return api_output(msg='Returning threads', result=result)

@bp.route('/messages.get_thread')
@jwt_required
def thread_get():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'thread_get_form')

    thread_id = args.get('thread_id', None)
    thread = db.thread.get_by_id(thread_id)

    if not thread or not user_id in thread.users_by_id:
        return api_output(error='Thread not found', code=404)

    # Even privileged user must join first
    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MESSAGE, 
            role_constants.ACTION_VIEW):  
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    return api_output(msg='Returning thread', result=thread.to_dict())

@bp.route('/messages.delete_thread', methods=['POST'])
@jwt_required
def thread_delete():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'thread_delete_form')

    thread_id =  args.get('thread_id', None)
    thread = db.thread.get_by_id(thread_id)

    if not thread:
        return api_output(error='Thread {} not found'.format(thread_id), code=500)

    if not thread or not user_id in thread.users_by_id:
        return api_output(error='Thread not found', code=404)

    if not (role_manager.check_permission(user_id, 
                role_constants.OPERATION_MESSAGE, 
                role_constants.ACTION_DELETE) or 
            role_manager.check_permission(user_id, 
            role_constants.OPERATION_MESSAGE, 
            role_constants.ACTION_OTHER_PRIV) ): # Check special privilege
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    db.thread.delete_one(thread)
    
    return api_output(msg='Thread deleted')

@bp.route('/messages.list_threads', methods=['GET'])
@jwt_required
def thread_list():
    user_id = get_jwt_identity()
    threads_with_unread_count = db.thread.get_unread_messages_count_full_thread(user_id)
    threads = []

    for thread, unread_count in threads_with_unread_count:
        item = thread.to_short_dict()
        ts = datetime.utcnow() if unread_count > 0 else None
        item['unread_msgs'] = {'count': unread_count, 'ts': ts}
        threads.append(item)

    return api_output(result=threads, text='List threads')

@bp.route('/messages.search_threads', methods=['GET'])
@jwt_required
def thread_search():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    args = form_checker.process(request)
    search_opt = 'all'
    output_type = args.pop('output_type', 'short')
    search_term = args.pop('search_term', '')
    current_page = int(args.pop('current_page', 1))
    num_pages, threads = db.thread.search(user,
        search_term=search_term, search_opt=search_opt, current_page=current_page
    )
    if output_type == 'long':
        threads = [t.to_dict() for t in threads]
    else:
        threads = [t.to_short_dict() for t in threads]

    result = {
        'search_term': search_term,
        'current_page': current_page,
        'num_threads': len(threads),
        'threads': threads
    }
    return api_output(result=result, msg='Threads returned')

@bp.route('/messages.get_thread_attachments', methods=['GET'])
@jwt_required
def thread_get_attachments():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    thread_id = args.get('thread_id', None)    
    thread = db.thread.get_by_id(thread_id)

    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=500)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to see messages', code=403)

    media = db.message.get_thread_attachment(thread.id)
    result = {
        'thread': thread.to_short_dict(),
        'count': len(media),
        'attachments': [m.to_dict() for m in media]
    }
    return api_output(result=result, msg='Returning thread attachment')

@bp.route('/messages.get_thread_messages')
@jwt_required
def thread_get_messages():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    thread_id = args.get('thread_id', None)    
    thread = db.thread.get_by_id(thread_id)

    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=500)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to see messages', code=403)

    limit = int(args.pop('limit', 40))
    last_index = args.pop('last_index', None)
    keyword = args.pop('keyword', '')

    messages = db.message.get_by_thread_id(thread_id, 
        limit=limit,
        last_index=last_index,
        keyword=keyword
    )

    result = [ m.to_dict() for m in messages ]    

    return api_output(result=result, text='Returning messages')

@bp.route('/messages.new_message', methods=['POST'])
@jwt_required
def message_new():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    thread_id = args.get('thread_id', None)
    thread = db.thread.get_by_id(thread_id)

    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=500)

    if user_id not in thread.users_by_id or not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MESSAGE, role_constants.ACTION_UPDATE):
        return api_output(error='No permission to create message', code=403)

    result = db.message.add(**args)

    return api_output(result=result, text='Message created')

@bp.route('/messages.pin_message', methods=['POST'])
@jwt_required
def message_pin():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    message_id = args.get('message_id', None)
    message = db.message.get_by_id(message_id)

    if not message:
        return api_output(error=f'Message {message_id} not found', code=500)

    thread = db.thread.get_by_id(message.thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to pin message', code=403)
                
    result = db.message.update_one(message, is_pinned=True, pinned_by=user_id)

    return api_output(result=result, text='Message pinned')

@bp.route('/messages.unpin_message', methods=['POST'])
@jwt_required
def message_unpin():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    message_id = args.get('message_id', None)
    message = db.message.get_by_id(message_id)

    if not message:
        return api_output(error=f'Message {message_id} not found', code=500)

    thread = db.thread.get_by_id(message.thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to unpin message', code=403)
                
    result = db.message.update_one(message, is_pinned=False, pinned_by=None)

    return api_output(result=result, text='Message unpinned')

@bp.route('/messages.add_users_to_thread', methods=['POST'])
@jwt_required
def thread_add_users():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    thread_id = args.pop('thread_id', None)

    thread = db.thread.get_by_id(thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to add users to thread', code=403)
    
    user_ids = args.pop('users', [])
    if type(user_ids) != list:
        user_ids = [user_ids]

    users = db.user.query().filter(db.user.Model.id.in_(user_ids)).distinct().all()
    #users = db.user.filter(db.user.Model.id.in_(user_ids)).distinct().all()

    for user in users:
        thread._users.append(user)

    db.commit()

    return api_output(result=thread.to_dict(), text='Users added to thread')

@bp.route('/messages.remove_users_from_thread', methods=['POST'])
@jwt_required
def thread_remove_users():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    thread_id = args.pop('thread_id', None)

    thread = db.thread.get_by_id(thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to remove users from thread', code=403)
    
    if user_id == thread.creator_id:
        return api_output(error='You are the creator of this thread', code=500)
    
    user_ids = args.pop('users', [])    

    if type(user_ids) != list:
        user_ids = [user_ids]
    
    thread._users = [ u for u in thread._users if  
        str(u.id) not in user_ids or u.id == thread.creator_id ]    

    db.commit()

    return api_output(result=thread.to_dict(), text='Users removed from thread')

@bp.route('/messages.create_media_message', methods=['POST'])
@jwt_required
def thread_new_media_message():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    args = form_checker.process(request)

    thread_id = args.pop('thread_id', None)
    thread = db.thread.get_by_id(thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to create new message', code=403)

    if not request.files.get('media', None):
        return api_output(error='Empty files', code=500)

    files = request.files.getlist('media')
    if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_MEDIA_PER_THREAD, 
        len(files), thread_id=thread_id, user_id=user_id):
        return api_output(error=f'Usage exceeding limit', code=403)

    message = db.message.add_media_message(
        user_id=user_id,
        thread_id=thread_id, 
        **args)

    folder = f'thread/{thread_id}'
    
    for obj in files:
        obj_name, obj_url = ds.save_obj(folder, obj)
        media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
        )

        message._media.append(media)
        
    db.commit()

    return api_output(code=200, text='Media message created')

@bp.route('/messages.get_message', methods=['GET'])
@jwt_required
def message_get():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    message_id = args.get('message_id', None)
    message = db.message.get_by_id(message_id)

    if not message:
        return api_output(error=f'Message {message_id} not found', code=500)

    thread = db.thread.get_by_id(message.thread_id)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to get message', code=403)
                
    result = message.to_dict()

    return api_output(result=result, text='Returning message')

@bp.route('/messages.get_unread_messages_count', methods=['GET'])
@jwt_required
def message_get_unread_messages_count():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    messages_count = dict(db.thread.get_unread_messages_count(user_id))        

    result = {
        'messages_count': messages_count,        
        'user_id': user_id
    }

    return api_output(result=result, text='Returning unread messages count')

@bp.route('/messages.ping_thread', methods=['POST'])
@jwt_required
def message_ping_thread():
    user_id = get_jwt_identity()

    args = form_checker.process(request)
    thread_id = args.pop('thread_id', None)
    
    thread = db.thread.get_by_id(thread_id)
    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=404)

    if user_id not in thread.users_by_id:
        return api_output(error='No permission to get message', code=403)
        
    dt = db.thread.set_user_last_active(user_id, thread_id)
    
    result = {'thread_id': thread_id, 'user_id': user_id, 'dt': dt}
    return api_output(text='Ping thread', result=result)

@bp.route('/messages.read_all_threads', methods=['POST'])
@jwt_required
def message_read_all_threads():
    user_id = get_jwt_identity()

    dt = db.thread.read_all_threads(user_id)
    result = {'last_active_time': dt}
    return api_output(text='All threads read', result=result)

@bp.route('/messages.archive_thread', methods=['POST'])
@jwt_required
def thread_archive():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    args = form_checker.process(request)
    thread_id = args.pop('thread_id', None)
    
    thread = db.thread.get_by_id(thread_id)
    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=404)

    #TODO: MORE RESTRICTIVE

    db.thread.archive_thread(user, thread)

    return api_output(text='Thread archived', result=True)


@bp.route('/messages.unarchive_thread', methods=['POST'])
@jwt_required
def thread_unarchive():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    args = form_checker.process(request)
    thread_id = args.pop('thread_id', None)
    
    thread = db.thread.get_by_id(thread_id)
    if not thread:
        return api_output(error=f'Thread {thread_id} not found', code=404)

    db.thread.unarchive_thread(user, thread)

    return api_output(text='Thread unarchived', result=False)
        