from flask import request, jsonify,  url_for
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import bp
from web.core.utils import api_output
from web import db, form_checker, role_manager, membership_manager, ds
from membership import membership_constants as mc

@bp.route('/trade_lead.get', methods=['GET', 'POST'])
def trade_lead_get():
    args = form_checker.process(request, 'trade_lead_get_form')
    trade_lead = db.trade_lead.get_by_id(args['trade_lead_id'])
    result = trade_lead.to_dict() if trade_lead else None
    return api_output(result=result)

@bp.route('/trade_lead.list', methods=['GET', 'POST'])
@jwt_required
def trade_lead_list():
    user_id = get_jwt_identity()
    trade_leads = db.trade_lead.get_by_user_id(user_id)
    result = [trade_lead.to_dict() for trade_lead in trade_leads]
    return api_output(result=result)

@bp.route('/trade_lead.search', methods=['GET', 'POST'])
def trade_lead_search():
    #user_id = get_jwt_identity()

    args = form_checker.process(request, 'search_trade_lead')

    keyword = args.pop('keyword', '')
    category = args.pop('category', None)
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))
    sorted_by = 'trade_lead.' + args.pop('sorted_by', 'create_time')

    num_pages, trade_leads = db.trade_lead.search(keyword=keyword, category=category, 
                                    limit=limit, current_page=current_page, sorted_by=sorted_by)

    results = [trade_lead.to_dict() for trade_lead in trade_leads]

    return api_output(result=results, current_page=current_page, num_pages=num_pages)

@bp.route('/trade_lead.create', methods=['POST'])
@jwt_required
def trade_lead_create():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'trade_lead_create_form')
    args['user_id'] = user_id
    
    company_id = db.user.get_by_id(user_id).company_id

    #try:
    try:
        if not membership_manager.check_membership_company(company_id, mc.LIMIT_NUMBER_TRADE_LEADS, 1, user_id=user_id):
            return api_output(error=f'Usage exceeding limit', code=403)
        trade_lead = db.trade_lead.add(**args)
        db.commit()

        if request.files.get('media', None):
            files = request.files.getlist('media')
            if not membership_manager.check_membership_company(company_id, mc.LIMIT_NUMBER_MEDIA_PER_TRADE_LEAD, 1, user_id=user_id, trade_lead_id=trade_lead.id):
                return api_output(error=f'Usage exceeding limit', code=403)
            folder = f'trade_lead/{trade_lead.id}'

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                trade_lead._media.append(media)

            db.commit()
    except:        
        return api_output(error='Unable to create trade_lead', code=500)
    
    return api_output(result=trade_lead.to_dict(), url=url_for('trade_lead.trade_lead_page', pr_id=trade_lead.id))

@bp.route('/trade_lead.update', methods=['POST'])
@jwt_required
def trade_lead_update():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'trade_lead_update_form')
    trade_lead_id = args.pop('trade_lead_id', None)
    trade_lead = db.trade_lead.get_by_id(trade_lead_id)
    if not trade_lead:
        return api_output(error='Unable to find trade_lead {}'.format(trade_lead_id), code=500)

    if request.files.get('media', None):
        files = request.files.getlist('media')
        folder = f'trade_lead/{trade_lead_id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            trade_lead._media.append(media)

    db.trade_lead.update_one(trade_lead, **args)

    return api_output(result=trade_lead.to_dict(), url=url_for('trade_lead.trade_lead_page', pr_id=trade_lead.id))

@bp.route('/trade_lead.delete', methods=['POST'])
@jwt_required
def trade_lead_delete():
    user_id = get_jwt_identity()    
    trade_lead_id = request.values['trade_lead_id']
    trade_lead = db.trade_lead.get_by_id(trade_lead_id)

    if not trade_lead:
        return api_output(error='trade_lead {} not found'.format(trade_lead_id), code=404)

    db.trade_lead.delete_one(trade_lead)
    
    return api_output(msg='trade_lead deleted')

@bp.route('/trade_lead.follow', methods=['POST'])
@jwt_required
def trade_lead_follow():
    user_id = get_jwt_identity()
