from flask_jwt_extended import jwt_required, get_jwt_identity

from flask import request, abort, jsonify, views, url_for
from flask_login import login_required, current_user, login_user

from rauth import OAuth2Service
from werkzeug import secure_filename

from web import db, form_checker, role_manager, ds
from web.core import constants
from . import bp
from role import role_constants

# This refers to get my company
@bp.route('/company.get', methods=['GET', 'POST'])
@jwt_required
def company_get():
    user_id = get_jwt_identity()
    if not user_id and current_user.is_authenticated:
        user_id = current_user.get_id()

    user = db.user.get_by_id(user_id)
    
    if user is None:
        return jsonify({'Error': 'No company found for current user', 'code': 401}), 401
    
    args = form_checker.process(request, 'company_get_form')
    temp_comp_id = args.get('company_id')
    
    if not temp_comp_id:
        # Use user company id by default
        temp_comp_id = user.company_id
    
    if not role_manager.check_permission(user_id, 
                    role_constants.OPERATION_COMPANY,
                    role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    # Company exists
    # Legacy: company_descr = user.db.company.get_by_user(args['user'])
    company = db.company.get_by_id(temp_comp_id)
    
    return jsonify({'msg': 'Returning company',
                    'result': company.to_dict(),
                    'code': 200})
              
@bp.route('/company.create', methods=['GET', 'POST'])
@jwt_required
def company_create():
    user_id = get_jwt_identity()
        
    # Getting user info
    user = db.user.get_by_id(user_id)
    
    # Check if user already has a linked company
    if user.company_id:
        # company exists already
        return jsonify({'Error': 'Company already exists', 'code': 500}), 500
    
    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_COMPANY, 
            role_constants.ACTION_OTHER_NONPRIV):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    # Permission check is not necessary ?
    #if not role.check_permission():
    #    return jsonify({'msg': 'Error', 'code': 500}), 500
    
    # Process form
    args = form_checker.process(request, 'company_create_form')
    
    try:
        # Add new company
        
        company = db.company.add(**args)
        db.user.update_by_id(user_id, company_id=company.id, company_name=company.name)

        db.role.add(user_id=user_id,company_id=company.id,operation=role_constants.OPERATION_GENERAL,
            permission=role_constants.PERMISSION_ROLE_ADMIN)
      
        return jsonify({'msg': 'Company created',
                    'result': company.to_dict(),
                    'code': 200})
        # TBU
        # Adding company_id field into jwt token for faster processing?
        # update_jwt_identity(company_id) ?
     
    except:
        return jsonify({'msg': 'Error', 'code': 401}), 401
   

# Updating new company
@bp.route('/company.update', methods=['GET', 'POST'])
@jwt_required
def company_update():
    user_id = get_jwt_identity()
    
    user = db.user.get_by_id(user_id)
        
    if not user.company_id:        
        return jsonify({'Error': 'No company associated with this account', 'code': 500}), 500
  
    # Process form     
    args = form_checker.process(request, 'company_update_form')    
    #try:
   
    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_COMPANY,
            role_constants.ACTION_UPDATE):
        return jsonify({'Error': 'No permission', 'code': 500}), 500
    
    company = db.company.update_by_id(user.company_id, **args)
    
    # Permission check is not necessary ?
    for attr in constants.list_company_media_attributes:
        if request.files.get(attr, None):

            # delete old files here            
            old_media = getattr(company, f'_{attr}', None)            
            if old_media:
                ds.delete_obj(old_media.folder, old_media.name)

            obj = request.files[attr]
            folder = f'company/{user.company_id}/misc'

            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            cond = { f'_{attr}' : media }
            
            db.company.update_one(company, **cond)

    return jsonify({'msg': 'Company updated',
                    'code': 200, 'result' : company.to_dict()})

# Searching new company
@bp.route('/company.search', methods=['GET', 'POST'])
def company_search():
    # assert user.company_id exists
    
    # Permission check is not necessary ?
    #if not role.check_permission():
    #    return jsonify({'msg': 'Error', 'code': 500}), 500
    
    # Process form 
    args = form_checker.process(request, 'company_search_form')
    
    try:
        # Add new company
        companies = db.company.filter_by(**args)
        result = [ company.to_dict() for company in companies ]
        return jsonify({'msg': 'Companies found',
                    'result': result,
                    'code': 200})
    except Exception as e:
        return jsonify({'error': 'Error searching: {}'.format(e),
                        'code': 500}), 500
    
# This refers to get the employee list
@bp.route('/company.employee_list', methods=['GET', 'POST'])
@jwt_required
def company_get_emplist():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    
    if user is None:
        return jsonify({'Error': 'No company found for current user', 'code': 401}), 401
    
    args = form_checker.process(request, 'company_employee_form')
    #if not args['company_id']:
    #    args['company_id'] = user.company_id
    
    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_USER_MANAGEMENT, 
            role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    company = user.company
    # Use role table instead
    #employees = company.users  
    #employee_list = [ employee.to_public_dict() for employee in employees ]  
    
    #employees = db.role.filter_by_q(company_id=user.company_id,
    #            operation=role_constants.OPERATION_GENERAL).filter(models.UserCompanyRole.permission >= role_constants.PERMISSION_ROLE_USER)
    
    employees = user.company.roles
    employees_list = [ emp.to_user_dict() for emp in employees if emp.permission >= role_constants.PERMISSION_ROLE_USER ]

    # Filter the list based on permissions again / remove hidden or VIP users
    
    return jsonify({'msg': 'Returning employees',
                    'result_list': employee_list,
                    'code': 200})

# Disabled
#   Permission/Ownership important here: Primary contact?
@bp.route('/company.delete', methods=['POST'])
@jwt_required
def company_delete():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    if user is None:
        return jsonify({'msg': 'Error', 'code': 401}), 401
    
    # Not fully TBI
    # Process form - delete with arguments/options 
    args = form_checker.process(request, 'company_delete_form')

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_USER_MANAGEMENT, 
            role_constants.ACTION_DELETE):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    try:
        # Add new company
        company = db.company.delete_by_id(user.company_id)
        
        # TBU
        # Adding company_id field into jwt token for faster processing?
        # update_jwt_identity(company_id) ?
        
    except:
        return jsonify({'msg': 'Error', 'code': 401}), 401
    
    return jsonify({'msg': 'Company deleted',
                    'code': 200})


# Leave company
@bp.route('/company.leave', methods=['POST'])
@jwt_required
def company_leave():
    user_id = get_jwt_identity()

    user = db.user.get_by_id(user_id)
    
    if user is None:
        return jsonify({'msg': 'Error', 'code': 401}), 401
    
    # Not fully TBI
    # Process form - delete with arguments/options 
    args = form_checker.process(request, 'company_leave_form')
    
    if not user.company_id:
        return jsonify({'msg': 'No company to leave from.', 'code': 401}), 401
 
    try:
        # Update user profile
        db.user.update_by_id(user_id, company_id=None, company_name=None)

        # Update role table
        roles = db.role.filter_by(user_id=user_id)
        #roles = db.role.filter_by(user_id=user_id, company_id=user.company_id)
        for role in roles:
            db.role.delete_one(role)

        # If user is the last non-master user in company
        # TBI Notify Master User

        # Clone user account object
        new_user = db.user.clone_user(user)
        

    except:
        return jsonify({'msg': 'Error', 'code': 401}), 401
    
    return jsonify({'msg': 'Left company',
                    'code': 200})

# Linking company to profile
@bp.route('/company.link', methods=['GET', 'POST'])
@jwt_required
def company_link():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    
    # Check if user already has a linked company
    if user.company_id:
        # no company yet
        return jsonify({'Error': 'User already linked', 'code': 500}), 500
        
    # Process form 
    args = form_checker.process(request, 'company_link_form')

    company = db.company.get_by_id(args.get('company_id', None))

    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_USER_MANAGEMENT, 
                role_constants.ACTION_OTHER_NONPRIV):
        return jsonify({'Error': 'No permission to link', 'code': 500}), 500
    
    if not company:
        return jsonify({'Error': 'No company found', 'code': 404}), 404

    #user = db.user.update_by_id(user_id, company_id=company.id, company_name=company.name)
    
    # Check if there is a pending request
    existing = db.role.get_all_positions_by_userid(user_id)
    if existing:
        # Currently overwriting request
        for exrole in existing:
            db.role.delete_one(exrole)

    # Only 1 request logically not correct

    #existing = db.role.get_position_by_ids(user_id,company.id)
    # Create a new pending item
    role = db.role.add(user_id=user_id,
        company_id=company.id,
        operation=role_constants.OPERATION_GENERAL,
        permission=role_constants.REQUEST_PENDING)
        
    ## TBI:
    ## Notify admin and company super users
    user = db.user.update_by_id(user_id, 
        company_name=company.name, 
        pending_company_id=company.id)
    
    # company = db.user.update_by_id(user_id, company_name=comp_id)
    return jsonify({'msg': 'Successful company link request',
                    'code': 200})


# Updating new company
@bp.route('/company.verify', methods=['GET', 'POST'])
@jwt_required
def company_verify():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    
    ### IMPORTANT TBI: check logic if we let user without company link submit verify
    if not user.company_id: # User submitted at least a link request        
        return jsonify({'Error': 'No company associated with this account', 'code': 500}), 500
  
    # Process form     
    args = form_checker.process(request, 'company_verify_form')    
   
    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_COMPANY,
                role_constants.ACTION_OTHER_NONPRIV):
        return jsonify({'Error': 'No permission', 'code': 500}), 500
    
    note = args.get('verify-note', '')
    has_file_input = request.files.get('media', None)

    #media_id = None
    
    try:
        verify_comp = db.company_verify.add(user_id=user_id, company_id=user.company_id, note=note)

        if has_file_input:
            files = request.files.getlist('media')
            folder = f'company/{user.company_id}/verify' # Saved based on company

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                verify_comp._media.append(media)
        db.commit()

        return jsonify({'msg': 'Company verification updated',
                        'code': 200, 'media' : 'Done'})
    except:
        return jsonify({'msg': 'Error', 'code': 401}), 401
   

    