from flask import request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from . import bp
from web import db, form_checker, role_manager, membership_manager, ds
from role import role_constants
from membership import membership_constants as mc
from web.core.utils import api_output

@bp.route('/quote.get', methods=['GET'])
@jwt_required
def quote_get():
    quote = db.quote.get_by_id(request.values['quote_id'])
    result = quote.to_dict() if quote else None
    return jsonify({'result': result, 'code': 200}), 200

@bp.route('/quote.list', methods=['GET'])
@jwt_required
def quote_list():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'quote_list_form')
    quotes = db.quote.get_by_seller_id(user_id)
    result = [quote.to_dict() for quote in quotes]
    return jsonify({'result': result, 'code': 200}), 200

@bp.route('/quote.create', methods=['POST'])
@jwt_required
def quote_create():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    if not user.company_id:
        return jsonify({'error': 'Only user with a valid company can create quote', 'code': 500}), 500

    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_RFQ, 
                role_constants.ACTION_UPDATE):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    args = form_checker.process(request, 'quote_create_form')
    args['seller_id'] = user_id
    rfq_id = args.get('rfq_id')
    rfq = db.rfq.get_by_id(rfq_id)
    if not rfq:
        return jsonify({'error': 'Unable to find RFQ {}'.format(rfq_id), 'code': 500}), 500
    
    if rfq.quotes_left <= 0:
        return jsonify({'error': f'RFQ {rfq.id} reached max number of quotes', 'code': 500}), 500

    try:
        if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_QUOTES, 1, user_id=user_id):
            return api_output(error=f'Usage exceeding limit', code=403)

        quote = db.quote.add(**args)
        if request.files.get('media', None):
            files = request.files.getlist('media')
            if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_MEDIA_PER_QUOTE, 1, user_id=user_id, rfq_id=rfq.id):
                return api_output(error=f'Usage exceeding limit', code=403)

            folder = f'quote/{quote.id}'

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                quote._media.append(media)

            db.commit()
    except Exception as e:
        raise
        return jsonify({'error': 'Unable to create quote', 'code': 500}), 500

    return jsonify({'result': quote.to_dict(), 'code': 200}), 200

@bp.route('/quote.update', methods=['POST'])
@jwt_required
def quote_update():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'quote_update_form')
    quote_id = args.pop('quote_id', None)
    quote = db.quote.get_by_id(quote_id)
    if not quote:
        return jsonify({'error': 'Unable to find quote {}'.format(quote_id), 'code': 500}), 500

    if request.files.get('media', None):
        files = request.files.getlist('media')
        folder = f'quote/{quote_id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            quote._media.append(media)

    quote = db.quote.update_one(quote, **args)
    return jsonify({'result': quote.to_dict(), 'code': 500}), 200

@bp.route('/quote.delete', methods=['POST'])
@jwt_required
def quote_delete():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'quote_delete_form')
    quote_id = args.pop('quote_id', None)
    
    quote = db.quote.get_by_id(quote_id)
    
    if not quote:
        return jsonify({'error': 'Quote {} not found'.format(quote_id), 'code': 500}), 500
    
    db.quote.delete_one(quote)

    return jsonify({'result': 'Quote deleted', 'code': 200}), 200
