from flask import request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from . import bp
from web.core.utils import api_output
from web import db, form_checker, role_manager
from web.core.constants import SUPPORT_TICKET_TYPES

@bp.route('/support_ticket.get', methods=['GET'])
@jwt_required
def support_ticket_get():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    ticket_id = args.get('ticket_id', None)
    ticket = db.support_ticket.get_by_id(ticket_id)
    result = ticket.to_dict() if ticket else None

    user_role = db.role.get_position_by_userid(user_id)

    if user_id != ticket.user_id and role_manager.is_master(user_id):
        return api_output(error='No permission to get ticket', code=401)

    return api_output(result=result)

@bp.route('/support_ticket.list', methods=['GET'])
@jwt_required
def support_ticket_list():
    user_id = get_jwt_identity()
    tickets = db.support_ticket.get_by_user_id(user_id)
    result = [t.to_dict() for t in tickets]
    return api_output(result=result)

@bp.route('/support_ticket.create', methods=['POST'])
@jwt_required
def support_ticket_create():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'create_support_ticket')

    try:
        ticket = db.support_ticket.add(
            user_id=user_id,            
            **args
        )
        thread = db.thread.get_or_create_support_thread(user_id, ticket)
    except:        
        return api_output(error='Unable to create support ticket', code=500)

    return api_output(result=ticket.to_dict())
    
@bp.route('/support_ticket.update', methods=['POST'])
@jwt_required
def support_ticket_update():
    user_id = get_jwt_identity()    
    args = form_checker.process(request, 'support_ticket_update')
    ticket_id = args.pop('ticket_id', None)
    ticket = db.support_ticket.get_by_id(ticket_id)
    
    if not ticket:
        return api_output(error='Unable to find ticket {}'.format(ticket_id), code=404)

    user_role = db.role.get_position_by_userid(user_id)

    if user_id != ticket.user_id and not role_manager.is_master(user_id):
        return api_output(error='No permission to update ticket', code=401)

    db.support_ticket.update_and_create_message(ticket, user_id, **args)

    return api_output(result=ticket.to_dict())

@bp.route('/support_ticket.delete', methods=['POST'])
@jwt_required
def support_ticket_delete():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    ticket_id = args.get('ticket_id', None)
    ticket = db.support_ticket.get_by_id(ticket_id)

    if not ticket:
        return api_output(error='Unable to find ticket {}'.format(ticket_id), code=404)

    if user_id != ticket.user_id and not role_manager.is_master(user_id):
        return api_output(error='No permission to delete ticket', code=401)

    db.support_ticket.delete_one(ticket)

    return api_output(message='Ticket deleted')