from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from web import db, form_checker, role_manager, ds, membership_manager
from role import role_constants
from . import bp
from web.core.utils import api_output
from membership import membership_constants as mc

@bp.route('/company.upload_media', methods=['POST'])
@jwt_required
def upload_company_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)    
    company_id = args.pop('company_id', None)

    company = db.company.get_by_id(company_id)    
    
    if not company:
        return api_output(error=f'Company {company_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MEDIA, 
            role_constants.ACTION_UPDATE):
        return api_output(error='No permission', code=403)

    if request.files.get('media', None):        
        files = request.files.getlist('media')
        folder = f'company/{company_id}'
        if not membership_manager.check_membership_company(company_id, mc.LIMIT_NUMBER_MEDIA, len(files)):
            return api_output(error=f'Usage exceeding limit', code=403)

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            company._media.append(media)
    db.commit()
    return api_output(code=200, text='Company media uploaded')

@bp.route('/company.delete_media', methods=['POST'])
@jwt_required
def delete_company_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    
    item_id = args.pop('item_id', None)
    company_id = args.pop('company_id', None)    

    company = db.company.get_by_id(company_id)    
    
    if not company:
        return api_output(error=f'Company {company_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_MEDIA, 
            role_constants.ACTION_DELETE):
        return api_output(error='No permission', code=403)

    media = company._media.filter_by(id=item_id).one()

    if not media:
        return api_output(error=f'Media file {item_id} not found', code=404)
    
    db.media.delete_one(media)
    ds.delete_obj(media.folder, media.name)
    return api_output(code=200, text='Company media deleted')

@bp.route('/product.upload_media', methods=['POST'])
@jwt_required
def upload_product_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)    
    product_id = args.pop('product_id', None)

    product = db.product.get_by_id(product_id)
    
    if not product:
        return api_output(error=f'Product {company_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_PRODUCT,
            role_constants.ACTION_UPDATE):
        return api_output(error='No permission', code=403)

    if request.files.get('media', None):        
        files = request.files.getlist('media')
        if not membership_manager.check_membership_company(product.company_id, mc.LIMIT_NUMBER_MEDIA_PER_PRODUCTS, len(files), product_id=product_id):
            return api_output(error=f'Usage exceeding limit', code=403)
        folder = f'company/{product.company_id}/product/{product_id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            product._media.append(media)

    db.commit()

    return api_output(code=200, text='Product media uploaded')

@bp.route('/product.delete_media', methods=['POST'])
@jwt_required
def delete_product_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    
    item_id = args.pop('item_id', None)
    product_id = args.pop('product_id', None)
    product = db.product.get_by_id(product_id)
    
    if not product:
        return api_output(error=f'Product {product_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_PRODUCT, 
            role_constants.ACTION_DELETE):
        return api_output(error='No permission', code=403)

    media = product._media.filter_by(id=item_id).one()

    if not media:
        return api_output(error=f'Media file {item_id} not found', code=404)
    
    db.media.delete_one(media)
    ds.delete_obj(media.folder, media.name)
    return api_output(code=200, text='Product media deleted')


@bp.route('/rfq.upload_media', methods=['POST'])
@jwt_required
def upload_rfq_media():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user)

    args = form_checker.process(request)    
    rfq_id = args.pop('rfq_id', None)

    rfq = db.rfq.get_by_id(rfq_id)
    
    if not rfq:
        return api_output(error=f'RFQ {rfq_id} not found', code=404)

    if rfq.buyer_id != user_id or \
        not role_manager.check_permission(user_id, 
            role_constants.OPERATION_RFQ, 
            role_constants.ACTION_UPDATE):
        return api_output(error='No permission', code=403)

    if request.files.get('media', None):        
        files = request.files.getlist('media')
        if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_MEDIA_PER_RFQ, len(files), user_id=user_id, rfq_id=rfq.id):
            return api_output(error=f'Usage exceeding limit', code=403)
        folder = f'rfq/{rfq.id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            rfq._media.append(media)

    db.commit()

    return api_output(code=200, text='RFQ media uploaded')

@bp.route('/rfq.delete_media', methods=['POST'])
@jwt_required
def delete_rfq_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    
    item_id = args.pop('item_id', None)
    rfq_id = args.pop('rfq_id', None)
    rfq = db.rfq.get_by_id(rfq_id)
    
    if not rfq:
        return api_output(error=f'RFQ {rfq_id} not found', code=404)

    if rfq.buyer_id != user_id or \
        not role_manager.check_permission(user_id, 
            role_constants.OPERATION_RFQ, 
            role_constants.ACTION_DELETE):
        return api_output(error='No permission', code=403)

    media = rfq._media.filter_by(id=item_id).one()

    if not media:
        return api_output(error=f'Media file {item_id} not found', code=404)
    
    db.media.delete_one(media)
    ds.delete_obj(media.folder, media.name)
    return api_output(code=200, text='RFQ media deleted')

@bp.route('/quote.upload_media', methods=['POST'])
@jwt_required
def upload_quote_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)    
    quote_id = args.pop('quote_id', None)

    quote = db.quote.get_by_id(quote_id)
    
    if not quote:
        return api_output(error=f'Quote {quote_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_QUOTE, 
            role_constants.ACTION_UPDATE):
        return api_output(error='No permission', code=403)

    if request.files.get('media', None):
        files = request.files.getlist('media')
        if not membership_manager.check_membership_company(quote.company_id, mc.LIMIT_NUMBER_MEDIA_PER_QUOTE, len(files), user_id=user_id, quote_id=quote.id):
            return api_output(error=f'Usage exceeding limit', code=403)
        folder = f'quote/{quote.id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            quote._media.append(media)

    db.commit()

    return api_output(code=200, text='Quote media uploaded')

@bp.route('/quote.delete_media', methods=['POST'])
@jwt_required
def delete_quote_media():
    user_id = get_jwt_identity()
    args = form_checker.process(request)
    
    item_id = args.pop('item_id', None)
    quote_id = args.pop('quote_id', None)
    quote = db.quote.get_by_id(quote_id)
    
    if not quote:
        return api_output(error=f'Quote {quote_id} not found', code=404)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_QUOTE, 
            role_constants.ACTION_DELETE):
        return api_output(error='No permission', code=403)

    media = quote._media.filter_by(id=item_id).one()

    if not media:
        return api_output(error=f'Media file {item_id} not found', code=404)
    
    db.media.delete_one(media)
    ds.delete_obj(media.folder, media.name)
    return api_output(code=200, text='Quote media deleted')

