from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from web import db, form_checker, role_manager, ds
from . import bp
from web.core.utils import api_output
from role import role_constants

@bp.route('/notification.get', methods=['GET'])
@jwt_required
def notification_get():
    user_id = get_jwt_identity()
    result = []
    return api_output(text='Returning notifications', result=result)

@bp.route('/notification.get_unread', methods=['GET'])
@jwt_required
def notification_get_unread():
    user_id = get_jwt_identity()
    result = []
    return api_output(text='Returning unread notifications', result=result)

@bp.route('/notification.get_unread_count', methods=['GET'])
@jwt_required
def notification_get_unread_count():
    user_id = get_jwt_identity()        
    user = db.user.get_by_id(user_id)
    unread_count = db.notification.get_unread_count(user.id, user.company_id)    
    return api_output(text='Counting unread notifications', result={'unread_count': unread_count})

@bp.route('/notification.read_all', methods=['POST'])
@jwt_required
def notification_read_all():
    user_id = get_jwt_identity()
    dt = db.notification.read_all(user_id)
    result = {'last_read_time': dt}
    return api_output(text='All notifications read', result=result)
