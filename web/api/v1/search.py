from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity

from web import db, form_checker, role_manager
from web.core.utils import api_output
from . import bp
from web.core import geo
from flask import request, jsonify

@bp.route('/search', methods=['GET', 'POST'])
@jwt_required
def search_default():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'search_default_form', empty_string=True)    
    full_text = True if args.get('full_text', None) is not None else False
    
    db_service = getattr(db, args.get('service', 'company'))

    if full_text:
        keyword = args.get('keyword', None)
        limit = args.pop('limit', None)
        objs = db.sql_search(db_service.query(), keyword).limit(limit).all()
        result = [ o.to_dict() for o in objs]        
        return api_output(result=result)
    
    return api_output(result=[])


# List all product categories
@bp.route('/region.list', methods=['GET'])
def region_list():
    #user_id = get_jwt_identity()
    parent_region = request.args.get('region_id', None) or None    
    
    results = None
    if parent_region:
        country_list = geo.region_and_countries_set[parent_region]
        results = { cat.name: idx for idx, cat in country_list }
    else:
        results = geo.region_and_countries_set

    return jsonify({
        'result': results, 'code': 200
    })