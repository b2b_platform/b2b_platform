from flask import request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from web import db, form_checker, role_manager
from . import bp

# Get product category and its subcategories
@bp.route('/product_category.get', methods=['GET'])
# not requiring login
def product_category_get():
    args = form_checker.process(request, 'product_category_get_form')
    # TBI : adding args into filter functions
    
    # Filtering second major categories
    
    # Recursively obtain json description of categories
    max_level = 2
    # Filtering first major categories
    current_level = args.get('cat_level')
    if 'cat_level' in args:    
        max_level = int(current_level)
        parent_cat_id = args.get('parent_cat_id')
        
        if parent_cat_id: # obtain specific second categories
            parent_product_cats = db.product_category.filter_by(parent_category_id=parent_cat_id)
            result = [ get_dict_category(par_cat, max_level-1) for par_cat in parent_product_cats ]
            return jsonify({'msg': 'Specific product categories obtained',
                        'code': 200, 'result' : result})
   
   
    # Regular flow no args
    parent_product_cats = db.product_category.filter_by(parent_category_id=None)
    result = [ get_dict_category(par_cat, max_level-1) for par_cat in parent_product_cats ]
    #for cur_cat in parent_product_cats:
    #    sub_cats = db.product_category.filter_by(parent_category_id=cur_cat.id)
       # print ("length sub: ", len(sub_cats))
     #   result.append( { 'text' : cur_cat.name, 'href' : cur_cat.id, \
     #     'nodes' : [ { 'text': sub_cat.name, 'href' : sub_cat.id } for sub_cat in sub_cats ] })
       
    return jsonify({'msg': 'Product categories obtained',
                        'code': 200, 'result' : result})
    
# Assume sub_cat is not empty
def get_dict_category(sub_cat, max_level):
    if not sub_cat:
        children_cats = db.product_category.filter_by(parent_category_id=None)
    else:
        children_cats = db.product_category.filter_by(parent_category_id=sub_cat.id)
    
    if max_level == 0 or not children_cats: # last level or leaf level (no children) 
        return  { 'text': sub_cat.name, 'href' : sub_cat.id } 
    else:
        return { 'text' : sub_cat.name, 'href' : sub_cat.id, \
          'nodes' : [ get_dict_category(child, max_level - 1) for child in children_cats ] }

# List all product categories
@bp.route('/product_category.list', methods=['GET'])
def product_category_list():
    #user_id = get_jwt_identity()
    parent_category_id = request.args.get('parent_category_id', None) or None    
    top_categories = db.product_category.filter_by(parent_category_id=parent_category_id)
    categories = { cat.name: cat.to_short_dict() for cat in top_categories }

    return jsonify({
        'result': categories, 'code': 200
    })

@bp.route('/product_category.create', methods=['POST'])
# disabled
def product_category_create():
    user_id = get_jwt_identity()
    return jsonify({})

@bp.route('/product_category.update', methods=['POST'])
def product_category_update():
# Allowed or not by specific user or needs request?
    user_id = get_jwt_identity()

@bp.route('/product_category.delete', methods=['POST'])
# Not allowed
def product_category_delete():
    user_id = get_jwt_identity()
    return jsonify({})
