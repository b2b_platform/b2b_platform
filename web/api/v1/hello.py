from flask import jsonify
from flask_jwt_extended import jwt_required
from . import bp

@bp.route('/hello', methods=['GET','POST','PUT','PATCH','DELETE'])
def hello():
    return jsonify({'text': 'hello world', 'code': 200}), 200

@bp.route('/hello/protected', methods=['GET','POST','PUT','PATCH','DELETE'])
@jwt_required
def hello_protected():
    return jsonify({'msg': 'OK', 'code': 200}), 200
