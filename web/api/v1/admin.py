from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from datetime import datetime, timedelta

from web import db, form_checker, role_manager, membership_manager, ds
from . import bp
from web.core.utils import api_output
from role import role_constants
from membership import membership_constants as mc

@bp.route('/admin.update_user', methods=['GET', 'POST'])
@jwt_required
def admin_user_update():
    # Manually add role - enforce without invite

    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if not role_manager.is_master(user.id):
        return api_output(error='Access Denied', code=404)

    args = form_checker.process(request, 'admin_user_update_form')

    # Non-master user
    target_user_id = args.pop('target_user', None)
    target_user = db.user.get_by_id(target_user_id)

    if not target_user:
        return jsonify({'msg': 'User not found', 'code': 404}), 404

    # Update user attribute
    db.user.update_by_id(target_user_id, **args)

    # Needs to ensure consistency here    
    return api_output(msg='Updated', result=None)

@bp.route('/admin.update_company', methods=['GET', 'POST'])
@jwt_required
def admin_company_update():

    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if not role_manager.is_master(user.id):
        return api_output(error='Access Denied', code=404)

    args = form_checker.process(request, 'admin_company_update_form')

    company_id = args.pop('company_id', None)
    if not company_id:
        return api_output(error='Invalid company_id', code=500)
    
    company = db.company.get_by_id(company_id)
    if not company:
        return api_output(error='Invalid company_id', code=500)
    
    company = db.company.update_one(company, **args)
    
    return api_output(msg='Updated', result=None)

@bp.route('/admin.update_company_membership', methods=['GET', 'POST'])
@jwt_required
def admin_company_update_membership():

    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if not role_manager.is_master(user.id):
        return api_output(error='Access Denied', code=404)

    args = form_checker.process(request)

    company_id = args.pop('company_id', None)
    if not company_id:
        return api_output(error='Invalid company_id', code=500)
    
    company = db.company.get_by_id(company_id)
    if not company:
        return api_output(error='Invalid company_id', code=500)

    try:
        membership_tier = args.pop('membership_tier', None)
    except:
        return api_output(error='Invalid tier', code=500)
    
    membership_duration = int(args.pop('membership_duration', 30))
    
    start = datetime.utcnow()
    end = start + timedelta(days=membership_duration)

    db.membership.add_new_membership_by_company(company.id, tier=membership_tier,
    membership_start=start, membership_end=end)

    return api_output(msg='Updated', result=None)