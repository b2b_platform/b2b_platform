from flask import request, jsonify, url_for, session
from flask_jwt_extended import jwt_required, get_jwt_identity
from datetime import datetime
import os

from . import bp
from web import db, form_checker, ds, r, basedir, role_manager, sess
from web.core import constants
from role import role_constants
from web.core.utils import api_output

@bp.route('/account.get')
@jwt_required
def account_get():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if user is None:
        return jsonify({'msg': 'User not found', 'code': 403}), 403

    target_user_id = request.values.get('user_id', None)

    # Might skip role checker
    if not role_manager.check_permission(user_id, 
                    role_constants.OPERATION_ACCOUNT,
                    role_constants.ACTION_VIEW, 
                    target_user_id=target_user_id):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    if target_user_id and target_user_id != user_id:
        user = db.user.get_by_id(target_user_id)
        return jsonify({'result': user.to_dict(), 'code': 200}), 200

    return jsonify({'result': user.to_dict(), 'code': 200}), 200

def adjust_settings_type(val, vtype):
    if vtype == bool:
        return val == 'True' or val == 'true'
    elif vtype == int:
        return int(val)
    if vtype == list:
        if not isinstance(val, (list, tuple)):
            val = [val]
        return list(filter(None, val))

@bp.route('/account.update', methods=['POST'])
@jwt_required
def account_update():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if not user:
        return api_output(error='User not found', code=404)

    # Only allowing updating current user    
    args = form_checker.process(request, 'account_update_form')

    privacy_args = {}
    site_args = {}
    
    for key in list(args.keys()):
        if key in constants.SETTINGS_PRIVACY_KEYS.keys():
            privacy_args[key] = adjust_settings_type(args.pop(key), 
                constants.SETTINGS_PRIVACY_KEYS[key])
        elif key in constants.SETTINGS_SITE_KEYS.keys():
            site_args[key] = adjust_settings_type(args.pop(key),
                constants.SETTINGS_SITE_KEYS[key])

    # profile pic    
    if request.files.get('profile_pic', None):        
        if user._profile_pic:
            ds.delete_obj(user._profile_pic.folder, user._profile_pic.name)

        obj = request.files['profile_pic']  
        folder = f'user/{user_id}'
        obj_name, obj_url = ds.save_obj(folder, obj)
        profile_pic = db.media.add(
            name = obj_name,
            url = obj_url,
            folder = folder,
            file_type = obj.content_type
        )
        args['_profile_pic'] = profile_pic

    # profile pic    
    if request.files.get('banner', None):        
        if user._banner:
            ds.delete_obj(user._banner.folder, user._banner.name)

        obj = request.files['banner']
        folder = f'user/{user_id}'
        obj_name, obj_url = ds.save_obj(folder, obj)
        banner = db.media.add(
            name = obj_name,
            url = obj_url,
            folder = folder,
            file_type = obj.content_type
        )
        args['_banner'] = banner
    
    user = db.user.update_one(user, privacy_args=privacy_args, site_args=site_args, **args)
    
    return jsonify({'msg': 'User profile updated', 'code': 200}), 200

@bp.route('/account.change_password', methods=['POST'])
@jwt_required
def account_change_password():
    user_id = get_jwt_identity()

    args = form_checker.process(request, 'account_change_password_form')

    user = db.user.get_by_id(user_id)
        
    if not user:
        return api_output(error='User not found', code=404)

    if not user.check_password(args['old_password']):
        return api_output(error='Old password is wrong', code=401)
    
    user = db.user.update_by_id(user_id, password=args['new_password'])

    return jsonify({'msg': 'Password updated', 'code': 200}), 200

@bp.route('/account.search', methods=['GET'])
@jwt_required
def account_search():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    if not user:
        return api_output(error='User not found', code=404)

    args = form_checker.process(request)
    search_term = args.get('search_term', None)

    if not search_term:
        return api_output(result=[])

    #users = db.user.query().filter(qs).distinct().all()
    # Search users via db services
    num_pages, users, num_results = db.user.search(search_term=search_term, id_term=search_term)
    
    # Exclude user itself from search result?
    result = [ u.to_msg_dict() for u in users if u.is_visible()  ]
    #result = [ u.to_msg_dict() for u in users if u.id != user_id ]
    return api_output(result=result)

EXPIRE_SECS = 10

@bp.route('/account.ping', methods=['GET', 'POST'])
@jwt_required
def account_ping():    
    user_id = get_jwt_identity()
    key = f'{user_id}_account_ping'
    value = datetime.utcnow().timestamp()
    r.set(key, value, ex=EXPIRE_SECS)

    return api_output(result={'ping': 'pong', 'time': value})

UA_LOG_FOLDER = os.path.join(os.path.abspath(os.path.join(basedir, os.pardir)), 'logs', 'user_activity')
UA_LOG_NAME = 'user_activity.{date}.log'
fmt_val = lambda x: '' if not x else str(x)

if not os.path.exists(UA_LOG_FOLDER):
    os.makedirs(UA_LOG_FOLDER)

@bp.route('/account.log', methods=['POST'])
def account_log():
    
    data = request.get_json()
    now = datetime.now().strftime('%Y%m%d')
    log_path = os.path.join(UA_LOG_FOLDER, UA_LOG_NAME.format(date=now))    

    first_line = None

    if not os.path.exists(log_path):
        first_line = '|'.join(data['activities'][0].keys()) + '\n'
    
    with open(log_path, 'a') as f:
        if first_line: f.write(first_line)
        lines = ['|'.join(fmt_val(val) for val in line.values()) for line in data['activities']]
        if len(lines): lines.append('')
        f.write('\n'.join(lines))
    
    return api_output(msg='Logged user activities')

@bp.route('/account.add_to_basket', methods=['POST'])
@jwt_required
def account_add_to_basket():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    product_id = args.get('product_id', None)
    if not product_id:
        return api_output(error='Invalid product', code=500)

    product = db.product.get_by_id(product_id)
    if not product:
        return api_output(error='Invalid product', code=500)
    
    if not session.get('BASKET_PRODUCTS', None):
        session['BASKET_PRODUCTS'] = set()
    
    basket = session.get('BASKET_PRODUCTS')
    if len(basket) >= 15:
        return api_output(error='You have reached maximum number (15) of products in basket', code=500)
    
    if product.id in basket:
        return api_output(error='Product already added in your basket', code=500)

    basket.add(product.id)

    return api_output(msg='Product added to basket')

@bp.route('/account.remove_from_basket', methods=['POST'])
@jwt_required
def account_remove_from_basket():
    user_id = get_jwt_identity()
    args = form_checker.process(request)

    product_id = args.get('product_id', None)
    if not product_id:
        return api_output(error='Invalid product', code=500)

    product = db.product.get_by_id(product_id)
    if not product:
        return api_output(error='Invalid product', code=500)
    
    if not session.get('BASKET_PRODUCTS', None):
        return api_output(msg='Empty basket', code=500) 
    
    basket = session.get('BASKET_PRODUCTS')    
    
    if product.id not in basket:
        return api_output(error='Product is not in the basket', code=500)

    basket.remove(product.id)

    return api_output(msg='Product removed from basket')    
