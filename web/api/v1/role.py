from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from web import db, form_checker, role_manager, membership_manager, ds
from . import bp
from web.core.utils import api_output
from role import role_constants
from membership import membership_constants as mc

@bp.route('/role.create', methods=['GET', 'POST'])
@jwt_required
def role_create():
    # Manually add role - enforce without invite

    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    args = form_checker.process(request, 'role_create_form')

    permission = args.pop('permission', role_constants.PERMISSION_ROLE_USER)

    # Route for Master user
    if role_manager.is_master(user_id):
        company_id = args.pop('company_id', None)
        target_user_id = args.pop('target_user', None)
        if company_id and target_user_id:           
            role = db.role.add(user_id=target_user_id,company_id=company_id,
                operation=role_constants.OPERATION_GENERAL, permission=permission)
            db.user.update_by_id(target_user_id, company_id=company_id)
            return api_output(msg='User role added', result=None)

    # Non-master user
    target_user_id = args.pop('target_user', None)
    target_user = db.user.get_by_id(target_user_id)

    if not user.company_id:
        return jsonify({'msg': 'Error no company associated with your account', 'code': 401}), 401

    if not target_user:
        return jsonify({'msg': 'User not found', 'code': 404}), 404

    # Check existing role
    existing = db.role.get_position_by_userid(target_user_id)
    if existing: # Here only used to add directly or accept invite
        # User already added somewhere
        return jsonify({'msg': 'User is already added or belonged to a different company', 'code': 401}), 401

    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_ROLE,
                role_constants.ACTION_UPDATE,
                target_permission=permission):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401
    
    if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_USERS, 1):
        return api_output(error=f'Usage exceeding limit', code=403)

    # Added invite email
    # Async check/lock?

    # Update user table
    # Approve user role and update user and add role to db
    db.user.update_by_id(target_user_id, company_id=user.company_id, pending_company_id=None)
    role = db.role.add(user_id=target_user.id,
        company_id=user.company_id,
        operation=role_constants.OPERATION_GENERAL,
        permission=permission)
    # Needs to ensure consistency here
    
    return api_output(msg='Role added', result=None)

@bp.route('/role.update', methods=['GET', 'POST'])
@jwt_required
def role_update():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    args = form_checker.process(request, 'role_update_form')
    
    operation = args.pop('operation', role_constants.OPERATION_GENERAL)
    permission = args.pop('permission', role_constants.PERMISSION_ROLE_USER)
    target_user_id = args.pop('target_user', None)
    
    if not target_user_id:
        # Missing target user id
        return jsonify({'msg': 'Error target user', 'code': 401}), 401

    # Route for Master user
    if role_manager.is_master(user_id):
        company_id = args.pop('company_id', None)

        if company_id :
            role = db.role.update_position_by_userid(target_user_id, operation=operation, permission=permission)
            # update target user 
            db.user.update_by_id(target_user_id, company_id=company_id)
            return api_output(msg='Role added', result=None)
        else:
            return jsonify({'msg': 'Error missing company', 'code': 401}), 401

    target_user = db.user.get_by_id(target_user_id)

    if target_user.company_id and target_user.company_id != user.company_id: # User in a different company
        return jsonify({'msg': 'Invalid access to manage user', 'code': 401}), 401

    # For accepting pending request target_user.company_id == user.company_id
    if target_user.pending_company_id and target_user.pending_company_id != user.company_id:
        # Target has pending request but not from company user
        return jsonify({'msg': 'Invalid access to manage user', 'code': 401}), 401

    # To update target user permission or accept pending request
    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_USER_MANAGEMENT,
                role_constants.ACTION_UPDATE,
                target_permission=permission,
                target_user_id=target_user_id): # To check for original permission
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    # Current update enforces only position
    # operation == role_constants.OPERATION_GENERAL
    # update target user permission
    
    if not target_user.company_id and target_user.pending_company_id:
        # Approve new user that had pending request
        if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_USERS, 1):
            return api_output(error=f'Usage exceeding limit', code=403)
        db.user.update_one(target_user, company_id=target_user.pending_company_id,
                pending_company_id=None)

    role = db.role.update_position_by_userid(target_user_id, permission=permission)

    return api_output(msg='Role updated', result=None)

@bp.route('/role.get', methods=['GET', 'POST'])
@jwt_required
def role_get():
    return api_output(msg='Not implemented', result=None)

@bp.route('/role.delete', methods=['POST'])
@jwt_required
def role_delete():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)
    
    args = form_checker.process(request, 'role_delete_form')
    target_user_id = args.pop('target_user', None)
  
    if not target_user_id:
        # Missing target user id
        return jsonify({'msg': 'Error target user', 'code': 401}), 401

    # Delete role
    target_user = db.user.get_by_id(target_user_id)

    # Skip users in a different companny
    if (target_user.company_id and target_user.company_id != user.company_id): # User in a different company
        return jsonify({'msg': 'Invalid access to manage user', 'code': 401}), 401

    # Reject pending request
    if (not target_user.company_id) and target_user.pending_company_id == user.company_id: 
        # User in a different company
        req = db.role.get_position_by_userid(target_user_id)
        if (req and req.permission == role_constants.REQUEST_PENDING 
                and req.company_id == user.company_id):
        
            # delete request
            if role_manager.check_permission(user_id, 
                    role_constants.OPERATION_GENERAL, 
                    role_constants.ACTION_DELETE, 
                    target_user_id=target_user_id):
                db.role.delete_one(req)
                db.user.update(target, company_id=None, pending_company_id=None)
                # db.user.update_one(target_user, company_id=None)
                return api_output(msg='Pending request deleted', result=None)
        
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    return jsonify({'msg': 'Invalid delete request', 'code': 401}), 401

@bp.route('/role.list', methods=['GET'])
@jwt_required
def role_list():
    
    return api_output(msg='Not implemented', result=None)
