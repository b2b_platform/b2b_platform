from flask import request, abort, jsonify, views, url_for, send_file
from flask_login import login_required, current_user, login_user
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity
from werkzeug import Headers

from web import db, form_checker, role_manager, membership_manager, ds
from . import bp
from web.core.utils import api_output
from role import role_constants
from membership import membership_constants as mc

# This refers to get the detail of current product
@bp.route('/product.get', methods=['GET', 'POST'])
@jwt_required
def product_get():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if user is None: # Product ID not set or user is not authorized
        return jsonify({'Error': 'No product found for current user', 'code': 401}), 401

    args = form_checker.process(request, 'product_get_form')

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_PRODUCT, 
            role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 403}), 403

    # company = db.company.get_by_id(temp_comp_id)
    product = db.product.get_by_id(args['product_id'])

    if not product:
        return api_output(code=404, error='Product not found')

    # Parsing products to JSON
    return jsonify({'msg': 'Returning products',
                    'product_id' : product.id,  # redirecting with product ID for redirecting purpose
                    'result': product.to_dict(),
                    'code': 200})

@bp.route('/product.list', methods=['GET', 'POST'])
@jwt_required
def product_list():
    user_id = get_jwt_identity()
    user = db.user.get_by_id(user_id)

    if user is None:
        return jsonify({'Error': 'No product found for current user', 'code': 401}), 401

    args = form_checker.process(request, 'product_list_form')

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_PRODUCT, 
            role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    if not user.company_id:
        return api_output(error='User company not found', code=404)

    keyword = args.pop('keyword', '')
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))
    sorted_by = 'product.' + args.pop('sorted_by', 'name')

    num_pages, products = db.product.search(limit=limit, current_page=current_page, sorted_by=sorted_by,
        company_id=user.company_id)

    result = [ item.to_dict() for item in products ]

    return jsonify({'msg': 'Returning products',
                    'result': result,
                    'current_page': current_page,
                    'num_pages': num_pages,
                    'code': 200})

@bp.route('/product.create', methods=['POST'])
@jwt_required
def product_create():
    user_id = get_jwt_identity()
    # Getting user info
    user = db.user.get_by_id(user_id)

    if not role_manager.check_permission(user_id, 
            role_constants.OPERATION_PRODUCT, 
            role_constants.ACTION_UPDATE):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    if not membership_manager.check_membership_company(user.company_id, mc.LIMIT_NUMBER_PRODUCTS, 1, user_id=user_id):
            return api_output(error=f'Usage exceeding limit', code=403)

    # Process form
    args = form_checker.process(request, 'product_create_form')

    try:
        
        product = db.product.add(**args)
        
        if request.files.get('display_pic', None):
        
            obj = request.files['display_pic']
            folder = f'company/{user.company_id}/product/{product.id}'

            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            product._display_pic = media

        if request.files.get('media', None):
            files = request.files.getlist('media')
            if not membership_manager.check_membership_company(product.company_id, mc.LIMIT_NUMBER_MEDIA_PER_PRODUCT, len(files), product_id=product.id, user_id=user_id):
                return api_output(error=f'Usage exceeding limit', code=403)

            folder = f'company/{product.company_id}/product/{product.id}'

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                product._media.append(media)

            db.commit()

        return jsonify({'msg': 'Product created',
                    'result' : product.to_dict(),
                    'code': 200})

    except Exception as e:
        return jsonify({'msg': 'Unable to create product {}'.format(e), 'code': 401}), 401

# Updating new product
@bp.route('/product.update', methods=['POST'])
@jwt_required
def product_update():
    user_id = get_jwt_identity()

    user = db.user.get_by_id(user_id)

    args = form_checker.process(request, 'product_update_form')

    product_id = args.pop('product_id', None)

    product = db.product.get_by_id(product_id)
    if not product:
        return api_output(error=f'Product {product_id} not found', code=404)

    if (product.company_id != user.company_id or 
            not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_UPDATE) ):
        # TBI: Add product_id to check permission parameter - separate permission for users
        return abort(403)

    if request.files.get('display_pic', None):
        old_media = product._display_pic
        if old_media:
            product._display_pic = None
            try:
                db.media.delete_one(old_media)
                ds.delete_obj(old_media.folder, old_media.name)
            except:
                # still dependency some where
                pass
    
        obj = request.files['display_pic']
        folder = f'company/{user.company_id}/product/{product_id}'

        obj_name, obj_url = ds.save_obj(folder, obj)
        media = db.media.add(
            name=obj_name,
            url=obj_url,
            folder=folder,
            file_type=obj.content_type
        )
        product._display_pic = media

    try:
        if request.files.get('media', None):
            files = request.files.getlist('media')
            if not membership_manager.check_membership_company(product.company_id, mc.LIMIT_NUMBER_MEDIA_PER_PRODUCT, len(files), product_id=product.id, user_id=user_id):
                return api_output(error=f'Usage exceeding limit', code=403)
                
            folder = f'company/{product.company_id}/product/{product.id}'

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                product._media.append(media)

        product = db.product.update_one(product, **args)
        return jsonify({'msg': 'Product updated',
                        'code': 200, 'result' : product.to_dict()})
    except Exception as e:
        return jsonify({'msg': 'Error {}'.format(e), 'code': 502}), 502

@bp.route('/product.search', methods=['POST'])
def product_search():

    args = form_checker.process(request, 'product_search')

    try:
        products = db.product.filter_by(**args)
        result = [ product.to_dict() for product in products ]

        return jsonify({'msg': 'Products found',
                    'result': result,
                    'code': 200})
    except:
        return jsonify({'msg': 'Error searching products', 'code': 502}), 502

# Not used for now
@bp.route('/product.delete', methods=['POST'])
@jwt_required
def product_delete():
    user_id = get_jwt_identity()

    args = form_checker.process(request, 'product_delete')
    product_id = args.pop('product_id', None)

    product = db.product.get_by_id(product_id)
    if not product:
        return api_output(error='invalid product id', code=404)

    if (product.company_id != user.company_id or 
            not role_manager.check_permission(current_user.id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_DELETE) ):
        return api_output(error='Error no permission', code=403)

    try:
        product = db.product.delete_by_id(product_id)

    except Exception as e:
        return jsonify({'error': str(e), 'code': 500}), 500

    return jsonify({'msg': 'Company deleted',
                    'code': 200})

@bp.route('/product.duplicate', methods=['POST'])
@jwt_required
def product_duplicate():
    user_id = get_jwt_identity()

    args = form_checker.process(request, 'product_duplicate')
    product_id = args.pop('product_id', None)
    if not product_id:
        return api_output(error='invalid product id', code=404)

    product = db.product.get_by_id(product_id)
    if not product:
        return api_output(error='Product not found', code=404)

    new_product = db.product.duplicate(product)

    return api_output(result=new_product.to_dict(), msg="Product duplicated")

@bp.route('/product.export', methods=['GET'])
@jwt_required
def product_export():
    user_id = get_jwt_identity()
    # Getting user info
    user = db.user.get_by_id(user_id)

    # Disable for non-company user so they cannot export product via API
    if not role_manager.check_permission(user_id, 
                role_constants.OPERATION_PRODUCT, 
                role_constants.ACTION_VIEW):
        return jsonify({'msg': 'Error no permission', 'code': 401}), 401

    try:
        filename, export_file = db.product.generate_export_file_for_company(user.company)

        return send_file(export_file,
                        attachment_filename=filename,
                        as_attachment=True)

    except Exception as e:        
        return jsonify({'msg': 'Unable to export product {}'.format(e), 'code': 401}), 401
