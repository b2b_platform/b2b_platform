from flask import request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import bp
from web.core.utils import api_output
from web import db, form_checker, role_manager, membership_manager, ds
from membership import membership_constants as mc

@bp.route('/rfq.get', methods=['GET', 'POST'])
def rfq_get():
    args = form_checker.process(request, 'rfq_get_form')
    rfq = db.rfq.get_by_id(args['rfq_id'])
    result = rfq.to_dict() if rfq else None
    return api_output(result=result)

@bp.route('/rfq.list', methods=['GET', 'POST'])
@jwt_required
def rfq_list():
    user_id = get_jwt_identity()
    rfqs = db.rfq.get_by_buyer_id(user_id)
    result = [rfq.to_dict() for rfq in rfqs]
    return api_output(result=result)

@bp.route('/rfq.search', methods=['GET', 'POST'])
def rfq_search():
    user_id = get_jwt_identity()

    args = form_checker.process(request, 'search_rfq')

    keyword = args.pop('keyword', '')
    category = args.pop('category', None)
    limit = int(args.pop('limit', 20))
    current_page = int(args.pop('current_page', 1))
    sorted_by = 'rfq.' + args.pop('sorted_by', 'create_time')

    num_pages, rfqs = db.rfq.search(keyword=keyword, category=category, 
                                    limit=limit, current_page=current_page, sorted_by=sorted_by)

    results = [rfq.to_dict() for rfq in rfqs]

    return api_output(result=results, current_page=current_page, num_pages=num_pages)

@bp.route('/rfq.create', methods=['POST'])
@jwt_required
def rfq_create():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'rfq_create_form')
    args['buyer_id'] = user_id
    
    company_id = db.user.get_by_id(user_id).company_id

    try:
        if not membership_manager.check_membership_company(company_id, mc.LIMIT_NUMBER_RFQS, 1, user_id=user_id):
            return api_output(error=f'Usage exceeding limit', code=403)
        rfq = db.rfq.add(**args)
        db.commit()

        if request.files.get('media', None):
            files = request.files.getlist('media')
            if not membership_manager.check_membership_company(company_id, mc.LIMIT_NUMBER_MEDIA_PER_RFQ, 1, user_id=user_id, rfq_id=rfq.id):
                return api_output(error=f'Usage exceeding limit', code=403)
            folder = f'rfq/{rfq.id}'

            for obj in files:
                obj_name, obj_url = ds.save_obj(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                rfq._media.append(media)

            db.commit()
    except:        
        return api_output(error='Unable to create rfq', code=500)
    
    return api_output(result=rfq.to_dict())

@bp.route('/rfq.update', methods=['POST'])
@jwt_required
def rfq_update():
    user_id = get_jwt_identity()
    args = form_checker.process(request, 'rfq_update_form')
    rfq_id = args.pop('rfq_id', None)
    rfq = db.rfq.get_by_id(rfq_id)
    if not rfq:
        return api_output(error='Unable to find rfq {}'.format(rfq_id), code=500)

    if request.files.get('media', None):
        files = request.files.getlist('media')
        folder = f'rfq/{rfq_id}'

        for obj in files:
            obj_name, obj_url = ds.save_obj(folder, obj)
            media = db.media.add(
                name=obj_name,
                url=obj_url,
                folder=folder,
                file_type=obj.content_type
            )
            rfq._media.append(media)

    db.rfq.update_one(rfq, **args)

    return api_output(result=rfq.to_dict())

@bp.route('/rfq.delete', methods=['POST'])
@jwt_required
def rfq_delete():
    user_id = get_jwt_identity()    
    rfq = db.rfq.get_by_id(request.values['rfq_id'])

    if not rfq:
        return api_output(error='RFQ {} not found'.format(rfq_id), code=404)

    db.rfq.delete_one(rfq)
    
    return api_output(msg='RFQ deleted')

@bp.route('/rfq.follow', methods=['POST'])
@jwt_required
def rfq_follow():
    user_id = get_jwt_identity()
