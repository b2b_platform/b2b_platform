from flask import request, abort, jsonify, views, render_template, url_for, session
from flask_login import login_required, current_user, login_user
from flask_jwt_extended import create_access_token, create_refresh_token, set_access_cookies, set_refresh_cookies, unset_jwt_cookies, jwt_required, jwt_refresh_token_required, get_jwt_identity
from rauth import OAuth2Service
from werkzeug import secure_filename
from datetime import datetime 

from web import db, form_checker
from web.core import utils, decorators, constants
from . import bp
from role import role_constants

@bp.route('/auth.is_logged_in')
def is_logged_in():
    resp = jsonify({'is_logged_in': current_user.is_authenticated})
    return resp, 200

@bp.route('/auth.request_token', methods=['POST'])
def auth_request_token():
    args = request.values
    user = db.user.get_by_email(args['email'])

    if user is None:
        return jsonify({'msg': 'Not authenticated', 'code': 401}), 401

    if not user.check_password(args['password']):
        return jsonify({'msg': 'Invalid password', 'code': 401}), 401

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    return jsonify({'msg': 'Successfully requested token',
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'code': 200}), 200

@bp.route('/auth.refresh_token', methods=['POST'])
@jwt_refresh_token_required
def auth_refresh_token():
    user_id = get_jwt_identity()
    access_token = create_access_token(identity=user_id)

    resp = jsonify({'refresh_token': True,
                    'access_token': access_token,
                    'code': 200})

    set_access_cookies(resp, access_token)

    return resp, 200

@bp.route('/auth.remove_token_cookies', methods=['POST'])
@jwt_required
def auth_remove_token_cookies():
    resp = jsonify({'remove_token_cookies': True, 'code': 200})
    unset_jwt_cookies(resp)
    return resp, 200

@bp.route('/auth.login', methods=['POST'])
@decorators.recaptcha_verified
def auth_login():
    if current_user.is_authenticated:
        return jsonify({'msg': 'Already logged in', 'code': 500}), 500
    
    ip_client = request.remote_addr
    args = form_checker.process(request, 'auth_login_form')
    user = db.user.get_by_email(args['email'])
   
    if user is None:
        return jsonify({'msg': 'Not authenticated', 'code': 401}), 401

    if not user.check_password(args['password']):
        return jsonify({'msg': 'Invalid password', 'code': 401}), 401

    login_user(user)

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    resp = jsonify({'msg': 'Successfully logged in',
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'code': 200})

    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)

    db.notification.new_account_activity(user.id, 'Logged In', 
        ip=request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    
    return resp, 200

@bp.route('/auth.register', methods=['POST'])
@decorators.recaptcha_verified
def auth_register():
    if current_user.is_authenticated:
        return jsonify({'msg': 'User already logged in', 'code': 403}), 403

    args = form_checker.process(request, 'auth_register_form')
    user = db.user.get_by_email(args['email'])

    if user is not None:
        return jsonify({'msg': 'User already exists', 'code': 403}), 403

    user = db.user.add(**args)

    # Handle invitation
    pending_company_id = args.get('pending_company_id', None)
    if pending_company_id:
        user.update(pending_company_id=pending_company_id)
        role = db.role.add(user_id=user.id,
            company_id=pending_company_id,
            operation=role_constants.OPERATION_GENERAL,
            permission=role_constants.REQUEST_PENDING)

    login_user(user, True)

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    resp = jsonify({'msg': 'Successfully registered',
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'code': 200})

    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)

    confirm_token = utils.generate_confirmation_token(current_user.email)
    confirm_url = url_for('auth.confirm', token=confirm_token, _external=True)
    html = render_template('emails/account_confirmation.html',
        confirm_url=confirm_url,
        user=current_user
    )
    subject = 'Email confirmation'

    r_resp = utils.send_mailgun(
        current_user.email,
        subject,
        html=html
    )

    session['EMAIL_CONFIRMATION_SENT'] = datetime.utcnow()

    return resp, 200
