const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const ManifestRevisionPlugin = require('manifest-revision-webpack-plugin');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const glob = require('glob');

const TARGET = process.env.WEBPACK_TARGET || 'DEV';
const COMPILE_JS_TRANSLATIONS = process.env.COMPILE_JS_TRANSLATIONS || 'true'


var I18nPlugin = require("i18n-webpack-plugin");

var languages = {};

if (COMPILE_JS_TRANSLATIONS == 'false' ) {
    languages = { 
	"en": null,
        "de": require("./translations/de/js_msg.json"),
        "fr": require("./translations/fr/js_msg.json"),
        "zh": require("./translations/zh/js_msg.json"),
        "vi": require("./translations/vi/js_msg.json")
        };
}

var language = 'en';

var common = {
    context: __dirname,
    entry: {
        main: './assets/js/main.js',
        auth: './assets/js/auth.js',
        company: './assets/js/company.js',
        product: './assets/js/product.js',
        account: './assets/js/account.js',
        rfq: './assets/js/rfq.js',
        quote: './assets/js/quote.js',
        trade_lead: './assets/js/trade_lead.js',
        sourcing_page: './assets/js/sourcing_page.js',
        manage_products_page: './assets/js/manage_products_page.js',
        company_products_page: './assets/js/company_products_page.js',
        messages: './assets/js/messages',
        manage_media: './assets/js/manage_media.js',
        search_page: './assets/js/search.js',
        home: './assets/js/home',
        support_ticket: './assets/js/support_ticket.js',
        my_basket_page: './assets/js/my_basket_page.js',
        my_notifications_page: './assets/js/my_notifications_page.js',
        admin: './assets/js/admin',
        hello: './assets/js/hello'
    },
    output: {
        path: path.join(__dirname, 'static/dist/wp'),
        publicPath: "/static/dist/wp/",
        filename: "[name].[chunkhash].js",
        chunkFilename: "[id].chunk.js",
        library: ["SUMO","[name]"],
        libraryTarget: "var"
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss', '.css'],
        alias: {
            "js": path.resolve(__dirname, "./assets/js/"),
            "styles": path.resolve(__dirname, "./assets/styles/"),  
            "fonts": path.resolve(__dirname, "./assets/fonts/")
        },
        modules: ['node_modules']        
    },    
    plugins: [
        new CleanWebpackPlugin(['static/dist', 'static/dist/wp']),        
        new ExtractTextPlugin({
            filename: '[name].[chunkhash].min.css',
            allChunks: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'main'
        }),        
        new webpack.optimize.CommonsChunkPlugin({
            name: 'runtime'
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            'Bind': 'bind.js',
            'Popper': ['popper.js', 'default'],
            //'magnificPopup': ['magific-popup']
        }),
        new ManifestRevisionPlugin(path.join(__dirname, 'static', 'manifest.json'), {
            rootAssetPath: './assets',
            ignorePaths: ['/js', '/styles']
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.optimize\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: true
        }),

        new I18nPlugin(
            languages[language]
        )
    ],
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.jsx?/, exclude: /node_modules/, loader: "babel-loader" },
            {
                test: /\.css$/,  
                include: /node_modules/,  
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [
                    'css-loader?minimize'
                ]})
           },
            { 
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [
                    'css-loader?minimize',
                    'sass-loader'
                ]})
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$/,
                loader: 'file-loader?name=/images/[name].[ext]?[hash]'
            },
            {
                test: /\.woff(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff2'
            },
            {
                test: /\.ttf(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]'
            },
            {
                test: /\.otf(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]&mimetype=application/font-otf'
            },
            {
                test: /\.svg(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.json(\?.*)?$/,
                loader: 'json-loader?name=/files/[name].[ext]'
            }            
        ]
    }
}

switch (TARGET) {
    case 'DEV':
        module.exports = merge(require('./webpack.dev'), common);
        break;
    case 'PROD':
        module.exports = merge(require('./webpack.prod'), common);
        break;
    default:
        console.log('Target configuration not found. Valid targets: "dev" or "prod".');
}

