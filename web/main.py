from gevent import monkey
monkey.patch_all()

from psycogreen.gevent import patch_psycopg
patch_psycopg()

from . import db, create_app, setup_sockets

db.set_threadlocal(True)
app = create_app()
setup_sockets(app)