
Compra 
Vendi 
commercio 
all'ingrosso 
Dropship 
Tutto il 
produttore 
Trading Company 
Comprare 
agente ufficio 
Distributore / Grossista 
Governo governativo / Ufficio di presidenza / 
Associazione 
commerciale Servizio aziendale (Logistica, Annunci, Finanza, ecc.) 
Altro 
Inglese 
Cinese 
Vietnamita 
Spagnolo 
Giapponese 
Coreano 
Arabo 
Tedesco 
Francese 
Hindi 
Italiano 
Russo 
Portoghese 
Nord America 
Sud America 
Europa orientale 
Sud-Est asiatico 
Africa 
Medio Oriente 
Oceania
Asia Orientale 
Europa occidentale 
America centrale 
Europa del Nord 
Sud Europa 
Asia meridionale 
nazionale 
migliore corrispondenza 
Più recente per prima 
prodotto dalla A alla Z 
prezzo al pezzo - basso al più alto 
prezzo al pezzo - decrescente 
Data aggiunta 
società A alla Z 
impresa dalla Z alla A 
seguito US $ 1M 
US $ 1-2.5M 
US $ 2.5-5M 
US $ 5-10m 
US $ 10-50M 
US $ 50-100M 
Sopra $ US 100M 
primaria 
secondaria 
di transito 
su misura 
di base 
Bronzo 
argento 
oro 
tipo di imballaggio 
tipo di unità 
dimensioni dell'azienda
Tipo di azienda 
Abbonamenti 
Opzione di pagamento 
Capacità 
Quantità di ordine minimo Fascia di 
prezzo 
Uso generale 
Fatturazione e vendita 
Servizi commerciali 
Abbonamento / abbonamento 
Problemi tecnici ed errori 
Accesso 
all'account 
aziendale Scam 
Informazioni obsolete 
Chiuso Informazioni commerciali 
non valide Informazioni 
offensive Commercio 
illegale 
Inglese (Stati Uniti) 
Thailandese 
Indonesiano 
Laos 
Malese 
Repubblica ceca 
Basic Member 
Bronze Member 
Silver Member 
Gold Member 
Numero di utenti 
Numero di file multimediali 
Numero di file
Numero di file multimediali per thread 
Numero di prodotti 
Numero di foto o file multimediali per prodotto richiesta / offerta / RFQ 
Numero di RFQ 
Numero di preventivi 
per società al mese Quote di 
dati 
restrittive 
Servizio clienti limitato Limitato a 2 utenti 
per azienda al mese fatturati annualmente 
Dati limitati quota 
24/7 servizio clienti 
Limitato a 2 utenti (più $ 5 / mo ogni utente aggiuntivo) 
Pagina aziendale personalizzabile 
Quota di dati generosa 
Limitata a 10 utenti (più $ 5 / mo ogni utente aggiuntivo) 
Primi 6 mesi GRATIS (solo per un periodo limitato) 
Praticamente no restrizione dei dati 
Acquisizione automatica acquirenti 
24/7 e servizi al cliente in ordine di priorità
Numero illimitato di utenti 
Lascia la mia azienda
Protezione del venditore 
Accesso immediato alle promozioni dei nostri partner 
solo ora 
Non disponibile 
Profilo utente 
Pagina account 
Gestisci 
messaggio 
Profilo Immagine 
Nome 
Azienda 
Località 
non verificata 
Paese 
Sito Web 
Account sociali 
Cambia password Il 
mio account 
Visualizza pagina pubblica 
Cambia password 
Vecchia password 
Nuova password 
Nuova password (di nuovo) 
Salva Il 
mio account 
Banner Immagine 
Nome 
Cognome 
Interessi 
Link la mia azienda 
Tipo di account 
Aggiornamento
Posizione Titolo 
Annulla 
Carrello prodotti personali 
Si prega di compilare il seguente modulo rapido per inviare richieste collettive ai venditori corrispondenti. 
Messaggio (limite di 5000 caratteri) 
Invia richieste collettive I 
miei contatti I 
miei colleghi I 
miei contatti di lavoro 
Nessun contatto. 
Le mie impostazioni 
Notifiche 
sulla privacy 
Nascondi il mio account utente 
Visualizza l'e-mail nella mia pagina pubblica 
Mostra i miei account social 
Inviami le ultime notifiche ogni 
12 ore 
24 ore 
7 giorni 
30 giorni 
Mai 
Inviami notifiche aziendali 
Supporto Biglietti 
Resolve Support Tickets 
ID biglietto 
Stato 
riassunto 
Aperto da 
Crea tempo 
Ultimo aggiornamento 
Chat 
Utente principale Gestione 
Utenti correnti 
Digitare una parola chiave per cercare e-mail, ID utente, nome o cognome 
E-mail 
UID 
Livello gestione 
Link profilo 
Mancante 
Nessuno 
Link 
Ruolo Aggiorna 
Selezionare un nuovo ruolo 
Scegliere l'opzione 
Chiudi 
Salva modifiche 
Aggiungi utente 
Cerca utente per nome o ID 
Digitare una parola chiave per cercare per cognome, nome o ID 
Ricerca azienda per nome o ID 
Digitare una parola chiave per cercare per nome società o ID 
Cerca 
ID risultati 
Aggiungi 
richiesta in corso 
account è disabilitato
Il tuo account o email associato a questo account è stato disabilitato. 
Si prega di contattare il nostro supporto 
Riavvia la 
password dimenticata 
Ti invieremo un link per reimpostare la password alla tua email. Si prega di compilare la tua email al modulo sottostante. 
Accedi 
Password 
Registrati ora! 
Ha dimenticato la password? 
Registrati 
Nome 
Cognome 
Password (di nuovo) 
Selezionando questa casella, accetto i 
Termini di servizio di Trade60.com 
già registrati? 
Reimposta password 
Nuova password 
Nuova password (di nuovo) 
Ti abbiamo appena inviato un link per reimpostare la password all'email che hai fornito. Si prega di controllare la tua casella di posta. 
Accedi ora
L'account non è confermato
Non hai confermato il tuo account. Controlla la tua casella di posta (e la tua cartella spam). Dovresti aver ricevuto un'email con un link di conferma 
Non hai ricevuto l'email? 
Change Company Roles 
Informazioni sulla società 
Crea profilo aziendale 
Crea azienda 
Nome società 
Desiderato ID 
società Descrizione 
dell'azienda Informazioni aggiuntive azienda 
Telefono 
Fax 
Anno di fondazione 
spedizione gestita dalla 
società Dimensioni 
Porte 
Tipi di pagamenti accettati 
Crea 
collegamento aziendale  
Attualmente fai parte della seguente organizzazione
Devi lasciare la tua organizzazione prima di passare a una nuova società. Puoi uscire selezionando società dalla pagina del tuo account: 
Qui
Hai inviato richieste di collegamento a 
Ulteriori informazioni 
Fornisci documento di verifica 
Trova la tua azienda 
Cerca per nome dell'azienda, parole chiave o id 
Non riesci a trovare la tua azienda? 
Creare un nuovo profilo aziendale 
link sul mio profilo 
Azienda Utenti 
account di livello 
Data Registrato 
Azione 
Digitare una parola chiave per la ricerca per cognome, nome, e-mail o ID 
Invita un utente 
Invia questo link invito ai tuoi colleghi o partner commerciali 
Richieste in sospeso 
Richiesta 
Richiesta Tempo 
Accettare 
profilo aziendale Importa ed esporta 
Importa profilo aziendale 
Rifiutare
Non ci sono richieste in attesa di revisione 
Sfondo
Si prega di scaricare il seguente modello, inserire le informazioni sulla società e caricare il foglio di calcolo Excel. 
Scarica modello 
Stiamo attualmente elaborando questi documenti caricati 
Import 
Export Profilo aziendale 
Seleziona un formato di esportazione 
Esporta La 
mia azienda 
Registrazione Informazioni Informazioni 
generali 
Produzione e produzione 
Contatta 
Altri dettagli 
 Pagina pubblica 
Categoria principale Categoria 
secondaria Tipo 
azienda 
È Azienda Pubblico 
Is Company Verified 
Verified 
Verifica qui 
Logo 
Banner 
Membership Digitare 
Cambia le 
parole chiave di appartenenza 
Ulteriori informazioni 
Capacità di produzione 
Ricavi totali 
Mercati principali 
Spedizione e gestione 
Tipi di pagamento 
Aggiorna Profilo 
aziendale Aggiorna 
Azienda Verifica 
azienda La tua azienda è già stata verificata. Grazie. 
Grazie per aver inviato una richiesta di verifica. 
Al fine di migliorare l'assistenza clienti e la fiducia commerciale, abbiamo bisogno di una prova che rappresenti la tua azienda. Lo stato della tua azienda verrà visualizzato come non verificato fino a quando non fornirai un documento di verifica. Questo non ha alcun impatto sul tuo account personale o sul tuo account aziendale.
Descrivi e / carichi i documenti che attestano la tua identità alla compagnia. Per favore offusca o nascondi tutte le informazioni riservate o sensibili secondo necessità. 
Supporto Documenti 
Descrizione e nota 
Supporto File e Media La 
tua richiesta sarà esaminata al momento dell'invio. 
Richieste precedenza
Informazioni aggiuntive 
inviate per 
data di invio
Visualizza sottomissione 
Rapporti personali 
Rapporti personali 
Sono stati inoltrati i seguenti rapporti. 
Società Pagina Web 
Tipo di 
rapporto Rapporto Data di invio Data 
risolta 
Aperto 
Motivo 
Nessun rapporto disponibile 
Report Azienda 
Grazie per la segnalazione. Esamineremo a breve la tua segnalazione e ti contatteremo se necessario.  
Gestisci prodotti 
Importa ed esporta dati
Invia i 
tuoi rapporti precedenti 
Impostazioni 
generali I 
miei contatti 
Gestisci le offerte RF 
Gestisci le quotazioni 
Mostra menu 
Posizione 
Invia messaggio privato 
Filtra contatti 
Gestisci utenti 
Gestisci contenuti multimediali 
Pagamento
Home 
Prodotti 
Profilo aziendale 
Galleria 
Informazioni di contatto 
Precedente 
Successivo 
Descrizione 
Prodotti in vetrina 
Questa azienda non ha prodotti in vetrina. 
Contattaci 
Mercato e capacità 
Rating di 
produzione 
Tradeshow 
Mercati e capacità 
Spedizione e gestione 
Port 
non ha ancora ricevuto nessun voto. 
non ha fiere in programma. 
Altri dettagli 
non forniscono alcuna informazione aggiuntiva. 
Elenco dei dipendenti 
Pagina utente Link 
PROFILO 
DELLA SOCIETÀ Mercati 
Sito aziendale Link sul 
nome della società 
Contatti Azienda 
Nessuna società corrispondente 
Chi siamo 
Team 
Lavora con noi 
Mappa del sito 
Politica sulla privacy 
Servizio clienti 
Guida 
Segnala un abuso 
Politica e regole 
Acquista su 
ricerca Prodotti 
Richiesta di offerta 
Vendi su 
abbonamento Premium Prezzi 
Centro di verifica 
Servizio di ispezione 
Tutti i diritti riservati 
Cerca 
messaggi
Min 
Max 
Nome campo 
Contenuto 
Inizia 
nuovo Nuovo 
supporto 
Per prodotto 
Tutti i prodotti 
In primo piano 
Nessun prodotto trovato. 
RIMUOVERE 
Nessun prodotto trovato
Selezionare la categoria di prodotto 
Sottocategoria Sottocategorie di 
nuovi prodotti personalizzati 
Limitato a 2 livelli 
È necessario selezionare una categoria padre dall'alto. 
Nuova categoria 
Seleziona una categoria 
Nuova discussione 
Cerca discussioni 
Nessuna offerta disponibile 
Contatta il venditore 
Rispondi a RFQ 
Tempo rimanente 
Quotes left 
Expired 
Active 
Nessuna RFQ trovata. 
Nuova citazione 
Contatta l'acquirente 
Nuovo ticket di assistenza
Visualizza biglietti aperti 
Tipo di biglietto 
Nessun thread trovato 
Crea subito una nuova discussione! 
Con Prodotti 
da società 
Sourcing  
Scopri 
i prodotti più popolari per categoria
migliori fornitori per area geografica 
Fonte 
Search ultime richieste di acquisto 
Creare una nuova RFQ 
preventivi istantanei 
Verificare società 
Gestione Pagina 
Profilo e Impostazioni 
gestire preventivi 
Log Out 
Supporto 
Modificare le impostazioni di lingua 
Accedi / Registrati 
presentare una richiesta di offerta 
Ricerca Acquista Le richieste 
circa 
il Prezzo 
Termini 
Seleziona la Lingua 
non autorizzata 
Non sei autorizzato ad accedere a questa pagina! 
Ci contatti per favore
Torna alla 
pagina principale 
Scuse vietate! Questa pagina è vietata 
Per favore Accedi 
o 
Quotes Left 
contattaci 
Pagina non trovata
Scuse! Non siamo riusciti a trovare quello che cerchi 
Apologie di 
errori del server ! Incontriamo errori del server. Per favore prova più tardi. 
Bentornati 
Visualizza tutti i 
miei 
preventivi I miei RFQ I 
Miei prodotti più visti 
Prodotti consigliati 
Cronologia delle ricerche 
Gestisci account 
Nuovi messaggi 
Vai alla mia home page 
Benvenuti in 
Cerca prodotti o aziende Parole 
chiave più popolari: Nuove 
richieste di acquisto 
Cerca tra migliaia di richieste 
Ora 
prodotto Tempo rimanente 
Cosa stai cercando per? 
Quantità 
Creare una RFQ 
popolari categorie di prodotti 
Altri 
prodotti in vetrina 
prodotti più Nuovi prodotti 
Trova fornitori per regione 
Più regioni 
Principali fornitori 
Unisciti a migliaia di venditori e acquirenti in tutto il mondo oggi! 
Inserisci il tuo nome 
Inserisci la tua e-mail 
Iscriviti 
Iscriviti ora! 
Una piattaforma di e-commerce globale 
Sei un acquirente? 
Sei un venditore? 
Inizia a scoprire 
come acquirente il 
mercato 
come venditore 
Perché scegliere Trade60? 
Perfetta comunicazione con contesto completo 
È possibile accedere ai dati da qualsiasi luogo in qualsiasi momento. 
Integra Store Integrato 
AI Smart Business Assistant
Crea un profilo aziendale 
in poche righe 
Gestisci i media aziendali 
Aggiungi nuovi media  
Caricamento 
Gestire i media del prodotto
Trova prodotti 
Nessun supporto trovato. 
Informazioni 
per l' iscrizione offre il seguente abbonamento e il piano di abbonamento. Al momento offriamo i seguenti pacchetti alle aziende: 
Il piano di adesione è applicato a tutta l'azienda. 
Data Quota 
Gestisci abbonamento 
Cambia abbonamento 
Il tuo attuale abbonamento 
Data di scadenza del 
pacchetto 
Contattaci tramite il modulo del ticket di supporto in basso. Grazie 
Iscrizioni e iscrizioni precedenti 
Livello 
Data di inizio Data di 
fine 
Durata 
Messaggi 
Tutti i thread 
Contatti 
Cerca utenti 
Ricerca thread di messaggi 
Filtro 
prodotto Inquiry 
privato
RFQ 
Nessuna discussione trovata. 
Si prega di compilare il seguente modulo rapido per informarsi sul venditore di questo prodotto. 
Indietro 
Nuovo thread del messaggio 
Invita utenti 
Cerca utente per nome, telefono o email 
È pubblico? 
Inviami per email gli ultimi messaggi 
SOLO consentire alle persone nella mia rubrica di inviare messaggi privati 
Informazioni filo 
infilate 
Utenti 
Allegati 
Esporta discussione 
Invia 
nuovo messaggio 
Seleziona modello 
Normale 
Tipo oggetto 
Creatore 
Rimuovi 
Invia 
Download CSV 
Archivio 
email Invia e -mail per inviare archivio 
Archivio 
Quotazioni 
acquirente 
partecipanti
Prodotti correlati Le 
mie notifiche Sono state 
rimosse automaticamente le notifiche più vecchie di 3 mesi. Vai a 
 per modificare le impostazioni delle notifiche 
Filtro per 
tipo 
Ora ricevuta 
Nessuna notifica trovata. 
Azienda Prodotti 
Categorie di prodotti 
Crea un nuovo prodotto 
Nome prodotto Fascia di 
prezzo (in dollari USA) 
Opzioni di pagamento 
Tempo di elaborazione 
Dettagli dell'imballaggio 
Descrizione del prodotto 
Esportare i prodotti  
Importa prodotti
Scarica il seguente modello, inserisci i prodotti della tua azienda e carica il foglio di calcolo Excel. 
Richieste dei clienti 
Richiedi ora 
Aggiungi al carrello 
Categoria di prodotto 
Packaging 
Aggiornamento prodotto 
l'immagine personale 
Creazione di un nuovo preventivo 
Crea un nuovo preventivo 
Titolo 
Prezzo 
Gestire mie citazioni 
Citazione 
Citazione per 
Rispondi 
Venditore 
Il venditore profilo 
Aggiornamento Citazione 
creare una nuova RFQ 
Parole (separati da virgola) 
gestire il mio RFQ 
creare il tempo 
ora di fine 
Tutte le citazioni 
Le mie quotazioni 
Profilo dell'acquirente 
Digita una parola chiave per cercare RFQ Le 
mie ricerche 
Aggiorna RFQ 
Aggiungi media 
Marketplace e Ricerca 
aziende 
raggruppate per società 
Ricerca per 
categorie 
Le mie biblioteche
Digitare una parola chiave per cercare 
Cancella 
Ordina per 
Filtro geografico 
Filtrato per 
Si prega di selezionare una regione / paese La 
ricerca qui
Informativa sulla privacy delle applicazioni di 
servizi di ispezione 
è una piattaforma B2B globale che facilita le negoziazioni in tutto il mondo. Gli acquirenti possono sfogliare milioni di prodotti dai nostri partner di fiducia in tutto il mondo, comunicare in modo trasparente con un accesso immediato e facile a tutte le informazioni aziendali, nonché effettuare ricerche e condurre revisioni aziendali. I venditori ricevono avvisi giornalieri di nuove richieste di acquisto e chattano immediatamente con gli acquirenti. Crediamo che la comunicazione sia la chiave numero uno nell'e-commerce cross-border che decide una relazione commerciale di successo, affidabile e duratura.
è fondata nel 2018 da un team di imprenditori appassionati e ingegneri che si sforzano di creare il miglior ecosistema B2B del pianeta. Siamo fermamente convinti che la nostra piattaforma rivoluzionerà il modo in cui le attività commerciali transfrontaliere vengono condotte online. 
Stiamo aggiornando questa pagina. Nel frattempo, facci sapere cosa ne pensi. 
Mandaci un'email 
e ti risponderemo entro 72 ore. 
Grazie!  
Closed Tickets
non condona alcuna azione illecita e attività commerciali fraudolente. Si prega di segnalare qualsiasi abuso o preoccupazione inviando questo modulo e vi risponderemo a breve. Grazie per la collaborazione. 
Il tuo indirizzo email Il 
tuo nome 
Dettagli (limite di 1000 caratteri) 
Centro di supporto 
Biglietti aperti
Il carrello è vuoto 
Inviano con successo {} richieste di prodotti ai venditori.
Biglietto 
È inoltre possibile accedere e creare un ticket di supporto qui: 
Accesso 
all'e-mail 
Problema Argomento 
Descrizione dell'emissione 
Supporto Biglietto 
Apri conversazione 
Risolvi 
Devi compilare il modulo del messaggio. 
Spiacenti, non sei autorizzato ad aggiornare il profilo aziendale. Si prega di contattare il proprio responsabile o noi per ulteriori dettagli 
Si prega di caricare un foglio di calcolo utilizzando il nostro modello 
Tipo di file non valido 
Hai caricato con successo il foglio di calcolo! Stiamo elaborando la tua azienda e ti terremo al corrente. 
Si prega di fornire sia il nome e l'e-mail nel modulo di iscrizione 
Grazie per l'iscrizione! 
Attività dell'account
Spiacenti, non sei autorizzato ad aggiornare la società. Per favore contatta il tuo manager o noi per maggiori dettagli 
Hai caricato con successo il foglio di calcolo! Stiamo elaborando i prodotti della tua azienda e ti terremo al corrente.