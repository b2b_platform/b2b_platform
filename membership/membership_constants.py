
USAGE_UNLIMITED = -1

DEFAULT_TIMEDELTA_DAYS = 180

# Values for field operation
( TIER_BASIC,
 TIER_1,
 TIER_2,
 TIER_3
) = range(4)

# Enums for domain

( LIMIT_NUMBER_USERS,
  LIMIT_NUMBER_MEDIA,
  LIMIT_SIZE_FILE,
  LIMIT_NUMBER_FILES,
  LIMIT_MEDIA_PER_THREAD,
  LIMIT_NUMBER_PRODUCTS,
  LIMIT_NUMBER_MEDIA_PER_PRODUCT,
  LIMIT_NUMBER_RFQS,
  LIMIT_NUMBER_MEDIA_PER_RFQ,
  LIMIT_NUMBER_QUOTES,
  LIMIT_NUMBER_MEDIA_PER_QUOTE,
  LIMIT_CONTACTS,
  LIMIT_NUMBER_TRADE_LEADS,
  LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS

) = range(14)

TIER_LIMITS = {
    TIER_BASIC : {
        LIMIT_NUMBER_USERS: 2,
        LIMIT_NUMBER_MEDIA: 20,
        LIMIT_SIZE_FILE: 10*1024*1024,
        LIMIT_NUMBER_FILES: 20,
        LIMIT_MEDIA_PER_THREAD: 50,
        LIMIT_NUMBER_PRODUCTS: 10,
        LIMIT_NUMBER_MEDIA_PER_PRODUCT: 3,
        LIMIT_NUMBER_RFQS: 20,
        LIMIT_NUMBER_QUOTES: 20,
        LIMIT_CONTACTS: USAGE_UNLIMITED,
        LIMIT_NUMBER_TRADE_LEADS: 20,
        LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS: 1
    },
    TIER_1 : {
        LIMIT_NUMBER_USERS: 5,
        LIMIT_NUMBER_MEDIA: 30,
        LIMIT_SIZE_FILE: 20*1024*1024,
        LIMIT_NUMBER_FILES: 30,
        LIMIT_MEDIA_PER_THREAD: 60,
        LIMIT_NUMBER_PRODUCTS: 20,
        LIMIT_NUMBER_MEDIA_PER_PRODUCT: 5,
        LIMIT_NUMBER_RFQS: 30,
        LIMIT_NUMBER_QUOTES: 30,
        LIMIT_CONTACTS: USAGE_UNLIMITED,
        LIMIT_NUMBER_TRADE_LEADS: USAGE_UNLIMITED,
        LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS: 3
    },
    TIER_2 : {
        LIMIT_NUMBER_USERS: 10,
        LIMIT_NUMBER_MEDIA: 100,
        LIMIT_SIZE_FILE: 30*1024*1024,
        LIMIT_NUMBER_FILES: 100,
        LIMIT_MEDIA_PER_THREAD: 100,
        LIMIT_NUMBER_PRODUCTS: 100,
        LIMIT_NUMBER_MEDIA_PER_PRODUCT: 10,
        LIMIT_NUMBER_RFQS: 100,
        LIMIT_NUMBER_QUOTES: 100,
        LIMIT_CONTACTS: USAGE_UNLIMITED,
        LIMIT_NUMBER_TRADE_LEADS: USAGE_UNLIMITED,
        LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS: 3
    },
    TIER_3 :{
        LIMIT_NUMBER_USERS: 100,
        LIMIT_NUMBER_MEDIA: 1000,
        LIMIT_SIZE_FILE: 50*1024*1024,
        LIMIT_NUMBER_FILES: 1000,
        LIMIT_MEDIA_PER_THREAD: 1000,
        LIMIT_NUMBER_PRODUCTS: 1000,
        LIMIT_NUMBER_MEDIA_PER_PRODUCT: 20,
        LIMIT_NUMBER_RFQS: 1000,
        LIMIT_NUMBER_QUOTES: 1000,
        LIMIT_CONTACTS: USAGE_UNLIMITED,
        LIMIT_NUMBER_TRADE_LEADS: USAGE_UNLIMITED,
        LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS: 5
    }
}