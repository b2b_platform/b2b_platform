from membership import membership_constants as mc
from datetime import datetime, timedelta

class MembershipManager(object):
    def __init__(self, db, ds):
        self.db = db
        self.ds = ds

    def get_membership_company(self, company_id):
        return self.db.membership.get_membership_by_company(company_id)

    def check_membership_company(self, company_id, limit_type, limit_change, **kwargs):
        if True:
        # try:
            if company_id:
                comp_mem = self.db.membership.get_membership_by_company(company_id)
                # Patching company membership
                if not comp_mem:
                    comp_mem = self.db.membership.add_basic_membership(company_id)

                max_limit = mc.TIER_LIMITS[comp_mem.tier][limit_type]
                current_usage = self.get_usage_company(company_id, limit_type, **kwargs)
                return (max_limit == mc.USAGE_UNLIMITED) or (current_usage + limit_change <= max_limit)
            else:
                user_id = kwargs.get('user_id', None)
                if not user_id:
                    return False
                max_limit = mc.TIER_LIMITS[mc.TIER_BASIC][limit_type]
                current_usage = self.get_usage_company(None, limit_type, **kwargs)
                return (max_limit == mc.USAGE_UNLIMITED) or (current_usage + limit_change <= max_limit)
        #except:
        #    return False

    def get_usage_company(self, company_id, limit_type, **kwargs):
        if limit_type == mc.LIMIT_NUMBER_MEDIA:
            return self.db.company.get_by_id(company_id)._media.count()
        elif limit_type == mc.LIMIT_NUMBER_USERS:
            return self.db.user.filter_by_q(company_id=company_id).count()
        elif limit_type == mc.LIMIT_NUMBER_PRODUCTS:
            return self.db.product.filter_by_q(company_id=company_id).count()
        elif limit_type == mc.LIMIT_NUMBER_FILES:
            return self.db.company.get_by_id(company_id)._media.count()
        elif limit_type == mc.LIMIT_NUMBER_RFQS:
            user_id = kwargs.get('user_id', None)
            return self.db.rfq.filter_by_q(buyer_id=user_id).count()
        elif limit_type == mc.LIMIT_NUMBER_QUOTES:
            user_id = kwargs.get('user_id', None)
            return self.db.quote.filter_by_q(seller_id=user_id).count()
        elif limit_type == mc.LIMIT_MEDIA_PER_THREAD:
            thread_id = kwargs.get('thread_id')
            user_id = kwargs.get('user_id')
            return self.db.message.get_number_media(thread_id, user_id)
        elif limit_type == mc.LIMIT_CONTACTS:
            return mc.USAGE_UNLIMITED
        elif limit_type == mc.LIMIT_NUMBER_MEDIA_PER_PRODUCT:
            product_id = kwargs.get('product_id')
            return self.db.product.get_by_id(product_id)._media.count()
        elif limit_type == mc.LIMIT_SIZE_FILE:
            folder = f'company/{company_id}'
            return self.ds.get_total_size(folder)
        elif limit_type == mc.LIMIT_NUMBER_MEDIA_PER_QUOTE:
            quote_id = kwargs.get('quote_id')
            return self.db.quote.get_by_id(quote_id)._media.count()
        elif limit_type == mc.LIMIT_NUMBER_MEDIA_PER_RFQ:
            rfq_id = kwargs.get('rfq_id')
            return self.db.rfq.get_by_id(rfq_id)._media.count()
        elif limit_type == mc.LIMIT_NUMBER_TRADE_LEADS:
            user_id = kwargs.get('user_id')
            return self.db.trade_lead.filter_by_q(user_id=user_id).count()
        elif limit_type == mc.LIMIT_NUMBER_MEDIA_PER_TRADE_LEADS:
            trade_lead_id = kwargs.get('trade_lead_id')
            return self.db.trade_lead.get_by_id(trade_lead_id)._media.count()
        return mc.USAGE_UNLIMITED

    def check_membership_user(self, user_id, limit_type, **kwargs):
        return True

    def update_membership_company(self, company_id, **kwargs):
        tier = kwargs.pop('tier', mc.TIER_BASIC)

        membership_start = kwargs.pop('membership_start', datetime.utcnow())
        membership_end = kwargs.pop('membership_end', membership_start + timedelta(days=mc.DEFAULT_TIMEDELTA_DAYS))
        
        if tier == mc.TIER_BASIC:
            membership_end = None

        existing_mem = self.db.membership.get_membership_by_company(company_id)
        if existing_mem:
            # Expire existing membership
            existing_mem.update(membership_end=membership_start)
        
        # Upgrading to a new tier
        # Adding a new entry
        new_mem = self.db.membership.add_new_membership_by_company(company_id, 
            tier=tier, 
            membership_start=membership_start,
            membership_end=membership_end)
                
        # Mark end date of previous membership subscription
        existing_mem.update(membership_end=membership_start)
        self.db.company.update_by_id(company_id, membership_id=new_mem.id)
        return new_mem

    # Extend membership without creating an existing entry date
    def extend_membership_company(self, company_id, **kwargs):
        existing_mem = self.db.membership.get_membership_by_company(company_id)
        if existing_mem:
            membership_end = existing_mem.membership_end + timedelta(**kwargs)
            if membership_end <= existing_mem.membership_end:
                return None
            existing_mem.update(membership_end=membership_end)
            return existing_mem
        return None

    def update_membership_with_payment(self, company_id, payer_id, payment_time=None, amount=0, 
            tier=None, membership_start=None, membership_end=None):
        mem = self.update_membership_company(company_id, tier=None, 
            membership_start=membership_start, 
            membership_end=membership_end)
        payment = self.db.membership.add_payment(self, payer_id,
            membership_id=mem.id,
            payment_time=payment_time,
            amount=amount)
        return mem, payment

    def get_all_memberships(self, company_id, sort_by='membership.membership_start desc'):
        return self.db.membership.filter_by_q(company_id=company_id).order_by(sort_by)
         