
#! /bin/bash

if [ "$#" -ne 1 ]
then
    echo "Usage: missing 'revision' argument"
    exit 1
fi

alembic -x db=dev revision -m "$1" --autogenerate


