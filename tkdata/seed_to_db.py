from tools import data_utils
import os
import subprocess
import sys
from slugify import slugify
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import re
import database.models as models
from database import Database
from datastores import fs
import settings
from dl_tk_prod_imgs import get_img

conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

ds = fs.FSDatastore(**settings.load('datastores')['FS'])


basedir = os.path.dirname(os.path.realpath(__file__))

companies = data_utils.load_data(os.path.join(basedir, 'tk_companies.lz'))

# check if logos.zip is unzip or not

logos_folder = os.path.join(basedir, 'logos')
logos_zip_file = os.path.join(basedir, 'logos.zip')
if not os.path.exists(logos_folder):
    p1 = subprocess.Popen(['unzip', logos_zip_file, '-d', basedir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    o,e = p1.communicate()

tk_prod_imgs_folder = os.path.join(basedir, 'tk_prod_imgs_all')
tk_prod_imgs_zip_file = os.path.join(basedir, 'tk_prod_imgs_all.zip')
if not os.path.exists(tk_prod_imgs_folder):
    print ('unzipping')
    p1 = subprocess.Popen(['unzip', tk_prod_imgs_zip_file, '-d', basedir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    o,e = p1.communicate()

if not os.path.exists(logos_folder):
    sys.exit()

if not os.path.exists(tk_prod_imgs_folder):
    print ('not found')
    sys.exit()


def clean(txt):
    if not txt:        
        return txt

    txt = txt.strip()\
        .replace('\n','').replace('\r','')\
        .replace('tradekey','Trade60')\
        .replace('TradeKey','Trade60')\
        .replace('Tradekey','Trade60')\
        .replace('TRADEKEY','Trade60')

    return ' '.join(txt.split())

def get(obj, field, default=None):
    val = clean(obj.get(field, default))
    if val == '-' or not val:
        return None

    return val

def get_company_size(val):
    if not val:
        return None
    
    items = [int(re.findall(r'\d+', v.strip())[0]) for v in val.split('-')]

    smax = items[-1]
    if smax <= 10:
        return '1-10'

    if smax <= 100:
        return '10-100'

    if smax <= 500:
        return '100-500'

    if smax <= 1000:
        return '500-1000'

    if smax <= 5000:
        return '1000-5000'

    return '5000+'


def parse(company):
    data = {}
    data['name'] = company['name']    
    data['keywords'] = [kw.strip().lower() for kw in clean(company['Products / Services']).split(',')]
    data['website'] = get(company, 'Website')
    data['company_info'] = get(company, 'company_info')
    data['company_short_hand'] = slugify(company['name'])
    data['location'] = get(company, 'Address')
    data['country'] = data['location'].split(',')[-1].strip()
    data['main_markets'] = get(company, 'Main Markets')
    data['year_founded'] = get(company, 'Year Established')
    data['company_types'] = get(company, 'Business Type')
    data['company_size'] = get_company_size(get(company, 'Number of Employees'))
    data['total_annual_sales'] = get(company, 'Total Annual Sales Volume')
    data['total_purchase_volume'] = get(company, 'Annual Purchase Volume')
    data['other_details'] = {}

    for key, val in company.items():
        if key in ['name', 'products_list', 'products_left', 'img', 'location','company_info',
            'products_url', 'description', 'Company', 'Address', 'Business Type', 'Products / Services',
            'Year Established', 'Number of Employees', 'Website', 'Total Annual Sales Volume', 'Annual Purchase Volume'
        ]:
            continue

        data['other_details'][key] = clean(val)

    return data

def parse_product(prod):        
    data = dict(
        name=get(prod, 'name'),
        description=get(prod, 'description'),
        min_order_quantity_text=get(prod, 'Minimum Order Quantity'),
        price_min_order=get(prod, 'Price Minimum Order'),
        packaging=get(prod, 'Packaging Detail'),
        payment_options=[po.strip() for po in get(prod, 'Payment Type').split(',')] if get(prod, 'Payment Type') else None,
        other_details={}
    )

    for key,val in prod.items():
        if key in ['name', 'description', 'Minimum Order Quantity', 'Price Minimum Order',
            'Packaging Detail', 'Payment Type'
        ]:
            continue

        data['other_details'][key] = clean(val)

    return data

def company_to_cat_mapping():
    cats = open(os.path.join(basedir, 'tk_prod_categories.txt'), 'r').read().splitlines()
    tk_to_t60 = {}
    comp_to_t60 = {}

    for item in cats:
        tkid, tkname, t60name = item.split('|')        
        tk_to_t60[tkid] = db.product_category.filter_by(name=t60name)[0].id

    
    cats_companies = open(os.path.join(basedir, 'tk_companies.txt'), 'r').read().splitlines()
    cats_companies = list(set(cats_companies))

    for item in cats_companies:
        tkid,cname,_ = item.split('|')
        comp_to_t60[cname] = tk_to_t60[tkid]

    return tk_to_t60, comp_to_t60

not_found = []

tk_to_t60, company_to_cats = company_to_cat_mapping()

def seed_company(data):    

    comp_data = parse(data)

    try:
        category_id = company_to_cats[comp_data['name']]
        comp_data['main_category_id'] = category_id
    except KeyError:
        not_found.append(comp_data['name'])
        return


    company = db.company.filter_one(name=comp_data['name'])

    if company:
        return company

    
    company = db.company.add()
    #print(comp_data)

    try:
        filename = os.listdir(os.path.join(logos_folder, data['name']))[0]
        logo_path = os.path.join(logos_folder, data['name'], filename)

        obj = FileStorage(open(logo_path, 'rb'))
        folder = f'company/{company.id}'
        obj_name, obj_url = ds.save_obj_shorten(folder, obj)

        media = db.media.add(
            name=obj_name,
            url=obj_url,
            folder=folder,
            file_type=obj.content_type
        )    
        comp_data['_logo'] = media
    except:
        pass

    try:
        company.update(**comp_data)
        db.commit()
    except:
        pass
    
    try:
        for prod in data['products_list']:
            prod_data = parse_product(prod)
            prod_img = get_img(company.name, prod_data['name'])

            # skip if no pic found
            if not prod_img:
                continue

            product = models.Product(
                company_id=company.id,
                product_category_id=tk_to_t60[prod['real_cat_id']] if 'real_cat_id' in prod else category_id
            )
            product.update(**prod_data)
            db.session.add(product)
            db.session.flush()
            
            if prod_img:
                obj = FileStorage(open(prod_img, 'rb'))
                folder = f'company/{company.id}/product/{product.id}'
                obj_name, obj_url = ds.save_obj_shorten(folder, obj)
                media = db.media.add(
                    name=obj_name,
                    url=obj_url,
                    folder=folder,
                    file_type=obj.content_type
                )
                product._display_pic = media
              
        db.commit()
    except KeyError:
        pass    


for idx, c in enumerate(companies):
    print('seeding #', idx, c['name'])
    seed_company(c)

print('NOT FOUND', not_found)
