from tools import data_utils
import database.models as models
from database import Database
import settings
import os
from sqlalchemy import func, desc

basedir = os.path.dirname(os.path.realpath(__file__))
location = lambda x: os.path.join(basedir, x)

companies = data_utils.load_data(location('tk_companies.lz'))


conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

cats = {}

first_level_cats = open(os.path.join(basedir, 'tk_categories.txt'), 'r').read().splitlines()
for item in first_level_cats:
    tkid, t60name = item.split('|')   
    cats[tkid] = [tkid, db.product_category.filter_by(name=t60name)[0].name,
        db.product_category.filter_by(name=t60name)[0].name]


not_found = set()

for comp in companies:    
    if 'products_list' not in comp: continue

    for prod in comp['products_list']:
        try:
            real_cat_id = prod['real_cat_id']
            real_cat = prod['real_cat']
            if real_cat_id in cats or real_cat_id in not_found: continue

            t60cat = db.product_category.query().order_by(
                desc(func.similarity(db.product_category.Model.name, real_cat))
            ).first()
            
            if not t60cat:
                not_found.add((real_cat_id,real_cat))
                continue

            cats[real_cat_id] = [real_cat_id, real_cat, t60cat.name]
            print('FOUND=', real_cat,'T60=', t60cat.name)
        except:
            continue

outfile = open('tkdata/tk_prod_categories.txt','w')
outfile.write('\n'.join(f'{c[0]}|{c[1]}|{c[2]}' for c in cats.values()))
outfile.close()
