from gevent.monkey import patch_all
patch_all()

import gevent
from gevent import pool
import os
from slugify import slugify
import shutil
import requests
from tools import data_utils
import glob

basedir = os.path.dirname(os.path.realpath(__file__))

comps = data_utils.load_data(os.path.join(basedir, 'tk_prod_img_urls.lz'))


def save_img(url, out_path):
    print('SAVING', url, 'TO', out_path)
    resp = requests.get(url, stream=True)
    if resp.status_code == 200:
        resp.raw.decode_content = True
        with open(out_path, 'wb') as f:
            shutil.copyfileobj(resp.raw, f)

def get_img(cname, pname):
    pattern = os.path.join(basedir, 'tk_prod_imgs_all', slugify(pname) + '.*')

    items = glob.glob(pattern)
    if items:
        return items[0]
    return None

def main():
    POOL = pool.Pool(50)
    jobs = []
    prod_imgs_folder = os.path.join(basedir, 'tk_prod_imgs_all')

    if not os.path.exists(prod_imgs_folder):
        os.makedirs(prod_imgs_folder)

    for cname, comp in comps.items():
        for pname, url in comp.items():        
            oname = slugify(pname) + '.' + url.split('.')[-1]
            out_path = os.path.join(prod_imgs_folder, oname)
            POOL.spawn(save_img, url, out_path)

    POOL.join()

if __name__ == '__main__':
    main()
