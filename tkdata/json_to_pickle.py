import sys
import ujson
import zlib
import pickle

data = []
iname = sys.argv[1] + '.json'
oname = sys.argv[1] + '.lz'
lines = open(iname, 'r').read().splitlines()

for line in lines:        
    try:
        data.append(ujson.loads(line))
    except:
        #print('ERROR LOADING JSON', line)
        pass

p = pickle.dumps(data)

open(oname, 'wb').write(zlib.compress(p))