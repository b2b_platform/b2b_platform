from tools import data_utils
import pickle
import zlib
import os

basedir = os.path.dirname(os.path.realpath(__file__))
location = lambda x: os.path.join(basedir, x)

files =[
    location('tk_companies1.lz'),
    location('tk1.lz'),
    location('tk2.lz'),
]

data = {}
for f in files:
    d = data_utils.load_data(f)
    for comp in d:
        if comp['name'] not in data:            
            data[comp['name']] = comp
            if 'products_list' not in data[comp['name']]: data[comp['name']]['products_list'] = []
        else:
            if 'products_list' in comp and len(comp['products_list']) > len(data[comp['name']]['products_list']):
                data[comp['name']]['products_list'] = comp['products_list']

print('TOTAL:', len(data.keys()))

data = list(data.values())
output = location('tk_companies.lz')

p = pickle.dumps(data)
f = open(output, 'wb')
f.write(zlib.compress(p))
f.close()

