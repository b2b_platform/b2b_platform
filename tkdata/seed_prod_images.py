
from tools import data_utils
import os
import subprocess
import sys
from slugify import slugify
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import re
import database.models as models
from database import Database
from datastores import fs
import settings

conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)

ds = fs.FSDatastore(**settings.load('datastores')['FS'])

basedir = os.path.dirname(os.path.realpath(__file__))

prev_company_id = None
prev_company_name = None

input_idx_fp = os.path.join(basedir, 'product_image_idx_file')
errors = 0

if len(sys.argv) == 2:
    input_idx_fp = sys.argv[1]

with open(input_idx_fp) as fp: 
    line = fp.readline()
    count = 1
    while line:
        if len(line.split('\t')) >= 5:
          company_name, product_idx, location, product_name, keywords, *rest = line.split('\t')
          print ('seeding line ', count, line)
          # To save requests to db
          if not prev_company_name or prev_company_name != company_name:
              company = db.company.filter_one(name=company_name)
              prev_company_id = company.id
              prev_company_name = company.name
             
          product = db.product.filter_one(name=product_name,company_id=prev_company_id)
          if not product:
              count += 1
              errors += 1
              line = fp.readline()
              continue # Skip
          if not product._media.all():
              subfiles = os.listdir(location)
              if subfiles:
                  # There are images to insert
                  for filename in subfiles:
                      print ('there are files')
                      
                      file_path = os.path.join(location, filename)
                      obj = FileStorage(open(file_path, 'rb'))
                      product_id = product.id
                      folder = f'company/{prev_company_id}/product/{product_id}'
                      
                      obj_name, obj_url = ds.save_obj_shorten(folder, obj)
                      media = db.media.add(
                          name=obj_name,
                          url=obj_url,
                          folder=folder,
                          file_type=obj.content_type
                      )    
                      product._media.append(media)
                      #print ('adding file to ', product_id, ' ', prev_company_id)
                  db.commit()
          else:
              print ('size:', len(product._media.all()))
        
        line = fp.readline()
        count += 1
    print ('errors', errors)
