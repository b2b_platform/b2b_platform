
from gevent.monkey import patch_all
patch_all()

import gevent
from gevent import pool
from tools import data_utils
import os
import subprocess
import sys
from slugify import slugify
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import re
import database.models as models
from database import Database
from datastores import fs
import settings

conf = settings.load('db')
db = Database(conf['database_uri'], models.metadata)
ds = fs.FSDatastore(**settings.load('datastores')['FS'])

basedir = os.path.dirname(os.path.realpath(__file__))

product_image_idx_file = open(os.path.join(basedir,
                              'product_image_idx_file'), 'w')
images_folder = os.path.join('tkdata', 'product_images')

# products = db.product.query().all()

companies = db.company.query().all()

# for idx, product in enumerate(products):
#    print (idx, product.name, product.company.name)

if not os.path.exists(images_folder):
    os.makedirs(images_folder)

empty_names = []
for (idx, company) in enumerate(companies):
    print (idx, company.name, '------')
    if not company.name:
        empty_names.append('empty id at: ' + str(idx) + ' company_name')
        continue
    comp_folder = os.path.join(images_folder, company.name)
    if not os.path.exists(comp_folder):
        os.makedirs(comp_folder)
    for (product_idx, product) in enumerate(company.products):
        print ('----', product.name)
        location = os.path.join(images_folder, company.name,
                                str(product_idx))
        if not os.path.exists(location):
            os.makedirs(location)
        keywords = product._keywords
        if product.product_category:
            keywords = ' '.join(keywords) + ' ' \
                + product.product_category.name

    # print (keywords)

        product_image_idx_file.write('\t'.join((company.name,
                str(product_idx), location, product.name, keywords))
                + '\n')

product_image_idx_file.flush()
product_image_idx_file.close()

print ('empty:', empty_names)

			
