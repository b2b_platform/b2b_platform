from gevent.monkey import patch_all
patch_all()

import gevent
from gevent import pool
import os
from tools import data_utils
from slugify import slugify
import shutil
import requests


basedir = os.path.dirname(os.path.realpath(__file__))

companies = data_utils.load_data(os.path.join(basedir, 'tk_companies.lz'))

urls = [(c['name'],c['img']) for c in companies]

#c1 = data_utils.load_data(os.path.join(basedir, 'tradekey_companies_1-200.lz'))
#c2 = data_utils.load_data(os.path.join(basedir, 'tradekey_companies_201-500.lz'))
#c3 = data_utils.load_data(os.path.join(basedir, 'tradekey_companies_501-899.lz'))

POOL = pool.Pool(20)


def save_img(url, out_path):
    print('SAVING', url, 'TO', out_path)
    resp = requests.get(url, stream=True)
    if resp.status_code == 200:
        resp.raw.decode_content = True
        with open(out_path, 'wb') as f:
            shutil.copyfileobj(resp.raw, f)

jobs = []

for iname, url in urls:
    oname = slugify(iname) + '.' + url.split('.')[-1]
    logos_folder = os.path.join(basedir, 'logos', iname)
    if not os.path.exists(logos_folder):
        os.makedirs(logos_folder)

    out_path = os.path.join(logos_folder, oname)

    jobs.append(POOL.spawn(save_img, url, out_path))


gevent.joinall(jobs)