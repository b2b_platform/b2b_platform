from gevent.monkey import patch_all
patch_all()

import gevent
from gevent import pool
import os
from tools import data_utils
from slugify import slugify
import shutil
import requests

# Import Libraries
import sys  # Importing the System Library

import urllib.request
from urllib.request import Request, urlopen
from urllib.request import URLError, HTTPError
from urllib.parse import quote
import time  
import os
import argparse
import ssl
import datetime


usage_rights = None
color = None
size = 'medium'
pictype = 'photo'
uptime = None

limit = 5
val = 0.1
jobs = []
POOL = pool.Pool(200)

basedir = os.path.dirname(os.path.realpath(__file__))

# Downloading entire Web Document (Raw Page Content)
def download_page(url):
   try:
            headers = {}
            headers[
                'User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
            req = urllib.request.Request(url, headers=headers)
            resp = urllib.request.urlopen(req)
            respData = str(resp.read())
            return respData
   except Exception as e:
            print(str(e))
   


def _images_get_next_item(s):
    if not s:
        return "no_links", 0

    start_line = s.find('rg_di')
    if start_line == -1:  # If no links are found then give an error!
        end_quote = 0
        link = "no_links"
        return link, end_quote
    else:
        start_line = s.find('class="rg_meta') # remove quote
        #print ('should not happen sline=', start_line)
        
        start_content = s.find('"ou"', start_line + 1)
        
        #print ('start_content sc', start_content)
        if start_content == -1:
            return "no_links", 0
                        
        end_content = s.find(',"ow"', start_content + 1)
        #print ('finding content', start_content, end_content)
        content_raw = str(s[start_content + 6:end_content - 1])
        return content_raw, end_content



#matching_site = ['tradekey.com']
matching_site = []

def is_ignored(url):
    for site in matching_site:
        if site in url:
            return True
    return False

def _images_get_all_items(page):
    items = []
    i = 0
    while i < limit:
        item, end_content = _images_get_next_item(page)
        if item == "no_links":
            break
        
        if item and not is_ignored(item):
            print ('appending', item)
            items.append(item)  # Append all the links in the list named 'Links'
            i += 1
        else:
            pass
        page = page[end_content:]
    
    return items



#Building URL parameters
def build_url_parameters():
    built_url = "&tbs="
    counter = 0
    params = {'color':[color,{'red':'ic:specific,isc:red', 'orange':'ic:specific,isc:orange', 'yellow':'ic:specific,isc:yellow', 'green':'ic:specific,isc:green', 'teal':'ic:specific,isc:teel', 'blue':'ic:specific,isc:blue', 'purple':'ic:specific,isc:purple', 'pink':'ic:specific,isc:pink', 'white':'ic:specific,isc:white', 'gray':'ic:specific,isc:gray', 'black':'ic:specific,isc:black', 'brown':'ic:specific,isc:brown'}],
              'usage_rights':[usage_rights,{'labled-for-reuse-with-modifications':'sur:fmc','labled-for-reuse':'sur:fc','labled-for-noncommercial-reuse-with-modification':'sur:fm','labled-for-nocommercial-reuse':'sur:f'}],
              'size':[size,{'large':'isz:l','medium':'isz:m','icon':'isz:i'}],
              'type':[pictype,{'face':'itp:face','photo':'itp:photo','clip-art':'itp:clip-art','line-drawing':'itp:lineart','animated':'itp:animated'}],
              'time':[uptime,{'past-24-hours':'qdr:d','past-7-days':'qdr:w'}]}
    for key, value in params.items():
        if value[0] is not None:
            ext_param = value[1][value[0]]
            #print(value[1][value[0]])
            # counter will tell if it is first param added or not
            if counter == 0:
                # add it to the built url
                built_url = built_url + ext_param
                counter += 1
            else:
                built_url = built_url + ',' + ext_param
                counter += 1
    return built_url


def download_with_keyword(keyword, target_location):                
        params = build_url_parameters()
        
        url = 'https://www.google.com/search?q=' + quote(keyword) + '&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch' + params + '&sa=X'
        raw_html = (download_page(url))        
        items = _images_get_all_items(raw_html)        
        print('FOUND', len(items), 'URLS FOR KEYWORD', keyword)
        for item in items:
            name_file = str(item[(item.rfind('/')) + 1:])
            *name_only, extension = name_file.split('.')
            out_path = os.path.join(target_location, slugify(''.join(name_only), max_length=20) + '.' + extension)
            POOL.spawn(save_img, item, out_path)

        return len(items)

def save_img(url, out_path):
    print('SAVING', url[:150], 'TO', out_path)
    err_count = 0
    headers = {
        'User-Agent': "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
    }

    while err_count < 1:
        try:
            resp = requests.get(url, stream=True, headers=headers, timeout=1)
            if resp.status_code == 200:
                resp.raw.decode_content = True
                with open(out_path, 'wb') as f:
                    shutil.copyfileobj(resp.raw, f)
                break
        except:
            print('RETRYING', url[:150])
            err_count += 1


if len(sys.argv) < 1:
    print('error missing index file')
    sys.exit(1)

input_idx_fp = os.path.join(basedir, 'product_image_idx_file')
if len(sys.argv) >= 2:
    input_idx_fp = sys.argv[1]

fresh_download = False
if len(sys.argv) >= 3:
    fresh_download = sys.argv[2] == "True"

start_line = 0
if len(sys.argv) >= 4:
    start_line = int(sys.argv[3])

end_line = None
if len(sys.argv) >= 5:
    end_line = int(sys.argv[4])

with open(input_idx_fp) as fp:
    for _ in range(start_line):
        next(fp)

    line = fp.readline()
    count = start_line
    batch = 0
    jobs = []
    while line:
    #       print(len(line.split('\t')))
        print ('LINE #', count)
        print (line)
        company_name, product_idx, location, product_name, keywords, *rest = line.split('\t')
        print ( '----'.join((company_name, product_name, keywords)))
        arr_keywords = keywords.split(',')    #replace(',',' ')
        if len(arr_keywords) >= 2:
            keywords = ','.join(arr_keywords)
        keywords = keywords.replace(',', ' ')

        if not os.path.exists(location):
            os.makedirs(location)
        elif fresh_download:
            shutil.rmtree(location)
            os.makedirs(location)


        if fresh_download or not os.listdir(location):

            #kw = product_name + ' ' + keywords + ' ' + company_name
            #print('FETCH GGL IMAGES:', kw)           
            #download_with_keyword(product_name + ' ' + keywords + ' ' + company_name, location)

            kw = product_name + ' ' + company_name
            print('FETCH GGL IMAGES:', kw)       
            num_items = download_with_keyword(kw, location)
            
            if not num_items:
                kw = product_name + ' ' + keywords
                print('FETCH GGL IMAGES:', kw)
                download_with_keyword(kw, location)
            
        else:
            print('there are images')

        if end_line and count >= end_line:
            print('REACH #', count)
            break

        line = fp.readline()
        count += 1

POOL.join()