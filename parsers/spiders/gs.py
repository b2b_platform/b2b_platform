import scrapy
import os
import re
import ujson
import time
import requests
import random
import lxml
from lxml.html.clean import Cleaner
from tools import data_utils
basedir = os.path.dirname(os.path.realpath(__file__))
cleaner = Cleaner()
cleaner.javascript = True
cleaner.style = True
cleaner.inline_style = True
cleaner.comments = True
cleaner.links = True
cleaner.kill_tags = ['a', 'img', 'script', 'style']

class GSCompanySpider(scrapy.Spider):
    name = "gs"
    rotate_user_agent = True
    
    def __init__(self, *args, **kwargs):
        super(GSCompanySpider, self).__init__(*args,**kwargs)


    def start_requests(self):
        url = 'http://www.globalsources.com/gsol/I/Auto-Part-suppliers/s/2000000003844/3000000151248/-1.htm?view=suppList'
        yield scrapy.FormRequest(url=url, method='GET', callback=self.parse)
    
    def parse(self, response):        
        print(response)