
import scrapy

class AlibabaSpider(scrapy.Spider):
    name = "alibaba"

    def start_requests(self):
        urls = [
            'https://jdbag.en.alibaba.com/company_profile.html'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, headers={'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'})

    def parse(self, response):
        trs = response.css('.information-content .content-table').xpath('//tr')
        company = {}
        for tr in trs:
            company[tr.xpath('//th[@class="col-title"]/text()').extract_first().lower().strip(': ')] = tr.xpath('//td[@class="col-value"]/text()').extract_first()

        print(company)