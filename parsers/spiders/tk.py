
import scrapy
import os
import re
import ujson
import time
import requests
import random
import lxml
from lxml.html.clean import Cleaner
from tools import data_utils

basedir = os.path.dirname(os.path.realpath(__file__))
cleaner = Cleaner()
cleaner.javascript = True
cleaner.style = True
cleaner.inline_style = True
cleaner.comments = True
cleaner.links = True
cleaner.kill_tags = ['a', 'img', 'script', 'style']

def get_proxy():
    url = 'https://gimmeproxy.com/api/getProxy?protocol=http'
    while True:
        resp = requests.get(url).json()
        try:
            if resp and resp['supportsHttps']:
                return resp['curl']
        except KeyError:
            pass

        time.sleep(2)

def get_proxies(limit=5):
    '''
    return [
        'http://147.135.210.114:54566',
        'http://185.93.3.123:8080',
        'http://41.76.150.154:65205',
        'http://71.13.112.152:3128',
        'http://210.5.149.43:8090'
    ]
    '''

    proxies = []
    for i in range(limit):
        proxies.append(get_proxy())

    print(proxies)
    return proxies


class TkCategoriesSpider(scrapy.Spider):
    name = "tk_categories"
    rotate_user_agent = True
    
    def __init__(self, *args, **kwargs):
        self.output_path = os.path.join(basedir, 'tk_categories.txt')
        self.f = open(self.output_path, 'w')

    def closed(self, reason):
        self.f.close()

    def start_requests(self):
        url = 'http://www.tradekey.com/product_cat.htm'
        yield scrapy.Request(url=url, callback=self.parse)            
    
    def parse(self, response):        
        cats = []
        items = response.xpath('//div[contains(@class,"sec-categories")]//font//a')
        for item in items:
            cat_id = re.findall(r'\d+', item.xpath('@href').extract_first())[0]
            cat_name = item.xpath('@title').extract_first()
            self.f.write(f'{cat_id}\n')

class TKCompaniesSpider(scrapy.Spider):
    name = "tk_companies"    
    rotate_user_agent = True
    NUMPAGES = 4
    BASEURL = 'http://www.tradekey.com/search/index.html?action=profile_search&sort_by=trust_points&search_category={category}&page_no={page_no}'

    def __init__(self, *args ,**kwargs):
        super(TKCompaniesSpider, self).__init__(*args, **kwargs)        

        self.categories = self.get_cats()

        self.output_path = os.path.join(basedir, 'tk_companies.txt')
        self.f = open(self.output_path, 'w')        
    
    def get_cats(self, limit=None):        
        lines = open(os.path.join(basedir, 'tk_categories.txt'), 'r').read().splitlines()
        cats = [line.split()[0] for line in lines]
        return cats[:limit]

    def closed(self, reason):        
        self.f.close()
    
    def start_requests(self):
        for cat in self.categories:
            for i in range(1,self.NUMPAGES):
                url = self.BASEURL.format(category=cat, page_no=i)           
                req = scrapy.Request(url=url, callback=self.parse)                    
                req.meta['category'] = cat
                yield req

    def parse(self, response):
        cat = response.meta.get('category')
        items = response.xpath('//div[contains(@class, "description_block")]//a[@class="title"]')
        links = response.xpath('//div[contains(@class, "description_block")]//a[contains(@class,"trust_point_link")]/text()')
        for item, link in zip(items, links):
            name = item.xpath('text()').extract_first().strip()
            comp = item.xpath('@href').extract_first()            

            try:
                point = re.findall(r'\d+', link.extract().strip())[0]
                if not point or int(point) < 100:
                    print('SKIP', name)
                    continue
            except:
                print('SKIP', name)
                continue                        
            
            self.f.write(f'{cat}|{name}|{comp}\n')

class TKCompanySpider(scrapy.Spider):
    name = "tk"    
    rotate_user_agent = True

    def __init__(self, is_test=False, start=None, end=None, output=None, conly=False, pimgonly=False, *args, **kwargs):
        super(TKCompanySpider, self).__init__(*args, **kwargs)

        if is_test is True:
            txt_path = os.path.join(basedir, 'tk_companies_test.txt') 
        else:
            txt_path = os.path.join(basedir, 'tk_companies.txt') 

        start = int(start) if start else 0
        end = int(end) if end else None

        all_comps = open(txt_path, 'r').read().splitlines()
        print('NUMBER OF COMPANIES', len(all_comps))
        self.companies = all_comps[start:end]

        output = output or 'tk_companies.json'

        self.output_path = os.path.join(basedir, output)

        self.f = open(self.output_path, 'a')

        #self.proxy_list = get_proxies(limit=30)


        self.companies_json = []
        self.company_only = conly == "True"
        self.prod_img_only = pimgonly == "True"

        #self.loaded_companies = set([c['name'] for c in data_utils.load_data('tkdata/tk_companies.lz') ])
        self.loaded_companies = set()
    
    def start_requests(self):

        for line in self.companies:
            cat_id,cname,url = line.split('|')
            if not url.startswith('http'): continue
            
            if cname in self.loaded_companies:
                print('SKIPPING', cname)
                continue

            req = scrapy.Request(url=url, callback=self.parse)
            #proxy = random.choice(self.proxy_list)
            #req.meta['proxy'] = proxy
            yield req
    
    def parse(self, response):
        company = {}
        # TITLE
        company['name'] = response.xpath('//a[@id="company-link-heading"]/@title').extract_first().strip()
        company['company_info'] = response.xpath('//div[@id="product-body"]//p/text()').extract_first().strip()
        company['description'] = company['name']
        company['location'] = response.css('[id="company-name"] p::text').extract()[1].strip()
        company['img'] = response.xpath('//div[@id="company-logo-img"]//img/@src').extract_first()
        #company['products_url'] = response.xpath('//div[@class="viewmore_product"]//a/@href').extract_first()
        company['products_url'] = response.xpath('//div[@id="nav_bar_fixed"]//a[@title="Products"]/@href').extract_first()
        
        items = response.xpath('//div[contains(@class, "ci-details")]')
    
        for item in items:
            key = item.xpath('label/text()').extract_first()
            val = item.xpath('p/text()').extract_first()
            if key and val:                
                company[key.strip()] = val.strip()

        if self.company_only:
            print('SAVE COMPANY ONLY', company['name'])
            self.f.write(ujson.dumps(company))
            self.f.write('\n')
        else:
            self.companies_json.append(company)
            products_list_req = scrapy.Request(url=company['products_url'], callback=self.parse_products_list)
            products_list_req.meta['company_idx'] = len(self.companies_json) - 1
            yield products_list_req

    def parse_products_list(self, response):
        NUM_PRODUCTS = 50

        company_idx = response.meta.get('company_idx')
        company = self.companies_json[company_idx]        

        if self.prod_img_only:
            items = response.xpath('//div[@class="listing-big-img"]//img[@class="lazy"]')
            prods = []
            for item in items:
                prod_name = item.xpath('@title').extract_first().strip()
                prod_img = item.xpath('@data-original').extract_first()
                prods.append([prod_name, prod_img])
            
            self.f.write('\n'.join(f'{company["name"]}@@@{prod_name}@@@{prod_img}' for prod_name, prod_img in prods))
            self.f.write('\n')
        else:
            urls = [t.extract().strip() for t in response.xpath('//div[@class="listing-big-img"]//a/@href')]
            urls = urls[:NUM_PRODUCTS]

            company['products_left'] = len(urls)
                        
            for url in urls:
                product_req = scrapy.Request(url=url, callback=self.parse_product)
                product_req.meta['company_idx'] = company_idx            
                yield product_req

    def parse_product(self, response):
        try:
            company_idx = response.meta.get('company_idx')
            product = {}
            company = self.companies_json[company_idx]

            if company is None:
                raise RuntimeError('None company')
            
            if 'products_list' not in company:
                company['products_list'] = []

            product['url'] = response.request.meta['redirect_urls'][-1]
            product['name'] = response.xpath('//div[@id="product-header"]//h1/text()')[0].extract().strip()
            product['img'] = response.xpath('//div[@id="product-img"]//img/@data-original')[0].extract()
            
            items = response.xpath('//div[@id="product-body"]//div[@class="pd-main"]')
            for item in items:
                key = item.xpath('label/text()').extract_first().strip().strip(':')
                val = item.xpath('p[@class="pd-desc"]/text()').extract_first().strip().strip(':')
                if key and val:
                    product[key] = val        

            cat_el = response.xpath('//div[contains(@class, "main-container-right")]//ol[@class="breadcrumb"]//li//a')[-1]
            product['real_cat_id'] = re.findall(r'\d+', cat_el.xpath('@href').extract_first())[0]
            product['real_cat'] = cat_el.xpath('text()').extract_first()

            elem = response.xpath('//div[contains(@class, "pd-details")]')[0].root
            elem.attrib.clear()
            elem_str = lxml.html.tostring(cleaner.clean_html(elem)).decode("utf-8") 
            product['description'] = ' '.join(elem_str.strip().split())        

            company['products_list'].append(product)            
        except:                       
            pass        

        company['products_left'] -= 1 
        if company['products_left'] == 0:
            self.f.write(ujson.dumps(company))
            self.f.write('\n')

            # delete
            self.companies_json[company_idx] = None

    def closed(self, reason):
        # make sure write any remaining company
        for c in self.companies_json:
            if c is not None and not self.prod_img_only:
                self.f.write(ujson.dumps(c))
                self.f.write('\n')

        self.f.close()