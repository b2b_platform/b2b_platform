
from . import role_constants as rc

 
class RoleManager(object):

    def __init__(self, db):
        self.db = db

    def _higher_than_master(self, permission):
        return permission == rc.PERMISSION_ROLE_MASTER

    def _higher_than_support(self, permission):
        return self._higher_than_master(permission) or (permission == rc.PERMISSION_ROLE_SUPPORT)

    def _higher_than_admin(self, permission):
        return self._higher_than_support(permission) or (permission == rc.PERMISSION_ROLE_ADMIN)

    def _higher_than_manager(self, permission):
        return self._higher_than_admin(permission) or (permission == rc.PERMISSION_ROLE_MANAGER)

    def _default_permission(self, permission):
        # Adjust based on company settings
        return permission >= rc.PERMISSION_ROLE_USER

    def is_master(self, user_id):
        current_user_role = self.db.role.get_position_by_userid(user_id)        
        return (current_user_role) and self._higher_than_support(current_user_role.permission)

    def get_permission_level(self, user_id):
        user_permission = self.db.role.get_position_by_userid(user_id).permission
        return user_permission if user_permission else -10

    def check_permission(self, *args, **kwargs):
        user_id = args[0]
        operation_domain = args[1]
        action = args[2]

        current_user_role = self.db.role.get_position_by_userid(user_id)

        
        if (current_user_role): # User already has assigned role in company
            #### Company context ####
            permission = current_user_role.permission
            
            if permission == rc.PERMISSION_ROLE_MASTER:
                return True

            if operation_domain == rc.OPERATION_COMPANY and action == rc.ACTION_OTHER_NONPRIV:
                # Non-privilege action: create company, link and verify company
                return True

            if operation_domain == rc.OPERATION_MESSAGE:
                if action == rc.ACTION_UPDATE:
                    # Join or update
                    return self._higher_than_support(permission)
                elif action == rc.ACTION_VIEW:
                    # View message only
                    return permission >= rc.PERMISSION_ROLE_USER
                elif action == rc.ACTION_DELETE:
                    return self._higher_than_manager(permission)
                elif action == rc.ACTION_OTHER_NONPRIV:
                    return True

            if operation_domain == rc.OPERATION_PRODUCT:
                if action == rc.ACTION_DELETE:
                    # Only manager can delete product
                    return self._higher_than_manager(permission)
                else:
                    # action == rc.ACTION_VIEW, rc.ACTION_UPDATE:
                    return True

            if operation_domain == rc.OPERATION_COMPANY:
                if action == rc.ACTION_DELETE:
                    return False # Currently not allowing user to delete company
                elif (action == rc.ACTION_UPDATE 
                            or action == rc.ACTION_OTHER_NONPRIV
                            or action == rc.ACTION_VIEW):
                    return self._default_permission(permission) # Standard user can update standard page
                elif action == rc.ACTION_OTHER_PRIV:
                    return self._higher_than_manager(permission) # only admin or higher
                else:
                    return False
            
            if operation_domain == rc.OPERATION_USER_MANAGEMENT:
                if action == rc.ACTION_OTHER_NONPRIV:
                    return True
                elif action == rc.ACTION_UPDATE or action == rc.ACTION_DELETE:
                    # Join or update
                    
                    target_user_id = kwargs.get('target_user_id', None)
                    target_original_role= self.db.role.get_position_by_userid(target_user_id)
                    target_original_permission = rc.REQUEST_PENDING
                    if target_original_role: # User not in a company yet or to be removed
                        target_original_permission = target_original_role.permission

                    # Default settings to standard user
                    target_permission = int(kwargs.get('target_permission', rc.PERMISSION_ROLE_USER))
                    return (permission >= target_original_permission and permission >= target_permission)
                     
                    #return self._higher_than_support(permission)

                elif action == rc.ACTION_VIEW:
                    return self._default_permission(permission)

                elif action == rc.ACTION_OTHER_PRIV:
                    return self._higher_than_admin(permission)
                else:
                    return True
            
            if operation_domain == rc.OPERATION_RFQ:
                return True
            
            if operation_domain == rc.OPERATION_QUOTE:
                return True

            if operation_domain == rc.OPERATION_MEDIA:
                if action == rc.ACTION_DELETE: # Only support and admin can
                    return self._higher_than_manager(permission)
                else:
                    return self._default_permission(permission)
    
        ### Users without role ####
        if operation_domain == rc.OPERATION_ACCOUNT:
            # If user in blacklist or disabled account
            # return False
            return True

        # For user not having any role
        if operation_domain == rc.OPERATION_COMPANY:
            # Non-privilege action: create company, link and verify company
            return (action == rc.ACTION_OTHER_NONPRIV) 

        if operation_domain == rc.OPERATION_RFQ:
            return True
            
        if operation_domain == rc.OPERATION_QUOTE:
            return True

        if operation_domain == rc.OPERATION_USER_MANAGEMENT:
            return (action == rc.ACTION_OTHER_NONPRIV) # Link profile/send request

        if operation_domain == rc.OPERATION_MESSAGE and action == rc.ACTION_OTHER_NONPRIV:
            return True

        # Should be changed to False
        return False # Allow any other path

    # Fast route check
    def check_permission_fast(self, *args, **kwargs):
        return True
