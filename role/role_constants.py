
# Values for field operation
(OPERATION_GENERAL, 
            OPERATION_COMPANY, # General information
            OPERATION_USER_MANAGEMENT, # User management
            OPERATION_MEDIA, # Media
            OPERATION_PRODUCT, # Product update and creation
            OPERATION_RFQ, 
            OPERATION_QUOTE, 
            OPERATION_MESSAGE, 
            OPERATION_ACCOUNT) = range(9)

# Enums for domain

OPERATION_TEXT_ARR = {
    OPERATION_GENERAL : 'Access Type',
    OPERATION_COMPANY : 'Company',
    OPERATION_USER_MANAGEMENT :  'User Management',
    OPERATION_MEDIA : 'Media',
    OPERATION_PRODUCT : 'Product',
    OPERATION_RFQ : 'RFQ',
    OPERATION_QUOTE : 'Quote',
    OPERATION_MESSAGE : 'Message'    
}

# Applicable for operations on specific sections
ACTION_VIEW = 1
ACTION_UPDATE = 2
ACTION_DELETE = 3
ACTION_CREATE = 4
ACTION_OTHER_NONPRIV = 10
ACTION_OTHER_PRIV = 20

# Applicable when OPERATION_GENERAL == 0
REQUEST_PENDING = -1

PERMISSION_ROLE_USER = 1
PERMISSION_ROLE_MANAGER = 10
PERMISSION_ROLE_ADMIN = 99

PERMISSION_ROLE_SUPPORT = 900
PERMISSION_ROLE_MASTER = 999

PERMISSION_TEXT_ARR = {
    #REQUEST_PENDING : 'Pending',
    PERMISSION_ROLE_USER : 'Standard User',
    PERMISSION_ROLE_MANAGER : 'Manager',
    PERMISSION_ROLE_ADMIN : 'Administrator',
    PERMISSION_ROLE_SUPPORT: 'Support User',
    PERMISSION_ROLE_MASTER : 'Master User',
}

            