
# Use an official Python runtime as a parent image

# Python version
FROM python:3.6.1

MAINTAINER Hoang Vo <voviethoang@gmail.com>

RUN groupadd flaskgroup && useradd -m -g flaskgroup -s /bin/bash flask

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN apt-get install -y zip
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y openssh-client
RUN apt-get install -y openssh-server

RUN curl -sL https://deb.nodesource.com/setup_8.x | sh
RUN apt-get install -y nodejs

RUN mkdir -p /home/flask/b2b_platform


WORKDIR /home/flask/b2b_platform
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /home/flask/b2b_platform
COPY . .

RUN chown -R flask:flaskgroup /home/flask

USER flask
ENV NAME=Trade60
ENV WEBPACK_TARGET=PROD
ENV SETTINGS_MODE=PROD
ENV PYTHONPATH=.

WORKDIR /home/flask/b2b_platform/web

RUN npm install
RUN npm run build-prod

WORKDIR /home/flask/b2b_platform


