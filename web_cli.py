from web import create_app

app = create_app(SERVER_NAME='www.trade60.com')
app.jinja_env.auto_reload = True