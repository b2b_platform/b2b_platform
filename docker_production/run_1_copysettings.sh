#! /bin/bash

# For storing logs and user settings
# Adjust if necessarily
mkdir -p logs 
mkdir -p data/postgres

# Install certbot

# For debian 8
# apt-get install certbot -t jessie-backports
# sudo certbot certonly
# sudo certbot certonly --standalone -d trade60.com -d www.trade60.com

# or
#RUN apt-get update \
	#  && apt-get install -y letsencrypt -t jessie-backports \
	#  && apt-get clean \
	#  && rm -rf /var/lib/apt/lists/* \
	#  && mkdir -p /etc/letsencrypt/live/trade60.com \
	#  && openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
	#    -keyout /etc/letsencrypt/live/trade60.com/privkey.pem \
	#    -out /etc/letsencrypt/live/trade60.com/fullchain.pem \
	#    -subj /CN=trade60.com

sudo cp -r /etc/letsencrypt docker_production/nginx/certs
# Change permission of the cert if necessarily

# Copy config files
cd settings && for filename in *.config_default; do cp -f "$filename" "$(echo $filename| rev | cut -d '_' -f2-|rev)"; done;




