import argparse
from web import create_app

def main():
    parser = argparse.ArgumentParser(description='Run web flask dev server')
    parser.add_argument('-p', '--port', type=int, default=8000,
                        help='port to run web app')
    args = parser.parse_args()

    
    port = args.port
    host = "0.0.0.0"
    app = create_app()
    app.jinja_env.auto_reload = True
    app.run(host=host, port=port)

if __name__ == '__main__':
    main()
