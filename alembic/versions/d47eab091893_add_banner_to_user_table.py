"""add banner to user table

Revision ID: d47eab091893
Revises: 743beadc6bd9
Create Date: 2018-02-23 01:00:27.903452

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd47eab091893'
down_revision = '743beadc6bd9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('banner_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'user', 'media', ['banner_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user', type_='foreignkey')
    op.drop_column('user', 'banner_id')
    # ### end Alembic commands ###
